<?php

namespace Database\Seeders;

use App\Models\LawyerExperience;
use App\Models\LawyerTitle;
use Illuminate\Database\Seeder;
use App\Models\Lawyer;
use Symfony\Component\Console\Output\ConsoleOutput;

class LawyerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
                array(
                    'experience' =>[
    ] , 'ID' => '327',
'services' => [4,6,7,13,17],
'first_name'=>'Habib Mohamed',
'last_name'=>'Al Hadi',
'post_title'
 => 'Habib Mohamed Al Hadi','post_content' => '<p>Habib is a Partner with the Litigation Practice in our Abu Dhabi office. Practising in the UAE since 2000, he specialises in several types of litigation cases including civil, criminal, commercial and Sharia law. On a day-to-day basis he advises our clients on the drafting of legal memorandums and legal contracts, attending expert meetings, arbitration sessions and giving verbal and written legal consultation.</p>
<p>With a particular interest in banking law, commercial companies, property, insurance and reinsurance and compensation cases, Habib has successfully concluded many settlements on behalf of our clients. His advice covers pre-litigation and creating strategies to follow in order to ensure a positive conclusion to a dispute.</p>
<p>Habib holds a Bachelor&rsquo;s degree in Law from the University of Khartoum-Faculty, Sudan.</p>',
                    'post_date' => '2014-01-26 07:04:54','post_name' => 'habib-al-hadi','post_parent' => '0','Image' => 'wp-content/uploads/2014/01/Habib-Al-Hadi-SQUARE-BW.jpg',
                    'Title' => 'Partner','Email' => 'habib.alhadi@bsabh.com','Excerpt' => 'Habib is a Partner with our Litigation practice, based in our Abu Dhabi office.','Publisher' => NULL,
                    'Telephone' => '+971 2 644 4474','Practice Areas' => '296','Locations' => '77','Office Title' => '310','Categories' => '33','Tags' => '56, 59'),

//
//            array(
//                    'experience' =>[
//    ] , 'ID' => '334',
//'services' => [],
//'first_name'=>'',
//'last_name'=>'',
//'industries' => [],
//'post_title'
// => 'Khalid Bujsaim','post_content' => '<p>Khalid is an Advocate in the Litigation Practice in our DIFC office in Dubai. With over 22 years\' experience, he has full rights of audience before the UAE Courts.
//</p>
//<p>Since joining us in 2009, Khalid has developed a reputation for delivering excellent legal advice on real estate, commercial, corporate, banking, intellectual property, copyright and patents matters, usually for clients in maritime, insurance and criminal litigation proceedings.
//</p>
//<p>Khalid holds an LLB from the Dubai Police College of Law and is bilingual in both Arabic and English.</p>','post_date' => '2014-01-26 07:33:45','post_name' => 'khalid-bujsaim','post_parent' => '0','Image' => 'wp-content/uploads/2014/01/Khalid-Bujsaim.jpg','Title' => 'Advocate','Email' => 'khalid.bujsaim@bsabh.com','Excerpt' => 'Khalid is an Advocate within our Litigation practice based in our DIFC office in Dubai.','Publisher' => NULL,'Telephone' => '+971 4 568 5555','Practice Areas' => '296','Locations' => '78','Office Title' => '320','Categories' => '33','Tags' => '56, 58'),
//

            array(
                    'experience' =>[
    ] , 'ID' => '341',
'services' => [4,17],
'first_name'=>'Saad',
'last_name'=>'Younes',
'post_title'
 => 'Saad Younes','post_content' => '<p>Saad is a Senior Associate in the Litigation Practice in our DIFC office in Dubai. Practicing since 1998, he specialises in various types of litigation and dispute resolution including real estate, employment and debt recovery.
</p>
<p>With clients including individuals and corporate entities such as banks and real estate companies, Saad has successfully negotiated a high number of settlements,
 provided pre-litigation advice and alternative measures to  finalising disputes.</p>','post_date' => '2014-01-26 07:38:36','post_name' => 'saad-younes','post_parent' => '0',
                'Image' => 'img/profiles/Saad-Younes.jpg','Title' => 'Senior Associate','Email' => 'saad.younes@bsabh.com','Excerpt' => 'Saad is a Senior Associate in our Litigation practice, based in our DIFC office in Dubai.','Publisher' => 'Saad Younes','Telephone' => '+971 4 568 5555','Practice Areas' => '296','Locations' => '78','Office Title' => '309','Categories' => '33','Tags' => '56, 58'),


            array(
                    'experience' =>[
            ['Advising Delivery Hero on the creation of an in-application wallet by users and operators of the Talabat platform.'],
            ['Assisting SH Capital, a B2B FinTech start-up, in obtaining their regulatory licence in DIFC.'],
            ['	Advising DEWA on the creation of a framework for FinTech investments out of and into the DIFC.'],
            ['	Advising X-Trade Brokers (XTB) on fulfilling their regulatory requirements before the DSFA.  '],
            ['	Advising Channel VAS, a leading FinTech provider of mobile financial and airtime credit services on drafting a series of contracts regarding micro-loans to be disbursed via smart phones. '],
            ['	Advising Baraka on various aspects related to the regulatory framework in the DIFC and compliance with DFSA regulations.'],
            ['	Advising Kuaishou Technology on the launch of their streaming reward system in the UAE and KSA.'],
            ['	Advising a DIFC based FinTech firm on two tokenization projects involving two different asset classes: real estate and art.'],
            ['	Advising Policy Bazaar on obtaining a funding round of USD 75 million – the largest funding round of InsurTech in the UAE. '],
            ['	Advising a US based FinTech on the incorporation and advisory entity regulated by the DFSA.'],
            ['	Advising Wariq Limited on obtaining their ITL from the DFSA to test an e-wallet platform between the UAE and KSA.  '],
    ] , 'ID' => '342',
'services' => [9,1,2,5,3,20,12,15,16,18],
'first_name'=>'Nadim',
'last_name'=>'Bardawil',
'post_title'
 => 'Nadim Bardawil','post_content' => '
<p>Nadim is a Partner both in our Corporate and TMT practices in our DIFC office in Dubai. He specialises in transactional corporate work across various industries including media, technology and healthcare.</p>
<p>In his day-to-day role Nadim advises local and regional corporate on commercial matters, including joint ventures, restructuring and commercial agencies. He also focuses on technology and advises clients on complex technology-related agreements, e-commerce matters, and data protection.</p>
<p>He regularly publishes articles in various TMT related matters and speaks at a number of seminars and panels on various legal issues.</p>
<p>Fluent in a number of key languages including English, French and Arabic, Nadim holds a JD degree from the Hofstra University School of Law and is admitted to practice in the State of New York. He also holds a Business Management degree.</p>','post_date' => '2014-01-26 07:39:31','post_name' => 'nadim-bardawil','post_parent' => '0','Image' => 'img/profiles/Nadim-Bardawil-.jpg','Title' => 'Partner','Email' => 'nadim.bardawil@bsabh.com','Excerpt' => 'Nadim is a Partner in our Corporate and M&A, and Insurance & Reinsurance practices, based in our DIFC office in Dubai.','Publisher' => 'Nadim Bardawil','Telephone' => '+971 4 528 5555','Practice Areas' => '294, 302, 935','Locations' => '78','Office Title' => '310','Categories' => '31, 37','Tags' => '56, 58'),


            array(
                    'experience' =>[
    ] , 'ID' => '343',
'services' => [1,2,21,4,3,10,20,17,19,14,16],
'first_name'=>'Mohammed',
'last_name'=>'Al Ahdal',
'post_title'
 => 'Mohammed Alahdal','post_content' => '<p>Mohammed is an Associate in the Corporate and M&A team in our DIFC office in Dubai. He advises private clients, real estate and property developers and organisations from the retail, lifestyle, leisure and hospitality sectors.
</p><p>
Predominately advising private clients on submitting new project details to the Dubai Land Department (RERA),
he also advises on registration and licenses, provides real estate and tenancy dispute advice and advice on breach of contract and employee rights.</p>','post_date' => '2014-01-26 07:41:16','post_name' => 'mohammed-alahdal',
                'post_parent' => '0',
                'Image' => 'img/profiles/Mohammed-Al-Ahdal-.jpg','Title' => 'Associate','Email' => 'mohammed.alahdal@bsabh.com','Excerpt' => 'Mohammed is an Associate in our Corporate and M&A team, based in our DIFC office in Dubai.','Publisher' => NULL,'Telephone' => '+971 4 568 5555','Practice Areas' => '304, 294, 306, 292','Locations' => '78','Office Title' => '308','Categories' => '31','Tags' => '56, 58'),


//            array(
//                    'experience' =>[
//    ] , 'ID' => '345',
//'services' => [],
//'first_name'=>'',
//'last_name'=>'',
//'industries' => [],
//'post_title'
// => 'Mahmoud Hussein El Gousi','post_content' => '<p>Mahmoud is an Associate with the Litigation Practice in our DIFC office in Dubai. Practicing since 2005, he regularly advises financial institutions and private clients on various types of litigation and dispute resolution matters such as foreclosures, debt recovery, employment matters and landlord and tenant contracts and disputes.
//</p>
//<p>Prior to moving to the region and joining BSA in 2013, Mahmoud practised at a law firm in Sudan. He obtained a Bachelor degree in Law from Cairo University in Egypt
//in 2003 and is a qualified Sudanese lawyer and member of the Sudanese Bar Association.</p>','post_date' => '2014-01-26 07:43:31','post_name' => 'mahmoud-elgousi','post_parent' => '0','Image' => 'wp-content/uploads/2014/01/Mahmoud-Elgousi-SQUARE-BW.jpg','Title' => 'Associate','Email' => 'mahmoud.elgousi@bsabh.com','Excerpt' => 'Mahmoud is an Associate with our Litigation practice based in our DIFC office in Dubai.','Publisher' => NULL,'Telephone' => '+971 4 568 5555','Practice Areas' => '296','Locations' => '78','Office Title' => '308','Categories' => '33','Tags' => '56, 58'),
//

            array(
                    'experience' =>[
            ['Advising a piling & foundation contracting company on a shareholders dispute wherein an investment group initiated proceedings claiming the amount of AED 60 million being the alleged liabilities of the company for shares bought.'],
            ['Defending proceedings against a private equity organization initiated by an individual claiming the amount of AED 25 million representing her commission for acting as an agent in the sale of a property.'],
            ['	Advising a FTSE 100 re-insurance company in connection with a multi-million dollar dispute for the value of AED 22 million involving a UAE based insurance company stemming from the Egyptian uprising in early 2011.'],
            ['	Defending and counterclaiming proceedings filed by a Petroleum company in relation to compensation for unlawful termination of 6 oil services related contracts for Abu Musa Island, where a signed MOU between Iran and the UAE potentially exposing an oil & gas equipment & services company to the risk of being in breach of US sanctions.'],
            ['	Representing a luxury fashion house in relation to an illegal termination of an exclusive distributorship agreement.'],
            ['	Advising Eternity Medicine Institute in a claim lodged through Dubai International Arbitration Centre (DIAC) by three investors who sought to nullify a Share Purchase Agreement, which they argued they were fraudulently induced into entering.'],
            ['Pursued current litigation proceedings commenced by the terminated subcontractors and assist Kleindienst form regain control from the subcontractors of the construction site at a remote part of Dubai’s territorial waters, following approaches to the public prosecutor and the police. We are also defending court proceedings and aiming to divert the dispute resolution process onto contractually agreed arbitration.
This matter is important because it relates to a high profile, unusual and ambitious '],
            ['	Legal opinion on local unregulated entities claiming interest on the loan, recovering debts, in addition to pursuing criminal proceedings against various real estate developers.'],
    ] , 'ID' => '359',
'services' => [7,9,5,4,6,3,13,10,12,17,19,14],
'first_name'=>'Asim',
'last_name'=>'Ahmed',
'post_title'
 => 'Asim Ahmed','post_content' => '<p>Asim is a Partner and the Head of the Litigation Department, based in our Dubai office.</p>
<p>Asim has vast knowledge of legal systems in the GCC and MEA regions, and specifically a vast experience of both the UAE and Sudanese Court systems and their applicable laws. Asim provides succinct, pragmatic and commercial legal advice to his clients and delivers clear-cut solutions to their problems. His expertise spans civil, criminal, maritime, insurance, commercial, employment and telecommunications matters and Asim has a particular interest in real estate.</p>
<p>Asim&rsquo;s valid and incisive arguments raised before the UAE Courts have been instrumental in the implementation of legal principles and doctrines, most of which were ultimately elevated to court precedents binding on lower Courts and generating practicable convention within the domestic litigation community. Asim regularly handles arbitrations and acts for clients in the Dubai International Arbitration Centre (DIAC) in addition to other forums of alternative dispute resolution.</p>
<p>Asim has been practicing for almost 25 years. Asim commenced his career back when he obtained his license from the Sudan Bar Association in 1992 and enjoys rights of audience before the superior and lower Courts in Sudan. In addition to this, Asim is licensed by the Chief of Justice in Sudan to act as a Commissioner for Oaths, drafting contracts and agreements, authenticating and attesting the execution of documents and signatures and administering oaths.</p>
<p>In the year 2000, Asim moved to the GCC region in order to diversify his litigation experience before the GCC Courts, exposing him to the rare versatility that comes with representing multinational and local clients through
 a well-honed bilingual Arabic-English fluency.</p>','post_date' => '2014-01-25 09:30:21','post_name' => 'asim-ahmed','post_parent' => '0',
                'Image' => 'img/profiles/Asim-Ahmed.jpg','Title' => 'Partner','Email' => 'asim.ahmed@bsabh.com','Excerpt' => 'Asim is a Partner and the Head of the Litigation Department, based in our Dubai office.','Publisher' => NULL,'Telephone' => '+971 4 528 5555','Practice Areas' => '296','Locations' => '78','Office Title' => '310','Categories' => '33','Tags' => '56, 58'),


            array(
                    'experience' =>[
            ['Representing clients in a wide range of commercial disputes including breach of contracts, partnership disputes and debt collection'],
            ['Representing clients in a vast number of employment court cases related to employment disputes for both employees and employers'],
            ['	Acting on behalf of various insurance and reinsurance companies in a wide range of disputes including medical malpractice'],
            ['	Representing a leading oil and gas services company in a vast range of disputes including breach of oil contracts, commercial agency disputes, employment disputes and other claims'],
            ['	Acting on behalf of clients in negotiating and concluding successful settlements'],
            ['	Formulating and applying strategies in complex disputes.'],
    ] , 'ID' => '363',
'services' => [9,5,4,8,6,12,11,15,18],
'first_name'=>'Abdullah',
'last_name'=>'Ishnaneh',
'post_title'
 => 'Abdullah Ishnaneh','post_content' => '<p>Abdullah is a Partner in our Litigation practice based in our DIFC office in Dubai. Practicing since 2005, he specialises in various types of litigation and dispute resolution including medical malpractice, employment and debt recovery. He predominately acts on behalf of international clients including insurance and reinsurance companies, hospitals and medical centres and oil and gas companies.</p>
<p>In addition to representing and advising clients during litigation proceedings, Abdullah has successfully concluded settlements on behalf of clients, provided pre-litigation advice and put forth strategies for clients to follow in order to ensure a positive conclusion to a dispute</p>','post_date' => '2014-01-26 07:59:25','post_name' => 'abdullah-ishnaneh','post_parent' => '0','Image' => 'img/profiles/Abdullah-Ishnaneh.jpg','Title' => 'Partner','Email' => 'abdullah.ishnaneh@bsabh.com','Excerpt' => 'Abdullah is a Partner in the Litigation practice based in our DIFC office in Dubai.','Publisher' => 'Abdullah Ishnaneh','Telephone' => '+971 4 528 5555','Practice Areas' => '306, 296','Locations' => '78','Office Title' => '310','Categories' => '33, 61','Tags' => '56, 58'),


            array(
                    'experience' =>[
            ['Initiating proceedings for a leading UAE contracting company against Modern Residential City LLC for non-payment of contracting work performed and delayed compensation due to claims that the work performed was not as per the specifications, valued at AED 210 million.'],
            ['Advising clients on several disputes including criminal disputes, debt recovery, employment, insurance, maritime and compensation.'],
            ['	Advising clients on several disputes including criminal disputes, debt recovery, employment, insurance, maritime and compensation.'],
            ['	Handling commercial cases against various developers /malls.'],
            ['	Initiating and advising different companies in joint venture disputes, litigation employment matters'],
            ['	Filing criminal complaint against companies in several sectors.'],
            ['	Defending rental claims and civil appeals before the court of cassation.'],
            ['	Advising and representing an international insurance company before the UAE Courts in a dispute related to the damage caused by the uprising in Egypt.'],
    ] , 'ID' => '364',
'services' => [4,6,17,10],
'first_name'=>'Shaaban',
'last_name'=>'Metwally',
'post_title'
 => 'Shaaban Metwally','post_content' => '<p>Shaaban is a Partner in our Litigation practice, based in the DIFC office in Dubai. Practicing since 1991, he specialises in various types of litigation and dispute resolution including insurance and reinsurance, debt recovery, maritime, commercial, real estate and other disputes.</p>
<p>Shaaban predominately acts on behalf of international clients including insurance and reinsurance companies, multinational, real estate companies and oil and gas companies.</p>
<p>In addition to representing and advising clients during litigation proceedings, Shaaban has successfully concluded settlements on behalf of clients, provided pre-litigation advice and put forth strategies for
clients to follow in order to ensure a positive conclusion to a dispute.</p>','post_date' => '2014-01-26 08:00:15','post_name' => 'shaaban-metwally','post_parent' => '0',
                'Image' => 'img/profiles/Shabaan-Metwally.jpg','Title' => 'Partner','Email' => 'shaaban.metwally@bsabh.com','Excerpt' => 'Shaaban is a Partner in our Litigation practice, based in the DIFC office in Dubai','Publisher' => 'Shaaban Metwally','Telephone' => '+971 4 528 5555','Practice Areas' => '296','Locations' => '78','Office Title' => '310','Categories' => '33','Tags' => '56, 58'),


            array(
                    'experience' =>[
            ['Advising and representing a building contracting company during a due diligence exercise carried out on a proposed construction of a real estate project in Umm Al Quwain, which included preparing a JV Agreement.'],
            ['Advising a US non-profit academic medical centre operating in the UAE through a partnership with Abu Dhabi Government in relation to their business operations in the UAE and particularly their regulatory and compliance requirements.'],
            ['	Advising a Bahraini based Islamic private bank and advising in relation to acquiring an African telecommunication operator entity through a UAE based company including the transfer of the employees and the re-arrangement of their accrued entitlements.'],
            ['	Advising an m-commerce company on the launch of a new gaming service in the UAE through the two telecommunications companies, DU and Etisalat that included: alignment with censorship laws in the UAE; data protection and privacy as stipulated by the consumer protection authority; disclaimer content when consumers availed the service; and reviewing and amendment of the terms and conditions and commercial agreements.'],
            ['	Advising a UAE multispecialty hospital in relation to the sale and purchase of its subsidiary entity based in the UAE.'],
            ['	Advising the Telecommunications Regulatory Authority (TRA) of the United Arab Emirates (UAE) in relation to the new Data Protection legislation in the UAE.'],
            ['	Advising an oil & gas company on setting up in Abu Dhabi and Fujairah and the appropriate corporate structure for commercial activities to get involved in within oil & gas. This included obtaining approvals from local authorities; Abu Dhabi Economic department (DED), Ministry of Economy and the Supreme Petroleum Council (SPC).'],
            ['	Our team is currently advising a highly confidential group of entities on the set up of the UAE’s first ever venture capital fund that will be tokenized. The fund will be set up via tokens issued out of a STO. '],
    ] , 'ID' => '365',
'services' => [1,2,9,5,21,4,8,3,20,10,12,11,17,19,15,16,18],
'first_name'=>'Rima',
'last_name'=>'Mrad',
'post_title'
 => 'Rima Mrad','post_content' => '
<p>Rima is a Partner with the Corporate and M&A Practice in our DIFC offices in Dubai. She is an experienced corporate and insurance lawyer who has practiced in the UAE for over sixteen years.</p>
<p>She specialises in advising insurance companies, corporate organisations, financial institutions, energy companies and private equity funds on a wide range of legal issues. These include M&A transactions, due diligence, commercial agreements, commercial related disputes, IT related transactions and various regulatory matters.</p>
<p>She has also advised and assisted international clients in developing their business throughout the GCC, particularly in relation to regulatory and compliance matters. She also provides employment advice to companies in relation to policies, structuring and breach of contract.</p>
<p>Rima was recently recognized by Financial News as the 50 Most Influential Women in the Middle East Finance &ndash; 2019</p>
<p>Rima holds a Masters Degree in Public Law and Sustainable Development Law from Universite Paris Descartes.</p>
','post_date' => '2014-01-25 10:40:51','post_name' => 'rima-mrad','post_parent' => '0','Image' => 'wp-content/uploads/2014/01/Rima-Mrad-BW.jpg','Title' => 'Partner','Email' => 'rima.mrad@bsabh.com','Excerpt' => 'Rima is a Partner with the Corporate and M&A practice, based in our DIFC offices in Dubai.','Publisher' => 'Rima Mrad','Telephone' => '+971 4 528 5555','Practice Areas' => '304, 294, 306, 302, 948','Locations' => '78','Office Title' => '310','Categories' => '31, 37, 38, 61','Tags' => '56, 58'),


            array(
                    'experience' =>[
                        ['Representing industry leader in PET manufacturing, in a debt-for equity swap; one of the largest restructuring exercises the UAE has witnessed. '],
                        ['Advising Dr BR. Shetty, founder of NMC Healthcare, Finablr Payments and Foreign Exchange Technology Platform, and Neopharma on restructuring his group of companies in the UAE.'],
                        ['	Advising a UAE bank-led syndicate of lenders on the UAE, DIFC and ADGM law perspective in an AED 5 billion debt restructuring of a privately owned regional business conglomerate '],
                        ['	Advising Mashreq Bank on their AED 135 million claim against KBBO Group '],
                        ['	Advising an Australian, publicly listed multinational contractor on potential insolvency, restructuring and exiting the region.'],
                        ['	Advising syndicate of banks on enforcement action and strategy involving securities and collateral (property mortgage, vessel mortgage, assignment of rights, assignments of receivables etc.) in various jurisdictions from more than 20 individual and corporate parties.'],
    ] , 'ID' => '366',
'services' => [2,3,4,9,1,20,12,11,19,15,16],
'first_name'=>'Michael',
'last_name'=>'Kortbawi',
'post_title'
 => 'Michael Kortbawi','post_content' => '<p>Michael is a partner in BSA&rsquo;s Corporate and M&A practice based in Dubai. Leading a team of eight corporate lawyers Michael oversees corporate and regulatory advice in relation to all aspects of corporate and finance law.&nbsp; This includes cross-border financing, real estate finance and transactions involving financial institutions and insurance companies.</p>
<p>Michael is an expert in financial restructuring &ndash; leading the team in several large restructuring matters over the past 3 years, including one of the largest restructuring exercises in the Middle East.</p>
<p>Also leading the firm&rsquo;s banking & finance function, Michael oversees the majority of the firm&rsquo;s banking matters, particularly corporate restructuring matters.&nbsp;</p>
<p>Michael has extensive experience in the Middle East region, with over 18 years&rsquo; experience practising in the UAE.&nbsp; Michael has successfully spearheaded the organic growth of BSA into the six countries in which it now operates.</p>
<p>His diverse array of key clients includes Halliburton, ADCB, Emirates NBD, Mashreq Bank, CIMIC Group, JBF Industries and Banijay Group.&nbsp;</p>
<p>Michael and his team are known to &lsquo;provide valuable and practical knowledge of the law delivered to international standards&rsquo;.&nbsp;&nbsp; </p>','post_date' => '2014-01-25 02:00:34','post_name' => 'michael-kortbawi',
                'post_parent' => '0',
                'Image' => 'img/profiles/Michael-Kortbawi-.jpg','Title' => 'Partner','Email' => 'michael.kortbawi@bsabh.com','Excerpt' => 'Michael is a Partner in our Corporate and M&A and Insurance & Reinsurance practices, based in our DIFC office in Dubai.','Publisher' => 'Michael Kortbawi','Telephone' => '+971 4 528 5555','Practice Areas' => '304, 294, 306, 302','Locations' => '78','Office Title' => '310','Categories' => '31, 37, 38, 61','Tags' => '56, 58'),


            array(
                    'experience' =>[
                        ['Dubai based contractor in DIAC proceedings for a AED 200 million claim for unpaid invoices and prolongation costs against major local real estate developer and defending counterclaim for liquidated damages in excess of AED 180 million.'],
                        ['Dubai based contractor and MEP subcontractor in DIAC proceedings for a AED 178 million claim for unpaid invoices, disputed variations and eight EoTs with associated prolongation costs against Abu Dhabi real estate developer in relation to a hotel development on the Palm Jumeirah.'],
                        ['	Dubai based contractor in DIAC proceedings for a AED 300 million claim for unpaid invoices and prolongation costs against major local real estate developer.'],
                        ['	Dubai based contractor in AED 105 million DIAC arbitral proceedings in relation to EoT, prolongation costs, loss of profit and head office overheads cost arising from a Dubai residential project.'],
                        ['	Dubai based MEP subcontractor in a AED 75 million DIAC arbitration in connection with claims for EoT, prolongation costs, value of disputed variations and unpaid/under-certified work in progress, arising out of the construction of a major petroleum company headquarters in Abu Dhabi.'],
                        ['	Abu Dhabi based main contractor in ADCCAC arbitration proceedings brought by the MEP subcontractor in connection with various claims and counterclaims totaling AED 220 million, arising from a design and built subcontract for the upgrade infrastructure of a sewerage plant.'],
                        ['	Abu Dhabi based main contractor in ADCCAC arbitration proceedings against a government department in connection with claims arising from the overvaluation of liquidated damages and EoT claims of AED 50 million, under a contract for the design and construction of a wildlife park and resort.'],
                        ['	Dubai based main contractor in DIFC-LCIA arbitration proceedings against a private developer for payment of AED 42 million of profit under a side agreement to the main construction contract.'],
    ] , 'ID' => '367',
'services' => [6,7,10,13,17],
'first_name'=>'Antonios',
'last_name'=>'Dimitracopoulos',
'post_title'
 => 'Antonios Dimitracopoulos','post_content' => '<p>Antonios is a Partner, and the Head of Arbitration &amp; Dispute Resolution, and Construction practices, based in our DIFC office in Dubai.</p>
<p>He is an accredited Fellow (FCIArb) of the Chartered Institute of Arbitrators and a Registered Arbitrator with the Dubai International Arbitration Centre.</p>
<p>Antonios is also practising Solicitor-Advocate (Higher Courts Civil Proceedings in England &amp; Wales) and undertakes his own advocacy in his arbitration practice.</p>
<p>He is also a DIFC Courts Registered Practitioner (Part 2) with rights of audience before the DIFC Courts.</p>
<p>Antonios has been practicing in the UAE for more than twenty years initially with Clifford Chance Dubai and subsequently with Al Tamimi &amp; Co. before joining BSA Ahmad Bin Hezeem (then known as Bin Shabib &amp; Associates (BSA) LLP) as a partner in 2007.</p>
<p>He specialises in arbitration and DIFC litigation mainly within the construction industry and acts primarily for contractors, subcontractors and architects.</p>
<blockquote>"Market sources comment that Antonios is excellent, and has a strong understanding of the market and the local culture, having been in the UAE for 27 years"</blockquote>
<p>CHAMBERS GLOBAL, 2018</p>','post_date' => '2014-01-25 03:02:33','post_name' => 'antonios-dimitracopoulos','post_parent' => '0',
                'Image' => 'img/profiles/Antonios-Dimitracopoulos.jpg','Title' => 'Partner','Email' => 'antonios.dimitracopoulos@bsabh.com','Excerpt' => 'Antonios is a Partner (FCIArb, Solicitor-Advocate) in the Arbitration & Dispute Resolution, and Construction practices based in our Dubai office.','Publisher' => 'Antonios Dimitracopoulos','Telephone' => '+971 4 528 5555','Practice Areas' => '298, 299','Locations' => '78','Office Title' => '310','Categories' => '33, 34, 35','Tags' => '56, 58'),


            array(
                    'experience' =>[
            ['Acting as amicus curiae on the winding up of major US law firm, including advising the liquidator on employment and insolvency issues.'],
            ['Advising an investment fund on creation and set up of first Shari’ah compliant fund out of Malta;'],
            ['	Advising on the structuring Islamic and conventional investment funds in global and private equities and real estate;'],
            ['	Advising a European-based Company on a USD 130 million Ijara facility to finance their operations in the GCC, involving active negotiations with 4 leading banks in the UAE.;'],
            ['	Advising MJ Hudson on behalf of Fortress Group Inc. (FGI) and Fortress Placement Services (UK) Limited on the marketing of foreign securities by investment funds in light if the Capital Market Authority’s rules and regulation (CMA) in Oman, as well as Saudi Laws.'],
    ] , 'ID' => '368',
'services' => [1,2,3,9,20,12,19,14,16],
'first_name'=>'Jimmy',
'last_name'=>'Haoula',
'post_title'
 => 'Jimmy Haoula','post_content' => '<p>Based in Dubai, Jimmy is the Managing Partner of the firm. He specialises in corporate, commercial and real estate transactions and has almost two decades of experience in the region.</p>
<p>Jimmy&rsquo;s specific expertise lies in local, regional and cross-border mergers and acquisitions, local IPOs, and real estate projects and developments.</p>
<p>After five years with Clifford Chance Dubai, Jimmy joined BSA as Managing Partner, focusing on developing our practice areas with an emphasis on corporate, commercial and real estate.</p>
<p>Jimmy was one of the legal pioneers who led the development in the regulatory framework of the real estate sector in Dubai. He has advised and assisted on many projects and developments including the first long-term lease real estate project, two developments in Dubai and many projects within Emaar Real Estate Developments.</p>
<p>In addition, Jimmy contributed to the development and improvement of the Strata regulations in Dubai and the Dubai International Financial Centre (DIFC).</p>','post_date' => '2014-01-25 01:30:19','post_name' => 'jimmy-haoula','post_parent' => '0',
                'Image' => 'img/profiles/Jimmy-Haoula-.jpg',
                'Title' => 'Managing Partner',
                'Email' => 'jimmy.haoula@bsabh.com',
                'Excerpt' => 'Jimmy is the Managing Partner of the firm, based in our DIFC offices in Dubai.  He specialises in Corporate and M&A, Commercial and Real Estate.','Publisher' => 'Jimmy Haoula',
                'Telephone' => '+971 4 528 5555',
                'Practice Areas' => '75, 304, 294, 292',
                'Locations' => '78',
                'Office Title' => '321','Categories' => '25, 31, 36, 38','Tags' => '56, 58'),


            array(
                    'experience' =>[
    ] , 'ID' => '378',
'services' => [1,2,4,3,20,17,16],
'first_name'=>'Mohammed Nedal',
'last_name'=>'Dajani',
'post_title'
 => 'Mohammed Nedal Dajani','post_content' => '<p>The Head of our Sharjah and Northern Emirates offices. Mohammed specialises in arbitration, litigation and dispute resolution.
</p><p>
Practising since 1992, Mohammed has extensive experience in various types of litigation matters. He acts for regional and international clients including developers, business owners, purchasing corporations and public bodies.
</p><p>
Mohammed\'s advice covers adjudications, conciliations, claim settlements, expert determinations, litigation and mediation, matter handling, client management, and agreement and contracts drafting.
He also negotiates on behalf of clients and advises on registration of trademarks and IP, filing opposition actions and registering companies and branch offices onshore and offshore.</p>',
                'post_date' => '2014-01-26 08:08:45','post_name' => 'mohd-nedal-dajani','post_parent' => '0',
                'Image' => 'img/profiles/Mohammed-Nidal.jpg','Title' => 'Head of Sharjah & Northern Emirates offices',
                'Email' => 'nedal.dajani@bsabh.com',
                'Excerpt' => 'Mohammed Nedal is the Head of Sharjah & Northern Emirates offices, based in Sharjah',
                'Publisher' => 'Mohammed Nedal Dajani',
                'Telephone' => '+971 7 227 3106',
                'Practice Areas' => '298, 299, 294, 296',
                'Locations' => '79, 80',
                'Office Title' => ' 309, 322','Categories' => '31, 33, 34','Tags' => '55, 56, 57'),


            array(
                    'experience' =>[
    ] , 'ID' => '379',
'services' => [1,2,4,6,3,9,10,20,12,11,17,16],
'first_name'=>'Mundhir',
'last_name'=>'Al Barwani',
'post_title'
 => 'Mundhir Albarwani','post_content' => '<p>Mundhir is a Partner in the Muscat office in the Sultanate of Oman. His experience covers high profile insurance and banking cases, in which he has acted for major international insurance and financial institutions.
</p><p>
He also advises several multinational firms on establishing presence and acquiring licenses to operate in Oman, initiating discussions with regulators such as the Capital Market Authority (CMA) and negotiating compliance-related matters.
</p><p>
Mundhir led a team which advised one of the most prominent banks in Oman on several matters including Islamic banking, loans and merchant banking transactions.
</p><p>
He holds an LL.B from the University of Applied Sciences in Jordan with an emphasis on Private and Public Laws.</p>','post_date' => '2014-01-26 08:11:09','post_name' => 'mundhir-albarwani','post_parent' => '0','Image' => 'wp-content/uploads/2014/01/Mundhir-Albarwani-BW-Square.jpg','Title' => 'Partner','Email' => 'mundhir.albarwani@bsabh.com','Excerpt' => 'Mundhir is a Partner based in the Muscat office, in the Sultanate of Oman.','Publisher' => 'Mundhir Albarwani','Telephone' => '+968 2 421 8555','Practice Areas' => '75, 294, 296','Locations' => '83','Office Title' => '310','Categories' => '33, 36, 37','Tags' => '53, 54'),


            array(
                    'experience' =>[
            ['Representing a high-net worth individual client in a OMR 3.5mln arbitration relating to a dispute on Musandam development project, for variations, EoTs, Prolongation Costs and the return of a performance bond'],
            ['Representing Oman Ministry of Housing as Lead Counsel in OMR 7m ICC arbitral proceedings in relation to EoT, prolongation costs, loss of profit and head office overheads cost claim arising from a residential project'],
            ['	Advising and representing Khaleeji Bank One in a loan default case totalling to USD 6mln claim'],
            ['	Advising an oil & gas company on setting up in the Sultanate of Oman and the appropriate corporate structure for commercial activities to get involved in within oil & gas sector, inclusive of assisting with and obtaining approvals from local authorities'],
            ['	Advising an oil & gas equipment and services company in relation to its employees who had special employment arrangements that protected them in specific jurisdictions in the Middle East region'],
            ['	Analysing the strength of claims to be brought by employers against contractors, advising on liability and risks of pursuing such claims'],
            ['	Drafting defences (incl. counter-arguments), on behalf of employers, against claims brought by contractors'],
            ['Advising employers on Alternative Dispute Resolution (ADR) strategy and risks (incl. adjudication risk)'],
    ] , 'ID' => '380',
'services' => [7,4,6,13,17],
'first_name'=>'Abdulaziz',
'last_name'=>'Al Rashdi',
'post_title'
 => 'Abdulaziz Alrashdi','post_content' => '<p>Abdulaziz is a Partner in our Muscat office. Experienced in litigation and dispute resolution, his expertise encompasses a wide range of industries including commercial, civil and administrative disputes, as well as thorough procedural advice on local laws.</p>
<p>One area in which Abdulaziz has a particular specialism is the US Foreign Accounts Tax Compliance Act (&ldquo;FATCA&rdquo;). Part of a team presenting full methodical advice to a major insurance client in respect of its exposure, he helped set out a mechanism which ensured compliance with both FATCA and the local privacy laws.</p>
<p>Abdulaziz holds an LLB from Sultan Qaboos University in Oman with an emphasis on Private and Public Laws.</p>','post_date' => '2014-01-26 08:11:55','post_name' => 'abdul-aziz-alrashdi','post_parent' => '0','Image' => 'wp-content/uploads/2014/01/Abdulaziz-Alrashdi-BW-Square.jpg','Title' => 'Partner','Email' => 'abdulaziz.alrashdi@bsabh.com','Excerpt' => 'Abdulaziz is a Partner based in our Muscat office in the Sultanate of Oman.','Publisher' => NULL,'Telephone' => '+968 2 421 8555','Practice Areas' => '298, 296','Locations' => '83','Office Title' => '310','Categories' => '33, 34','Tags' => '53, 54'),


//            array(
//                    'experience' =>[
//    ] , 'ID' => '382',
//'services' => [7,9,6],
//'first_name'=>'Abdelmuneem',
//'last_name'=>'El Rufaai',
//'industries' => [13,12],
//'post_title'
// => 'Abdelmuneem El Rufaai','post_content' => '<p>Abdelmuneem is a Senior Associate in our Muscat office. With over 20 years\' experience, he has worked on some of the biggest commercial, insurance, banking and dispute resolution matters in the Sultanate of Oman, earning himself a reputation as one of the most respected litigation lawyers in the country.
//</p><p>
//This technical excellence, coupled with an in-depth knowledge of the banking system, has made BSA a formidable force in the pursuit of defaulting debtors.
//</p><p>
//Abdelmuneem holds an LLB from the University of Cairo – Khartoum campus and is a member of the Sudanese Bar Association with right of audience before the Omani courts.</p>','post_date' => '2014-01-26 08:13:32','post_name' => 'abdul-moneem-al-rafei','post_parent' => '0','Image' => 'wp-content/uploads/2014/01/Abdelmunem-El-Rufaai-BW-Square.jpg','Title' => 'Senior Associate','Email' => 'abdulmeneem.alrafei@bsabh.com','Excerpt' => 'Abdelmunem is a Senior Associate based in the Muscat office in the Sultanate of Oman.','Publisher' => NULL,'Telephone' => '+968 2 421 8555','Practice Areas' => '298, 75, 296','Locations' => '83','Office Title' => '309','Categories' => NULL,'Tags' => '53, 54'),


            array(
                    'experience' =>[
    ] , 'ID' => '383',
'services' => [1,2,3,4,20,16,17],
'first_name'=>'Johnny',
'last_name'=>'El Hachem',
'post_title'
 => 'Johnny El Hachem','post_content' => '<p>Johnny is a Partner in our Lebanon office. She has a full range of legal services expertise in a variety of matters ranging from penal and civil law to commercial and business law, general corporate work, mergers and acquisitions and structuring foreign investments to specific industry sectors.
</p><p>
She has also built a strong and valuable experience in all matters relating to commercial law and more specifically competition, franchising, distribution, and management agreements.
</p><p>
Johnny assists our clients from the initial establishment of their company through all aspects of its evolution and operation. This includes building appropriate corporate and tax structures and advising them on how to take advantage of all the exemptions and particularities of the Lebanese system.
</p><p>
She has also handled several IPO and PPM projects, as well as advising on incorporation and privatisation within several industries including entertainment and hospitality.</p>','post_date' => '2014-01-26 08:14:50','post_name' => 'johnny-el-hachem','post_parent' => '0','Image' => 'wp-content/uploads/2014/01/johnny.elhachem-BW-276x276.jpg','Title' => 'Head of Lebanon office','Email' => 'Johnny.hachem@bsalb.com','Excerpt' => 'Johnny is a Partner in our Beirut office in Lebanon, specialising in Corporate and M&A and Commerical Law.','Publisher' => 'Johnny El Hachem','Telephone' => '+961 1 992 4510','Practice Areas' => '304, 294','Locations' => '82','Office Title' => '310, 660','Categories' => '31, 38','Tags' => '51, 52'),


//                array(
//                    'experience' =>[
//    ] , 'ID' => '656',
//'services' => [4,5,6,7],
//'first_name'=>'Hassan',
//'last_name'=>'El Shahat Mohamed',
//'industries' => [13,17],
//'post_title'
// => 'Hassan El Shahat Mohamed','post_content' => '<p>Hassan is an Associate with the Litigation Practice in our DIFC office in Dubai. He  specialises in several types of litigation and dispute resolution including real estate, employment and debt recovery, acting for various clients including individuals and corporate entities including banks and real estate companies.
//</p><p>
//A qualified Egyptian lawyer, Hassam is a member of the Egyptian Bar Association.</p>',
//                    'post_date' => '2014-02-10 22:20:27','post_name' => 'hassan-el-shahat-mohamed','post_parent' => '0',
//                    'Image' => 'wp-content/uploads/2014/02/Hassan-El-Shahat-Mohamed-BW.jpg','Title' => 'Associate','Email' => 'hassan.mohamed@bsabh.com','Excerpt' => 'Hassan is an Associate with our Litigation practice, based in our DIFC office in Dubai.','Publisher' => NULL,'Telephone' => '+971 4 568 5555','Practice Areas' => '296','Locations' => '78','Office Title' => '308','Categories' => '33','Tags' => '56, 58'),


            array(
                    'experience' =>[
    ] , 'ID' => '781',
'services' => [4,6,7,13,14,17,19],
'first_name'=>'Hadiel',
'last_name'=>'Hussien',
'post_title'
 => 'Hadiel Hussien','post_content' => '<p>Hadiel is an Associate with our litigation department based in our DIFC office in Dubai. After completing the prestigious University of Sorbonne\'s undergraduate law program, Hadiel joined our team 7 years ago, whereby she handles litigation and alternative dispute resolution matters. Hadiel\'s current position has enabled her to garner the legal skills and knowledge for a wide array of matters, including real estate, rental, employment, construction, commercial, medical malpractice, and criminal disputes.
</p><p>
Hadiel is also in charge of liaising with multinational companies\' legal departments, as she is fluent in English, French, and Arabic. She regularly provides analytical and pragmatic legal advice on local laws and regulations tailored to each client\'s individual requirements.
</p><p>
She has successfully concluded several settlements between disputing parties, provided pre-litigation advice, and formulated bespoke strategies to ensure positive conclusion to disputes and eliminate litigation proceedings wherever possible.
</p><p>
Besides her focal practice, Hadiel regularly handles matters involving alternative dispute resolution tracts that are available in the UAE such as mediation and arbitration.</p>','post_date' => '2014-08-09 01:00:52',
                'post_name' => 'hadiel-hussien','post_parent' => '0',
                'Image' => 'img/profiles/Hadiel-Hussien.jpg','Title' => 'Associate','Email' => 'hadiel.hussien@bsabh.com','Excerpt' => 'Hadiel is an Associate in the Litigation practice, based in our DIFC office in Dubai.','Publisher' => NULL,'Telephone' => '+971 4 568 5555','Practice Areas' => '296','Locations' => '78','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


//            array(
//                    'experience' =>[
//    ] , 'ID' => '783',
//'services' => [5,6,7,4],
//'first_name'=>'Hassan',
//'last_name'=>'El Agawani',
//'industries' => [13,17],
//'post_title'
// => 'Hassan El Agawani','post_content' => '<p>Hassan is an Associate in the Litigation Practice in our DIFC office in Dubai. He  specialises in various types of litigation, criminal cases and rent issues.
//</p><p>
//In his day-to-day role he works for various clients including individuals and businesses such as banks and real estate companies.
//</p><p>
//Hassan is a qualified Egyptian lawyer and member of the Egyptian Bar Association.</p>','post_date' => '2014-08-09 01:05:16','post_name' => 'hassan-el-agawani','post_parent' => '0','Image' => 'wp-content/uploads/2014/08/Hassan-El-Agawani-BW.jpg','Title' => 'Associate','Email' => 'hassan.elagawani@bsabh.com','Excerpt' => 'Hassan is an Associate in our litigation practice, based in our DIFC office in Dubai','Publisher' => NULL,'Telephone' => '+971 4 568 5555','Practice Areas' => '296','Locations' => '78','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
            ['Advising a Middle Eastern holding company in the negotiation and execution of a restructuring of the entity and its subsidiaries that included structuring for certain subsidiaries and liquidation and deregistration of others. The exercise further involved change of control and management. The company owns and manages total assets and investments for value exceeding AED 600 million.'],
            ['Advising a private client on their USD 10 million acquisition of an organic vegetation plant in Ras Al Khaimah.'],
            ['	Advising on the set up and organization of a multi-million-dollar establishment of a bakery franchise, which includes aspects from the setup of multi-tiered company structures to the conclusion of master franchise and sub-franchising agreements and also the finalization of site licenses for the operation and construction of production facilities for the franchise.'],
            ['	Advising a big conglomerate on the set up and organization of a multi-million-dollar establishment of fitness related operations, which includes aspects from the setup of multi-tiered company structures to the conclusion of shareholders’ agreements, lease agreements, service agreements, concession agreements, consignment agreements and also the finalization of site licenses for the operation and construction of production facilities for the franchise.'],
            ['	Advising a GCC real estate business in the acquisition of a majority stake in several Oil and Gas facilities as well as contracting companies in Dubai and Abu Dhabi which further included due diligence, negotiating the terms of the transaction; regulatory advice, post-acquisition modus operandi and compliance with UAE laws.'],
            ['	Advising a contracting group on the acquisition of a 25% stake in an international Hotel, Ajman.'],
            ['	Advising clients on the acquisition and disposal of all types of property including all due diligence work.'],
            ['	Advising clients on the acquisition and disposal of all types of property including all due diligence work.'],
    ] , 'ID' => '789',
'services' => [1,2,21,3,10,20,19,14,16],
'first_name'=>'Lara',
'last_name'=>'Barbary',
'post_title'
 => 'Lara Barbary','post_content' => '
<p>Lara is a Partner in the Corporate and M&A practice at our DIFC office in Dubai. With a Masters in Law specialising in Sustainable Development Law from the Universite Paris Descartes &ndash; a part of the Sorbonne &ndash; she has led teams in numerous high profile and high-value transactions for well-known international and regional clients across a broad range of industries.</p>
<p>Equally in the Middle East, working on oil and gas, real estate, hospitality, aviation, healthcare, education, technology and renewable energy industry projects, Lara is highly experienced in complex international business transactions. Her skillset covers mergers and acquisitions, private equity, dispositions, strategic investments, joint ventures, establishment of entities, and general corporate advisory work.</p>
<p>In terms of M&A, Lara&rsquo;s expert knowledge spans from structure and strategy to negotiation and documentation, providing cost-effective due diligence investigations in diversified industries; including representation of parties on either side.</p>
<p>She also regularly advises on drafting commercial contracts, government procurement, agency agreements, sponsorship, distribution, franchise and sale of goods and services agreements. This includes providing commercial agency advice and coordination with our Litigation and Dispute Resolution teams, when the need arises to mitigate claims and negotiate terminations.</p>
<p>Fluent in English, French and Arabic, Lara joined us in 2010 from an international Agency for International Development. Lara also holds an LL.B from the Lebanese University.</p>
',
                'post_date' => '2014-08-09 01:21:09','post_name' => 'lara-barbary','post_parent' => '0',
                'Image' => 'img/profiles/Lara-Barbary.jpg','Title' => 'Partner','Email' => 'lara.barbary@bsabh.com','Excerpt' => 'Lara is a Partner in the Corporate and M&A practice, based in our DIFC office in Dubai.','Publisher' => 'Lara Barbary','Telephone' => '+971 4 528 5555','Practice Areas' => '304, 294, 292, 948','Locations' => '78','Office Title' => '310','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '1035',
'services' => [5,6,7,13,14,19],
'first_name'=>'Asma',
'last_name'=>'Siddiqui',
'post_title'
 => 'Asma Siddiqui','post_content' => '<p>Asma is an Associate in our Litigation practice, based in our DIFC office in Dubai. She represents corporate bodies and individuals and specialises in various litigation and alternative dispute resolution sectors including real estate and tenancy matters, employment, criminal proceedings, civil matters, and medical negligence.
</p><p>
Asma is proactive in concluding out of court or amicable settlements by means of providing legal opinions, pre-litigation advice, and formulating strategies for clients,  ensuring a positive conclusion to their disputes.
</p><p>
Asma graduated from the Government Law College (GLC), one of the most renowned educational institutions in India, with a meritorious rank.','post_date' => '2014-10-09 14:41:14',
                'post_name' => 'asa-siddiqui','post_parent' => '0',
                'Image' => 'img/profiles/Asma-Siddiqui.jpg','Title' => 'Associate','Email' => 'asma.siddiqui@bsabh.com','Excerpt' => 'Asma is an Associate in the Litigation practice, based in our DIFC office in Dubai.','Publisher' => 'Asma Siddiqui','Telephone' => '+971 4 568 5555','Practice Areas' => '298, 296','Locations' => '78','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '1039',
'services' => [6],
'first_name'=>'Salman',
'last_name'=>'Madkour',
'post_title'
 => 'Salman Madkour','post_content' => '<p>Salman is an Associate in the Corporate and M&A Practice in our DIFC office in Dubai. At BSA since 2014, he specialises in incorporating companies and has advised multinational clients on corporate structure, setting up legal entities or companies in the GCC and finalising all corporate and setting up requirements.
</p><p>
</p>He represents and advises clients on incorporation requirements for various UAE free zones, and alternative offshore options, and has performed application and registration procedures for international clients on various business models.
<p>
Salman holds an LLB from Tanta University in Egypt.</p>','post_date' => '2014-10-09 14:46:31','post_name' => 'salman-madkour','post_parent' => '0',
                'Image' => 'img/profiles/Salman-Madkour.jpg','Title' => 'Associate','Email' => 'salman.madkour@bsabh.com','Excerpt' => 'Salman is an Associate in the Corporate practice, based in our DIFC office in Dubai.','Publisher' => NULL,'Telephone' => '+971 4 568 5555','Practice Areas' => '294','Locations' => '78','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


//            array(
//                    'experience' =>[
//    ] , 'ID' => '1044',
//'services' => [6,7],
//'first_name'=>'Ali',
//'last_name'=>'Abdelhalim',
//'industries' => [],
//'post_title'
// => 'Ali Abdelhalim','post_content' => '<p>Ali is an Associate in the Litigation practice based in our Abu Dhabi office. He joined BSA in 2013 and has over seven years\' legal experience gained from practicing law both in Egypt and the UAE.
//</p><p>
//His main area of expertise are litigation and dispute resolution for industry sectors in real estate, corporate and financial institutions. Ali specializes in criminal cases and often represents clients in commercial cases, civil cases and labor cases.
//</p><p>
//Ali holds an LLB from Assuit University in Egypt.</p>','post_date' => '2014-10-09 14:57:40','post_name' => 'ali-abdelhalim','post_parent' => '0','Image' => 'wp-content/uploads/2014/10/Ali-Abdelhalim-BW-High-Res.jpg','Title' => 'Associate','Email' => 'ali.abdelhalim@bsabh.com','Excerpt' => 'Ali is an Associate in our Litigation practice, based in Abu Dhabi.','Publisher' => NULL,'Telephone' => '+971 2 644 4474','Practice Areas' => '296','Locations' => '77','Office Title' => '308','Categories' => NULL,'Tags' => NULL),

//
//            array(
//                    'experience' =>[
//    ] , 'ID' => '1053',
//'services' => [],
//'first_name'=>'',
//'last_name'=>'',
//'industries' => [],
//'post_title'
// => 'Yad Ezzuldin Dezay','post_content' => 'Yad is an Associate in the Corporate and M&A Practice in our Erbil office in Iraq. He has over eight years\' experience and specialises in taxation, corporate transactions, mergers and acquisitions, commercial matters, employment claims and contract review.
//</p><p>
//Since joining us in 2013, Yad has mostly represented NGOs, oil and gas companies, trade companies and international companies.
//</p><p>
//Yad holds an LLB from Cihan University in Kurdistan Iraq and speaks Kurdish, Arabic and English.</p>','post_date' => '2014-10-09 15:43:41','post_name' => 'yad-ezzuldin-dezay','post_parent' => '0','Image' => 'wp-content/uploads/2014/10/Yad-Ezzuldin-Dezay-Square-BW.jpg','Title' => 'Senior Associate','Email' => 'yad.dezay@bsabh.com','Excerpt' => 'Yad is an Associate in the Corporte and M&A practice, based in our Erbil office in Iraq.','Publisher' => 'Yad Dezay','Telephone' => '+964 6 6257 5888','Practice Areas' => '294','Locations' => '81','Office Title' => '309','Categories' => NULL,'Tags' => NULL),
//

            array(
                    'experience' =>[
            ['Served at Dubai Police in various locations of law enforcement including the stations and headquarters.'],
            ['Lectured at the Dubai Police Academy.'],
            ['	Served at the Executive Office of HH Sheikh Mohammad Bin Rashid Al Maktoum as the Deputy Director General of the Ruler Court of Dubai Government.'],
            ['	Former member of strategic governmental bodies such as the Executive Council of Dubai, The Judicial Council of Dubai, The Federal Judicial Council, and the Dubai Judicial Institution’s Board.'],
            ['	 VICE Chairman of Dubai International Arbitration Centre (DIAC).'],
            ['	Dr Ahmad is well versed with the legal system in the UAE having worked as the Director General of the Dubai Courts.'],
    ] , 'ID' => '2045',
'services' => [7,21,3,6,13],
'first_name'=>'Dr. Ahmad',
'last_name'=>'Bin Hezeem',
'post_title'
 => 'Dr. Ahmad Bin Hezeem','post_content' => '<p>Dr. Ahmad Bin Hezeem is BSA&rsquo;s Senior Partner, based in our Dubai office. He has over 24 years&rsquo; experience working in legal and judicial governmental institutions in Dubai.</p>
<p>Dr. Ahmad was the former Director General of the Dubai Courts, Dr. Ahmad Bin Hezeem who provides unparalleled knowledge of the region&rsquo;s laws and regulations.</p>
<p>His litigation experience is expansive, covering civil law, criminal law, family and private clients and breach of contracts. For nine years, he served at Dubai Police in various locations of law enforcement, including the stations and headquarters, as well as teaching at the Dubai Police Academy. &nbsp;</p>
<p>In 2005, he moved away from teaching and served at the Executive Office of HH Sheikh Mohammad Bin Rashid Al Maktoum as the Deputy Director General of the Ruler Court of Dubai Government. Dr Ahmad is well versed with the legal
system in the UAE. As the Director General of the Dubai Courts, he engaged in the daily operations of the courts and judicial institutions at local and federal level for over eight years. He is also a former member of
strategic governmental bodies such as the Executive Council of Dubai, The Judicial Council of Dubai, The Federal Judicial Council, and the Dubai Judicial Institution&rsquo;s Board.</p>',
                'post_date' => '2014-01-25 01:00:28','post_name' => 'dr-ahmad-bin-hezeem-2','post_parent' => '0',
                'Image' => 'img/profiles/Dr-Ahmad-Bin-Hezeem.jpg','Title' => 'Senior Partner','Email' => 'ahmad.hezeem@bsabh.com','Excerpt' => 'Dr. Ahmad is the Senior Partner based in our DIFC office in Dubai.','Publisher' => 'Dr Ahmad Bin Hezeem','Telephone' => '+971 4 528 5555','Practice Areas' => '296','Locations' => '78','Office Title' => '315','Categories' => NULL,'Tags' => NULL),


//            array(
//                    'experience' =>[
//    ] , 'ID' => '2091',
//'services' => [],
//'first_name'=>'',
//'last_name'=>'',
//'industries' => [],
//'post_title'
// => 'Moustafa Arfa','post_content' => '<p>Moustafa is a Litigation Associate in our DIFC office in Dubai. Practicing since 2003, he handles litigation cases before the Dubai Courts and Federal Courts, executing both registration and follow-up of cases for clients. He also offers regularly advise on rental disputes, commercial disputes and criminal cases.
//</p><p>
//Prior to joining us in 2015, Moustafa worked in Egypt for nine years where he specialised in criminal law and handled numerous criminal cases. He later moved to the UAE to broaden his experience in litigation.
//</p><p>
//Moustafa holds an LLB from Tanta University and speaks English and Arabic.</p>','post_date' => '2015-05-28 08:26:02','post_name' => 'moustafa-arfa','post_parent' => '0','Image' => 'wp-content/uploads/2015/07/Moustafa-Arfa.jpg','Title' => 'Associate','Email' => 'moustafa.arfa@bsabh.com','Excerpt' => 'Moustafa is an Associate in the Litigation practice, based in our DIFC office in Dubai.','Publisher' => NULL,'Telephone' => '+971 4 568 5555','Practice Areas' => '296','Locations' => '78','Office Title' => '308','Categories' => NULL,'Tags' => NULL),
//

            array(
                    'experience' =>[
    ] , 'ID' => '2552',
'services' => [6,7,13],
'first_name'=>'Haitham',
'last_name'=>'Al Naabi',
'post_title'
 => 'Haitham Al Naabi','post_content' => '<p>Haitham is an Associate in the Litigation Practice in our Muscat office in the Sultanate of Oman. As an Omani-qualified lawyer, he has rights of audience in civil, commercial and administrative courts, where he has represented various clients.
</p><p>
In his day-to-day role, Haitham advises on corporate and commercial disputes, real estate, construction, and complex procedural matters, civil, employment and criminal cases. He further assists in preparation of memorandums, motions and legal research.
</p><p>
Haitham has a Bachelor of Law Degree (LLB) from Sultan Qaboos University, College of Law. Prior to joining BSA, he was an Associate at Mohammed Al Shuaili Advocates and Legal Consultants based in Oman.</p>','post_date' => '2015-09-09 16:49:06','post_name' => 'haitham-al-naabi','post_parent' => '0','Image' => 'wp-content/uploads/2015/09/DSC_2035-Sq-Bw.jpg','Title' => 'Associate','Email' => 'haitham.alnaabi@bsabh.com','Excerpt' => 'Haitham is an Associate in the Litigation practice, based in our Muscat office in the Sultanate of Oman.','Publisher' => NULL,'Telephone' => '+968 2 421 8555','Practice Areas' => '296','Locations' => '83','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


//            array(
//                    'experience' =>[
//    ] , 'ID' => '2888',
//'services' => [4],
//'first_name'=>'Khaled',
//'last_name'=>'El Gohari',
//'industries' => [17],
//'post_title'
// => 'Khaled Elgohari','post_content' => '<p>Khaled is an Associate in the Litigation practice in our DIFC office in Dubai. With over five years\' litigation experience practicing law in Egypt and the UAE, he specialises in employment disputes, commercial disputes, rental disputes and criminal cases.
//</p><p>
//In his day-to-day role, Khaled often represents clients who require advice and legal assistance on family law and civil cases.
//</p><p>
//Prior to joining us in 2015, he worked at one of the leading law firms in Al Mansoura in Egypt, where he handled many criminal cases.
//</p><p>
//He holds an LLB from Al Mansoura University and is bilingual in both Arabic and English.</p>','post_date' => '2015-11-16 17:10:40','post_name' => 'khaled-elgohari','post_parent' => '0',
//                'Image' => 'img/profiles/Khaled-Gohari.jpg','Title' => 'Associate','Email' => 'khaled.elgohari@bsabh.com','Excerpt' => 'Khaled is an Associate in the Litigation practice, based in our DIFC office in Dubai.','Publisher' => NULL,'Telephone' => '+971 4 568 5555','Practice Areas' => '296','Locations' => '78','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '3788',
'services' => [6,7,13],
'first_name'=>'Omar',
'last_name'=>'Al Kharoosi',
'post_title'
 => 'Omar Alkharoosi','post_content' => '<p>Omar is an Associate in the Litigation Practice in our Muscat office in the Sultanate of Oman. A qualified Omani lawyer, he specialises in various types of litigation and dispute resolution including civil, insurance, commercial, criminal, employment and debt recovery.
</p><p>
In his day-to-day role, Omar acts for various clients including individuals and corporate entities including banks and insurance companies.
</p><p>
Omar holds an LLB from Sultan Qaboos University and is currently pursuing his LLM in Criminal Law at Sultan Qaboos University.</p>','post_date' => '2016-05-08 15:42:29','post_name' => 'omar-alkharoosi','post_parent' => '0','Image' => 'wp-content/uploads/2016/05/Omar-Al-Kharoosi-2.jpg','Title' => 'Associate','Email' => 'omar.alkharoosi@bsabh.com','Excerpt' => 'Omar is an Associate in the Litigation practice, based in our Muscat office in the Sultanate of Oman.','Publisher' => NULL,'Telephone' => '+968 2 421 8555','Practice Areas' => '296','Locations' => '83','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '3849',
'services' => [1,2,21,3,20,10,19,14,16],
'first_name'=>'Robert',
'last_name'=>'Mitchley',
'post_title'
 => 'Robert Mitchley','post_content' => '<p>Robert is a Senior Associate with the Corporate, M&A and Real Estate Practices in our DIFC office in Dubai.
</p><p>
He advises clients in relation to real estate, corporate and commercial law. He has extensive experience in drafting commercial contracts and structuring business transactions with a particular focus on joint ventures, sales of shares, commercial agencies and private equity. He also advises on conveyancing of real estate and property development.
</p><p>
He was admitted to practice as an Attorney in the High Court of South Africa in 1998 prior to relocating to Dubai, he practiced in a commercial and real estate law firm in Durban, South Africa.</p>',
                'post_date' => '2016-06-01 17:51:11','post_name' => 'robert-mitchley','post_parent' => '0',
                'Image' => 'img/profiles/Robert-Mitchley.jpg','Title' => 'Senior Associate','Email' => 'robert.mitchley@bsabh.com','Excerpt' => 'Robert is a Senior Associate with the Corporate and M&A and Real Estate practice, based in our DIFC office in Dubai.','Publisher' => 'Robert Mitchley','Telephone' => '+971 4 568 5555','Practice Areas' => '294, 292','Locations' => '78','Office Title' => '309','Categories' => NULL,'Tags' => NULL),


//            array(
//                    'experience' =>[
//    ] , 'ID' => '3959',
//'services' => [],
//'first_name'=>'',
//'last_name'=>'',
//'industries' => [],
//'post_title'
// => 'Akram Albaiti','post_content' => '<p>Akram is the Head of our Abu Dhabi office. An advocate with extensive experience in litigation, his diverse expertise includes advising clients on real estate matters, commercial issues and corporate disputes.
//</p><p>
//Specialising in banking, intellectual property, copyright and patents and maritime, Akram often represents clients in insurance and criminal litigation proceedings as well as providing advice on tenancy disputes, breach of contract and employee rights.
//</p><p>
//Akram has the full rights of audience before the UAE Courts.</p>','post_date' => '2016-08-04 15:28:50','post_name' => 'akram-albaiti','post_parent' => '0','Image' => 'wp-content/uploads/2016/08/Akram-Albaiti-BW-low-res-sq.jpg','Title' => 'Advocate','Email' => 'akram.albaiti@bsabh.com','Excerpt' => 'Akram is an Advocate with our Litigation team based in our office in Abu Dhabi.','Publisher' => NULL,'Telephone' => '+971 2 644 4474','Practice Areas' => '296','Locations' => '77','Office Title' => '318, 320','Categories' => NULL,'Tags' => NULL),

            array(
                    'experience' =>[
    ] , 'ID' => '4448',
'services' => [4,17,13],
'first_name'=>'Mohammed',
'last_name'=>'Alghazali',
'post_title'
 => 'Mohammed Alghazali','post_content' => '<p>Mohammed is an Associate in the Litigation Practice in our Muscat office in the Sultanate of Oman. With expertise including commercial, civil, administrative and employment law, he represents clients in real estate, criminal, family and construction cases.
</p><p>
In his day-to-day role, he often drafts legal documents including memorandums and legal advice both for government and the private sector.</p>
<p>
Mohammed has the right of audience before the local courts in Oman. He holds an LLB from Irbid National University in Jordan. Mohammed is bilingual in Arabic and English</p>

&nbsp;','post_date' => '2017-05-29 14:24:45','post_name' => 'mohammed-alghazali','post_parent' => '0','Image' => 'wp-content/uploads/2017/05/Mohammed-Alghazali.jpg','Title' => 'Associate','Email' => 'mohammed.alghazali@bsabh.com','Excerpt' => 'Mohammed is an Associate with our Litigation practice and is based in Muscat, Oman','Publisher' => 'Mohammed Alghazali','Telephone' => '+968 24218555','Practice Areas' => '306, 296','Locations' => '83','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


//            array(
//                    'experience' =>[
//    ] , 'ID' => '4452',
//'services' => [],
//'first_name'=>'',
//'last_name'=>'',
//'industries' => [],
//'post_title'
// => 'Mundhir Al Rasbi','post_content' => '<p>Mundhir is an Associate in the Litigation Practice in our Muscat office in the Sultanate of Oman. A qualified Omani lawyer, he specialises in litigation and other forms of dispute resolution, with a particular expertise in civil, commercial, employment law and debt recovery.
//</p><p>
//He is a graduate of Bachelor of laws from the University of Irbid, Kingdom of Jordan.</p>','post_date' => '2017-05-29 14:32:54','post_name' => 'mundhir-al-rasbi','post_parent' => '0','Image' => 'wp-content/uploads/2017/05/Mundhir-Al-Rasbi.jpg','Title' => 'Associate','Email' => 'mundhir.alrasbi@bsabh.com','Excerpt' => 'Mundhir is a Associate for the Litigation, Commercial and Civil practices in Muscat, Oman','Publisher' => NULL,'Telephone' => '+968 24218555','Practice Areas' => '304, 296','Locations' => '83','Office Title' => '308','Categories' => NULL,'Tags' => NULL),
//
//
//            array(
//                    'experience' =>[
//    ] , 'ID' => '4568',
//'services' => [5,6],
//'first_name'=>'Ashraf',
//'last_name'=>'Ibrahim',
//'industries' => [],
//'post_title'
// => 'Ashraf Ibrahim','post_content' => '<p>Ashraf is a Senior Associate in the Litigation practice in our Dubai office. With nearly 30 years\' experience practicing law, he joined BSA in 2017 and currently represents governments and high net worth individuals in claim cases, liquidation and real estate matters. He also acts for companies in the banking, commercial and mining sector.
//</p><p>
//Prior to joining BSA, Ashraf worked for twenty years at two of the leading law firms in Egypt, where he has the right of audience before the courts. During his practice in Egypt he represented numerous commercial and government agencies in litigation cases. He was also a Legal Consultant to the Ministry of Housing, Utilities & Urban Communities in Egypt for three years.
//</p><p>
//Seven years ago, Ashraf moved to the GCC to broaden his experience in litigation. With the right of audience in the Omani courts, he specialized in personal law, criminal law and civil law.</p>
//<p>
//Ashraf holds an LLB and LLM from Ain Shams University in Cairo, Egypt.</p>','post_date' => '2017-06-29 16:13:56','post_name' => 'ashraf-ibrahim','post_parent' => '0',
//                'Image' => 'img/profiles/Ashraf-Ibrahim-El-Sayed.jpg','Title' => 'Senior Associate','Email' => 'ashraf.ibrahim@bsabh.com','Excerpt' => 'Ashraf is an Senior Associate in the Litigation practice, based in our Dubai office.','Publisher' => NULL,'Telephone' => '+971 4 568 5555','Practice Areas' => '296','Locations' => '78','Office Title' => '309','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '4617',
'services' => [7,9,6,11,12,13,15],
'first_name'=>'Ahmed',
'last_name'=>'Al Taher',
'post_title'
 => 'Ahmed Al Taher','post_content' => '<p>Ahmed is a Senior Associate in our Oman office, with over twenty years\' experience in corporate/commercial matters, litigious matters and arbitration.
His extensive experience covers company and commercial laws, criminal, insurance, banking and civil, administrative and criminal disputes.</p>
<p>
He has appeared before the primary, appellate and supreme courts in Sudan and Abu Dhabi as well as the appellate and supreme courts in the Sultanate of Oman.</p>
<p>
Ahmed is a graduate of Bachelor of laws from the University of Cairo, Egypt, 1991 and a member of the Sudanese Bar Association.</p>','post_date' => '2018-01-28 14:54:39','post_name' => 'ahmed-al-taher-2','post_parent' => '0','Image' => 'wp-content/uploads/2017/08/Ahmed-Al-Taher.jpeg','Title' => 'Senior Associate','Email' => 'ahmed.altaher@bsabh.com','Excerpt' => 'Ahmed is a Senior Associate for Litigation, Commercial, Civil, Insurance & Banking practices in Muscat, Oman','Publisher' => NULL,'Telephone' => '+968 24218555','Practice Areas' => '75, 304, 302','Locations' => '83','Office Title' => '309','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '4782',
'services' => [9,2,1,3,20,12,16],
'first_name'=>'Barry',
'last_name'=>'Greenberg',
'post_title'
 => 'Barry Greenberg','post_content' => '<p>Barry is an Of Counsel in the Restructuring & Bankruptcy, Corporate, and Insurance and Reinsurance practices in our DIFC office in Dubai.
<p>
On the Restructuring & Bankruptcy side, his experience includes:</p>
<ul>
 	<li>Acting on behalf of a distressed industrial conglomerate, which produces 10% of global polyethylene terephthalate output, to restructure debts involving 20 local and international creditor banks, with a value of approximately US$800 million.</li>
 	<li>Advising an international conglomerate with offices throughout the GCC, as to the best options to facilitate a successful restructuring of their regional entities. Provided advice and support as to the ramifications of potential restructuring, M&A, and insolvency options – including bankruptcy – upon all company operations as well as the exposure of its officers, directors, shareholders and related parties, in view of its debt of over US$2.5 billion and a problematic shareholder structure.</li>
 	<li>Negotiating a settlement on behalf of one of the Abu Dhabi\'s leading Real Estate Investment Trusts in relation to the restructuring of its loans and obtaining facilities with another bank at favorable terms to the borrower, whilst removing all mortgages in their property portfolio, for a value of over US$100 million.</li>
 	<li>Advising lenders in multiple ongoing enforcement proceedings against entities with debts in excess of US$1 billion each.</li>
</ul>
<p>He advises insurance industry clients doing business in the UAE and GCC region on issues including incorporation, regulatory, employment, and claims management. He also represents international corporations in cross-border transactions, due diligence for pending acquisitions and general conduct of business in the UAE. Projects include:
</p><ul>
 	<li>First-in-kind strategic investment by international insurer into UAE-based insurance company.</li>
 	<li>Significant reduction in regulatory sanctions imposed against UAE-based insurers and TPAs.</li>
 	<li>Provision of advice to international companies as to sanctions relating to Iran.</li>
</ul><p>
Prior to joining BSA, Barry gained extensive experience in the US insurance industry as both a litigator and claims counsel on behalf of liability insurers in the competitive New York market.
He has defended insurers and their insured\'s before New York Courts and Arbitration panels, in both third-party actions and coverage disputes. As a previous claims counsel for a high-volume New
York liability insurer, he served as a coverage expert and maintained best practices within their busy Claims department.</p>',
                'post_date' => '2017-11-30 10:44:59','post_name' => 'barry-greenberg','post_parent' => '0',
                'Image' => 'img/profiles/Barry-Greenberg.jpg','Title' => 'Of Counsel','Email' => 'barry.greenberg@bsabh.com','Excerpt' => 'Barry is a Senior Associate in our Insurance and Reinsurance practice based in DIFC, Dubai.','Publisher' => 'Barry Greenberg','Telephone' => '+971 4 568 5555','Practice Areas' => '75, 294, 302','Locations' => '78','Office Title' => '1681','Categories' => NULL,'Tags' => NULL),

//            array(
//                    'experience' =>[
//    ] , 'ID' => '4919',
//'services' => [],
//'first_name'=>'',
//'last_name'=>'',
//'industries' => [],
//'post_title'
// => 'Andrée Hobson','post_content' => 'Andrée practices as Government Relations Counsel from our DIFC offices in Dubai. As a qualified English solicitor, she has over twelve years\' experience, ten of which have been in Dubai, in private practice and in-house governmental roles.
//
//Andrée\'s main area of expertise is governmental and DIFC regulation and compliance.','post_date' => '2018-01-25 17:29:54','post_name' => 'andree-hobson','post_parent' => '0','Image' => 'wp-content/uploads/2018/01/4b-1.jpg','Title' => 'Consultant','Email' => 'andreehobson@bsabh.com','Excerpt' => 'Andrée is a Government Relations Counsel in the DIFC offices in Dubai.','Publisher' => NULL,'Telephone' => '+971 4 567 5555','Practice Areas' => '294','Locations' => '78','Office Title' => '917','Categories' => NULL,'Tags' => NULL),

            array(
                    'experience' =>[
            ['Advising the leading media group in the Middle East and North Africa, MBC Group, in relation to a major advisory and trademark prosecution project globally.  '],
            ['Supervising anti-illicit trade and anti-counterfeiting in the Middle East on behalf of various clients that represent internationally well-known trademarks and/or their licensees, namely in the fields of cosmetics, watches, electronics and auto-parts.'],
            ['	Representation the leading media group in the region to defend more than 40 different media and IP disputes before local committees, regulatory authorities and/or courts. In addition, strategic role in handling digital and offline anti-piracy legal actions.  '],
            ['	Handling patent inquiries and protection on behalf of patent owners and inventors. Inquiries included advice on vaccination patentability and/or freedom to operate for testing kits during the global pandemic.'],
            ['	Representing the well-known WWE in IP various matters in the region, including prosecution and enforcement of IP rights. '],
            ['	Assisting one of the top 6 global tax advisory firms with HQ in the USA in complex trademark, trade name and domain name disputes as well as regional trademark portfolio management. '],
            ['	Representing a French brand in the field of women’s clothing and lingerie to terminate existing commercial agent and franchisee in the UAE. '],
            ['	Assisting well known French cosmetic clinics in trademark and IP services, including prosecution, clearances, advisory, licensing and other IP commercial transactions. '],
    ] , 'ID' => '4927',
'services' => [5,6,8,3,15,18],
'first_name'=>'Munir A.',
'last_name'=>'Suboh',
'post_title'
 => 'Munir A. Suboh','post_content' => '<p>Munir joined BSA in 2018 as a Partner and Head of the Intellectual Property Department, based in our Dubai office. He began his practice in the Middle East since 2006 and has worked on numerous scopes of Intellectual proeprty, media, cyber and technology matters. He also worked on different corporate and commercial projects, such as transactions and contracts, due diligence, advisory on commercial context, competition and employment matters. His main speciality is in the field of IP and media including trademarks, copyrights, patents, trade secrets, domain names, media advisory, regulatory compliance and representing in contentious matters. He also worked on commercialization of IP and handled complex disputes of IP and media rights.</p>
<p>Munir also works on major alternative dispute resolution cases that involve negotiations and settlement mandates, as well as managing litigations locally, regionally and internationally. For cases in the MENA region, Munir collaborates with local litigation practice groups and litigators to supervise such disputes.</p>
<p>Munir represents a range of clients from various industry sectors, including Media, Entertainment, FMCG, Tobacco, industrial, pharmaceuticals, Industrial, Automotive, cosmetics and many more. His principal set of expertise encompasses negotiations, settlement mandates and litigation. He has successfully resolved critical commercial disputes related to media, technology and IP deals. He has extensive experience in regulatory works and has experience in consulting clients on consumer protection, antitrust and competition law.</p>
<p>Munir works with many multinational clients and is ranked by leading legal directories, such as Chambers Global, Legal 500, The Best Lawyers, World Trademarks Review (WTR) for many years. Munir is praised for being a &ldquo;very commercial and proactive lawyer&rdquo; who maintains a high level of responsiveness, &ldquo;He always assesses legal risks and simplifies potential legal impacts in a simple and clear manner for decision making&rdquo;.</p>
<p>Munir was named by Asia Legal Business (Thomson Reuters) as one of the Top 50 Lawyers in the Middle East in 2021</p>
<p>Munir is a bilingual lawyer (i.e. English and Arabic) and holds an LLB from the University of Jordan and an LL.M in Intellectual Property & Information Technology Laws from the University of San Francisco.</p>',
                'post_date' => '2018-01-24 12:31:36',
                'post_name' => 'munir-abdallah-suboh',
                'post_parent' => '0',
                'Image' => 'img/profiles/Munir-Suboh.jpg',
                'Title' => 'Partner',
                'Email' => 'munir.suboh@bsabh.com',
                'Excerpt' => 'Munir is a Partner and Head of the Intellectual Property based in our Dubai office.',
                'Publisher' => 'Munir Suboh',
                'Telephone' => '+971 4 528 5555',
                'Practice Areas' => '294, 676',
                'Locations' => '78',
                'Office Title' => '310, 920',
                'Categories' => NULL,
                'Tags' => NULL),

            array(
                    'experience' =>[
    ] , 'ID' => '5603',
'services' => [5,8,18],
'first_name'=>'Amal',
'last_name'=>'Atieh',
'post_title'
 => 'Amal Atieh','post_content' => '<p>Amal is a Senior Associate in our Intellectual Property practice.
</p><p>
Before joining BSA, she worked at one of the leading Intellectual Property firms in Jordan and UAE and she gained more than ten years of extensive experience in the field of Intellectual Property laws in the Middle East and North Africa with an emphasis on trademarks and brand protection.
</p><p>
She assisted international and domestic Brand Owners in devising and implementing tailored strategies for trademark protection in the MENA region, with regard to prosecution, enforcement and litigation.
</p><p>
She has extensive experience in comprehensive IP Legal Consultancy services such as IP Strategy & Enforcement, IP Portfolio Management, anti-counterfeiting strategic planning, infringement and unfair competition litigation.
</p><p>
She managed and handled opposition actions, cancellation actions, civil and criminal litigation, administrative enforcement, border seizures and customs actions; online enforcement and domain names disputes.
</p><p>
She is experienced in dealing with cases involving different types of IP Rights, including trademarks, industrial designs and models, copyrights, domain names and trade secrets. She is also experienced in drafting and negotiating various types of IP, Commercial and Corporate Agreements as well as advising on IP aspects of corporate transactions.
</p><p>
Amal holds an LL. B as well as LL.M in Intellectual Property Laws from the University of Jordan.</p>',
                'post_date' => '2018-07-03 15:52:38',
                'post_name' => 'amal-atieh',
                'post_parent' => '0',
                'Image' => 'wp-content/uploads/2018/07/Amal-Atieh_BW_sq-768x768.jpg','Title' => 'Senior Associate','Email' => 'amal.atieh@bsabh.com','Excerpt' => 'Amal is a Senior Associate in the IP department in our Dubai office, UAE','Publisher' => NULL,'Telephone' => '+971 4 568 5555','Practice Areas' => '676','Locations' => '78','Office Title' => '309','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[

    ] , 'ID' => '55351',
'services' => [9,2,1,3,10,20,12,16],
'first_name'=>'Arsalan',
'last_name'=>'Tariq',
'post_title'
 => 'Arsalan Tariq','post_content' => '
<p>Arsalan is commercially-minded and western-educated lawyer with broad experience advising across corporate/commercial, banking and finance, real estate, hospitality, dispute resolution and oil and gas practice areas and is currently working as a Partner with our Oman Office (BSA Al Rashdi &amp; Al Barwani Advocates and Legal Consultants), after spending two years in our DIFC Office. Arsalan has previously worked in-house with a large oil and gas company and for top-tier law firms in Pakistan and Oman. Arsalan acts external legal advisor to international companies and local/government entities and is known to offer pragmatic and solution-focused advice.</p>
<p>Arsalan regularly advises clients in Oman, UAE and Saudi Arabia. He is a barrister-at-law and has been called to the bar of England and Wales at Lincolns Inn, UK. He also holds an LLB from the University of London, UK and has completed his Bar Vocation Training Course from City University in London.</p>','post_date' => '2018-12-10 14:59:27',
                'post_name' => 'arsalan-tariq','post_parent' => '0',
                'Image' => 'wp-content/uploads/2018/12/2B.jpg','Title' => 'Partner','Email' => 'arsalan.tariq@bsabh.com',
                'Excerpt' => 'Arsalan is a Partner based in the Oman office (BSA Al Rashdi & Al Barwani Advocates & Legal Consultants)','Publisher' => NULL,
                'Telephone' => '+968 2 421 8555','Practice Areas' => '75, 294','Locations' => '83','Office Title' => '310','Categories' => NULL,'Tags' => NULL),

//            array(
//                    'ID' => '55723',
//'services' => [],
//'first_name'=>'',
//'last_name'=>'',
//'industries' => [],
//'post_title'
// => 'Noor Ambusaidi','post_content' => '<p>Noor is an Associate in the Corporate Practice of our Muscat office in the Sultanate of Oman. She has extensive experience with government and private sector clients, to whom she provides pragmatic and innovative legal advice.
//<p>
//Prior to joining BSA, she primarily focused on litigation, successfully litigating a number of cases for clients in commercial, employment, family, criminal and civil cases. Her main area of expertise lies in corporate law and she regularly assists with company registration and establishment in Oman.
//<p>
//Noor holds an LLB from Sultan Qaboos University, College of Law. She has also completed a Diploma in Professional Skills for International Lawyers at Nottingham Trent University in the United Kingdom.</p>','post_date' => '2018-12-23 14:56:44','post_name' => 'noor-ambusaidi__trashed','post_parent' => '0','Image' => 'wp-content/uploads/2018/12/Noor-Ambusaidi.jpg','Title' => 'Associate','Email' => 'noor.ambusaidi@bsabh.com','Excerpt' => 'Noor is an Associate in the corporate Practice of our Muscat office in the Sultanate of Oman','Publisher' => 'Noor Ambusaidi','Telephone' => '+968 2 421 8555','Practice Areas' => '294','Locations' => '83','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


            array('ID' => '55732',
'services' => [5,6,3,11,15,18],
'first_name'=>'Simon',
'last_name'=>'Isgar',
'post_title'
 => 'Simon Isgar','post_content' => '<p>Simon has 20 years of experience representing international insurers and intermediaries throughout insurance markets globally. &nbsp;Previously the Head of Corporate & Regulatory Insurance MENA at Kennedys, Simon joined BSA as Partner and Head of Insurance/Reinsurance in 2018, where he undertakes both contentious and non-contentious insurance/reinsurance work.</p>
<p>His specialist niche areas of practice involve international private medical insurance and international health regulations. Simon also leads the firm&rsquo;s Middle East Cyber Response Team working with Munich Re and several cedent insurers. He is often instructed by leading law firms in the United Kingdom and Europe as regional counsel and recently was involved with the negotiations and settlement of an 85million USD claim.</p>
<p>Simon is a barrister, called to the Bar (England & Wales) in 1998. His professional memberships include DIFC Insurance Association, British Insurance Lawyers Association (BILA) and the International Bar Association.&nbsp; </p>',
                'post_date' => '2018-12-23 15:47:00','post_name' => 'simon-isgar','post_parent' => '0',
                'Image' => 'img/profiles/Simon-Isgar.jpg','Title' => 'Partner','Email' => 'simon.isgar@bsabh.com','Excerpt' => 'Simon is a Partner and Head of Insurance Practice in Dubai office','Publisher' => 'Simon Isgar','Telephone' => '+971 4 528 5555','Practice Areas' => '294, 302','Locations' => '78','Office Title' => '310','Categories' => NULL,'Tags' => NULL),


//            array(
//                    'experience' =>[
//    ] , 'ID' => '57681',
//'services' => [],
//'first_name'=>'',
//'last_name'=>'',
//'industries' => [],
//'post_title'
// => 'Anand Singh','post_content' => '<p>Anand is a Senior Associate in the Insurance & Reinsurance Practice based in our DIFC office in Dubai. With over 8 years of experience as a corporate insurance and regulatory lawyer having practiced in India and across the Middle East.
//</p><p>
//Anand has extensive experience in advising insurance companies, reinsurers, insurance brokers and agents across multiple jurisdictions on incorporation, distribution strategy, regulatory, compliance and other advisory matters. Having worked across jurisdictions, he is able to provide cross-jurisdictional advice to financial services clients who have a global footprint.
//</p><p>
//Prior to joining BSA, Anand worked for Kennedys, as an Associate in the Dubai office for 4 years, prior to which he was an Associate in Tuli & Co in India, an associate of Kennedys. His experience includes advising international and regional clients on commercial arrangements, shareholding structure, nominee arrangements, employment agreements and he has been instrumental in growth and expansion of a number of Fintech and Insuretech startups in the region.
//</p><p>
//Anand is a regular contributor of articles and commentaries for local insurance magazines and online publications on current topics in the GCC insurance market. He was recently recognised by the India Business Law Journal as one of the go-to lawyers for India related transactions.
//</p><p>
//Anand holds an LL.B (Hons) from ICFAI University, Dehradun, India and was admitted to the Bar in 2010.</p>','post_date' => '2019-02-18 09:05:08','post_name' => 'anand-singh','post_parent' => '0','Image' => 'wp-content/uploads/2019/02/P-18.jpg','Title' => 'Senior Associate','Email' => 'anand.singh@bsabh.com','Excerpt' => 'Anand is a Senior Associate with Insurance & Reinsurance Practice, based in our DIFC office in Dubai.','Publisher' => 'Anand Singh','Telephone' => '+971 4 568 5555','Practice Areas' => '302','Locations' => '78','Office Title' => '309','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '57696',
'services' => [],
'first_name'=>'Alaaeldin',
'last_name'=>'Ghoneim',
'post_title'
 => 'Alaaeldin Ghoneim','post_content' => '<p>Alaaeldin is an Associate in the Litigation Practice based in our DIFC office in Dubai. He represents clients in criminal litigation proceedings, civil disputes, rental disputes and employment matters.
</p><p>
Before joining BSA, Alaaeldin worked for 6 years\' as a banking and financial lawyer, advising financial institutions, investments companies and banks on banking and financial matters.
</p><p>
Alaaeldin holds an LL.B and an LL.M from the Mansoura University in Egypt.</p>','post_date' => '2019-02-18 09:31:42','post_name' => 'alaaeldin-ghoneim','post_parent' => '0',
                'Image' => 'img/profiles/Alaa-Ghoneim.jpg','Title' => 'Associate','Email' => 'alaaeldin.ghoneim@bsabh.com','Excerpt' => 'Alaaeldin is an Associate with the Litigation practice, based in our DIFC office in Dubai.','Publisher' => 'Alaaeldin Ghoneim','Telephone' => '+971 4 568 5555','Practice Areas' => '296','Locations' => '78','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '58182',
'services' => [7,9,6,13,10,12,19,14],
'first_name'=>'Abdalla',
'last_name'=>'Al Suwaidi',
'post_title'
 => 'Abdalla Alsuwaidi','post_content' => '<p>Abdalla is an advocate based in Dubai. Abdalla has built a solid experience in different areas of litigation in the UAE and has the right of audience before the local courts. Abdalla handles IP disputes along with the IP team.
</p><p>
Prior to joining BSA, Abdalla was an inhouse counsel for Al Futtaim Group Companies for 4 years. He specialised in the field of commercial agency complaints with a focus on infringement of commercial agencies. Abdalla also handles trademark litigation, intellectual property rights and labour disputes. He is well versed and often deals with criminal cases.
</p><p>
Abdalla is an Emirati qualified lawyer, he holds an LLB from the University of Sharjah with a distinction. He will be one of up and coming advocates in the UAE legal market.</p>',
                'post_date' => '2019-03-28 16:40:21','post_name' => 'abdalla-alsuwaidi','post_parent' => '0',
                'Image' => 'img/profiles/Abdallah-Al-Suweidi.jpg','Title' => 'Advocate','Email' => 'abdalla.alsuwaidi@bsabh.com',
                'Excerpt' => 'Abdalla is an Advocate in our Litigation Practice based in our DIFC office','Publisher' => 'Abdalla Alsuwaidi',
                'Telephone' => '+971 4 568 5555','Practice Areas' => '296','Locations' => '78',
                'Office Title' => '308, 1105','Categories' => NULL,'Tags' => NULL),

            array(
                    'experience' =>[
    ] , 'ID' => '58465',
'services' => [1,2,3,4,20,11,19,15,16],
'first_name'=>'Nour',
'last_name'=>'Gemayel',
'post_title'
 => 'Nour Gemayel','post_content' => '<p>Nour is an Associate in the Insurance & Reinsurance Practice based in our DIFC office in Dubai. Fluent in English, Arabic and French, she represents insurance and reinsurance companies, loss adjusters and third-party administrators on non-contentious insurance matters.
</p><p>
During her time with us, Nour has advised on both local and international transactions relating to corporate structuring and has assisted in negotiating out-of-court settlements.
</p><p>
She also advises clients on drafting all types of commercial agreements and arrangements and has experience dealing with the financial and insurance regulators in the UAE, including the Insurance Authority and the DFSA.
</p><p>
Prior to joining BSA, Nour worked as an In-House Legal Counsel in an engineering consultancy firm.</p>','post_date' => '2019-05-13 12:47:02','post_name' => 'nour-gemayel','post_parent' => '0',
                'Image' => 'img/profiles/Nour-Gemayel.jpg','Title' => 'Associate','Email' => 'nour.gemayel@bsabh.com','Excerpt' => 'Nour is an Associate with Insurance & Reinsurance Practice, based in our DIFC office in Dubai.','Publisher' => 'Nour Gemayel','Telephone' => '+971 4 368 5555','Practice Areas' => '302','Locations' => '78','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '58949',
'services' => [4,17],
'first_name'=>'Swati',
'last_name'=>'Soni',
'post_title'
 => 'Swati Soni','post_content' => '<p>Swati is an Associate in the Litigation Practice in our DIFC office in Dubai. Fluent in English, Hindi and Panjabi, her expertise includes white collar crimes, bank frauds, bounced cheques, prevention of money laundering, criminal defamation, matrimonial disputes (both criminal and civil) property disputes and insolvency.
<p>
Prior to joining BSA, Swati worked with a private litigation practice in New Delhi for three years. There she focused on the Coal Block Allocation case relating to public corruption and the alleged fraudulent allotment of coal blocks to private companies.
</p><p>
Swati holds an LL.B. (Hons.) from GGSIP University in New Delhi, India.</p>','post_date' => '2019-08-05 16:35:32','post_name' => 'swati-soni','post_parent' => '0','Image' => 'wp-content/uploads/2019/08/P-2-2.jpg','Title' => 'Associate','Email' => 'swati.soni@bsabh.com','Excerpt' => 'Swati is an Associate in the Litigation Practice in our DIFC office','Publisher' => 'Swati Soni','Telephone' => '+971 4 568 5555','Practice Areas' => '296','Locations' => '78','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


//            array(
//                    'experience' =>[
//    ] , 'ID' => '58959',
//'services' => [],
//'first_name'=>'',
//'last_name'=>'',
//'industries' => [],
//'post_title'
// => 'Ralph Hejaily','post_content' => '<p>Ralph is a Senior Associate in the corporate practice based in the Sultanate of Oman (Muscat).</p>
//<p>
//Fluent in English, Arabic and French, he has over 20 years\' experience in the Middle East and GCC region, including a decade in Oman.</p>
//<p>
//Prior to joining BSA, Ralph practiced for eight years as Head of the Legal & Recovery Department at an international bank. Before that he was Senior Associate-Manager of the Corporate team at an international law firm in Oman.
//</p><p>
//Before moving to the Gulf region, Ralph was a lawyer at prestigious law firms in Lebanon, providing commercial and international law advice including contractual negotiations and the set-up of companies.</p>
//<p>
//Ralph holds an L.L.B from La Sagesse University in Beirut and is an accredited Arbitrator with both the Ministry of Justice in Oman and the GCC Commercial Arbitration Centre.</p>
//<p>
//Ralph has been recognized by Legal500 as a Banking Inhouse Lawyer in 2017 & 2019.</p>
//
//&nbsp;','post_date' => '2019-08-05 16:49:35','post_name' => 'ralph-hejaily','post_parent' => '0','Image' => 'wp-content/uploads/2019/08/01-Sq-Co-Bw.jpg','Title' => 'Senior Associate','Email' => 'ralph.hejaily@bsabh.com','Excerpt' => 'Ralph is a Senior Associate in the Muscat office.','Publisher' => 'Ralph Hejaily','Telephone' => '+968 2 421 8555','Practice Areas' => '304, 294','Locations' => '83','Office Title' => '309','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '58969',
'services' => [6,7,13],
'first_name'=>'Khulood',
'last_name'=>'Al Wahaibi',
'post_title'
 => 'Khulood Al Wahaibi','post_content' => '<p>Khulood is an Associate in the Litigation practice in our Muscat office in the Sultanate of Oman. Fluent in Arabic and English, she is qualified to draft legal documents and provide legal advice to both the public and private sectors.
Prior to joining to BSA, Khulood worked for a local law firm in Oman for two and a half years. </p>
<p>During that time she litigated in several commercial cases, bringing successful outcomes for her clients via determined negotiation and swift settlement.
Khulood holds an LLB from the College of Law at Sultan Qaboos University.</p>','post_date' => '2019-08-05 17:12:57','post_name' => 'khulood-al-wahaibi','post_parent' => '0','Image' => 'wp-content/uploads/2019/08/DSC_9470-Sq-BW.jpg','Title' => 'Associate','Email' => 'khulood.alwahaibi@bsabh.com','Excerpt' => 'Khulood is an Associate in the Litigation Practice in our Muscat office.','Publisher' => 'Khulood Al Wahaibi','Telephone' => '+968 2 421 8555','Practice Areas' => '296','Locations' => '83','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


//            array(
//                    'experience' =>[
//    ] , 'ID' => '59528',
//'services' => [6,7],
//'first_name'=>'Elhag',
//'last_name'=>'Mohamed Ali',
//'industries' => [13],
//'post_title'
// => 'Elhag Mohamed Ali','post_content' => '<p>Elhag is an Associate in the Litigation team in our Abu Dhabi office. A member of the Sudan Bar Association since 1988, he has extensive knowledge of the legal systems in both the UAE and Sudan.
//</p><p>
//Experienced in all aspects of litigation – from drafting memorandums and contracts to overseeing arbitration sessions – Elhag specializes in civil, criminal, commercial, employment, insurance, rental, real estate, and Sharia law cases.
//</p><p>
//Elhag has been practicing in the UAE since 2003, with a license issued from the Ministry of Justice as a pleading lawyer. He has eight years\' experience attending hearings at the courts in the UAE.
//</p><p>
//He also has the right of audience before the superior and lower courts in Sudan, where he is licensed by the Chief of Justice in Sudan to act as a Commissioner for Oaths, drafting contracts and agreements, authenticating and attesting the execution of documents and signatures and administering oaths.
//</p><p>
//A native Arabic speaker who is also confident in English, Elhag has a Batchelor in Law degree from Al Khartoum University.</p>','post_date' => '2019-10-27 16:33:02','post_name' => 'elhag-mohamed-ali','post_parent' => '0','Image' => 'wp-content/uploads/2019/10/3b.jpg','Title' => 'Associate','Email' => 'elhag.mohamed@bsabh.com','Excerpt' => 'Elhag is an Associate with our Litigation practice, based in our Abu Dhabi office','Publisher' => 'Elhag Mohamed','Telephone' => '+971 2 644 4474','Practice Areas' => '296','Locations' => '77','Office Title' => '308','Categories' => NULL,'Tags' => NULL),
//

            array(
                    'experience' =>[
    ] , 'ID' => '60338',
'services' => [1,2,3,4,5,10,20,17,16,18],
'first_name'=>'Jean',
'last_name'=>'Abboud',
'post_title'
 => 'Jean Abboud','post_content' => '<p>Jean Abboud is the Head of Corporate for BSA&rsquo;s KSA practice. He has twelve years&rsquo; experience working in the Middle East, with ten years spent in Saudi Arabia.&nbsp; Jean specialises in corporate and commercial law (including foreign investment), telecom and technology-related laws and securities and insurance, as well as capital markets. His expertise covers joint ventures, mergers and acquisitions, public and private offerings, insurance-related transactions, and corporate restructurings.</p>
<p>Jean has in-depth knowledge of local laws and practices. He has advised a large number of multinationals, US, European, and East Asian clients in Saudi Arabia and the Middle East on a broad range of matters, including in relation to project development, commercial contracts, company setups, franchising, as well as matters of general company law and corporate governance and in major litigation.</p>
<p>Jean has also worked with a number of funds, private equity and investment firms, venture capital companies, and start-ups in providing services related to acquisition, financing, and strategic transactions.</p>
<p>Jean holds an LL.B. from La Sagesse University (Beirut, Lebanon) and a BA in Computer Science from the Lebanese American University (Beirut, Lebanon) and is a member of the Beirut Bar Association and is fluent in Arabic, English, and French.</p>','post_date' => '2020-01-07 11:43:52','post_name' => 'jean-abboud','post_parent' => '0',
                'Image' => 'img/profiles/Jean-Abboud.jpg','Title' => 'Head of Corporate - KSA Office','Email' => 'jean.abboud@bsabh.com','Excerpt' => 'Jean is Head of Corporate - KSA Office, based in Riyadh, KSA.','Publisher' => 'Jean Abboud','Telephone' => '+966 57 186 0000 (KSA)
+971 58 547 9807 (UAE)
','Practice Areas' => '294, 948','Locations' => '84','Office Title' => '309,1103','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '60584',
'services' => [8,18],
'first_name'=>'Felicity',
'last_name'=>'Hammond',
'post_title'
 => 'Felicity Hammond','post_content' => '<p>Felicity joined BSA Ahmad Bin Hezeem & Associates LLP as an Associate earlier this year in the Intellectual Property Department. Felicity has been working in the Middle East since 2016 and has expertise in both trademark and patent prosecution, in addition to a number of high profile contentious IP matters.
</p><p>
Felicity has worked with numerous high profile, well-known regional clients and has been part of creating their portfolio and brand enforcement strategies. Felicity has also worked with high profile international clients who have sought to enforce their rights in the Middle East. Felicity has represented clients from a range of backgrounds including, Aviation, Telecoms, Luxury Fashion brands, and Hospitality.
</p><p>
Ms Hammond obtained her LLB degree in Law from the University of Liverpool.</p>','post_date' => '2020-02-09 15:48:49','post_name' => 'felicity-hammond','post_parent' => '0',
                'Image' => 'img/profiles/Felicity-Hammond-.jpg','Title' => 'Associate','Email' => 'felicity.hammond@bsabh.com','Excerpt' => 'Felicity joined BSA Ahmad Bin Hezeem & Associates LLP as an Associate earlier this year in the Intellectual Property Department.','Publisher' => 'Felicity Hammond','Telephone' => '+971 4 528 5555','Practice Areas' => '676','Locations' => '78','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '60590',
'services' => [9,1,2,3,20,12,11,15,3,16],
'first_name'=>'Nadia',
'last_name'=>'El Tannir',
'post_title'
 => 'Nadia El Tannir','post_content' => '<p>Nadia is an Associate within the Insurance Practice, based in Dubai.
</p><p>
Before joining BSA, Nadia has practiced law in Lebanon with a focus on corporate law. She has provided a wide range of clients with legal assistance on several matters and queries relating to commercial, insurance, employment, consumer protection, and taxation laws. She has also advised on corporate transactions and restructuring strategies, as well as on various contractual matters.
</p><p>
Nadia was admitted to the Beirut Bar Association in February 2016 and holds an LL.B in Private Law as well as an LL.M in Business Law, both from Saint Joseph University of Beirut - Lebanon.
</p><p>
She is fluent in Arabic, English, and French.</p>','post_date' => '2020-02-09 16:03:25','post_name' => 'nadia-el-tannir','post_parent' => '0',
                'Image' => 'img/profiles/Nadia-El-Tannir.jpg','Title' => 'Associate','Email' => 'nadia.eltannir@bsabh.com','Excerpt' => 'Nadia is an Associate within the Insurance Practice, based in Dubai.','Publisher' => 'Nadia El Tannir','Telephone' => '+971 4 528 5555','Practice Areas' => '302','Locations' => '78','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '60811',
'services' => [6,7,10,13,17],
'first_name'=>'Jonathan',
'last_name'=>'Brown',
'post_title'
 => 'Jonathan Brown','post_content' => '<p>Jonathan Brown is Of Counsel within the Arbitration practice based in Dubai. Jonathan qualified as a Solicitor in 1991 and worked in the City of London before coming to Dubai in 1998 to head up the Contentious Business Practice of Clifford Chance.
<p>
Jonathan\'s career has been split between in-house and private practice. He served for five years as the Director of English Law at CMA-CGM (the world\'s third largest maritime and logistics group) in Marseille, France.
More recently, Jonathan returned to the UAE, as a partner with Hadef & Partners and subsequently Charles Russell Speechlys. He specialises in international arbitration with a focus on international trade, maritime and transportation.',
                'post_date' => '2020-03-23 12:19:18','post_name' => 'jonathan-brown','post_parent' => '0',
                'Image' => 'img/profiles/Jonathan-Brown-.jpg','Title' => 'Of Counsel','Email' => 'jonathan.brown@bsabh.com','Excerpt' => 'Jonathan Brown is Of Counsel within the Arbitration practice based in Dubai','Publisher' => NULL,'Telephone' => '+971 4 528 5555','Practice Areas' => '298, 299','Locations' => '78','Office Title' => '1681','Categories' => NULL,'Tags' => NULL),


//            array(
//                    'experience' =>[
//    ] , 'ID' => '61807',
//'services' => [],
//'first_name'=>'',
//'last_name'=>'',
//'industries' => [],
//'post_title'
// => 'Haytham Alieh','post_content' => '<p><span >Haytham is an experienced lawyer and legal counselor, with over 26 years\' experience in the Middle East. He has extensive wide-ranging experience specializing in litigation across a variety of sectors. Haytham is a registered Lawyer and member of the Beirut Bar Association in Lebanon and has been practising since 1996.</span></p>
//</p><p>Haytham has in-depth experience in family law, banking, construction, commercial and criminal law in a contentious and advisory capacity and is an expert in drafting and reviewing complex commercial documentation. Haytham has co-authored six legal books regarding business, commercial and criminal laws and their interpretation and implantation in the Middle East, he has also published several articles on new laws that were introduced in the UAE. He is also a visiting lecturer with the Dubai Legal Affairs Department and several law schools in the UAE. He also specializes in the enforcement of arbitral awards and foreign judgments within the UAE courts.</span></p>
//</p><p>Prior to joining BSA, Haytham was a Partner and Head of the Local Litigation Team at DWF Middle East and previously a Counsel and Sector Leader of the Litigation Department in Baker & McKenzie Habib Al Mulla\'s Abu Dhabi office.</span></p>','post_date' => '2018-12-09 15:53:31','post_name' => 'haytham-alieh','post_parent' => '0','Image' => 'wp-content/uploads/2020/06/Haytham-Alieh.jpg','Title' => 'Partner','Email' => 'haytham.alieh@bsabh.com','Excerpt' => 'Haytham is a Partner within our litigation practice based in Dubai, UAE.','Publisher' => NULL,'Telephone' => '+971 4 528 5555','Practice Areas' => '296','Locations' => '78','Office Title' => '310','Categories' => NULL,'Tags' => NULL),

            array(
                    'experience' =>[
    ] , 'ID' => '62264',
'services' => [4,5,6,10,17,18],
'first_name'=>'Ahmed',
'last_name'=>'El Sayed',
'post_title'
 => 'Ahmed Elsayed','post_content' => '<p><span lang="en-GB">Ahmed is an Associate within the Litigation practice based in Dubai. He advises clients in relation to civil, and commercial law, with a main area of expertise in construction and infrastructure disputes. He has extensive experience in drafting all kind of submissions at the three tiers of litigation.</span>
</p><p>
<span lang="en-GB">Ahmed also advises clients on establishing contracts, and the resolution of construction, civil, and commercial complex issues.</span>',
                'post_date' => '2020-10-12 15:21:58','post_name' => 'ahmed-elsayed','post_parent' => '0',
                'Image' => 'img/profiles/Ahmed-El-Sayed.jpg','Title' => 'Associate','Email' => 'ahmed.elsayed@bsabh.com','Excerpt' => 'Ahmed is an Associate within the Litigation practice based in Dubai. He advises clients in relation to civil, and commercial law, with a main area of expertise in construction and infrastructure disputes.','Publisher' => 'Ahmed Elsayed','Telephone' => '+971 4 528 5555','Practice Areas' => '296','Locations' => '78','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '62301',
'services' => [2,1,21,3,4,16,17,20],
'first_name'=>'Bradley',
'last_name'=>'Moran',
'post_title'
 => 'Bradley Moran','post_content' => '<p>Bradley is an Associate in BSA\'s Corporate and M&A Practice in their DIFC office in Dubai.
</p><p>
Recently returned to the region from the United Kingdom where he specialized in residential and commercial property matters Bradley is now developing his experience and UAE knowledge base across numerous sectors including M&A transactions, joint ventures, corporate restructurings, commercial agencies, due diligence, commercial agreements, and employment matters.
</p><p>
He is an admitted Solicitor of the Courts of England and Wales, holds a LLB with Honours in English law from University of Law, London as well as a BA with Honours in Politics from Loughborough University, England.</p>

','post_date' => '2020-11-25 14:02:17','post_name' => 'bradley-moran','post_parent' => '0',
                'Image' => 'img/profiles/Bradley-Moran.jpg','Title' => 'Associate','Email' => 'bradley.moran@bsabh.com','Excerpt' => 'Bradley is an Associate in BSA\'s Corporate and M&A Practice in their DIFC office in Dubai.','Publisher' => 'Bradley Moran','Telephone' => '+971 4 568 5555','Practice Areas' => '294','Locations' => '78','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '62386',
'services' => [9,1,2,3,10,20,12,19,14,16],
'first_name'=>'Musab',
'last_name'=>'Iftikhar',
'post_title'
 => 'Musab Iftikhar','post_content' => '<p><span lang="en-GB">Musab is an Associate in the banking and finance team of the firm and based in our DIFC office in Dubai. He has over six years of experience in Pakistan, Oman and Qatar.</span>
</p><p>
<span lang="en-GB">Musab\'s experience includes working on big-ticket domestic and cross-border transactions with financial institutions and government and semi-government entities. His notable transactional work includes structuring, negotiating and advising on asset, structured and project financing (under both conventional and Islamic modes of financing), handling complex debt restructuring, and working on acquisition financing, involving multiple stakeholders.</span><span lang="en-GB"> </span>
</p><p>
<span lang="en-GB">Before joining BSA, Musab worked in the Qatar office of the largest Middle Eastern law firm where he primarily advised local and foreign clients on financial regulatory matters, debt restructuring and lending transactions. In Oman, he worked at one of the oldest law firms of the country and advised on a range of issues as diverse as banking, corporate and M&A, and real estate. He has also worked at a top-tier law firm in Pakistan where he was involved in transactional and advisory work in relation to banking, corporate and M&A, JVs, capital markets, and investments and divestments. He has also been seconded in past to the Qatar office of a global bank.</span>
</p><p>
<span lang="en-GB">Musab holds a BA (Hons) in Law from the London School of Economics and LLM in Financial Regulation and Compliance from BPP University Law School, London.</span>',
                'post_date' => '2021-02-01 16:13:39','post_name' => 'musab-iftikhar','post_parent' => '0',
                'Image' => 'img/profiles/Musab-Iftikar.jpg','Title' => 'Associate','Email' => 'musab.iftikhar@bsabh.com','Excerpt' => 'Musab is an Associate in the banking and finance team of the firm and based in our DIFC office in Dubai. He has over six years of experience in Pakistan, Oman and Qatar.','Publisher' => 'Musab Iftikhar','Telephone' => '+971 4 5285555','Practice Areas' => '75, 294','Locations' => '78','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


//            array(
//                    'experience' =>[
//    ] , 'ID' => '62531',
//'services' => [],
//'first_name'=>'',
//'last_name'=>'',
//'industries' => [],
//'post_title'
// => 'Roger Bowden','post_content' => 'Experienced senior litigator with 5 years\' DIFC experience and a demonstrated record of achievement. Previous background as a barrister in New Zealand with a broad practice area developed through 30 years of experience and hundreds of cases. Expertise in contract law and litigation strategy. Experienced and accomplished litigator with both a deep and wide knowledge of the law. Ability to define and execute complex litigation strategy.','post_date' => '2017-05-29 18:08:32','post_name' => 'roger-bowden','post_parent' => '0','Image' => 'wp-content/uploads/2021/03/image006.jpg','Title' => 'Of Counsel','Email' => 'roger.bowden@bsabh.com','Excerpt' => 'Experienced senior litigator with 5 years\' DIFC experience and a demonstrated record of achievement.','Publisher' => NULL,'Telephone' => '+971 4 528 5555','Practice Areas' => '294','Locations' => '78','Office Title' => '1681','Categories' => NULL,'Tags' => NULL),
//

            array(
                    'experience' =>[
    ] , 'ID' => '62535',
'services' => [1,2,3,9,20,12,16],
'first_name'=>'Reem',
'last_name'=>'Al Habsi',
'post_title'
 => 'Reem Al Habsi','post_content' => '<p><span lang="en-US">Reem is an Associate within the corporate department based in Muscat. Adept at drafting and reviewing polices and contracts. A focused lawyer who has the ability of analyzing the problems and provide the needed advice orally and in writing. Reem has the ability to hardworking and advising the clients to establish their commercial contracts and tackle the issues by the focused review that she can provide. She maintains a well writing, negotiation and research skills that enable her in fulfilling all tasks and tackle all issues to help the clients.</span>
</p><p>
<span lang="en-US">Prior to joining BSA, Reem worked for Oman Post.</span>
</p><p>
<span lang="en-US">Reem holds a </span><span lang="en-US">Bachelor in Commercial Law, Modern College of Business and Science</span><span lang="en-US">.</span>','post_date' => '2021-03-15 13:37:49','post_name' => 'reem-al-habsi','post_parent' => '0','Image' => 'wp-content/uploads/2021/03/image006-1.jpg','Title' => 'Associate','Email' => 'reem.alhabsi@bsabh.com','Excerpt' => 'Reem is an Associate within the corporate department based in Muscat','Publisher' => NULL,'Telephone' => '+968 2 421 8555','Practice Areas' => '294','Locations' => '83','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '62536',
'services' => [6,10,11,15,18],
'first_name'=>'Bassel',
'last_name'=>'Boutros',
'post_title'
 => 'Bassel Boutros','post_content' => '<p>Bassel graduated from Sagesse University in Beirut and obtained his Master of Advanced Studies in comparative law. He has been a member of the Beirut Bar Association since 2013 and has over 9 years of experience in private practice in Lebanon, UAE, KSA, and the region throughout he handled different types of complex litigation pertaining to civil and commercial matters such as disputes among shareholders subject to minority and majority rights, arbitrary use of rights, fictitious dividend distribution, real estate, employment, insurance, and inheritance.
</p><p>
Throughout his career, he worked on establishing companies and drafting all related documents such as By-Laws, Minutes of Meetings, shareholders\' agreements.He also worked on property development in Lebanon and UAE, advising developers, sellers, and buyers on different real estate transactions and legal provisions.
</p><p>
His broad experience in handling cases before the courts and conducting legal research on laws and precedents constitute an important factor while negotiating and drafting agreements in a way that serves the client
interest and mitigate risks towards the latter.</p>','post_date' => '2021-03-11 18:20:41','post_name' => 'bassel-boutros','post_parent' => '0',
                'Image' => 'img/profiles/Bassel-Boutros.jpg','Title' => 'Associate','Email' => 'bassel.boutros@bsabh.com','Excerpt' => 'Bassel is an Associate with our Litigation department based in our DIFC office in Dubai after his practice is predominantly Corporate, Commercial and Civil Litigation, Real Estate, Insurance (contentious and non-contentious).','Publisher' => NULL,'Telephone' => '+971 4 528 5555','Practice Areas' => '296','Locations' => '78','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '62620',
'services' => [],
'first_name'=>'Taronish',
'last_name'=>'Mistry',
'industries' => [],
'post_title'
 => 'Taronish Mistry','post_content' => '<p>Taronish is part of the Corporate and Commercial practice in our DIFC office.
</p><p>
He is currently developing his experience in the UAE across numerous sectors including banking, restructuring, employment and real estate. In his day-to-day role Taronish is regularly involved in corporate transactions, drafting commercial agreements, and labour matters. Taronish has also formulated strategies for clients in DIFC litigation in order to secure positive outcomes in their disputes.
</p><p>
He regularly contributes to client updates and legal articles for well-known publications, including Arabian Business.
</p><p>
Taronish holds an LLB honors degree and MSc in Law, Business & Management from the University of Law, United Kingdom as well as a Bachelor of Science in Industrial Engineering from Purdue University, USA.</p>',
                'post_date' => '2021-05-05 12:26:02','post_name' => 'taronish-mistry','post_parent' => '0',
                'Image' => 'img/profiles/Taronish-Mistry.jpg','Title' => 'Paralegal','Email' => 'taronish.mistry@bsabh.com','Excerpt' => 'Taronish is part of the Corporate and Commercial practice in our DIFC office.','Publisher' => 'Taronish Mistry','Telephone' => '+971 4 528 5555','Practice Areas' => '304, 294','Locations' => '78','Office Title' => '1946','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '62790',
'services' => [5,6,11,15,18],
'first_name'=>'Adam',
'last_name'=>'Tighe',
'post_title'
 => 'Adam Tighe','post_content' => '<p>Adam has worked exclusively in private practice since 2011, with a strong focus on insurance litigation, where he has advised and represented international insurers/reinsurers, brokers, loss adjusters, and large self-insured corporates in Australia, the United Kingdom and the United Arab Emirates. His areas of speciality include professional negligence, medical negligence, and property damage claims (including significant natural disaster events).
</p><p>
He is a technically focused lawyer who advises his clients across a broad range of matters (including, inter alia: policy interpretation/coverage, compliance matters, claim prospects).
</p><p>
Adam also has considerable experience within the UAE in the corporate and commercial practice areas, where he has established companies, advised multi-national companies on restructuring and compliance matters, and drafted complex multi-jurisdictional agreements.
</p><p>
Adam holds an LLB (Hons) from the Queensland University of Technology (Australia) and a GradDip in Law from the Australian National University. He is registered with the Queensland Law Society (Australia), the Dubai International Financial Centre Courts, and the Dubai Legal Affairs Department.','post_date' => '2021-08-23 17:26:52','post_name' => 'adam-tighe','post_parent' => '0','Image' => 'img/profiles/Adam-Tighe.jpg','Title' => 'Senior Associate','Email' => 'adam.tighe@bsabh.com','Excerpt' => 'Adam has a strong focus on insurance litigation, where he has advised and represented international insurers/reinsurers, brokers, loss adjusters, and large self-insured corporates in Australia, the United Kingdom and the United Arab Emirates','Publisher' => NULL,'Telephone' => '+971 4 528 5555','Practice Areas' => '304, 294, 302','Locations' => '78','Office Title' => '309','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '62792',
'services' => [2,1,5,21,4,3,20,10,17,16,18],
'first_name'=>'Dhruv',
'last_name'=>'Agarwal',
'post_title'
 => 'Dhruv Agarwal','post_content' => '<p>Dhruv is an Associate in BSA\'s Corporate and M&A Practice in our DIFC office.
</p><p>
Prior to joining BSA, Dhruv worked at a leading full-service law firm in India, where he advised clients across many industries on a range of mergers & acquisitions, corporate, commercial, competition, and regulatory matters. He also has banking and finance experience, having worked as an in-house counsel for a pre-eminent private Indian bank.
</p><p>
Dhruv holds an LL.M. in Law & Economics (with distinction) from the European Erasmus Mundus program from partner universities at Hamburg, Ghent and Warsaw. Earlier, he completed his law graduation (B.A., LL.B. (Hons)) from the National Law School of India University (India\'s top law school), and "A" levels from Singapore (on a full scholarship granted by the Singapore Government).
</p><p>
Dhruv is admitted to practice law in India, and is fluent in English and Hindi.','post_date' => '2021-08-23 17:32:18','post_name' => 'dhruv-agarwal','post_parent' => '0',
                'Image' => 'img/profiles/Dhruv-Agarwal.jpg','Title' => 'Associate','Email' => 'dhruv.agarwal@bsabh.com','Excerpt' => 'Dhruv is an Associate in BSA\'s Corporate and M&A Practice in our DIFC office. ','Publisher' => NULL,'Telephone' => '+971 4 528 5555','Practice Areas' => '304, 294, 1982','Locations' => '78','Office Title' => '308','Categories' => NULL,'Tags' => NULL),


            array(
                    'experience' =>[
    ] , 'ID' => '62915',
'services' => [1,2,3,19,14],
'first_name'=>'Yasin',
'last_name'=>'Chowdhury',
'post_title'
 => 'Yasin Chowdhury','post_content' => '<p>Working as an associate in the corporate team of our Muscat office, Yasin has extensive experience in representing various Omani and foreign companies, including major real estate developers, funds and construction companies, on issues relating to real estate and construction laws in Oman. His real estate and construction experience includes advising on various aspects of structuring the transaction, issues related to acquisition of land in Oman and related documentation.
</p><p>
Yasin regularly advises on issues pertaining to Mergers & Acquisitions (M&A), joint ventures (both entry and exit), foreign collaborations, technology transfers, asset sales (slump sales) etc. and has been the lead attorney in successfully representing several Omani and foreign companies in several transactions.
<p>
He frequently advises leading international companies in their operations in Oman and has assisted major retail brands to set up their businesses in the region.</p>',
                    'post_date' => '2021-10-07 11:28:39',
                    'post_name' => 'yasin-chowdhury',
                    'post_parent' => '0','Image' => 'wp-content/uploads/2021/10/Mr-yasin-BW.jpg',
                    'Title' => 'Associate',
                    'Email' => 'yasin.chowdhury@bsabh.com',
                    'Excerpt' => 'Working as an associate in the corporate team of our Muscat office, Yasin has extensive experience in representing various Omani and foreign companies',
                    'Publisher' => NULL,
                    'Telephone' => '+968 2 421 8555',
                    'Practice Areas' => '294, 292',
                    'Locations' => '83',
                    'Office Title' => '308',
                    'Categories' => NULL,
                'Tags' => NULL
                )
        ];

        foreach($data as $item){

            $new['name'] = $item['post_title'];
            $new['first_name'] = $item['first_name'];
            $new['last_name'] = $item['last_name'];
            $new['slug'] = $item['post_name'];
            $new['content'] = $item['post_content'];
            $new['photo'] = $item['Image'];
            $new['telephone'] = $item['Telephone'];
            $new['email'] = $item['Email'];
            $new['excerpt'] = $item['Excerpt'];

            $data = Lawyer::create($new);

            if($data){
                $practices = explode(',',$item['Practice Areas']);
                $locations = explode(',',$item['Locations']);
                $titles = explode(',',$item['Office Title']);

                if(count(array_filter($practices)))
                    $data->practices()->sync($practices);
                if(count(array_filter($locations)))
                    $data->locations()->sync($locations);

                if(count(array_filter($titles))){
                    $tcount = 1;
                    foreach($titles as $tt){
                        LawyerTitle::create(['lawyer_id'=>$data->id,'title_id'=>$tt,'primary'=>$tcount]);
                        $tcount = 0;
                    }
                }

                if(isset($item['services']))
                    if(count($item['services']))
                        $data->services()->sync($item['services']);

//                if(isset($item['industries']))
//                    if(count($item['industries']))
//                        $data->industries()->sync($item['industries']);

                if(isset($item['experience'])){
                    if(count($item['experience'])>0){
                        foreach($item['experience'] as $experiences)
                            foreach($experiences as $experience){
                                LawyerExperience::create(['content' => $experience, 'lawyer_id' => $data->id]);
                            }
                    }
                }

            }
        }
    }
}
