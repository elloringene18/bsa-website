<?php

namespace Database\Seeders;

use App\Models\Title;
use Illuminate\Database\Seeder;

class TitleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('term_id' => '321','name' => 'Managing Partner','slug' => 'managing-partner','term_group' => '0'),
            array('term_id' => '315','name' => 'Senior Partner','slug' => 'senior-partner','term_group' => '0'),
            array('term_id' => '310','name' => 'Partner','slug' => 'partner','term_group' => '0'),

            array('term_id' => '318','name' => 'Head of Abu Dhabi office','slug' => 'head-of-abu-dhabi-office','term_group' => '0'),
            array('term_id' => '322','name' => 'Head of Sharjah & Northern Emirates offices','slug' => 'head-of-sharjah-northern-emirates-offices','term_group' => '0'),
            array('term_id' => '660','name' => 'Head of Lebanon office','slug' => 'head-of-lebanon-office','term_group' => '0'),
            array('term_id' => '919','name' => 'Head of Indirect Tax and Conveyancing','slug' => 'head-of-indirect-tax-and-conveyancing','term_group' => '0'),
            array('term_id' => '920','name' => 'Head of Intellectual Property','slug' => 'head-of-intellectual-property','term_group' => '0'),
            array('term_id' => '921','name' => 'Head of Office','slug' => 'head-of-office','term_group' => '0'),
            array('term_id' => '922','name' => 'Head of Department','slug' => 'head-of-department','term_group' => '0'),
            array('term_id' => '936','name' => 'Head of Spanish and Latin American Desk','slug' => 'head-of-spanish-and-latin-american-desk','term_group' => '0'),
            array('term_id' => '1010','name' => 'Head of Corporate Department','slug' => 'head-of-corporate-department','term_group' => '0'),
            array('term_id' => '1087','name' => 'Head of Corporate Practice','slug' => 'head-of-corporate-practice','term_group' => '0'),
            array('term_id' => '1102','name' => 'Head of Insurance','slug' => 'head-of-insurance','term_group' => '0'),
            array('term_id' => '1103','name' => 'Head of Corporate – KSA','slug' => 'head-of-corporate-ksa','term_group' => '0'),

            array('term_id' => '1681','name' => 'Of Counsel','slug' => 'of-counsel','term_group' => '0'),
            array('term_id' => '309','name' => 'Senior Associate','slug' => 'senior-associate','term_group' => '0'),
            array('term_id' => '1105','name' => 'Head of Advocacy','slug' => 'head-of-advocacy','term_group' => '0'),
            array('term_id' => '308','name' => 'Associate','slug' => 'associate','term_group' => '0'),
            array('term_id' => '320','name' => 'Advocate','slug' => 'advocate','term_group' => '0'),

            array('term_id' => '1946','name' => 'Paralegal','slug' => 'paralegal','term_group' => '0'),

            array('term_id' => '899','name' => 'Consultant','slug' => 'consultant','term_group' => '0'),
            array('term_id' => '917','name' => 'Government Relations Counsel','slug' => 'government-relations-counsel','term_group' => '0'),
        );

        foreach ($data as $item)
        {
            $data = [];
            $data['t_id'] = $item['term_id'];
            $data['name'] = $item['name'];
            $data['slug'] = $item['slug'];

            Title::create($data);
        }
    }
}
