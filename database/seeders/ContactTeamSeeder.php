<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ContactTeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            //----------------------------------------------------- SERVICES --------------------------------------------

            'services' => [
                [
                    'title' => 'Team button text',
                    'content' => 'Contact Team',
                    'type' => 'text',
                ],
            ],
        ];

        foreach($data as $key=>$sub){
            foreach($sub as $item){
                $item['page'] = $key;
                $item['is_visible'] = isset($item['is_visible']) ? $item['is_visible'] : 1;
                $item['type'] = isset($item['type']) ? $item['type'] : 'html';
                $item['slug'] = \Illuminate\Support\Str::slug($item['title']);

                \App\Models\PageSection::create($item);
            }
        }
    }
}
