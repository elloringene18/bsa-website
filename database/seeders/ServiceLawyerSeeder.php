<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Lawyer;
use App\Models\Service;
use App\Models\ServiceTeam;
use Illuminate\Database\Seeder;
use Symfony\Component\Console\Output\ConsoleOutput;

class ServiceLawyerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        $data = [

            array(
                'contacts' => 'Jimmy Haoula, Lara Barbary, Nadim Bardawil, Michael Kortbawi, Rima Mrad, Arsalan Tariq, Jean Abboud',
                // 'team' => 'Robert Mitchley, Barry Greenberg, Antoine Iskandar, Bradley Moran, Derek Robins, Dhruv Agarwal, Hashem Al Ahdal, Maria Nevirkovets, Mohammed Alahdal, Nadia El Tannir, Nour Gemayel, Omar Al Masri, Reina Al Ali, Nurul Shaharudin, Reem Al Habsi, Yasin Khan,  Musab Iftikhar',
                'post_name' => 'corporate-law-and-ma'
            ),

            array(
                'contacts' => 'Jimmy Haoula, Lara Barbary, Nadim Bardawil, Michael Kortbawi, Rima Mrad, Arsalan Tariq, Jean Abboud',
                // 'team' => 'Robert Mitchley, Barry Greenberg, Antoine Iskandar, Bradley Moran, Derek Robins, Dhruv Agarwal, Hashem Al Ahdal, Maria Nevirkovets, Mohammed Alahdal, Nadia El Tannir, Nour Gemayel, Omar Al Masri, Reina Al Ali, Nurul Shaharudin, Reem Al Habsi, Yasin Khan,  Musab Iftikhar',
                'post_name' => 'commercial-law'
            ),

            array(
                'contacts' => 'Jimmy Haoula, Nadim Bardawil, Abdullah Ishnaneh, Michael Kortbawi',
                // 'team' => 'Robert Mitchley, Barry Greenberg, Antoine Iskandar, Bradley Moran, Derek Robins, Dhruv Agarwal, Hashem Al Ahdal, Maria Nevirkovets, Mohammed Alahdal, Nadia El Tannir, Nour Gemayel, Omar Al Masri, Reina Al Ali, Nurul Shaharudin, Reem Al Habsi, Yasin Khan, Musab Iftikhar',
                'post_name' => 'regulatory-and-compliance-1',
            ),

            array(
                'contacts' => 'Asim Ahmed, Abdullah Ishnaneh, Shaaban Metwally, Rima Mrad',
                // 'team' => 'Bradley Moran, Dhruv Agarwal, Hadiel Hussien, Hazem Balbaa, Nour Geymayel, Mohammed Alahdal, Mohammed Alghazali, Saad Younes, Swati Soni, Reina Al Ali, Maria Nevirkovets, Khaled Elgohari, Hashem El Agawani, Hassan El Shahat Mohamed, Ahmed Elsayed, Rita Ayoub',
                'post_name' => 'employment-law-firm',
            ),

            array(
                'contacts' => 'Nadim Bardawil, Simon Isgar, Rima Mrad',
                // 'team' => 'Adam Tighe, Amal Atieh, Amany Essam, Ahmed Elsayed, Ashraf Ibrahim, Asma Siddiqui, Dhruv Agarwal, Derek Robins, Hassan El Agawani, Hassan El Shahat Mohamed',
                'post_name' => 'cybersecurity',
            ),

            array(
                'contacts' => 'Dr. Ahmad Bin Hezeem, Asim Ahmed, Abdullah Ishnaneh, Shaaban Metwally',
                // 'team' => 'Adam Tighe, Ahmed Elsayed, Ahmed Labib, Alaaeldin Morad, Amany Essam, Ashraf Ibrahim, Asma Siddiqui, Bassel Boutros, Hadiel Hussien, Hassan El Agawani, Hassan El Shahat Mohamed, Hazem Balbaa, Ali Abdelhalim, El Hussaini El Wasila, Elhag Mohamed Ali, Abdelmunem El Rufaai, Ahmed Al Taher, Haitham Al Naabi, Khulood Al Wahaibi, Mohammed Alghazali, Omar Alkharoosi, Jonathan Brown, Salman Madkour',
                'post_name' => 'litigation-law-firm'
            ),

            array(
                'contacts' => 'Antonios Dimitracopoulos, Jonathan Brown',
                // 'team' => 'Jonathan Brown, Noran Al Mekhlafi, Asma Siddiqui, Hadiel Hussien, Hassan El Agawani, Hassan El Shahat Mohamed, Hazem Balbaa, Ali Abdelhalim, El Hussaini El Wasila, Elhag Mohamed Ali, Abdelmunem El Rufaai, Ahmed Al Taher, Haitham Al Naabi, Khulood Al Wahaibi, Mohammed Alghazali, Omar Alkharoosi',
                'post_name' => 'arbitration-dispute-resolution'
            ),
            array(
                'contacts' => 'Munir A. Suboh',
                // 'team' => 'Amal Atieh, Felicity Hammond, Sabrina Berrahoui ',
                'post_name' => 'intellectual-property-law'
            ),
            array(
                'contacts' => 'Jimmy Haoula, Michael Kortbawi, Abdullah Ishnaneh',
                // 'team' => 'Reem Al Habsi, Musab Iftikhar, Derek Robins, Barry Greenberg, Nadia El Tannir, Ahmed Al Taher, Abdelmuneem El Rufaai, Maria Nevirkovets, Abdalla Alsuwaidi',
                'post_name' => 'banking-finance-law'
            ),
            array(
                'contacts' => 'Lara Barbary, Rima Mrad',
                // 'team' => 'Robert Mitchley, Abdalla Alsuwaidi, Ahmed Elsayed, Amany Essam, Jonathan Brown, Omar Al Masri, Musab Iftikhar, Mohammed Alahdal, Maria Nevirkovets, Noran Al Mekhlafi, Dhruv Agarwal, Bassel Boutros',
                'post_name' => 'energy-law',
            ),
            array(
                'contacts' => 'Simon Isgar',
                // 'team' => 'Venna Iyer, Adam Tighe, Bassel Boutros, Nour Gemayel, Ahmed Al Taher, Nadia El Tannir',
                'post_name' => 'insurance-reinsurance-law'
            ),
            array(
                'contacts' => 'Rima Mrad',
                // 'team' => 'Reem Al Habsi, Musab Iftikhar, Derek Robins, Barry Greenberg, Nadia El Tannir, Ahmed Al Taher, Abdelmuneem El Rufaai, Maria Nevirkovets, Abdalla Alsuwaidi',
                'post_name' => 'regulatory-and-compliance',
            ),
            array(
                'contacts' => 'Antonios Dimitracopoulos',
                // 'team' => 'Jonathan Brown, Noran Al Mekhlafi, Asma Siddiqui, Hadiel Hussien, Hassan El Agawani, Hassan El Shahat Mohamed, Hazem Balbaa, Ali Abdelhalim, El Hussaini El Wasila, Elhag Mohamed Ali, Abdelmunem El Rufaai, Ahmed Al Taher, Haitham Al Naabi, Khulood Al Wahaibi, Mohammed Alghazali, Omar Alkharoosi',
                'post_name' => 'construction-law',
            ),
            array(
                'contacts' => 'Jimmy Haoula, Asim Ahmed, Lara Barbary',
                // 'team' => 'Robert Mitchley, Mohammed Alahdal, Hadiel Hussien, Hazem Balbaa, Asma Siddiqui, Abdullah Al Suwaidi, Derek Robins, Maria Nevirkovets, Omar Al Masri, Musab Iftikhar, Yasin Chowdhury ',
                'post_name' => 'real-estate-law',
            ),
            array(
                'contacts' => 'Simon Isgar, Michael Kortbawi',
                // 'team' => 'Venna Iyer, Adam Tighe, Bassel Boutros, Nour Gemayel, Ahmed Al Taher, Nadia El Tannir ',
                'post_name' => 'life-sciences-and-healthcare',
            ),
            array(
                'contacts' => 'Lara Barbary, Munir A. Suboh',
                // 'team' => 'Robert Mitchley, Barry Greenberg, Antoine Iskandar, Bradley Moran, Derek Robins, Dhruv Agarwal, Hashem Al Ahdal, Maria Nevirkovets, Mohammed Alahdal, Nadia El Tannir, Nour Gemayel, Omar Al Masri, Reina Al Ali, Nurul Shaharudin, Reem Al Habsi, Yasin Khan,  Musab Iftikhar ',
                'post_name' => 'retail-and-consumer-goods',
            ),

            array(
                'contacts' => 'Antonios Dimitracopoulos, Rima Mrad, Jonathan Brown',
                // 'team' => 'Jonathan Brown, Noran Al Mekhlafi, Bradley Moran, Dhruv Agarwal, Hadiel Hussien, Hazem Balbaa, Mohammed Alahdal, Mohammed Alghazali, Saad Younes, Swati Soni, Reina Al Ali, Maria Nevirkovets, Khaled Elgohari, Hashem El Agawani, Hassan El Shahat Mohamed, Ahmed Elsayed, Rita Ayoub',
                'post_name' => 'international-trade-transport-and-maritime ',
            ),
            array(
                'contacts' => 'Nadim Bardawil',
                // 'team' => 'Dhruv Agarwal, Ahmed Elsayed, Adam Tighe, Reina Al Ali, Felicity Hammond, Amal Atieh, Bassel Boutros, Rita Ayoub',
                'post_name' => 'technology-media-and-telecommunications'
            ),
            array(
                'contacts' => 'Jimmy Haoula, Asim Ahmed',
                // 'team' => 'Robert Mitchley, Mohammed Alahdal, Hadiel Hussien, Hazem Balbaa, Asma Siddiqui, Abdullah Al Suwaidi, Derek Robins, Maria Nevirkovets, Omar Al Masri, Musab Iftikhar, Yasin Chowdhury, Nour Gemayel',
                'post_name' => 'leisure-and-hospitality',
            ),
            array(
                'contacts' => 'Jimmy Haoula, Lara Barbary',
                // 'team' => 'Robert Mitchley, Barry Greenberg, Antoine Iskandar, Bradley Moran, Derek Robins, Dhruv Agarwal, Hashem Al Ahdal, Maria Nevirkovets, Mohammed Alahdal, Nadia El Tannir, Nour Gemayel, Omar Al Masri, Reina Al Ali, Nurul Shaharudin, Reem Al Habsi, Yasin Khan,  Musab Iftikhar',
                'post_name' => 'education',
            ),
            array(
                'contacts' => 'Lara Barbary, Rima Mrad',
                // 'team' => 'Robert Mitchley, Bradley Moran, Derek Robins, Dhruv Agarwal, Maria Nevirkovets, Mohammed Alahdal, Omar Al Masri',
                'post_name' => 'environment-social-and-governance',
            ),
            array(
                'contacts' => 'Jimmy Haoula, Dr. Ahmad Bin Hezeem',
                // 'team' => 'Jimmy Haoula, Michael Kortbawi, Dr. Ahmad Bin Hezeem, Rima Mrad, Lara Barbary, Asim Ahmed, Abdullah Ishnaneh, Shabaan Metwally ',
                'post_name' => 'private-client',
            ),
            array(
                'contacts' => 'Jimmy Haoula, Dr. Ahmad Bin Hezeem',
                // 'team' => 'Robert Mitchley, Barry Greenberg, Nadia El Tannir, Nour Geymayel, Antoine Iskandar, Hadiel Hussien ',
                'post_name' => 'high-net-worth-individuals',
            ),
            array(
                'contacts' => 'Jimmy Haoula, Dr. Ahmad Bin Hezeem',
                // 'team' => 'Jimmy Haoula, Michael Kortbawi, Dr. Ahmad Bin Hezeem, Rima Mrad, Lara Barbary, Asim Ahmed, Abdullah Ishnaneh, Shabaan Metwally  ',
                'post_name' => 'family-owned-businesses',
            ),
        ];

        foreach($data as $item){

            $service = Service::where('slug',$item['post_name'])->first();

            if($service){

//                if(isset($item[// 'team'])){
//                    $output->writeln('TEAM------------------------');
//                    $output->writeln($item[// 'team']);
//                    $team = explode(', ',$item[// 'team']);
//
//                    if(count(array_filter($team))){
//                        foreach($team as $t){
//                            $d = Lawyer::where('name',$t)->first();
//                            if($d){
//                                $service->team()->create(['lawyer_id'=>$d->id]);
//                            }
//                        }
//                    }
//                }

                if($item['contacts']){
                    $contacts = explode(', ',$item['contacts']);

                    if(count(array_filter($contacts))) {
                        foreach($contacts as $t){
                            $d = Lawyer::where('name',$t)->first();
                            if($d){
                                $service->contacts()->create(['lawyer_id'=>$d->id]);
                            }
                        }
                    }
                }
            }
        }
    }
}
