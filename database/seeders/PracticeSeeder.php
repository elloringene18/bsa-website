<?php

namespace Database\Seeders;

use App\Models\Practice;
use Illuminate\Database\Seeder;

class PracticeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('term_id' => '298','name' => 'Arbitration & Dispute Resolution','slug' => 'arbitration-disputeresolution','term_group' => '0'),
            array('term_id' => '75','name' => 'Banking & Finance','slug' => 'banking-financial','term_group' => '0'),
            array('term_id' => '304','name' => 'Commercial','slug' => 'commercial','term_group' => '0'),
            array('term_id' => '299','name' => 'Construction','slug' => 'construction','term_group' => '0'),
            array('term_id' => '294','name' => 'Corporate and M&A','slug' => 'corporate-ma','term_group' => '0'),
            array('term_id' => '306','name' => 'Employment','slug' => 'employment','term_group' => '0'),
            array('term_id' => '302','name' => 'Insurance & Reinsurance','slug' => 'insurance-reinsurance','term_group' => '0'),
            array('term_id' => '296','name' => 'Litigation','slug' => 'litigation','term_group' => '0'),
            array('term_id' => '292','name' => 'Real Estate','slug' => 'real-estate','term_group' => '0'),
            array('term_id' => '676','name' => 'Intellectual Property','slug' => 'intellectual-property','term_group' => '0'),
            array('term_id' => '918','name' => 'VAT','slug' => 'vat','term_group' => '0'),
            array('term_id' => '935','name' => 'TMT','slug' => 'tmt','term_group' => '0'),
            array('term_id' => '948','name' => 'Energy','slug' => 'energy','term_group' => '0'),
            array('term_id' => '1982','name' => 'Competition & Regulatory','slug' => 'competition-regulatory','term_group' => '0')
        );

        foreach ($data as $item)
        {
            $data = [];
            $data['p_id'] = $item['term_id'];
            $data['name'] = $item['name'];
            $data['slug'] = $item['slug'];

            Practice::create($data);
        }
    }
}
