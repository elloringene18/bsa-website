<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PageSectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'title','parent_sub_id','content'
        $data = [

                'home' => [
                    [
                        'title' => 'Intro Heading',
                        'content' => 'WELCOME <br/>TO <div class="line"></div> BSA',
                        'type' => 'textarea',
                    ],
                    [
                        'title' => 'Video',
                        'content' => 'img/bsa-video.mp4',
                        'type' => 'video',
                    ],
                    [
                        'title' => 'Intro Content',
                        'content' => '
                            <p>Established since 2001, BSA Ahmed Bin Hezeem & Associates LLP has expanded organically to <span class="hovertext">nine offices in five countries </span> across the Middle East.
                            Our energy and ambition proudly demonstrate the region’s innovative and entrepreneurial spirit through our <span class="hovertext">premium legal services</span> in new and evolving and sectors.
                            </p>
                            <p>
                            Our curated team of <span class="hovertext">talented lawyers and 15 partners</span>  not only help our clients succeed but provide an innate cultural understanding, depth of expertise, and an unmatched contact network.
                            BSA is driven, modern, and truly in tune with the market.
                            </p>
                            <p>
                            More than just a law firm, we pride ourselves on being connective: sparking collaboration, creating synergy, <span class="hovertext">driving change </span>.
                            </p>
                        ',
                        'type' => 'textarea',
                    ],
                    [
                        'title' => 'Hover message 1',
                        'content' => '<span class="title">We are where you are</span><span class="details">a Middle East law firm with international reach</span>',
                        'type' => 'textarea',
                    ],
                    [
                        'title' => 'Hover message 2',
                        'content' => '<span class="title">We understand your business</span><span class="details">a holistic legal approach to meet your commercial objectives</span>',
                        'type' => 'textarea',
                    ],
                    [
                        'title' => 'Hover message 3',
                        'content' => 'A diverse & inclusive team</span><span class="details">50/50 gender split covering 35 different nationalities who speak 22 languages</span>',
                        'type' => 'textarea',
                    ],
                    [
                        'title' => 'Hover message 4',
                        'content' => '<span class="title">Dedicated to Better</span><span class="details">A unique client service with a commitment to community</span>',
                        'type' => 'textarea',
                    ],
                    [
                        'title' => 'Article Highlights',
                        'content' => '[{"id":"220","value":"Oman Firm of the Year: The Oath Legal Awards"},{"id":"203","value":"The Future of Finance: Digital transformation, innovation, and the FinTech ecosystem in the GCC"},{"id":"167","value":"GCC Restructuring & Insolvency Report"}]',
                        'type' => 'tags',
                    ],
                ],

            //----------------------------------------------------- ABOUT --------------------------------------------


            'about' => [
                [
                    'title' => 'Intro',
                    'content' => '<p>Many law firms can give you legal advice in the Middle East. However, very few have started out in Dubai and grown to encompass the UAE, Saudi Arabia, Oman, Lebanon and Iraq.
So what makes us different? Well, let’s just say <b>it’s not what we do but the way that we do it.</b></p>',
                ],
                [
                    'title' => 'Inner Content Heading',
                    'content' => 'Giving back to our community',
                    'type' => 'text',
                ],
                [
                    'title' => 'Inner Content Body',
                    'content' => '<p>We care about the communities we work in. For us, corporate social responsibility isn’t a box-ticking exercise. It’s an essential part of our identity. Since launching in 2001, we’ve worked with a number of charities, including the Philippines Red Cross, Cambodia Children’s Fund, and Dubai Cares to raise money and awareness on a range of important issues.</p>

            <p>We also understand that people who need legal support the most are often those who can least afford to pay for it. That’s why we are an active member of
            the DIFC Courts’ Pro Bono Programme and have offered pro bono services to people in Dubai via DLAD’s Platform For Pro Bono Services.</p>',
                ],
                [
                    'title' => 'Promobox',
                    'content' => '1',
                    'type' => 'promobox',
                ],
            ],

            //----------------------------------------------------- LOCATIONS --------------------------------------------


            'locations' => [
                [
                    'title' => 'Intro',
                    'content' => '<p>Our offices are <b>strategically located to support our clients</b>
with any and all of their legal concerns and challenges, wherever they are.</p>',
                ],
                [
                    'title' => 'Heading',
                    'content' => 'We are<br/> where <br/>you are',
                    'type' => 'text',
                ],
                [
                    'title' => 'Promobox',
                    'content' => '2',
                    'type' => 'promobox',
                ],
            ],

            //----------------------------------------------------- OUR PEOPLE --------------------------------------------

            'our-people' => [
                [
                    'title' => 'Intro',
                    'content' => '<p><b>We help people do business all over the Middle East.</b> It doesn’t matter what sector you operate
                        in or whether you’re a fledgling local business or a multi-national corporate.</p>
                    <p>
                        As a regional law firm with offices in Dubai, UAE, Saudi Arabia, Oman, Lebanon and Iraq,
                        we can help you negotiate the maze of local regulations and <b>ensure your business interests
                            are protected at all times.</b></p>',
                ],
                [
                    'title' => 'Promobox',
                    'content' => '1',
                    'type' => 'promobox',
                ],
            ],

            //----------------------------------------------------- KNOWLEDGE --------------------------------------------

            'knowledge' => [
                [
                    'title' => 'Intro',
                    'content' => '',
                ],
                [
                    'title' => 'Promobox',
                    'content' => '5',
                    'type' => 'promobox',
                ],
            ],

            //----------------------------------------------------- CAREERS --------------------------------------------

            'careers' => [
                [
                    'title' => 'Intro',
                    'content' => '<p><b>At BSA, we think diversity is a strength.</b> We celebrate differences and welcome applications from people of all ethnic and racial backgrounds. It doesn’t matter where you’re from; if you can think the BSA way and can help our business to continue to evolve and grow, we’d love to hear from you.</p>',
                ],
                [
                    'title' => 'Banner Image',
                    'content' => 'img/careers-banner.png',
                    'type' => 'image',
                ],
                [
                    'title' => 'Left Column Content',
                    'content' => '<h5>Why BSA Ahmad Bin Hezeem &amp; Associates LLP?</h5>
                <p>BSA Ahmad Bin Hezeem &amp; Associates LLP seeks sustainable growth through the recruitment of talented lawyers, support staff and other professionals. In an increasingly complex and challenging business environment, we appreciate the need to be responsive to the needs of our people, whose diverse backgrounds and aspirations influence the way in which they bring value and develop their careers with us.</p>
                <p>As one of the leading regional law firms in the Middle East, we are committed to providing innovative legal services and solutions, establishing strong, personable and sustainable client relationships.</p>',
                ],
                [
                    'title' => 'Right Column Content',
                    'content' => '<h5>Working at BSA</h5>
                <p>BSA’s performance and reputation depend upon a good working culture and environment for our people. We are committed to developing and growing our highly experienced and robust network of diverse lawyers, support staff and business professionals, expanding our existing regional network of offices, enabling us to offer the finest and most comprehensive range of legal services. </p>
                <p>With an enviable client list and fast-track growth potential in a dynamic region, we provide our staff a multitude of diverse and stimulating work.</p>',
                ],
                [
                    'title' => 'Bottom Content Heading',
                    'content' => 'Internships',
                    'type' => 'text',
                ],
                [
                    'title' => 'Career Listing Heading',
                    'content' => 'Look for a vacancy',
                    'type' => 'text',
                ],
                [
                    'title' => 'Bottom Content Body',
                    'content' => '<p>We offer an internship programme to talented students, fresh graduates, and lawyers qualified in other jurisdictions who wish to explore another culture and learn how law is practised in the UAE.</p>
                    <p>Interns can expect to be involved in legal research and analysis, agree ment preparation, client management, marketing and ad-hoc legal support. You’ll have the opportunity to work alongside some of the finest lawyers in the Middle East, for some of the best clients and in one of the most supportive environments you could wish to find.</p>
                    <p>Interested or just want to learn more? Send us a message at <a href="mailto:careers@bsabh.com">careers@bsabh.com</p>',
                ],
            ],

            //----------------------------------------------------- CONTACT --------------------------------------------

            'contact' => [
                [
                    'title' => 'Banner Image',
                    'content' => 'img/contact-us-banner.png',
                    'type' => 'image',
                ],
                [
                    'title' => 'Form Heading',
                    'content' => 'Need help or have a question? Contact us online.',
                    'type' => 'text',
                ],
                [
                    'title' => 'Promobox',
                    'content' => '4',
                    'type' => 'promobox',
                ],
            ],

            //----------------------------------------------------- SERVICES --------------------------------------------

            'services' => [
                [
                    'title' => 'Promobox',
                    'content' => '3',
                    'type' => 'promobox',
                ],
            ],
        ];

        foreach($data as $key=>$sub){
            foreach($sub as $item){
                $item['page'] = $key;
                $item['is_visible'] = isset($item['is_visible']) ? $item['is_visible'] : 1;
                $item['type'] = isset($item['type']) ? $item['type'] : 'html';
                $item['slug'] = \Illuminate\Support\Str::slug($item['title']);

                \App\Models\PageSection::create($item);
            }
        }
    }
}
