<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = array(
            array('term_id' => '1','name' => 'Uncategorized','slug' => 'uncategorized'),
            array('term_id' => '25','name' => 'Real Estate','slug' => 'real-estate'),
            array('term_id' => '31','name' => 'Corporate and M&amp;A','slug' => 'corporate-ma'),
            array('term_id' => '33','name' => 'Litigation','slug' => 'litigation'),
            array('term_id' => '34','name' => 'Arbitration &amp; Dispute Resolution','slug' => 'arbitration-disputeresolution'),
            array('term_id' => '35','name' => 'Construction','slug' => 'construction'),
            array('term_id' => '36','name' => 'Banking &amp; Finance','slug' => 'banking-finance'),
            array('term_id' => '37','name' => 'Insurance &amp; Reinsurance','slug' => 'insurance-reinsurance'),
            array('term_id' => '38','name' => 'Commercial','slug' => 'commercial'),
            array('term_id' => '61','name' => 'Employment','slug' => 'employment'),
            array('term_id' => '64','name' => 'Press Releases','slug' => 'press-releases'),
            array('term_id' => '86','name' => 'Publications','slug' => 'publications-2'),
            array('term_id' => '100','name' => 'News &amp; Events','slug' => 'news-events'),
            array('term_id' => '102','name' => 'Events','slug' => 'events'),
            array('term_id' => '103','name' => 'News','slug' => 'news'),
            array('term_id' => '106','name' => 'Articles','slug' => 'articles'),
            array('term_id' => '107','name' => 'Books','slug' => 'books'),
            array('term_id' => '291','name' => 'All','slug' => 'all'),
            array('term_id' => '649','name' => 'غير مصنف','slug' => '%d8%ba%d9%8a%d8%b1-%d9%85%d8%b5%d9%86%d9%81'),
            array('term_id' => '678','name' => 'Intellectual Property','slug' => 'intellectual-property'),
            array('term_id' => '911','name' => 'Franchise','slug' => 'franchise'),
            array('term_id' => '927','name' => 'TMT','slug' => 'tmt'),
            array('term_id' => '949','name' => 'Tax and VAT','slug' => 'tax-and-vat'),
            array('term_id' => '1045','name' => 'Immigration','slug' => 'immigration'),
            array('term_id' => '1048','name' => 'Cybercrime','slug' => 'cybercrime'),
            array('term_id' => '1049','name' => 'Fintech','slug' => 'fintech'),
            array('term_id' => '1059','name' => 'Bankruptcy Law','slug' => 'bankruptcy-law'),
            array('term_id' => '1060','name' => 'Insolvency','slug' => 'insolvency'),
            array('term_id' => '1061','name' => 'Netting Agreements','slug' => 'netting-agreements'),
            array('term_id' => '1526','name' => 'Sanctions','slug' => 'sanctions'),
            array('term_id' => '1607','name' => 'Regulations','slug' => 'regulations'),
            array('term_id' => '1610','name' => 'Investment','slug' => 'investment'),
            array('term_id' => '1657','name' => 'Expo 2020','slug' => 'expo-2020'),
            array('term_id' => '1687','name' => 'Commodities','slug' => 'commodities'),
            array('term_id' => '1696','name' => 'Coronavirus Insight Centre','slug' => 'covid-19'),
            array('term_id' => '1714','name' => 'Wills','slug' => 'wills'),
            array('term_id' => '1723','name' => 'Healthcare','slug' => 'healthcare'),
            array('term_id' => '1849','name' => 'Maritime','slug' => 'maritime'),
            array('term_id' => '1852','name' => 'Logistics','slug' => 'logistics'),
            array('term_id' => '1870','name' => 'GDPR','slug' => 'gdpr'),
            array('term_id' => '1912','name' => 'Arbitration','slug' => 'arbitration'),
            array('term_id' => '1913','name' => 'Insights','slug' => 'insights'),
            array('term_id' => '1930','name' => 'Saudi Arabia','slug' => 'saudi-arabia'),
            array('term_id' => '1952','name' => 'Succession Planning','slug' => 'succession-planning'),
            array('term_id' => '1958','name' => 'Oman','slug' => 'oman'),
            array('term_id' => '1959','name' => 'UAE','slug' => 'uae'),
            array('term_id' => '1981','name' => 'AML','slug' => 'aml'),
            array('term_id' => '1988','name' => 'Renewable Energy','slug' => 'renewable-energy'),
            array('term_id' => '1996','name' => 'DIFC','slug' => 'difc'),
            array('term_id' => '1998','name' => 'Cryptocurrency','slug' => 'cryptocurrency'),
            array('term_id' => '2004','name' => 'Blockchain','slug' => 'blockchain'),
            array('term_id' => '2006','name' => 'GCC','slug' => 'gcc'),
            array('term_id' => '2014','name' => 'SPACs','slug' => 'spacs'),
            array('term_id' => '2021','name' => 'ESG','slug' => 'esg'),
            array('term_id' => '2022','name' => 'India','slug' => 'india'),
            array('term_id' => '2026','name' => 'Islamic Finance','slug' => 'islamic-finance')
        );

        foreach ($data as $item)
        {
            $category = [];
            $category['cat_id'] = $item['term_id'];
            $category['name'] = $item['name'];
            $category['slug'] = $item['slug'];

            Category::create($category);
        }
    }
}
