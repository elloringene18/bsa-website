<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\HighlightArticle;
use App\Models\Timeline;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class HighlightArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $data = [
            'oman-firm-of-the-year-the-oath-legal-awards',
            'bsa-fintech-report-2021-the-future-of-finance-in-the-gcc-2__trashed',
            'gcc-restructuring-insolvency-report'
        ];

        foreach ($data as $item)
        {
            $article = Article::where('slug',$item)->first();

            if($article)
                HighlightArticle::create(['article_id'=>$article->id]);
        }
    }
}
