<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Lawyer;
use App\Models\Service;
use App\Models\ServiceTeam;
use App\Models\ServiceType;
use Illuminate\Database\Seeder;
use Symfony\Component\Console\Output\ConsoleOutput;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        $data = [

            array(
                'ID' => '259',
                'type' => 'services',
                'post_title' => 'Corporate & M&A',
                'image' => '2018/07/shutterstock_108213236.jpg','post_content' => '<p>From routine domestic corporate transactions to complex high-value international projects, mergers, acquisitions and IPOs, our Corporate and M&amp;A team provide the full range of corporate solutions to our regional and international clients.</p>
                <p>Our lawyers have a deep understanding of local and regional laws and regulations and provide unique regulatory expertise due to their close relationships with local authorities. We understand the nuances of the market and the regulatory challenges our clients face, ensuring they are well prepared for every eventuality. We partner with our litigators meaning our contracts are robust from the get-go, reducing your risk and protecting your commercial interests. From start-ups, SMEs to some of the world&rsquo;s most prestigious conglomerates, family businesses and financial institutions, we have the knowledge and expertise to partner with our clients from set-up to maturity across every sector.</p>
                <p>Our sector knowledge in the Middle East is unrivalled. Across new and evolving industries, our dynamic team are knowledge leaders, able to provide commercial advice which directly impacts the success of our clients&rsquo; businesses. Not just lawyers, we are business partners who provide a holistic service, first-class legal advice and a personalised client service.</p>
                <p><strong>Services </strong></p>
                <ul>
                <li>Mergers and acquisitions</li>
                <li>Joint ventures</li>
                <li>Private equity &amp; venture capital</li>
                <li>Investment funds</li>
                <li>Public offerings</li>
                <li>Investment structuring</li>
                <li>Debt &amp; equity capital markets</li>
                <li>Corporate transactions</li>
                <li>Corporate structuring &amp; restructuring</li>
                <li>Asset &amp; wealth management</li>
                </ul>
                <blockquote>Winner: M&A Deal of the Year</blockquote>
                <p>The IFLR Middle East Awards 2021</p>
                <blockquote>Winner: Corporate Team of the Year</blockquote>
                <p>The Oath Legal Awards 2019</p>',
                'post_date' => '2014-01-24 02:04:48','post_name' => 'corporate-law-and-ma','post_parent' => '27','Image' => NULL,'Categories' => NULL,'Tags' => NULL),

            array(
                'ID' => '248',
                'type' => 'services',
                'post_title' => 'Commercial',
                'image' => '2018/12/0050-BSA-Website-Banners-1.jpg',
                'post_content' => '<p>As Islamic legal systems develop to accommodate the demands of contemporary commerce, the regulatory landscape is constantly evolving. With years of experience working on some of the region&rsquo;s most prominent commercial law cases, we can help you successfully negotiate every aspect of this ever-changing and uniquely nuanced area of law.</p>
                <p>Our team of experienced lawyers provide advice on all forms of commercial contracts and arrangements to support our clients&rsquo; day to day and strategic business activities, both at home and abroad. We advise on all areas of commercial contracting including, logistics, agency, distribution, sales and marketing, packaging and manufacturing and franchising, outsourcing and offshoring, e-commerce, licensing, advertising and sales promotion.</p>
                <p>What truly sets us about is our relationships and network of connections with key industry players, including regulatory bodies and governmental entities. We pride ourselves on our collaborative approach, providing you with not only legal advice, but a platform to elevate your business and connect with like-minded industry peers.</p>
                <p><strong>Services</strong></p>
                <ul>
                <li>Commercial contracts</li>
                <li>Government procurement</li>
                <li>Establishment &amp; set-up</li>
                <li>Sponsorship</li>
                <li>Distribution</li>
                <li>Franchise and sales of goods and services agreements</li>
                <li>Commercial agency advice</li>
                <li>Outsourcing and technology</li>
                <li>Corporate advisory</li>
                <li>Strategic negotiation and documentation</li>
                <li>Strategic alliances</li>
                </ul>
                <p>&nbsp;</p>
                <blockquote>"They have a personal approach and will find you the solution you need. They have the expertise but also the right mindset to get you where you need to be."</blockquote>
                <p>Chambers Global 2021</p>',
                'post_date' => '2014-01-24 01:42:34','post_name' => 'commercial-law','post_parent' => '27','Image' => NULL,'Categories' => NULL,'Tags' => NULL),

            array(
                'ID' => '',
                'type' => 'services',
                'post_title' => 'Regulatory & Compliance',
                'image' => '',
                'post_content' => '
                      <p>Businesses today face an increasingly complex framework of established and evolving legislation and regulations concerning civil, criminal and regulatory risks. We partner with our clients to navigate the potential challenges head-on through our deep knowledge and understanding of the local business environments across the region.</p>
                <p>Our regulatory and compliance lawyers provide a holistic service, advising clients on both their internal and external objectives. We help our clients manage their reputation through advice and insights into good corporate culture, effective corporate risk and crisis management, and a sound understanding of the political, regulatory and legal environment. We provide business continuity consultancy services, as well as AML training for employees with a particular focus on Know-Your-Client, bribery, corruption and fraud.</p>
                <p>Covering the full range of sectors, our team understand the specific complexities faced by healthcare organisations, FinTech companies, insurers and reinsurers, amongst other heavily regulated industries. Informed by our sector knowledge and our relationships with key regulatory bodies, we help our clients develop the right business model, foster strategic relationships, navigate complex regulation, manage risks, and address challenges their businesses faces every day.</p>
                <p><strong>Services</strong></p>
                <ul>
                <li>Corporate governance</li>
                <li>Business continuity and strategic risk management</li>
                <li>Boardroom risk and reputation</li>
                <li>Crisis management</li>
                <li>Anti-Money Laundering Regulations and KYC</li>
                <li>Antitrust and competition</li>
                <li>Bribery, corruption and fraud</li>
                <li>International trade, sanctions and export controls</li>
                <li>Environmental, social &amp; governance matters</li>
                <li>Data privacy and protection</li>
                </ul>
                <blockquote>"They\'re always readily accessible, understand the market very well and are very good at anticipating risks."</blockquote>
                <p>Chambers Global 2021 </p>
                ',
                'post_date' => '2020-12-22 19:08:04',
                'post_name' => 'regulatory-and-compliance',
                'post_parent' => '',
                'Image' => NULL,
                'Categories' => NULL,
                'Tags' => NULL
            ),

            array(
                'ID' => '264',
                'type' => 'services',
                'post_title' => 'Employment',
                'image' => '2018/07/shutterstock_642714586.jpg','post_content' => '<p>Our Employment lawyers have deep experience covering corporate support, due diligence, compliance, and litigation. We provide advice to multinational and regional companies on policies, procedures and contractual documentation for all levels of staff including service agreements, employment contracts, and secondment arrangements.</p>
<p>Our relationships with regulators across the region mean we are kept afresh of new legal developments when it comes to the rights of employers and employees, covering both onshore and offshore jurisdictions. We have built a strong reputation advising multinational clients on their HR policies and procedures in the region, providing not only an application of the law, but also an appreciation of local cultural considerations, providing integrated solutions to their people strategy and planning.</p>
<p>Our priority is to mitigate the risk of employment disputes for our clients. However, when dispute proceedings are unavoidable, our experienced lawyers have the rights of audience to defend you before all relevant labour courts and government authorities. We have successfully represented both employers and employees in a significant number of employment disputes, which in many cases related to non-conventional issues such as hypo tax and claims related to stocks and stock options. Whatever your issue, we can help you protect your interests.</p>
<p><strong>Services</strong></p>
<ul>
<li>Corporate restructuring and reorganization</li>
<li>Employee onboarding &amp; offboarding</li>
<li>Contracts &amp; entitlements</li>
<li>Grievance, disciplinary and dismissal procedures</li>
<li>HR policies and procedures</li>
<li>Immigration and residency</li>
<li>Labour disputes &amp; litigation</li>
<li>Discrimination and harassment claims</li>
</ul>
<p>Boardroom disputes &amp; severance claims </p>
<blockquote>"[They are] exceptional at breaking down an issue and laying it out for you to appreciate the legal challenges."</blockquote>
<p>Chambers Global 2021</p>
','post_date' => '2014-01-24 02:08:31','post_name' => 'employment-law-firm','post_parent' => '27','Image' => NULL,'Categories' => NULL,'Tags' => NULL),

            array(
                'ID' => '60599',
                'type' => 'services',
                'post_title' => 'Cyber security & data protection ',
                'image' => '2018/07/BSA-WEBSITE-MAIN-SLIDIER-4.jpg','post_content' => '<p>Our data protection and cybersecurity practice help clients manage legal risks related to cybersecurity, privacy, data governance, eDiscovery, information technology, ecommerce and intellectual property. Data privacy and protection are top of the agenda and regulations are continually evolving &ndash; we partner with our clients to keep them ahead of developments.</p>
<p>Data is an increasingly valuable business asset. With the increased regulatory focus regarding personal information, businesses need to understand the procedures and risks of non-compliance in order to manage their reputational and financial risks. Our legal team is able to help you design and implement privacy and security programs for your existing processes and procedures, as well as instant support in a time of cyber breach or crisis.</p>
<p>Our clients include financial institutions, data-rich corporations, insurers, government entities and startups. We advise them on maximizing technological innovation, enhancing and safeguarding their digital data, trade secrets and tech and ensuring compliance with all local and international laws.</p>
<p><strong>Services</strong></p>
<ul>
<li>Cyber risk management</li>
<li>Cyber incident response</li>
<li>Data privacy regulatory advice</li>
<li>Data protection policies &amp; procedures</li>
<li>Data storage</li>
<li>M&amp;A advisory</li>
<li>Dispute resolution &amp; litigation</li>
</ul>

<blockquote>"The service they provide is always at the highest level of proficiency."</blockquote>
<p>Chambers Global 2021</p>','post_date' => '2020-02-10 09:24:47','post_name' => 'cybersecurity','post_parent' => '27','Image' => NULL,'Categories' => NULL,'Tags' => NULL),

            array(
                'ID' => '270',
                'type' => 'services',
                'post_title' => 'Litigation',
                'image' => '2018/12/0050-BSA-Website-Banners-7.jpg','post_content' => '<p>Our Litigation department consists of skilled practitioners from across the Middle East, speaking over 15 languages and with decades of collective experience in the region. With rights of audience in every country where we have an office, we are one of the few law firms able to litigate in all Courts, providing our clients with tailored solutions to their most important challenges.</p>
                <p>The sheer size of our Litigation department allows us to assign a dedicated procedural team to each case happening in the Court of First Instance or other higher Courts. These teams specialise in the procedures of each Court and are familiar with the staff at the Courts, allowing us to ensure swift and efficient processing of procedural matters.</p>
                <p>We have a specialist team who works exclusively with the Court of Execution, helping to facilitate coordination with the various government departments involved in identifying and tracing assets for attachment purposes. We are also one of the very few firms in the region to have a dedicated Advocacy Department.</p>
                <p><strong>Services</strong></p>
                <ul>
                <li>Criminal litigation</li>
                <li>Employment &amp; HR disputes</li>
                <li>Debt recovery</li>
                <li>Bankruptcy &amp; insolvency</li>
                <li>Common law &amp; civil law disputes</li>
                <li>Commercial disputes</li>
                <li>Construction &amp; real estate litigation</li>
                <li>Insurance-related claims</li>
                <li>Fraud</li>
                <li>Cyber security breaches &amp; data protection</li>
                <li>Private client, individual and family matters</li>
                </ul>
                <blockquote>"The team at BSA is extremely hands on and approachable. They do not just have legal knowledge but also a firm grasp on ground realities and the market pulse. Their advice always factors all of this."</blockquote>
                <p>The Legal 500 EMEA 2021</p>','post_date' => '2014-01-24 02:16:08','post_name' => 'litigation-law-firm','post_parent' => '27','Image' => NULL,'Categories' => NULL,'Tags' => NULL),

            array(
                'ID' => '240',
                'type' => 'services',
                'post_title' => 'Arbitration',
                'image' => '2018/07/BSA-WEBSITE-MAIN-SLIDIER-4.jpg',
                'post_content' => '<p>Our arbitration team handles some of the most complex and high profile local and international arbitration cases in the market. Our team of dedicated specialists are leaders in their field with experience spanning several decades and positions of authority in local Courts.</p>
                <p>Our lawyers undertake representation of clients in local and international arbitration proceedings across a variety of industry sectors including construction and engineering, insurance and reinsurance, corporate, franchising, marketing and media. We apply our proficiency and expertise when representing and advising clients seeking legal support in arbitration proceedings on all stages encountered, from helping in choosing the right arbitral tribunal to applying our effective advocacy skills in arbitration hearings.</p>
                <p>And with the valuable support of our own experienced litigation teams with local rights of audience before the Courts across all our office locations, we can offer our clients, an all-inclusive range of professional legal services, that will ensure that the procedural and substantive integrity of a final arbitral award is addressed at the all important stage of local Court ratification.</p>
                <p><strong>Services</strong></p>
                <ul>
                <li>Alternative dispute resolution</li>
                <li>Adjudication</li>
                <li>International arbitration</li>
                <li>Pre-arbitration advisory</li>
                <li>Mediation</li>
                <li>Construction &amp; engineering arbitration</li>
                <li>Post-award enforcement</li>
                <li>UNCITRAL arbitrations</li>
                </ul>
                <blockquote>"Acclaimed by the market for their depth of expertise."</blockquote>
                <p>Chambers Global 2021</p>
                ','post_date' => '2014-01-24 00:54:46','post_name' => 'arbitration-dispute-resolution','post_parent' => '27','Image' => NULL,'Categories' => NULL,'Tags' => NULL),


            array(
            'ID' => '3704',
            'type' => 'services',
            'post_title' => 'Intellectual Property',
            'image' => '2018/12/0050-BSA-Website-Banners-10.jpg','post_content' => '<p>Our Intellectual Property practice is highly regarded for its strong advisory, commercial and contentious capabilities. We advise clients on their copyright, trademark, patent and design matters, covering the healthcare, pharmaceuticals, media, retail, consumer goods, technology and automotive sectors.</p>
            <p>Our Intellectual Property practice is a full-service, leading IP team, dedicated to providing a highly personable and client focused service that has earnt the trust of many loyal clients, including private client and high net worth individuals. We can help you to register your intellectual property so it is legally safeguarded from your competitors. We also know that disputes are a fact of life and we will rigorously defend your interests both in local Courts, where we have rights of audience and international courts, where we are experienced in multi-jurisdictional litigation.</p>
            <p>Our diverse team have experience in both civil and common law jurisdictions and speak several languages including Arabic and English. With years of experience acting for clients in some of the region&rsquo;s most important IP cases, we can ensure your interests are protected.</p>
            <p><strong>Services</strong></p>
            <ul>
            <li>Clearance availability searches for trademarks, designs and patents</li>
            <li>Overseeing IP portfolio, preparing and filing applications and processing them to registration</li>
            <li>Maintenance of trademarks and enforcement</li>
            <li>Transferring title for all forms of IP</li>
            <li>Negotiating, drafting and recording licenses and assignments of IP rights including licenses and distribution, franchise and joint venture agreements</li>
            <li>Commercialisation of IP and support to corporate transactions</li>
            <li>IP due diligence, asset audit and management</li>
            <li>Privacy and data protection</li>
            <li>Pharmaceutical and regulatory advice</li>
            <li>Media content protection</li>
            <li>Regulatory and compliance work</li>
            <li>Domain name protection and UDRP</li>
            <li>Consumer Protection</li>
            <li>Competition law</li>
            <li>Labelling and standards compliance</li>
            </ul>
            <blockquote>"They always assess legal risks and simplify potential legal impacts in a simple and clear manner for decision making."</blockquote>
            <p>Chambers Global 2021</p>
            ','post_date' => '2016-03-07 16:41:16','post_name' => 'intellectual-property-law','post_parent' => '27','Image' => NULL,'Categories' => NULL,'Tags' => NULL),


            array(
                'ID' => '245',
                'type' => 'services',
                'post_title' => 'Banking & Finance',
                'image' => '2018/08/shutterstock_106005239-LQ.jpg',
                'post_content' => '<p>Our Banking and Finance team advises a range of major international and domestic banks, financial institutions, government entities and corporations internationally and across the Middle East. We cover the full range of financial legal services including financial regulatory matters, FinTech and restructuring, bankruptcy and insolvency.</p>
            <p>With specialist experience in drafting syndicated loans and security documentation, we have led on the incorporation and licensing of a number of financial institutions across the Middle East, in both onshore and offshore (free zone) locations. We have also been at the forefront of Islamic finance development, spearheading a range of innovative Shari&rsquo;a compliant instruments and structures. Our team is highly skilled in contract law, drafting syndicated loans and security documentation as well as structuring and advising on investment funds. We also advise contractors, developers and sponsors, as well as banks, on their project finance documentation.</p>
            <p>Clients depend on us for legal advice on day-to-day banking and finance issues, as well as complex transactions and regulatory matters. With rights of audience in local Courts, we are also the first port of call for any litigious matters.</p>
            <p><strong>Services</strong></p>
            <ul>
            <li>Corporate lending</li>
            <li>Islamic finance</li>
            <li>Structured and asset finance</li>
            <li>Project finance</li>
            <li>Fund formation &amp; structuring</li>
            <li>FinTech</li>
            <li>Cryptocurrency &amp; blockchain</li>
            <li>Financial restructuring, bankruptcy &amp; insolvency</li>
            <li>Financial litigation</li>
            <li>Fraud</li>
            <li>Trade &amp; export finance</li>
            <li>Financial regulatory</li>
            <li>Private equity and venture capital</li>
            </ul>
            <blockquote>"They are responsive, quick on the uptake, and gets what the client needs."</blockquote>
            <p>Chambers FinTech Legal 2022</p>
            ','post_date' => '2014-01-24 01:08:53','post_name' => 'banking-finance-law','post_parent' => '27','Image' => NULL,'Categories' => NULL,'Tags' => NULL),

            array(
                'ID' => '5170',
                'type' => 'industry',
                'post_title' => 'Energy',
                'image' => '2018/07/shutterstock_551422339.jpg','post_content' => '
            <p>The Middle East is a key market for Energy Transition with a particular focus on renewables, decarbonization and pursuing radical regulatory change. From electric vehicles to solar panel farms, and the step away from traditional power sources, the region is investing into cleaner fuels and new technologies.</p>
            <p>Our multi-disciplinary team of lawyers are on hand to assist our clients with their energy transition strategies, providing not only legal and regulatory advice, but also risk advisory services, divestment and expansion planning, and industry insight. Our team have the knowledge and understanding needed to lead on your energy projects, from M&amp;A, to government tenders, to litigation; we provide our energy clients with on-the-ground specialist service.</p>
            <p>The legal and regulatory frameworks for energy are constantly evolving &ndash; our relationships with local authorities and regulators mean that you stay one step ahead. We can be your trusted partner to navigate potential challenges and to ensure the success of your objectives.</p>
            <p><strong>Services</strong></p>
            <ul>
            <li>Regulatory &amp; compliance</li>
            <li>M&amp;A</li>
            <li>Divestures</li>
            <li>Digital infrastructure</li>
            <li>Construction and engineering</li>
            <li>Energy litigation and arbitration</li>
            <li>eMobility</li>
            <li>Environment, social and governance matters</li>
            <li>Corporate governance</li>
            <li>Risk advisory</li>
            <li>Project finance</li>
            <li>Intellectual property</li>
            <li>Procurement &amp; government tenders</li>
            <li>Legal due diligence</li>
            </ul>
            <blockquote>"They have a very good team. They are really supportive and know the ins and outs."</blockquote>
            <p>Chambers Global 2022</p>
        ','post_date' => '2018-03-12 18:04:39','post_name' => 'energy-law','post_parent' => '27','Image' => NULL,'Categories' => NULL,'Tags' => NULL),


            array(
                'ID' => '267',
                'type' => 'industry',
                'post_title' => 'Insurance',
                'image' => '2018/07/shutterstock_1043834362.jpg','post_content' => '<p>BSA is the Middle East&rsquo;s premier regional insurance and reinsurance law firm. Providing our clients with advisory, transactional and dispute resolution services, we provide a holistic service with a multi-disciplinary team of industry experts.</p>
            <p>Our practice doesn&rsquo;t just enable our clients to prosper. We help to shape the market. With one of the largest Insurance/Reinsurance teams in the Middle East, we advise regulators and national governments on legislative developments for the insurance sector and assist legislative bodies in drafting insurance laws.</p>
            <p>We&rsquo;re also involved in some of the region&rsquo;s biggest and most groundbreaking deals. Our clients range from local companies to multinational corporations, for whom we advise on market penetration throughout Bahrain, Oman, Jordan, Beirut, Kuwait and the Kingdom of Saudi Arabia.</p>
            <p>Our portfolio covers both conventional and Islamic insurance. We also have the ability and resources to advise, develop, structure and document Shari&rsquo;a compliant Takaful and Re-Takaful products, as well as providing a speciality line of insurance. This includes event cancellation, offering expert advice for complex claims handling and providing regulatory and legal advice for underwriters (including Lloyds of London syndicates).</p>
            <p><strong>Services</strong></p>
            <p><b>Insurance and Reinsurance (non-contentious)</b></p>
            <ul>
            <li>Regulatory and operational advice;</li>
            <li>Reviewing and preparing commercial agreements;</li>
            <li>Fronting and reinsurance partnerships;</li>
            <li>Structuring start-up operations;</li>
            <li>Product development and review.</li>
            </ul>
            <p><b>Dispute Resolution</b></p>
            <ul>
            <li>Litigation;</li>
            <li>Arbitration;</li>
            <li>Alternative dispute resolution.</li>
            </ul>
            <p><b>Coverage issues</b></p>
            <ul>
            <li>Property;</li>
            <li>Marine;</li>
            <li>Financial lines;</li>
            <li>Reinsurance;</li>
            <li>Life and health.</li>
            </ul>
            <p><b>Corporate and commercial</b></p>
            <ul>
            <li>Foreign investment;</li>
            <li>Entity formation;</li>
            <li>Licensing and structuring;</li>
            <li>Mergers and acquisitions, joint ventures;</li>
            <li>Regulatory investigations.</li>
            </ul>
            <p>&nbsp;</p>
            <p><b>InsurTech</b></p>
            <ul>
            <li>Strategy advice;</li>
            <li>Regulatory guidance and support;</li>
            <li>Investment agreements and funding support;</li>
            <li>Licensing and compliance support.</li>
            </ul>
            <p>&nbsp;</p>
            <p><b>Cyber</b></p>
            <ul>
            <li>First response and incident management;</li>
            <li>Legal advice and coverage analysis;</li>
            <li>PR and reputation management;</li>
            <li>Claims handling.</li>
            </ul>
            <blockquote>"The insurance team is user-friendly, pro-active and have a thorough knowledge of insurance law across the region. They provide a great service to foreign lawyers who need advice on the application of local law to losses and claims which occur in the region."</blockquote>
            <p>Legal 500 2021</p>','post_date' => '2014-01-24 02:14:25','post_name' => 'insurance-reinsurance-law','post_parent' => '27','Image' => NULL,'Categories' => NULL,'Tags' => NULL),

            array(
                'ID' => '',
                'type' => 'industry',
                'post_title' => 'Financial Institutions',
                'image' => '',
                'post_content' => '<p>We are one of the leading law firms advising financial institutions in the Middle East. Our team handles complex, high-value financings for clients and are at the forefront of new technologies and regulatory changes impacting the industry.</p>
                <p>Our bankruptcy and insolvency lawyers advise on some of the largest cases in the region. Acting on behalf of both debtors and creditors, we find our clients solutions to their NPLs, underfunded liabilities, and other areas of financial distress, by considering all options available under the new bankruptcy law in relation to insolvency, preventative composition, bankruptcy and liquidation.</p>
                <p>We provide services across the full spectrum of financial technology and we benefit from the ability to draw upon our extensive wider corporate, insurance, regulatory and financial experience to address the various micro-industries that form the FinTech ecosystem. We advise financial institutions, venture capitalists and start-ups on maximising technological innovation, enhancing and safeguarding their trade secrets and tech, and ensuring compliance with all local and international laws.</p>
                <p>Working within an ever-changing regulatory ecosystem, both from a regulatory and technological perspective, requires us to be in touch with various stakeholders. We regularly interact with financial regulators in various capacities: (1) when providing comments on draft legislation when and if we are brought on board in a consultatory capacity, (2) when approaching regulators on behalf of clients to enquire on the applicability and malleability of the current regulatory framework, and (3) when attempting to match new products and services to existing legislation.</p>
                <p><strong>Services</strong></p>
                <ul>
                <li>FinTech</li>
                <li>Payments &amp; digital currencies</li>
                <li>Investment Funds</li>
                <li>Financial regulatory and compliance</li>
                <li>Financial restructuring, bankruptcy and insolvency</li>
                <li>Project financings</li>
                <li>Corporate financings</li>
                <li>Asset and trade finance</li>
                <li>Insurance and reinsurance</li>
                <li>Licensing</li>
                <li>Corporate governance</li>
                <li>Private equity and venture capital</li>
                <li>High Net Worth Individuals and private financing</li>
                <li>Wealth management</li>
                <li>Cybersecurity &amp; data protection</li>
                <li>Financial litigation</li>
                <li>Commercial fraud</li>
                </ul>
                <blockquote>"BSA are very good; commercially minded, and have good knowledge of the market.
                 They are sound in their advice and easy to use."</blockquote>
                <p>Chambers FinTech Legal 2022</p>
                ',
                'post_date' => '2020-12-22 19:08:04',
                'post_name' => 'regulatory-and-compliance',
                'post_parent' => '',
                'Image' => NULL,
                'Categories' => NULL,
                'Tags' => NULL
            ),

            array(
                'ID' => '252',
                'type' => 'industry',
                'post_title' => 'Construction and Engineering',
                'image' => '2018/07/BSA-WEBSITE-HOMEPAGE-IMAGES-New-Corporate.jpg','post_content' => '<p>Our Construction practice is driven by some of the most experienced legal professionals in construction law. Providing the full spectrum of legal support to the industry, we offer intuitive advice, insight as well as adaptive exit routes to the complex legal scenarios often encountered in this challenging sector.</p>
                <p>As a result of the Middle East&rsquo;s vision of growth and development, construction is the main source of wealth and activity in the region. Our dedicated team of experts provide an end-to-end service covering a construction project across the full life cycle. We provide responsive, commercial and efficient legal support to ensure success with a multi-disciplinary team of industry experts.</p>
                <p>We pride ourselves in possessing one of the most experienced teams in the Middle East on construction and engineering related disputes. We have extensive experience in large-scale and complex matters faced by owners, developers, contractors, lenders, and sub-contractors and can help you successfully navigate your local judicial system&rsquo;s pitfalls. We work with specialist claim analysts to build strong submissions and ensure that our clients&rsquo; position is substantively convincing and procedurally unassailable. We collaborate with some of the region&rsquo;s most experienced and effective independent delay, engineering and quantum experts to bolster the evidential weight of our client&rsquo;s position and ensure they receive the best quality of services in legal consultancy and representation. We also provide services of ratification on foreign judgments and arbitration awards in both English and Arabic.</p>
                <p><strong>Services</strong></p>
                <ul>
                <li>Arbitration</li>
                <li>Litigation and alternative dispute resolution</li>
                <li>Drafting and negotiating construction contracts</li>
                <li>Risk advisory</li>
                <li>Due diligence reviews</li>
                <li>Joint venture contract review and advice</li>
                <li>Pre and post tender contract and subcontract review</li>
                <li>Architectural, design and engineering contract review and drafting</li>
                <li>Arbitral Award Enforcement</li>
                <li>Construction procurement advice</li>
                </ul>
                <blockquote>"They are straight forward and accurate in their legal opinions and advice."</blockquote>
                <p>Chambers Global 2021</p>
            ','post_date' => '2014-01-24 01:53:37','post_name' => 'construction-law','post_parent' => '27','Image' => NULL,'Categories' => NULL,'Tags' => NULL),


            array(
                'ID' => '274',
                'type' => 'industry',
                'post_title' => 'Real Estate',
                'image' => '2018/07/shutterstock_526689706.jpg',
                'post_content' => '<p>Middle East real estate is one of the fastest growing sectors in the world. Overseas developers have invested billions of dollars into Oman, Qatar, Dubai and Saudi Arabia, which has pushed the growth of the market in areas such as retail, residential and hospitality.</p>
                <p>Our Real Estate team forms one of the largest, most specialised legal teams supporting the region&rsquo;s most active industry. We can help you successfully conclude every type of transaction, development or investment in the region. Whether you&rsquo;re an individual purchaser, developer, financial institution, contractor, real estate broker or government body, we understand your position and can provide innovative, commercially focused advice on the most complex issues.</p>
                <p>We understand all types of real estate and property related matters including disputes stemming from the acquisition of commercial, residential and mixed-use real estate; disputes between property and joint venture partners; disputes between developers and investors; and disputes arising from the sale and purchase transactions. Utilizing the latest legal precedents, we make sure you achieve a timely and cost-effective resolution on your litigation or pre-litigation settlement.</p>
                <p><strong>Services </strong></p>
                <ul>
                <li>Real estate M&amp;A</li>
                <li>Joint ventures</li>
                <li>Licensing and registration with the relevant authorities</li>
                <li>Escrow advice</li>
                <li>Real estate financing options</li>
                <li>Real estate operations &amp; management</li>
                <li>Residential, retail and mixed-use developments and projects</li>
                <li>Hotels, leisure &amp; hospitality</li>
                <li>Tenancy advice and disputes advice</li>
                <li>Real estate litigation</li>
                <li>Strata Law document and compliance</li>
                </ul>
                <blockquote>"BSA are very good; commercially minded."</blockquote>
                <p>Chambers Global 2021</p>
                ','post_date' => '2014-01-24 02:18:28','post_name' => 'real-estate-law','post_parent' => '27','Image' => NULL,'Categories' => NULL,'Tags' => NULL),


            array(
                'ID' => '',
                'type' => 'industry',
                'post_title' => 'Life Sciences & Healthcare',
                'image' => '',
                'post_content' => '<p>With extensive experience of advising clients in the life sciences &amp; healthcare sector, we understand the key issues and motivators facing medical organisations today. Our team consists of experts in M&amp;A, regulatory matters, commercial contracts, real estate, IP, insurance, professional conduct, defence medical work, local litigation and alternative dispute resolution.</p>
                    <p>The experienced lawyers who comprise our multifaceted healthcare team act for numerous clients across the healthcare sector, including hospitals and health systems, academic medical centers, retail pharmacies and wholesale distributors, multi-disciplinary physicians groups and health insurers. We advise our clients on all aspects of healthcare law, including transactions, telemedicine, private financings, insurance coverage and reimbursement, medical liability claims, healthcare fraud and abuse issues, clinical integration, construction and licensing of healthcare facilities and labor and employment matters.</p>
                    <p>Our unique position of being able to seamlessly link Middle Eastern jurisdictions has allowed us to fully support clients looking to operate across multiple regions. Recent experience includes collaborating with NHS bodies looking to expand into the Middle East, and supporting innovative tech start-ups across the region in offering services in the US and Europe. We are also driving the development of pioneering value-based approaches for organisations across Europe, and helping clients in the US to establish operations overseas.</p>
                    <p><strong>Services</strong></p>
                    <ul>
                    <li>Regulatory and compliance</li>
                    <li>Antitrust and competition</li>
                    <li>Risk advisory &amp; crisis management</li>
                    <li>Construction and operational contracts</li>
                    <li>Supply and distribution agreements</li>
                    <li>Intellectual property</li>
                    <li>Medical malpractice claims</li>
                    <li>Insurance and reinsurance</li>
                    <li>M&amp;A</li>
                    <li>Joint ventures</li>
                    <li>Distribution agreements</li>
                    <li>Dispute resolution</li>
                    <li>Employment &amp; labor matters</li>
                    </ul>
                    <blockquote>"The team is user-friendly, pro-active and have a thorough knowledge."</blockquote>
                    <p>Legal 500 2021</p>
                    ',
                'post_date' => '2020-12-22 19:08:04',
                'post_name' => 'life-sciences-and-healthcare',
                'post_parent' => '',
                'Image' => NULL,
                'Categories' => NULL,
                'Tags' => NULL
            ),

            array(
                'ID' => '',
                'type' => 'industry',
                'post_title' => 'Retail & Consumer Goods',
                'image' => '',
                'post_content' => '<p>From international e-commerce platforms to large regional developers, we advise the key players within the retail &amp; consumer goods market across the Middle East. Our diverse team of multi-disciplinary lawyers provide hands-on and commercial advice, covering all of our clients&rsquo; legal needs. From protecting your brand to international expansion, as well as the day-to-day, we are one of the regions&rsquo; most trusted law firms in the retail sector.</p>
                <p>We regularly provide legal and commercial advice in regard to joint venture agreements, local partner arrangements, franchising, licensing, intellectual property matters, real estate, as well as dispute resolution matters. We have rights of audience before all local courts within the jurisdictions we operate in, and we have strong relationships with local regulators.</p>
                <p>We are often the port of call for multinational, luxury brands with their expansion plans into the Middle East. Our understanding of the retail &amp; consumer goods market in the Middle East is unrivalled, meaning from set-up to local partner agreements to regulatory and compliance, our team acts as a trusted, commercial partner on-the-ground, where you need us.</p>
                <p><strong>Services</strong></p>
                <ul>
                <li>Real estate, leasing &amp; tenancy</li>
                <li>M&amp;A</li>
                <li>Joint ventures</li>
                <li>Restructuring &amp; insolvency</li>
                <li>Regulatory &amp; compliance</li>
                <li>Intellectual property</li>
                <li>Franchising &amp; licensing</li>
                <li>Dispute resolution</li>
                <li>Employment &amp; labor matters</li>
                <li>Corporate governance</li>
                <li>Risk analysis and strategy development</li>
                <li>Construction and operational contracts</li>
                </ul>
                ',
                'post_date' => '2020-12-22 19:08:04',
                'post_name' => 'retail-and-consumer-goods',
                'post_parent' => '',
                'Image' => NULL,
                'Categories' => NULL,
                'Tags' => NULL
            ),
            array(
                'ID' => '',
                'type' => 'industry',
                'post_title' => 'International Trade, Transport & Maritime ',
                'image' => '',
                'post_content' => '<p>Our International Trade, Transport &amp; Maritime team offer clients a comprehensive understanding of the laws and regulations impacting international business and logistics. Advising owners, operators, financial institutions, insurers, vessel and airline operators, commodity traders and charterers on the full range of legal services, both contentious and non-contentious.</p>
                    <p>Our clients include regional and international market players who require specialist legal advice and on-the-ground regulatory expertise. We provide technical advice commercially, ensuring that no matter the challenge, a successful outcome is achieved.</p>
                    <p>Shipping litigation is a speciality field in which we excel. We regularly represent transportation companies, port terminals and authorities, and warehouses on a range of different litigation matters arising from M&amp;A, employment, insurance, building and financing transactions, including non-maritime litigation arising from these transactions. Whatever your needs, we&rsquo;ll meet them.</p>
                    <p><strong>Services</strong></p>
                    <ul>
                    <li>Dispute resolution: arbitration &amp; litigation</li>
                    <li>Distribution and importation arrangements</li>
                    <li>M&amp;A</li>
                    <li>Joint ventures</li>
                    <li>Asset and trade financing &amp; acquisition</li>
                    <li>Regulatory &amp; compliance</li>
                    <li>Trade law</li>
                    <li>Insurance and reinsurance</li>
                    <li>Sanctions</li>
                    <li>Leasing agreements</li>
                    <li>Financing options</li>
                    <li>Risk advisory &amp; crisis management</li>
                    </ul>
                    ',
                'post_date' => '2020-12-22 19:08:04',
                'post_name' => 'international-trade-transport-and-maritime ',
                'post_parent' => '',
                'Image' => NULL,
                'Categories' => NULL,
                'Tags' => NULL
            ),

            array(
                'type' => 'industry',
                'ID' => '5104','post_title' => 'Technology, Media & Telecommunications (TMT) ','image' => '2018/07/shutterstock_236889052.jpg','post_content' => '<p>The TMT sector continues to prosper in the Middle East. Whether you are an experienced player or a disruptive start-up looking to enter the Middle East market, our experienced TMT lawyers can help you. We&rsquo;ve worked on some of the biggest TMT cases the region has seen and can use this experience to guide your business safely through this constantly evolving area of law, ensuring your interests are protected at all times.</p>
                <p>Our multi-disciplinary team have deep regulatory expertise ensuring our clients are kept afresh of the latest legal frameworks impacting their business. We draw on a portfolio of experience, covering complex matters relating to digital &amp; data, advertising and marketing, film and television, internet and social media, entertainment, sports, and technology and telecommunications. We help our clients navigate the risks of digital disruption as well as the competitive opportunities afforded by investment in digital capital.</p>
                <p>We are recognized as being at the forefront of the Middle East TMT sector, providing insights on cutting-edge technology developments, unchartered areas of regulation, as well as providing a connective platform for our clients. We truly understand your business and will partner with you, providing invaluable, commercial legal advice directly contributing to your success.</p>
                <p><strong>Services</strong></p>
                <ul>
                <li>Ecommerce</li>
                <li>Cryptocurrencies and digital financial markets</li>
                <li>FinTech</li>
                <li>Internet of Things</li>
                <li>Big Data</li>
                <li>Regulatory and compliance</li>
                <li>M&amp;A</li>
                <li>Joint ventures</li>
                <li>Private equity and venture capital</li>
                <li>Intellectual property</li>
                <li>Dispute resolution &amp; litigation</li>
                <li>Public offerings</li>
                <li>Technology agreements</li>
                <li>Outsourcing &amp; consulting contracts</li>
                <li>Asset financing &amp; acquisition</li>
                <li>Infrastructure</li>
                <li>Cybersecurity &amp; data protection</li>
                <li>Construction &amp; engineering</li>
                <li>Risk advisory &amp; crisis management</li>
                </ul>
                <blockquote>"They are extremely approachable and explain the pros and cons of an action in a simple manner which can be further
                explained to business."</blockquote>
                <p>The Legal 500 2021</p>
                ','post_date' => '2018-02-25 11:54:28','post_name' => 'technology-media-and-telecommunications','post_parent' => '27','Image' => NULL,'Categories' => NULL,'Tags' => NULL),


            array(
                'type' => 'industry',
                'ID' => '',
                'post_title' => 'Leisure & Hospitality',
                'image' => '',
                'post_content' => '<p>Our team of leisure and hospitality lawyers advise on both contentious and non-contentious legal matters concerning large-scale sporting and music events, shopping &amp; entertainment complexes, cinemas &amp; theatres, convention centers &amp; conferences, hotels, restaurants and health &amp; fitness organisations. Our clients are regional powerhouses, multinational entertainment brands, as well as SMEs and start-ups. From the World Cup to the newest hotel development, our team have the expertise to get the job done.</p>
                    <p>Understanding the local legal and regulatory framework is key for all leisure &amp; hospitality industry business owners. Our team provide a one-stop-shop offering transactional and advisory legal services, as well as robust defence and litigation in the Courts, should that be required.</p>
                    <p>Our deep understanding of local laws, as well as our strong relationships with the relevant authorities and regulators, mean our clients benefit from our holistic knowledge and market connections.</p>
                    <p><strong>Services</strong></p>
                    <ul>
                    <li>Regulatory and compliance</li>
                    <li>Distribution and management agreements</li>
                    <li>Intellectual property</li>
                    <li>Licensing</li>
                    <li>Real estate</li>
                    <li>M&amp;A and joint ventures</li>
                    <li>Corporate structuring and governance</li>
                    <li>Employment and labor</li>
                    <li>Dispute resolution</li>
                    <li>Construction and engineering</li>
                    <li>Financing options</li>
                    </ul>
                    ',
                'post_date' => '2020-12-22 19:08:04',
                'post_name' => 'leisure-and-hospitality',
                'post_parent' => '',
                'Image' => NULL,
                'Categories' => NULL,
                'Tags' => NULL
            ),
            array(
                'type' => 'industry',
                'ID' => '',
                'post_title' => 'Education',
                'image' => '',
                'post_content' => '<p>From regulatory and compliance to intellectual property, construction, financing, and litigation, we provide our clients in the education sector with a holistic approach to legal services. We take our support a step further, offering our clients a trusted business partner who proactively share your ambitions, plans and objectives.</p>
                    <p>We advise education providers including colleges, universities, schools, training academies and digital platforms on both contentious and non-contentious issues, including establishment, operations, employment and HR matters, as well as corporate advisory matters, policies and procedures. We also advise private equity houses and other investment agencies on the financing and acquisition of education portfolios across the Middle East and internationally.</p>
                    <p>We partner with our technology lawyers to provide legal and regulatory advice to EdTech providers, providing risk advisory and compliance advice. We also have rights of audience before the local Courts and are able to provide robust litigation services when needed.</p>
                    <p><strong>Services</strong></p>
                    <ul>
                    <li>EdTech</li>
                    <li>Cybersecurity and data protection</li>
                    <li>Regulatory and compliance</li>
                    <li>Technology contracts and IT procurement</li>
                    <li>Employment and labor</li>
                    <li>M&amp;A</li>
                    <li>Joint ventures</li>
                    <li>Corporate governance and advisory</li>
                    <li>Construction and infrastructure</li>
                    <li>Operation and management services</li>
                    <li>Real estate and tenancy</li>
                    <li>Financing</li>
                    <li>Private equity and venture capital</li>
                    <li>Dispute resolution</li>
                    </ul>
                    ',
                'post_date' => '2020-12-22 19:08:04',
                'post_name' => 'education',
                'post_parent' => '',
                'Image' => NULL,
                'Categories' => NULL,
                'Tags' => NULL
            ),
            array(
                'ID' => '',
                'type' => 'services',
                'post_title' => 'Environment, social and governance',
                'image' => '',
                'post_content' => '<p>Legal, regulatory, and ethical frameworks regarding ESG matters are evolving at a fast pace across the Middle East. Our multidisciplinary team of knowledge leaders utilize our relationships with key industry players, government entities and regulatory authorities to keep afresh of change to guide our clients&rsquo; as they work towards a sustainable future.</p>
                <p>Our lawyers advise multinationals, regional powerhouses, start-ups and SMEs, as well as government authorities and financial institutions on their ESG risks, due diligence, corporate analysis and governance matters, green and sustainable investments, digital and data responsibility and cybersecurity as well as dispute resolution matters across the Middle East. We advise on the most pressing challenges facing our clients today and in the future, as the business environment across the region evolves and organisations seek a positive change to outdated practices and considerations.</p>
                <p>We guide our clients through their business transitions, partnering with them to navigate the legal and regulatory landscape, keeping always in mind their business objectives and seeking the best solutions to create sustainable progress.</p>
                <p><strong>Services </strong></p>
                <ul>
                <li>ESG investments</li>
                <li>Green and sustainable loan structuring</li>
                <li>M&amp;A</li>
                <li>Joint ventures</li>
                <li>Employment and HR</li>
                <li>Corporate governance</li>
                <li>Cybersecurity and data protection</li>
                <li>Regulatory and compliance</li>
                <li>Litigation</li>
                <li>Risk advisory and strategic analysis</li>
                <li>Insurance</li>
                <li>Environmental law</li>
                </ul>',
                'post_date' => '2020-12-22 19:08:04',
                'post_name' => 'environment-social-and-governance',
                'post_parent' => '',
                'Image' => NULL,
                'Categories' => NULL,
                'Tags' => NULL
            ),
            array(
                'ID' => '',
                'type' => 'groups',
                'post_title' => 'Private Client',
                'image' => '',
                'post_content' => '<p>Our lawyers provide our private clients and individuals with a pragmatic approach to legal services, removing burden, resolving complexities, and providing a personal service to guide you through any challenge. We understand that you need a straightforward solution, and you can trust us to support you every step of the way.</p>
                <p>With a team of lawyers covering 22 languages, we provide best-in-region legal support, communicated clearly, with flexible and achievable fee structures. We will seek the best possible outcome for your situation, keeping you afresh of developments and showing the utmost care and consideration to your needs. We understand that seeking legal counsel is often your last resort so our primary goal is to efficiently and effectively achieve a solution to your latest challenge.</p>
                <p><strong>Services</strong></p>
                <ul>
                <li>Family law matters</li>
                <li>Wills &amp; probate</li>
                <li>Tax &amp; trusts</li>
                <li>Criminal procedures</li>
                <li>Real estate &amp; tenancy agreements</li>
                <li>Immigration</li>
                <li>Wealth management</li>
                <li>Estate and administration planning</li>
                <li>Asset protection</li>
                <li>Investment structuring</li>
                <li>Employment</li>
                <li>Insurance</li>
                </ul>',
                'post_date' => '2020-12-22 19:08:04',
                'post_name' => 'private-client',
                'post_parent' => '',
                'Image' => NULL,
                'Categories' => NULL,
                'Tags' => NULL
            ),
            array(
                'type' => 'groups',
                'ID' => '',
                'post_title' => 'High Net Worth Individuals',
                'image' => '',
                'post_content' => '<p>Our high net worth clients require unique and specific legal services to ensure the protection and growth of their assets and investments. At BSA, we provide our HNW clients with the personal service they require, partnering with them to provide clear solutions to their challenges. We are your trusted legal advisor who share your goals and ambitions for success.</p>
                <p>Our personal approach to providing legal services means we work with you, providing clear and straight-forward solutions with flexible fee arrangements, designed to remove the burden from your shoulders. We will provide you with a relationship partner who will focus on your business challenges, utilizing our wider teams as required, and demonstrating a clear roadmap to achieve your goals.</p>
                <p>With BSA, you can trust you will receive the best legal service, with advice delivered clearly and in a straightforward manner, and an authentic partner who will take the utmost care and responsibility of your portfolio.</p>
                <p><strong>&nbsp;Services</strong></p>
                <ul>
                <li>Wealth portfolio management</li>
                <li>Tax &amp; trusts</li>
                <li>Investment structuring &amp; management</li>
                <li>Art &amp; Antiques</li>
                <li>Estate and administration planning</li>
                <li>Asset protection</li>
                <li>Wills &amp; probate</li>
                <li>Real estate &amp; tenancy agreements</li>
                <li>Immigration</li>
                <li>Employment</li>
                <li>Family law matters</li>
                <li>Succession planning</li>
                <li>Criminal law &amp; fraud</li>
                <li>Aviation</li>
                <li>Insurance</li>
                </ul>',
                'post_date' => '2020-12-22 19:08:04',
                'post_name' => 'high-net-worth-individuals',
                'post_parent' => '',
                'Image' => NULL,
                'Categories' => NULL,
                'Tags' => NULL
            ),
            array(
                'ID' => '',
                'type' => 'groups',
                'post_title' => 'Family-owned Businesses',
                'image' => '',
                'post_content' => '<p>Our family business practice specializes in providing clear and practical advice to the regions&rsquo; largest conglomerates, SMEs and start-ups. We understand the unique nuances of leading and operating a family business and provide the full range of legal services designed to support you at every stage of your journey.</p>
                <p>Our team of highly experienced family business lawyers bring together skills from a variety of disciplines. Whatever your legal need, we can support you, from investment structuring and management, to establishment and set-up, acquisition advice and employment matters, our team works with you to ensure you have the most robust business support and protection.</p>
                <p>&nbsp;</p>
                <p><strong>Services:</strong></p>
                <ul>
                <li>Brand &amp; reputation management</li>
                <li>Corporate structuring &amp; restructuring</li>
                <li>Succession planning</li>
                <li>Dispute resolution &amp; litigation</li>
                <li>Acquisitions &amp; divestures</li>
                <li>Asset management</li>
                <li>Wealth management</li>
                <li>Tax &amp; trusts</li>
                <li>Corporate governance</li>
                <li>Cybersecurity &amp; data protection</li>
                <li>Employment &amp; HR</li>
                <li>Insurance</li>
                <li>Real estate &amp; tenancy</li>
                </ul>',
                'post_date' => '2020-12-22 19:08:04',
                'post_name' => 'family-owned-businesses',
                'post_parent' => '',
                'Image' => NULL,
                'Categories' => NULL,
                'Tags' => NULL
            ),
        ];

        foreach($data as $item){
            $new['title'] = $item['post_title'];
            $new['content'] = $item['post_content'];
            $new['date'] = $item['post_date'];
            $slug = $item['post_name'];

            $slugLoop = 1;

            while(Service::where('slug',$slug)->count()>0){
                $slug = $slug . '-' . $slugLoop;
                $slugLoop++;
            }

            $new['photo'] = $item['image'];
            $new['slug'] = $slug;
            $new['service_type_id'] = ServiceType::where('slug',$item['type'])->first()->id;

            $article = Service::create($new);
        }
    }
}
