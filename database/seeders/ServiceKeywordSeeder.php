<?php

namespace Database\Seeders;

use App\Models\Service;
use Illuminate\Database\Seeder;

class ServiceKeywordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = [
            array('id' => '1','slug' => 'corporate-law-and-ma','keywords' => 'Corporate Law and M&A, Mergers and Acquisitions, Corporate Law'),
            array('id' => '2','slug' => 'commercial-law','keywords' => 'Commercial Law Firm, Commercial Companies Laws in the Middle East, '),
            array('id' => '3','slug' => 'regulatory-and-compliance','keywords' => NULL),
            array('id' => '4','slug' => 'employment-law-firm','keywords' => 'Employment Law Firm, Labour Law'),
            array('id' => '5','slug' => 'cybersecurity','keywords' => 'Cybersecurity Law Firm, Cyber Laws in the Middle East'),
            array('id' => '6','slug' => 'litigation-law-firm','keywords' => 'Litigation Law Firm, Litigation'),
            array('id' => '7','slug' => 'arbitration-dispute-resolution','keywords' => 'Arbitration and Dispute Resolution Law Firm, Dispute Resolution'),
            array('id' => '8','slug' => 'intellectual-property-law','keywords' => 'Intellectual Property Law Firm, Intellectual Property Laws in the Middle East'),
            array('id' => '9','slug' => 'banking-finance-law','keywords' => 'Banking & Finance Law Firm, Banking Law Firm, Finance Law Firm'),
            array('id' => '10','slug' => 'energy-law','keywords' => 'Energy Law Firm, Energy Law Firm'),
            array('id' => '11','slug' => 'insurance-reinsurance-law','keywords' => 'Insurance and Reinsurance Law Firm, Insurance Authority'),
            array('id' => '12','slug' => 'regulatory-and-compliance-1','keywords' => NULL),
            array('id' => '13','slug' => 'construction-law','keywords' => 'Construction Law Firm, Construction Laws in the Middle East'),
            array('id' => '14','slug' => 'real-estate-law','keywords' => 'Real Estate Law Firm, Real Estate Laws in the Middle East'),
            array('id' => '15','slug' => 'life-sciences-and-healthcare','keywords' => NULL),
            array('id' => '16','slug' => 'retail-and-consumer-goods','keywords' => NULL),
            array('id' => '17','slug' => 'international-trade-transport-and-maritime ','keywords' => 'Maritime Law Firm, Maritime Laws in the Middle East'),
            array('id' => '18','slug' => 'technology-media-and-telecommunications','keywords' => 'Technology, Media & Telecommunications (TMT) Law, Media and Telecommunications Laws in the Middle East, Information Technology Law Firm'),
            array('id' => '19','slug' => 'leisure-and-hospitality','keywords' => NULL),
            array('id' => '20','slug' => 'education','keywords' => NULL),
            array('id' => '21','slug' => 'environment-social-and-governance','keywords' => NULL),
            array('id' => '22','slug' => 'private-client','keywords' => NULL),
            array('id' => '23','slug' => 'high-net-worth-individuals','keywords' => NULL),
            array('id' => '24','slug' => 'family-owned-businesses','keywords' => NULL)
        ];

        foreach ($services as $service){
            $target = Service::find($service['id']);

            if($target){
                if($service['keywords']){
                    $target->keywords = $service['keywords'];
                    $target->save();
                }
            }
        }
    }
}
