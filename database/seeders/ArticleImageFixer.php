<?php
namespace Database\Seeders;
use App\Models\Article;
use Illuminate\Database\Seeder;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ArticleImageFixer extends Seeder
{

    public function run()
    {
        $data = Article::orderBy('date','DESC')->get();

        foreach($data as $article){
            if($article->photo){

                if(strpos($article->photo, 'articles-optimized') !== false){

                } else {
                    if(strpos($article->photo, 'https://bsabh.com/') !== false)
                        $article->photo = trim($article->photo, "https://bsabh.com/");

                    $image =  new UploadedFile( public_path($article->photo), 'tmp.jpg', 'image/jpeg',null,true);

                    $destinationPath = 'public/uploads/articles-optimized';
                    $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

                    Image::make($image->getRealPath())->fit(300,200)->save($destinationPath.'/'.$newFileName);
                    $article->photo = 'uploads/articles-optimized/'. $newFileName;

                    $image =  new UploadedFile( public_path($article->photo), 'tmp.jpg', 'image/jpeg',null,true);
                    $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

                    Image::make($image->getRealPath())->fit(1296, 332)->save($destinationPath.'/'.$newFileName);
                    $article->photo_full = 'uploads/articles-optimized/'. $newFileName;

                    $article->save();
                }
            }
        }
    }
}
