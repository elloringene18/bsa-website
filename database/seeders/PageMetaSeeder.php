<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PageMetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [

            'home' => [
                [
                    'title' => 'Meta Title',
                    'content' => 'BSA Ahmad Bin Hezeem & Associates LLP',
                    'type' => 'text',
                ],
                [
                    'title' => 'Meta Description',
                    'content' => 'We help people do business all over the Middle East. We have offices in Dubai, UAE, Saudi Arabia, Oman, Lebanon and Iraq.',
                    'type' => 'text',
                ],
            ],

            //----------------------------------------------------- ABOUT --------------------------------------------


            'about' => [
                [
                    'title' => 'Meta Title',
                    'content' => 'About Us - A Middle East Law Firm | Legal careers | BSA Dubai',
                    'type' => 'text',
                ],
                [
                    'title' => 'Meta Description',
                    'content' => 'We help people do business all over the Middle East. We have offices in Dubai, UAE, Saudi Arabia, Oman, Lebanon and Iraq.',
                    'type' => 'text',
                ],
            ],

            //----------------------------------------------------- LOCATIONS --------------------------------------------


            'locations' => [
                [
                    'title' => 'Meta Title',
                    'content' => 'Locations',
                    'type' => 'text',
                ],
                [
                    'title' => 'Meta Description',
                    'content' => '',
                    'type' => 'text',
                ],
            ],

            //----------------------------------------------------- OUR PEOPLE --------------------------------------------

            'our-people' => [
                [
                    'title' => 'Meta Title',
                    'content' => 'Lawyers and Advocates',
                    'type' => 'text',
                ],
                [
                    'title' => 'Meta Description',
                    'content' => '',
                    'type' => 'text',
                ],
            ],

            //----------------------------------------------------- KNOWLEDGE --------------------------------------------

            'knowledge' => [
                [
                    'title' => 'Meta Title',
                    'content' => 'Knowledge Hub',
                    'type' => 'text',
                ],
                [
                    'title' => 'Meta Description',
                    'content' => '',
                    'type' => 'text',
                ],
            ],

            //----------------------------------------------------- CAREERS --------------------------------------------

            'careers' => [
                [
                    'title' => 'Meta Title',
                    'content' => 'Legal careers | Legal advice | BSA Dubai',
                    'type' => 'text',
                ],
                [
                    'title' => 'Meta Description',
                    'content' => 'BSA Ahmad Bin Hezeem & Associates LLP seeks sustainable growth through the recruitment of talented lawyers, support staff and other professionals.',
                    'type' => 'text',
                ],
            ],

            //----------------------------------------------------- CONTACT --------------------------------------------

            'contact' => [
                [
                    'title' => 'Meta Title',
                    'content' => 'Contact BSA',
                    'type' => 'text',
                ],
                [
                    'title' => 'Meta Description',
                    'content' => '',
                    'type' => 'text',
                ],
            ],

        ];

        foreach($data as $key=>$sub){
            foreach($sub as $item){
                $item['page'] = $key;
                $item['is_visible'] = isset($item['is_visible']) ? $item['is_visible'] : 1;
                $item['type'] = isset($item['type']) ? $item['type'] : 'html';
                $item['slug'] = \Illuminate\Support\Str::slug($item['title']);

                \App\Models\PageSection::create($item);
            }
        }
    }
}
