<?php

namespace Database\Seeders;

use App\Models\Promo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class PromoboxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $promos = array(
            array(
                'title' => 'News',
                'heading' => 'Read More',
                'subheading' => 'Stay up to date with the latest news',
                'content' => NULL,
                'link_text' => 'Go to page',
                'link' => 'https://bsabh.com/knowledge-hub',
                'image' => NULL,
                'type' => 'long'
            ),
            array(
                'title' => 'Contact Us',
                'heading' => 'CONTACT US',
                'subheading' => 'Got a question or enquiry?',
                'content' => NULL,
                'link_text' => 'Go to page',
                'link' => 'https://bsabh.com/contact',
                'image' => NULL,
                'type' => 'long'
            ),
            array(
                'title' => 'Our People',
                'heading' => 'VISIT US',
                'subheading' => 'Stop and meet the team',
                'content' => NULL,
                'link_text' => 'Go to page',
                'link' => 'https://bsabh.com/our-people',
                'image' => NULL,
                'type' => 'long'
            ),
            array(
                'title' => 'Newsletter',
                'heading' => 'NEWSLETTER',
                'subheading' => NULL,
                'content' => NULL,
                'link_text' => 'Subscribe',
                'link' => '#',
                'image' => NULL,
                'type' => 'newsletter'
            ),
            array(
                'title' => 'Social Media',
                'heading' => 'FOLLOW US',
                'subheading' => 'Follow us on social media',
                'content' => NULL,
                'link_text' => '#',
                'link' => '#',
                'image' => NULL,
                'type' => 'social'
            ),
        );

        foreach ($promos as  $item){
            $item['slug'] = Str::slug($item['title']);
            Promo::create($item);
        }
    }
}
