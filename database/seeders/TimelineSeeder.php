<?php

namespace Database\Seeders;

use App\Models\Timeline;
use App\Models\Title;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TimelineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('date' => '01-01-2001','content' => "BSA is established in Dubai, United Arab Emirates",'image' => 'img/timeline/1s.jpg'),
            array('date' => '23-05-2003','content' => "Managing Partner, Jimmy Haoula, joins BSA",'image' => 'img/timeline/2s.jpg'),
            array('date' => '16-01-2004','content' => "Abu Dhabi office opens",'image' => 'img/timeline/3.jpg'),
            array('date' => '23-05-2006','content' => "BSA is the first law firm to open in the Dubai International Financial Centre",'image' => 'img/timeline/4.jpg'),
            array('date' => '16-06-2006','content' => "Ras Al Khaimah office opens",'image' => 'img/timeline/5.jpg'),
            array('date' => '16-01-2007','content' => "Sharjah office opens",'image' => 'img/timeline/6.jpg'),
            array('date' => '16-01-2010','content' => "Beirut office opens",'image' => 'img/timeline/7.jpg'),
            array('date' => '16-01-2011','content' => "Muscat office opens",'image' => 'img/timeline/8.jpg'),
            array('date' => '16-01-2012','content' => "Erbil office opens",'image' => 'img/timeline/9.jpg'),
            array('date' => '16-01-2013','content' => "Rima Mrad is promoted to Partner, making BSA one of the first law firms to embrace diversity at a senior level",'image' => 'img/timeline/10.jpg'),
            array('date' => '16-01-2015','content' => "Dr. Ahmad Bin Hezeem joins as Senior Partner",'image' => 'img/timeline/11.jpg'),
            array('date' => '16-02-2015','content' => "The BSA Board is formed with 6 Equity Partners",'image' => 'img/timeline/12.jpg'),
            array('date' => '16-01-2019','content' => "BSA wins 'Regional Law Firm of the Year' at the Middle East Legal Awards",'image' => '/img/timeline/13.jpg'),
            array('date' => '16-02-2019','content' => "BSA wins 'Corporate Team of the Year' at the Oath Legal Awards",'image' => 'img/timeline/14.jpg'),
            array('date' => '16-01-2021','content' => "BSA celebrates 20 years of excellence in the Middle East",'image' => 'img/timeline/15s.jpg'),
            array('date' => '16-02-2021','content' => "BSA wins 'Regional Law Firm of the Year' at the Middle East Legal Awards",'image' => 'img/timeline/16.jpg'),
            array('date' => '16-03-2021','content' => "BSA wins 'Oman Team of the Year' at the Oath Legal Awards",'image' => '/img/timeline/17.jpg'),
            array('date' => '16-04-2021','content' => "BSA wins 'M&A Deal of the Year' at the IFLR Middle East Awards",'image' => 'img/timeline/18.jpg'),
        );

        foreach ($data as $item)
        {
            $item['date'] = Carbon::parse($item['date'])->format('Y-m-d');
            Timeline::create($item);
        }
    }
}
