<?php

namespace Database\Seeders;

use App\Models\Award;
use Illuminate\Database\Seeder;

class AwardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = array(
            array(
                'content' => 'Regional Law Firm of the Year, Middle East Legal Awards 2021 ',
                'image' => 'img/timeline/18.jpg',
            ),
            array(
                'content' => 'M&A Deal of the Year, IFLR Middle East Awards 2021',
                'image' => 'img/timeline/17.jpg',
            ),
            array(
                'content' => 'Oman Law Firm of the Year, The Oath Legal Awards 2021',
                'image' => 'img/timeline/16.jpg',
            ),
            array(
                'content' => 'Regional Law Firm of the Year, Middle East Legal Awards 2019',
                'image' => 'img/timeline/14.jpg',
            ),
            array(
                'content' => 'Corporate Team of the Year, The Oath Legal Awards 2019',
                'image' => 'img/timeline/13.jpg',
            ),
        );

        foreach ($data as $item)
        {
            Award::create($item);
        }
    }
}
