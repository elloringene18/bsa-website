<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ServiceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [ 'name' => 'Services', 'slug' => 'services' ],
            [ 'name' => 'Industry', 'slug' => 'industry' ],
            [ 'name' => 'Groups', 'slug' => 'groups' ],
        ];

        foreach ($data as $index => $item){
            \App\Models\ServiceType::create($item);
        }
    }
}
