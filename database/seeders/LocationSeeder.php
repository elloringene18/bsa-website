<?php

namespace Database\Seeders;

use App\Models\Location;
use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'term_id' => '77',
                'name' => 'Abu Dhabi, UAE',
                'slug' => 'abu-dhabi-uae',
                'term_group' => '0',
                'content' => '<p><b>Location:</b></p>
                                <p>Office No. 2204, Floor 22nd, Al Wahda Commercial Tower </p>
                                <p>PO Box 36678, Abu Dhabi, United Arab Emirates</p>
                                <p><b>Phone:</b></p><p> +971 2 644 4474</p>
                                <p><b>Email:</b></p><p> info@bsabh.com</p>',
                'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3631.3920784224715!2d54.3720301508728!3d24.471868384161322!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5e6795a58f6167%3A0x5ed7237523283b74!2sAl%20Wahda%20Commercial%20Tower!5e0!3m2!1sen!2sae!4v1639641918354!5m2!1sen!2sae'
            ),

            array('term_id' => '78','name' => 'Dubai, UAE','slug' => 'dubai-uae','term_group' => '0',
                'content' => '<p><b>Location:</b></p>
                                <p>Level 6, Building 3, Dubai International Financial Centre (DIFC) The Gate District, PO Box 262.</p>
                                <p>Dubai, United Arab Emirates</p>
                                <p><b>Phone:</b></p> <p>+971 4 528 5555</p>
                                <p><b>Email:</b></p> <p>info@bsabh.com</p>',
                'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7219.58117906377!2d55.27547174748908!3d25.210283635244053!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f429202af274d%3A0x806c1855509a83d6!2sDubai!5e0!3m2!1sen!2sae!4v1639642318737!5m2!1sen!2sae'),

            array('term_id' => '79','name' => 'Ras Al Khaimah, UAE','slug' => 'ras-al-khaimah-uae','term_group' => '0',
                'content' => '<p><b>Location:</b></p> <p>Al Nahda Street Blue Wage Real Estate Building, level 2<br/>
                                <p>Office 202/H Ras Al Khaimah, UAE</p>
                                <p><b>Phone:</b></p><p> +971 7 2273106</p>
                                <p><b>Email:</b></p><p> info@bsabh.com</p>',
                'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3594.312538322065!2d55.904136820724126!3d25.727176090780265!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ef675b08285df19%3A0x52dc4bccebf88e1c!2zNjYg2LfYsdmK2YIg2KfZhNmG2YjZgSAtIEFsIERoYWl0IFNvdXRoIC0gUmFzIGFsIEtoYWltYWg!5e0!3m2!1sen!2sae!4v1639642172027!5m2!1sen!2sae'),

            array('term_id' => '80','name' => 'Sharjah, UAE','slug' => 'sharjah-uae','term_group' => '0',
                'content' => '
                                <p><b>Location:</b><p>
                                <p>Regus Expo Business Centre, Al Tawon Road Expo Centre Sharjah, 1st Floor</p>
                                <p>P.O. Box 1216, Sharjah, UAE</p>
                                <p><b>Phone:</b></p> <p>+971 6 5754222</p>
                                <p><b>Email:</b></p> <p>info@bsabh.com</p>',
                'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10201.61800775627!2d55.36696410911991!3d25.310363984423567!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f5b8f6f9bb6e5%3A0xc8b1c4b90db56312!2sRegus%20-%20Sharjah%2C%20Expo%20Centre!5e0!3m2!1sen!2sae!4v1639641876155!5m2!1sen!2sae'),

            array('term_id' => '81','name' => 'Erbil, Iraq','slug' => 'erbil-iraq','term_group' => '0',
                'content' => '<p><b>Location:</b></p>
                                <p>Office No.5., 19th floor,T4, K,</p>
                                <p>Empire Business Towers,</p>
                                <p>Erbil, Kurdistan, Iraq</p>
                                <p><b>Phone:</b></p> +964 66 278 5339</p>
                                <p><b>Email:</b></p> info@bsabh.com</p>',
                'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3219.67127301007!2d43.970882122706854!3d36.198875766890325!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x400723b6b6a8a0d3%3A0x7d5d589e728d4d2b!2sEmpire%20Business%20Towers%20-%20EBT%204!5e0!3m2!1sen!2sae!4v1639642353215!5m2!1sen!2sae'),

            array('term_id' => '82','name' => 'Beirut, Lebanon','slug' => 'beirut-lebanon','term_group' => '0',
                'content' => '<p><b>Location:</b></p>
                                <p>Hibat Al Maarad Bldg, 1st Floor, Emir Bashir Road</p>
                                <p>PO Box 1146, Beirut Central District, Lebanon</p>
                                <p><b>Phone:</b></p> +961 1 99 2450</p>
                                <p><b>Email:</b></p> info@bsabh.com</p>',
                'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d413.9960297917303!2d35.49705888025676!3d33.89047183344047!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151f172101c3921b%3A0x894ca706b6f38d71!2sVFRW%2B5VJ%2C%20Patrakyieh%2C%20Bayrut%2C%20Lebanon!5e0!3m2!1sen!2sae!4v1639642458393!5m2!1sen!2sae'),

            array('term_id' => '83','name' => 'Muscat, Oman','slug' => 'muscat-oman','term_group' => '0',
                'content' => '<p><b>Location:</b></p> B
                                <p>SA Al Rashdi & Al Barwani Advocates & Legal Consultants</p>
                                <p>Office 510, Block 5, Al Assalah Towers, Saud Bahwan Plaza, Sultan Qaboos Street, Al Ghubra South</p>
                                <p>PO Box 435 Muscat, Oman</p>
                                <p><b>Phone:</b></p> +968 2 421 8555</p>
                                <p><b>Email:</b></p> info@bsabh.com</p>',
                'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d914.0513949642533!2d58.362924488120946!3d23.596958999036897!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x87b015ffb2ff5cef!2zMjPCsDM1JzQ5LjEiTiA1OMKwMjEnNDguNSJF!5e0!3m2!1sen!2sae!4v1639642634663!5m2!1sen!2sae'),

            array('term_id' => '84','name' => 'Riyadh, KSA','slug' => 'riyadh-ksa','term_group' => '0',
                'content' => '<p><b>Location:</b></p> <p>2722 Al Malek Saud, Al Murabba Dist., Unit No 29</p>
                                <p>| Riyadh 12631 – 8745 | Saudi Arabia</p>
                                <p><b>Phone:</b></p><p> +966 57 186 0000</p>
                                <p><b>Email:</b></p><p> info@bsabh.com</p>',
                'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1077.9727412711136!2d46.68962477717828!3d24.66316515912869!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8d02e0b7080b390a!2zMjTCsDM5JzQ3LjAiTiA0NsKwNDEnMjIuMSJF!5e0!3m2!1sen!2sae!4v1639642697029!5m2!1sen!2sae')
        );

        foreach ($data as $item)
        {
            $data = [];
            $data['l_id'] = $item['term_id'];
            $data['name'] = $item['name'];
            $data['slug'] = $item['slug'];
            $data['content'] = $item['content'];
            $data['map'] = $item['map'];

            Location::create($data);
        }
    }
}
