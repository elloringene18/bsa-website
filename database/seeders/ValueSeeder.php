<?php

namespace Database\Seeders;

use App\Models\Award;
use App\Models\Value;
use Illuminate\Database\Seeder;

class ValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'content' => 'Integrity and commitment ',
                'image' => 'img/careers/integrity.jpg',
            ),
            array(
                'content' => 'Ethics and approachability',
                'image' => 'img/careers/approachability.jpg',
            ),
            array(
                'content' => 'Local knowledge and experience',
                'image' => 'img/careers/knowledge.jpg',
            ),
        );

        foreach ($data as $item)
        {
            Value::create($item);
        }
    }
}
