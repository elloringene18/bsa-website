<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Lawyer;
use App\Models\Service;
use App\Models\ServiceTeam;
use Illuminate\Database\Seeder;
use Symfony\Component\Console\Output\ConsoleOutput;

class ServiceArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        $data = [

            array(
                'articles' => '
                 have-spac-listings-hit-the-wall,
                 precautions-to-consider-before-investing-in-a-uae-business,
                 spacs-in-the-uae-how-does-it-work,
                 spacs-in-the-uae-how-does-it-work,
                 movables-pledge-law-new-executive-regulations
                 ',
                'post_name' => 'corporate-law-and-ma'
            ),

            array(
                'articles' => '
                  family-business-regulations-in-the-uae,
                  companies-in-violation-of-ubo-regulations-may-face-up-to-aed-100000-fine,
                  how-the-new-consumer-protection-regulations-impact-insureds,
                  update-saudi-companies-law,
                  new-sme-market-conduct-regulation
                  ',
                'post_name' => 'commercial-law'
            ),

            array(
                'articles' => '
                   movables-pledge-law-new-executive-regulations,
                   update-saudi-companies-law,
                   new-sme-market-conduct-regulation,
                   difc-wills,
                   how-the-new-consumer-protection-regulations-impact-insureds
                  ',
                'post_name' => 'regulatory-and-compliance',
            ),

            array(
                'articles' => '
                    difc-wills,
                    difc-employment-law-new-amendments-explained,
                    returning-to-the-workplace-post-covid-19-the-advantages-and-challenges-faced-by-employers-and-employees,
                    ministerial-resolution-no-279-of-2020-on-employment-stability-in-private-sector,
                    ministerial-resolution-no-279-of-2020-on-employment-stability-in-private-sector
                  ',
                'post_name' => 'employment-law-firm'
            ),

            array(
                'articles' => '
                    cybersecurity-for-smes,
                    how-the-uae-itc-data-law-impacts-businesses,
                    the-new-uae-health-data-law-an-in-depth-look,
                    returning-to-the-workplace-post-covid-19-the-advantages-and-challenges-faced-by-employers-and-employees,
                    difc-data-protection-law-2020
                  ',
                'post_name' => 'cybersecurity'
            ),

            array(
                'articles' => '
                    keeping-it-civil-the-latest-key-amendments-to-the-uae-civil-procedures-law,
                    dubais-recently-established-money-laundering-court-explained,
                    nuts-and-bolts-of-decision-no-26-of-2020-covering-small-claims-tribunals,
                    uae-civil-court-judgments-now-enforceable-in-india,
                    uae-insurance-litigation-2021
                  ',
                'post_name' => 'litigation-law-firm'
            ),
            array(
                'articles' => '
                    legal-500-uae-international-arbitration-chapter,
                    pay-when-paid-clause-can-be-defeated,
                    testing-the-legal-basis-of-the-lack-of-pursuit-argument,
                    virtual-hearings-in-arbitration-are-they-as-effective-as-they-are-efficient,
                    the-new-uae-arbitration-law-it-is-not-what-it-says-but-how-you-read-it
                 ',
                'post_name' => 'arbitration-dispute-resolution'
            ),
            array(
                'articles' => '
                    iclg-uae-franchise-laws-and-regulations-2022,
                    cartels-practice-guide-2021,
                    the-end-of-the-gcc-patent-what-next,
                    dubai-difcs-new-ip-regulations-provide-welcome-clarity,
                    recent-updates-and-developments-in-uae-intellectual-property-practice-and-procedures
                ',
                'post_name' => 'intellectual-property-law'
            ),
            array(
                'articles' => '
                    islamic-finance-in-oman,
                    uae-anti-money-laundering,
                    gcc-restructuring-insolvency-report,
                    saudi-central-bank-launches-open-banking-initiative,
                    uae-bankruptcy-law-amendments
                ',
                'post_name' => 'banking-finance-law'
            ),

            array(
                'articles' => '
                    india-renews-its-green-pledge-with-a-commitment-to-triple-renewable-energy-generation-by-2030,
                    the-middle-east-plugged-into-renewable-energy,
                    what-fidics-new-green-book-holds,
                    pay-when-paid-clause-can-be-defeated,
                    the-practical-obstacles-in-leading-uae-construction-claims',
                'post_name' => 'energy-law'
            ),
            array(
                'articles' => '
                uae-insurance-litigation-2021,
                insurance-in-uae-ready-for-the-big-leap,
                revised-central-bank-guidance-regarding-saving-and-investment-insurance,
                draft-insurance-broker-regulations-what-could-change,
                money-laundering-regulations',
                'post_name' => 'insurance-reinsurance-law'
            ),

            array(
                'articles' => '
                    the-uaes-new-sca-licensing-regime,
                    bsa-fintech-report-2021-the-future-of-finance-in-the-gcc,
                    finance-for-smes,
                    marka-group-bankruptcy-update,
                    central-bank-digital-currencies
                ',
                'post_name' => 'regulatory-and-compliance',
            ),

            array(
                'articles' => '
                what-fidics-new-green-book-holds,
                pay-when-paid-clause-can-be-defeated,
                the-practical-obstacles-in-leading-uae-construction-claims,
                testing-the-legal-basis-of-the-lack-of-pursuit-argument,
                qa-legal-considerations-for-construction-in-saudi-arabia
                ',
                'post_name' => 'construction-law'
            ),


            array(
                'articles' => '
                renting-in-dubai-here-are-your-rights-as-a-tenant,
                tokenization-of-real-estate-assets-explained,
                considering-fractional-property-ownership-using-tokens,
                real-estate-crowdfunding-as-potential-opportunity-for-investors-and-entrepreneurs,
                dubai-real-estate-institute-and-bsa-sign-mou-to-boost-skill-development-and-emiratisation-in-uaes-property-industry
                ',
                'post_name' => 'real-estate-law'
            ),


            array(
                'articles' => '
                the-new-uae-health-data-law-an-in-depth-look,
                the-concept-of-healthcare-medical-trust,
                health-insurance-in-the-uae-and-the-concepts-of-medical-benefits-trusts,
                health-insurance-in-the-uae-capitation-schemes-confirmed-unlawful-by-the-insurance-authority,
                new-mandated-health-insurance-law-in-oman,
                ',
                'post_name' => 'life-sciences-and-healthcare',
            ),

            array(
                'articles' => '
            cybersecurity-for-smes,
            iclg-uae-franchise-laws-and-regulations-2022,
            cartels-practice-guide-2021,
            difc-employment-law-new-amendments-explained,
            how-the-new-consumer-protection-regulations-impact-insureds
        ',
                'post_name' => 'retail-and-consumer-goods',
            ),



            array(
                'articles' => '
                    dubai-drone-law-announced-to-provide-new-opportunities-and-boost-dubais-aviation-sector,
                    cartels-practice-guide-2021,
                    islamic-finance-in-oman,
                    100-foreign-ownership-for-the-maritime-sector,
                    overview-on-compliance-sanctions-in-the-uae
                    ',
                'post_name' => 'international-trade-transport-and-maritime ',
            ),

            array(
                'articles' => '
                    iclg-uae-tmt-laws-and-regulations-2022,
                    uae-legal-update-regulatory-alert-healthcare,
                    cybersecurity-for-smes,
                    iclg-uae-franchise-laws-and-regulations-2022,
                    how-the-uae-itc-data-law-impacts-businesses
                    ',
                'post_name' => 'technology-media-and-telecommunications',
            ),


            array(
                'articles' => '
                    precautions-to-consider-before-investing-in-a-uae-business,
                    movables-pledge-law-new-executive-regulations,
                    how-the-new-consumer-protection-regulations-impact-insureds,
                    update-saudi-companies-law
                    ',
                'post_name' => 'leisure-and-hospitality',
            ),
            array(
                'articles' => '
                iclg-uae-franchise-laws-and-regulations-2022,
                how-the-uae-itc-data-law-impacts-businesses,
                real-estate-crowdfunding-as-potential-opportunity-for-investors-and-entrepreneurs,
                movables-pledge-law-new-executive-regulations,
                keeping-it-civil-the-latest-key-amendments-to-the-uae-civil-procedures-law
                ',
                'post_name' => 'education',
            ),
            array(
                'articles' => '',
                'post_name' => 'environment-social-and-governance',
            ),
            array(
                'articles' => '',
                'post_name' => 'private-client',
            ),
            array(
                'articles' => '',
                'post_name' => 'high-net-worth-individuals',
            ),
            array(
                'articles' => '',
                'post_name' => 'family-owned-businesses',
            ),
        ];

        foreach($data as $item){

            $service = Service::where('slug',$item['post_name'])->first();

            if($service){
                if(isset($item['articles'])){
                    $articles = explode(',',$item['articles']);

                    if(count(array_filter($articles))) {
                        foreach($articles as $t){
                            $d = Article::where('slug',trim($t))->first();
                            if($d){
                                $service->articles()->create(['article_id'=>$d->id]);
                            }
                        }
                    }
                }
            }
        }
    }
}
