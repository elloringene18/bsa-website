<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\ArticleLawyer;
use App\Models\Lawyer;
use Illuminate\Database\Seeder;

class ArticleLawyerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Article::with('tags')->get();

        foreach($data as $article){
            foreach($article->tags as $tag) {
                $lawyer = Lawyer::where('name',$tag->name)->first();
                if($lawyer){
                    $exists = ArticleLawyer::where('article_id',$article->id)->where('lawyer_id',$lawyer->id)->first();

                    if(!$exists)
                        $article->lawyer()->create(['lawyer_id'=>$lawyer->id]);
                }
            }
        }
    }
}
