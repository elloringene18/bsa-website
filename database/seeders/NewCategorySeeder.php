<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class NewCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = array(
            array('term_id' => '3000','name' => 'News','slug' => 'c-news'),
            array('term_id' => '3002','name' => 'Events','slug' => 'c-events'),
            array('term_id' => '3001','name' => 'Reports','slug' => 'reports'),
            array('term_id' => '3003','name' => 'Regulatory & Legal Updates','slug' => 'updates'),
            array('term_id' => '3004','name' => 'Publications','slug' => 'publications'),
        );

        foreach ($data as $item)
        {
            $old = Category::where('cat_id',$item['term_id'])->first();

            if($old)
                $old->delete();
        }

        $data2 = array(
            array('term_id' => '3001','name' => 'Reports','slug' => 'reports'),
            array('term_id' => '3003','name' => 'Regulatory & Legal Updates','slug' => 'regulatory-and-legal-updates'),
//            array('term_id' => '103','name' => 'News','slug' => 'news'),
//            array('term_id' => '102','name' => 'Events','slug' => 'events'),
        );

        foreach ($data2 as $item)
        {
            $category = [];
            $category['cat_id'] = $item['term_id'];
            $category['name'] = $item['name'];
            $category['slug'] = $item['slug'];

            if(!Category::where('cat_id',$item['term_id'])->count())
                Category::create($category);
        }
    }
}
