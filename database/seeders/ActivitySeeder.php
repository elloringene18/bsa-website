<?php

namespace Database\Seeders;

use App\Models\Activity;
use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $items = Activity::get();

        foreach($items as $item)
            $item->delete();

        $data = array(
            array('title' => 'Sponsoring the Emirates Fine Arts Society on their 34th Annual Exhibition'),
            array('title' => 'Visiting the Dubai Autism Center'),
            array('title' => 'Giving a student lecture at the American University in Dubai covering the essential aspects of UAE law'),
            array('title' => 'Empowering Emiratisation alongside JAFZA under the Tumoohi Project'),
            array('title' => 'Presenting at the Career and Study Fair at Lycée Libanais Francophone Privé in Dubai'),
            array('title' => 'Hosting a one-day legal clinic where our lawyers gave free legal advice to the public at the Dubai Land Department'),
        );

        $category = Category::where('slug','csr-activity')->first();

        foreach ($data as $item)
        {
            $item['slug'] = Str::slug($item['title']);

            $toDelete = Article::where('slug',$item['slug'])->first();

            if($toDelete)
                $toDelete->delete();

            $article = Article::create($item);

            ArticleCategory::create(['article_id'=>$article->id,'category_id'=>$category->cat_id]);
        }
    }
}
