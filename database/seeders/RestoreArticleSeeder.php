<?php

namespace Database\Seeders;

use App\Models\Article;
use Illuminate\Database\Seeder;

class RestoreArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            array(
                'ID' => '58522',
                'post_title' => 'معلومات عنا',
                'post_content' => '
                    <h5 style="text-align: left;"><span style="color: #000000;">تأسست BSA أحمد بن هزيم وشركاه للمحاماة في دبي في عام 2001، وتقدم خدمات التي تجمع بين المعرفة الشاملة للقانون المحلي مع النهج الحديث والتقدمي</span></h5>
                    منذ ذلك الحين ونحن على انضم الشريك، الدكتور أحمد بن هزيم، مدير عام محاكم دبي السابق، وفتح المزيد من مكاتب في مختلف أنحاء الشرق الأوسط، والآن فرنسا وترسيخ حضورنا الإقليمي يحركها التميز. لدينا الوصول إلى السلطات المحلية الرئيسية والخبرة القانونية الصلبة عبر طيف واسع من الصناعات يميزنا باعتبارها واحدة من عدد قليل من الممارسات القانونية دبي مقرها التي تجاوزت حدودها الأصلية.
                    <h6>المجالات الرئيسية للقوة ما يلي:</h6>
                    <ul class="list">
                        <li><a title="Arbitration &amp; Dispute Resolution" href="https://bsatest.net/arbitration-dispute-resolution/" target="_blank" rel="noopener">التحكيم وتسوية المنازعات</a></li>
                        <li><a title="الخدمات المصرفية والمالية" href="https://bsatest.net/banking-finance/" target="_blank" rel="noopener">الخدمات المصرفية والمالية</a></li>
                        <li><a title="Commercial" href="https://bsatest.net/commercial/" target="_blank" rel="noopener">تجاري</a></li>
                        <li><a title="Construction" href="https://bsatest.net/construction/" target="_blank" rel="noopener">إنشاءات</a></li>
                        <li><a title="Corporate and M&amp;A" href="https://bsatest.net/corporate-and-m-a/" target="_blank" rel="noopener">الشركات وM &amp; A</a></li>
                        <li><a title="Employment" href="https://bsatest.net/employment/" target="_blank" rel="noopener">توظيف</a></li>
                        <li><a title="Insurance &amp; Reinsurance" href="https://bsatest.net/insurance-reinsurance/" target="_blank" rel="noopener">تأمين وإعادة التأمين</a></li>
                        <li><a title="Litigation" href="https://bsatest.net/litigation/" target="_blank" rel="noopener">دعوى</a></li>
                        <li><a title="Real Estate" href="https://bsatest.net/real-estate/" target="_blank" rel="noopener">العقارات</a></li>
                    </ul>
                    وتشمل قائمة عملاء BSA في شركات القطاع العام والخاص، والشركات المحلية والشركات متعددة الجنسيات من مختلف الصناعات والكيانات المملوكة للحكومة وشركات المحاماة الأخرى. نحن نمثل بانتظام والتعاون مع مكاتب المحاماة الدولية، والمساعدة في تقديم الخدمات القانونية الراقية.

                    محامينا يأتون من خلفيات متنوعة، مع الأغلبية المزدوجة المؤهلة في كل من الولايات القضائية الإقليمية والمتعددة الجنسيات، الناطقة باللغة العربية والإنجليزية والفرنسية بطلاقة. في اتصال مع التطورات القانونية الإقليمية والدولية، ونحن نقدم أيضا معرفة القطاع والمشورة مصممة من ذوي الخبرة في قطاعات محددة.

                    BSA عضو في Meritas - شبكة عالمية من شركات المحاماة المستقلة التي تهدف إلى منح عملاء الوصول إلى الخبرة القانونية الجودة في جميع أنحاء العالم. ونحن أيضا الممثل الحصري في دولة الإمارات العربية المتحدة IsFin - الشبكة الرائدة في العالم من المحامين التمويل الإسلامي.
                ',
                'post_date' => '2019-06-02 15:37:39',
                'post_name' => 'معلومات-عنا',
                'post_parent' => '0',
                'Image' => null,
                'Categories' => '3003',
                'Tags' => null
            ),
            array('ID' => '61483','post_title' => 'Why Expats in the UAE should have a Power of Attorney','post_content' => '<h4>A power of attorney is a legally binding document which, when properly executed, allows the receiver of the power of attorney (known as the agent) to act on behalf of the grantor in relation to their personal affairs, business decisions or legal representation. A power of attorney can be general in its scope or be limited to specific act(s) or time period(s).</h4>
According to various global indices, as of 2019 roughly 90% of the population of the UAE are expatriates. As an expat, you should give your spouse, family member or a trusted friend, a power of attorney to act on your behalf in cases where you are incapable of making decisions or acting for yourself, such as where you are out of the country or are physically incapable of carrying out functions for yourself.
<br/><br/>
For a power of attorney to be executed in the UAE, it must be either in Arabic, or bilingual – in Arabic and English and certified by a legal translator. The grantor can give the power of attorney to any person provided that they are of legal age and of sound mental capacity. However, given the nature and extent of the powers that the agent will possess, it is recommended that the grantor only appoint a person whom he/she trusts completely, to act as their agent through this instrument.
<br/><br/>
Power of attorney is used for various reasons, for example, in the courts here, a claimant or defendant gives a power of attorney to their lawyers, allowing them to legally represent the grantor before the court and authorities. Similarly, a person who is not ordinarily residing in the UAE may give a power of attorney to their spouse, or any other person to conduct financial and business transactions on their behalf, to ensure there is no hinderance.
<br/><br/>
In uncertain times such as these, where one may require accessing their bank accounts, signing or encashing cheques, or other urgent legal formalities, and the person is not present in the country, or is in the hospital and unable to carry out such acts, a power of attorney will allow their agent to seamlessly deal with the authorities.
<br/><br/>
<strong>A power of attorney must contain three main components:</strong>
<ul>
 	<li>The complete details of the grantor of the power, which will include his full legal name, nationality, a form of identification (passport number or Emirates ID number, in the case of the UAE) and the capacity in which the power is granted, i.e. personal capacity and/or professional capacity. This is especially useful for persons having their own businesses or sole proprietorship who may wish to give their spouses powers to act for the business as well.</li>
 	<li>The complete details of the receiver (agent) of the power, which will include their full legal name, nationality, a form of identification, and may also include their relation to the grantor.</li>
 	<li>The scope of the powers granted by the document- this will include the exact nature and scope of the powers being granted to the agent, and the details of the assets/bank accounts in respect of which the power is granted. For example, if the power is being granted for the operation of bank accounts, then the account numbers, the name of the bank where the accounts are held and the complete name of the account holder must be mentioned within the document, so that the bank can verify such details prior to the agent exercising his power, and to prevent fraud.</li>
</ul>
A power of attorney may be general or specific. Usually, between spouses, a general power of attorney is advisable which allows the spouse to deal with broadly everything in the same manner as their spouse. However, if a power of attorney is being given to a person not being the spouse, parent or child (not minor), then it is strongly advised that a specific power of attorney with a limited scope as to the acts that can be done by the agent be issued, to prevent fraudulent misuse of the power.
<br/><br/>
<strong>Conclusion</strong>
<br/><br/>
In conclusion, the current global situation is a great reminder that life is uncertain and living in a country away from your own can be greatly stressful at such times. In such circumstances, having granted a power of attorney can allow some peace of mind as to the fact that should anything happen which doesn’t allow you to do certain acts, your agent would be able to carry out the same for you.
<br/><br/>
Dubai Notary Public is accessible online and is continuing its operations including granting of power of attorneys, for further information regarding this please click<a href="https://bsabh.com/dubai-notary-public-services-to-be-provided-remotely/">here.</a>
<br/><br/>
Authored by Associate, <a href="https://bsabh.com/lawyer/swati-soni/">Swati Soni</a>
<br/><br/>
For further information please contactHead of Indirect Tax and Conveyancing,<a href="https://bsabh.com/lawyer/john-peacock/">John Peacock</a>','post_date' => '2020-04-14 13:21:44','post_name' => 'why-expats-in-the-uae-should-have-a-power-of-attorney','post_parent' => '0','Image' => 'wp-content/uploads/2020/04/shutterstock_793257898.jpg','Categories' => '31, 1696','Tags' => '56, 104, 170, 1672, 1813'),

            array('ID' => '61009','post_title' => 'Virtual Attendance of Shareholders and Board Meetings','post_content' => 'As we are approaching the end of the first quarter of 2020, which for various companies marks the end of their fiscal year, shareholders and managers begin to plan their annual general meetings. At the same time, this also marks the deadline for the ratification of financial statements for corporate entities in the UAE formed under UAE Federal Law No. 2 of 2015 on Commercial Companies (the “<strong>UAE Commercial Companies Law</strong>”). Due to various restrictions in place to address the impact of Covid-19, shareholders and managers of corporate bodies are considering alternative arrangements to ensure business continuity for their various activities.
<br/><br/>
Companies are also inquiring as to the best structure they can implement to ease the convening of shareholders and managers meetings in the context of travel limitations and remote working initiatives widely adopted due to present circumstances.
<br/><br/>
Many business owners are now asking whether it is permitted to hold official shareholders and managers meetings virtually and whether such decisions are binding. There are two main documents which need to be considered when examining these questions:
<ol>
 	<li>The first is the UAE Commercial Companies Law, particularly Article 92 on the formation of the general assembly, which allows shareholders to agree on the protocol for the determination of the time and place where meetings shall be held. Article 92 reads as follows: “The Limited Liability Company shall have a General Assembly consisting of all the partners. The General Assembly shall be convened by an invitation from the Manager or the Board of Directors at least once in a year during the four months following the end of the financial year of the company. The General Assembly shall be convened at such time and place as set out in the letter inviting to convene the meeting”.</li>
 	<li>The second is the Memorandum of Association (“<strong>MOA</strong>”) that was enacted at the time when the company in question was established and any subsequent addendums thereto to verify the procedures to call for meetings. In the event where the MOA is not clear enough to cover the possibility of calling for virtual meetings via video or audio conference calls, or otherwise restricts the use of such means, the shareholders will always have the ability to pass a <strong>written</strong> resolution by circulation to confirm their consent for such a mechanism to be put in place with immediate effect. Decisions passed remotely will have the same effect as the decisions taken during actual meetings held in person. The minutes and any extracts thereof can be circulated for signature when in need.</li>
</ol>
For the avoidance of doubt, the quorum and voting rights will continue to apply as per the MOA and where the MOA is silent on these matters, the minimum terms of the UAE Commercial Companies Law on this will apply.
<br/><br/>
If your MOA is not updated to address this issue, we recommend that you have an update made as soon as possible to address this gap and ensure that your shareholders and managers are able to convene remotely.
<br/><br/>
Please do reach out to us if you have any queries on the above or if you need any legal assistance in the meantime.
<br/><br/>
Authored by Partner <a href="https://bsabh.com/lawyer/rima-mrad/">Rima Mrad</a>','post_date' => '2020-03-22 10:23:05','post_name' => 'virtual-attendance-of-shareholders-and-board-meetings','post_parent' => '0','Image' => 'wp-content/uploads/2020/03/shutterstock_1626085978.jpg','Categories' => '31, 1696','Tags' => '97, 286, 1672, 1720'),


            array('ID' => '61009','post_title' => 'Free VAT Information','post_content' => '',
                'post_date' => '2020-03-22 10:23:05',
                'post_name' => 'vat','post_parent' => '0',
                'Image' => 'wp-content/uploads/2020/03/shutterstock_1626085978.jpg',
                'Categories' => '31, 1696',
                'Tags' => '97, 286, 1672, 1720'),

            array('ID' => '60796','post_title' => 'UAE Insurance Authority Issues Circular on Coronavirus (COVID-19)','post_content' => 'Circular No. (3) of 2020 on Precautionary and Preventive Measures to Maintain Public Safety, Health and Prevent Spread of Coronavirus (COVID-19) (“COVID-19 Circular”), addressed to all insurance companies and insurance-related professions operating in the UAE, provides guidance from the Insurance Authority on precautionary and preventive measures to maintain public health in the workplace and ensure continuity of business related to the outbreak of COVID-19.
<br/><br/>
Based on an informal translation of the COVID-19 Circular, we summarize below the salient features:
<ul>
 	<li>Companies should adopt the necessary policies and take measures that reduce the risks associated with the consequences of Coronavirus on the insurance sector in the UAE;</li>
 	<li>The company should form an appropriate policy in relation to dealing with company\'s customers and the policyholders affected by the virus. The policy must be based on its financial position, solvency, and liabilities, and the company will remain responsible for the decisions taken in this regard;</li>
 	<li>The company should provide Disaster Recovery Plans to ensure business continuity, and the plan must include the essential roles in the company, provision for equipment’s to ensure work can be carried out remotely, funds required for this plan and approval from the company management for such costs;</li>
 	<li>The company must apply to all its employees and customers, all conditions and instructions in relation to precautionary and preventive measures issued by the relevant authorities in relation to Coronavirus, such as quarantine in case of travel from high-risk countries, whether for work or for personal reasons or any other instruction issued by the authorities;</li>
 	<li>The company should consider cancelling all internal events within the workplace and also participation in conferences, workshops, trainings or meetings within and outside UAE;</li>
 	<li>The company should consider safety measures such as requesting employees to work remotely and also consider flexible working hours for working mothers;</li>
 	<li>Set-up and publish a phone number for responding to emergencies, which can be used by the company’s customers for any enquiries that they may have.</li>
</ul>
The issuance of the COVID-19 Circular reaffirms the commitment of UAE towards excellence in governance and of always being prepared for any eventuality.
<br/><br/>
Should you require a copy of the COVID-19 Circular or wish to seek advice related to the drafting and reviewing of policies and procedures please feel free to reach out to us on (contact details below):
<br/><br/>
Authored by Partner<a href="https://bsabh.com/lawyer/simon-isgar/">Simon Isgar</a>and Senior Associate <a href="https://bsabh.com/lawyer/anand-singh/">Anand Singh</a>','post_date' => '2020-03-09 11:44:15','post_name' => 'uae-insurance-authority-issues-circular-on-coronavirus-covid-19','post_parent' => '0','Image' => 'wp-content/uploads/2020/03/shutterstock_1048705850.jpg','Categories' => '37, 1696','Tags' => '56, 125, 1057, 1203, 1672, 1675, 1678'),

            array('ID' => '61345','post_title' => 'UAE Government Launches Around the Clock Services Portal','post_content' => '<h4>In an amalgamation of its efforts to support the public and the business community and to try to reduce the impact of the global pandemic COVID-19 on all of its sectors, the UAE government, keeping up with its ever-evolving digital strategy aimed at working smartly and efficiently, has announced the roll-out of its official portal (u.ae) aimed at providing mostly end to end digital services around the clock.</h4>
The announcement comes after a series of initiatives to activate working remotely and preserve public safety while ensuring continuity of services. This is a giant leap in the digital transformation that the country has embarked on since the introduction of the e-dirham back in 2001 and the announcement of a long-term e-Government strategy which has since yielded several online services.
<br/><br/>
The UAE has 3730 federal and local eServices and the function of the official portal is to act as a single window or a single entry-point for users to access the different federal and local government eServices. The portal also boosts communication between customers and government representatives with the end goal being the enablement of customers to complete their transactions remotely, in a way that ensures their safety and supports the UAE’s efforts towards facing the challenges of the novel coronavirus.
<br/><br/>
Services such as police clearance certificates, renewal of work permits and contract\\mission work permits, renewal of Emirates ID cards, issuing emergency passports, attestations of official documents and invoices, registration of labor complaints, issuance/renewal/ cancellation of labor contracts and a myriad of other services offered by the various UAE ministries will all be available on the portal. The Ministry of Justice for instance will be offering services such as the application for ‘To Whom It May Concern’ certificates in Public Prosecution, registration of Order of Payment, registration of cases before the Federal Courts of Appeal/First Instance, registration of disputes before alternative litigation systems as well as the registration of Execution Files.
<br/><br/>
In addition to round the clock availability, remote working and ease of use, the portal has a number of features including the ability to upload documents, save your request and submit at a later date, and electronically signed documents and forms. This massive investment is set to additionally increase efficiency and is expected to expedite the process.
<br/><br/>
Authored by Partner, <a href="https://bsabh.com/lawyer/lara-barbary/">Lara Barbary</a>','post_date' => '2020-04-02 09:12:39','post_name' => 'uae-government-launches-around-the-clock-services-portal','post_parent' => '0','Image' => 'wp-content/uploads/2020/04/shutterstock_1137710192.jpg','Categories' => '31, 1696','Tags' => '119, 286, 689, 1672, 1786'),

            array('ID' => '61069','post_title' => 'The Impact of Defamation and Privacy on Social Media in COVID-19 Era','post_content' => '<h4>We are frequently asked the question: are we allowed to publish, post, republish any information, content, picture, video or data on COVID-19 when using our private social media accounts? Afterall, it is in the public interest and everyone is doing it!</h4>
In an era where often more questions arise than answers, the public is eager to read, share and explore everyone’s views on a particular topic. Whether you are journalists or reporters providing professional coverage, business owners responsible for your employees’ conduct, family members responsible for your children or dependents’ actions, we all need some guidance and practical answers, in order to be aware of the legal implications of how we use our social media.
<br/><br/>
Whilst naturally we are all expressing and discussing our thoughts on the impact of COVID-19 (and given the Governmental advice of self-isolation, this communication is largely via the internet), it is important to be aware of the potential legal implications that the dissemination of information and expressions of opinion or fact could have in the UAE.
<br/><br/>
Within the contents of this short article, we will outline the key points which individuals should be aware of before submitting and/or disseminating information, pictures, posts or any audio, audio-visual or written material regarding COVID-19 into the public domain. The points covered by this article are applicable to individuals, , patients, advice, corporations or any other form of information. The two major risks can be broadly summarized as an i) Invasion of Privacy and ii) Defamation.
<br/><br/>
<strong>Privacy</strong>. The right to a private and dignified family life is considered inherent in the UAE and is appropriately safeguarded by numerous applicable laws and regulations. The disclosure of information or secrets relating to someone’s private or family’s life will attract liability under the Penal Code, the Cyber Crimes Law as well as laws related to media and publications in UAE , if no prior consent is obtained from the individual. This can include an image, photo, short videos or any materials that expose individual(s) to the public without their consent, even for the purpose of public awareness.
<br/><br/>
To put this into the context of COVID-19, to expose an individual as exhibiting symptoms or as having the virus is likely to be interpreted as an invasion of an individual’s right to a private life. Furthermore, to take a picture of another person(s) in a public place and ‘disseminate it’ by publishing it online could also be interpreted as an invasion of privacy.
<br/><br/>
The UAE Criminal Court of Cassation issued a binding court judgment in relation to privacy laws in 2016, where it affirmed the imposition of serious sanctions against all entities involved in publishing content that violates the privacy of individuals. The individuals, in this case, were walking in public areas (commercial malls) yet, they were filmed without their consent and this subsequently raised a claim for the invasion of privacy. The sanctions imposed by the Court of Cassation as a result of this invasion of a right to a private life included fines and deportation from UAE territory.
<br/><br/>
The fundamental point is that patients (including their family members), children, names, images, medical situations or related data, can all be classified as private information, which is exclusive owned by the individual concerned The disclosure of this information, in any form, should be carefully reviewed and assessed, in order to mitigate any potential risk. It is important that Employers or Individuals with dependents, raise awareness on the implications of reporting on social media platforms in respect of the applicable laws to ensure no violation, even if unintentional, occurs. Ignorance of the law or lack of intention to violate another’s right to privacy is not an excuse. It should be noted that simply resharing what someone else has shared or published will not exempt an individual or entity from liability.
<br/><br/>
We have observed a high level of professionalism and adherence with the applicable laws and the third parties rights of media service providers, including TV channels, radio stations, online newspapers and other mediums. This is apparent from all the reports and audiovisual content that we are receiving in relation to COVID-19. We have also witnessed reliable content on the topic that provides sufficient information to public and corroborates with official sources.
<br/><br/>
<strong>Defamation</strong>. Defamatory or libelous posts on social media could result in defamation claims under the applicable laws in the UAE. Whilst the creation and dissemination of parodic posts and content is a common occurrence in the UK and Europe, it is important to be aware that parody is not an available defense under UAE Law. Instead, it is more likely that a parody may be seen as an attempt to humiliate an individual or an entity and to harm their reputation, no matter how ridiculous the parody is.
<br/><br/>
More recently, an exception to this rule was passed in the UAE in DIFC Intellectual Property Law number 4 of 2019, in which it is considered that a registered Trademark, or a well-known Trademark, is not infringed in the DIFC if it is used in news reporting, news commentary or parody. However, this exception is specifically limited to DIFC and in relation to trademarks. This shows a willingness for parodic content to be recognized in the future, but for now, the public should be aware that parodic content, could be pursued by the concerned individuals and/or entities in UAE under the applicable defamation laws. To put this into context, any posts relating to patients, medical staff members, law enforcement agencies or the public reacting to incidents of public interest can be subject to legal liability. Defamation criminal liability is pursuable within a strict time bar from publishing defamatory content. However, civil liability and invasion of privacy criminal claims can be longer than defamation offenses.
<br/><br/>
It is worth reminding everyone that in accordance with articles 372 and 373 of UAE Federal Law No. 3 of 1987 in UAE (as amended), a defamatory statement is one that exposes a person to public hatred or contempt, even if the statement is true and correct. This means that a person is potentially exposed to a claim for defamation by publishing or disseminating <em>any negative news</em> about an individual or an entity. If the defamatory statement is made against a public officer or governmental entity, the imposed sanction could be significantly worse.
<br/><br/>
Based on the above, an individual, before communicating an opinion, posting or sharing any videos in relation to COVID-19, by whatever method of communication, should consider:
<ol>
 	<li>Could this statement <em>be interpreted as</em> defamatory or an invasion of privacy for others? (i.e. does this statement suggest anything negative about an individual or entity in particular? Does it reveal any information or post any material regarding someone that can be classified as private content, a private location and/or unsuitable for public display); and</li>
 	<li>Could this statement, post or content cause harm? (particularly to reputation and honour to an individual or entity, on a national and international basis).</li>
</ol>
We should all be aware that the protection of privacy, for the data of patients, defamation, cybercrimes and all other related legal provisions in UAE are going to be likely reviewed and subject to enforcement proceedings should any violations be revealed. The priority now is for public safety but authorities and concerned individuals will be monitoring and documenting posted content that may be revisited in the future to explore any legal liabilities.
<br/><br/>
On a final note, social media platforms are extremely beneficial to the general public as they enable the transmission of awareness, encourage the freedom of speech and facilitate communication on an international basis, at a time where countries are shutting their borders and encouraging people to isolate. For example, the level of awareness that people gained on COVID-19 in such a short period is unprecedented. However, users should be aware that social media platforms are not private and the misuse of such platforms by sharing any content, statement or image that they come across, is subject to appropriate sanctions. Freedom of speech is granted and protected so long as it is in compliance with local regulations and public orders.
<br/><br/>
Authored by Partner and Head of Intellectual Property<a href="https://bsabh.com/lawyer/munir-abdallah-suboh/"> Munir Suboh</a> and Associate<a href="https://bsabh.com/lawyer/felicity-hammond/"> Felicity Hammond</a>','post_date' => '2020-03-24 09:56:29','post_name' => 'the-impact-of-defamation-and-privacy-on-social-media-in-covid-19-era','post_parent' => '0','Image' => 'wp-content/uploads/2020/03/shutterstock_196741457.jpg','Categories' => '678, 1696','Tags' => '268, 923, 1672, 1735, 1738'),

            array('ID' => '61438','post_title' => 'Saudi Ministry of Labor Decision to Regulate Contractual Relationship Between Employers and Their Employees','post_content' => '<h4>The Saudi Arabian Ministry of Labor and Social Development approved the reduction of working hours, or any other precautionary measures that limit the effects of the pandemic. This decision is aimed at limiting the application of the Labor Law in relation to employers severing the contractual relationship due to Force Majeure. These measures will be detailed further by the ministry in the coming days.</h4>
According to this decision, the employer in agreement with its employee can:
<ul>
 	<li>reduce the worker\'s wages in proportion to the number of actual work hours worked, or</li>
 	<li>give him leaves taken from his due annual leaves, or</li>
 	<li>give him an exceptional leave, as stipulated in the labor Law (agreed upon non-paid leave).</li>
</ul>
The regulation prohibits the employer from ending the contractual relationship should he have benefitted from any COVID-19 related state subsidy.
<br/><br/>
We note that the Ministry previously issued a regulation to sponsor 60% of wages of Saudi national employees in the private sector (for companies that cannot pay their employees’ wages) for a period of 3 months on the condition that the employer is subscribed to GOSI’s unemployment insurance.
<br/><br/>
The Ministry also made it possible to take advantage of the temporary surplus labor services through the “Ajir” portal, which facilitates the lending of employees to other businesses.
<br/><br/>
Authored by Senior Associate, <a href="https://bsabh.com/lawyer/jean-abboud/">Jean Abboud</a>','post_date' => '2020-04-08 09:18:34','post_name' => 'saudi-ministry-of-labor-decision-to-regulate-contractual-relationship-between-employers-and-their-employees','post_parent' => '0','Image' => 'wp-content/uploads/2020/04/shutterstock_1564477654.jpg','Categories' => '61, 1696','Tags' => '118, 163, 1598, 1672, 1744, 1747'),

            array('ID' => '61762','post_title' => 'Saudi Arabian Labor Law amended to cover COVID-19','post_content' => '<h4>The Saudi Arabian Ministry of Human Resources and Social Development (“MHRSD”) has issued several resolutions and directives to deal with the measures taken in relation to employee entitlements during COVID-19.</h4>
Notably, Article 41 has been inserted in the Implementing Regulation of the Labor Law, which enables the employer and employee – for a period of 6 months – to agree to any of the following:
<ul>
 	<li>a reduction in salary provided that there is a corresponding reduction in working hours;</li>
 	<li>placing the employee on paid annual leave (as part of their holiday entitlement);</li>
 	<li>implementing a period of unpaid leave.</li>
</ul>
Article 41 became applicable on 13/8/1441H, which corresponds to April 6, 2020, and only applies whenever the government takes any measures regarding a general situation that warrants a reduction of hours or any precautionary measure to prevent the worsening of such general situation, i.e., in the present COVID-19 times.
<br/><br/>
In an explanatory memorandum concerning Article 41, the MHRSD noted that the (i) reduction in salary and (ii) placing the employee on a paid annual leave are prerogatives of the employer and not the employee and therefore, may not be challenged by the employee. However, implementing a period of unpaid leave would need the approval of the employee.
<br/><br/>
We note that Saudi companies can now take any of the measures in Article 41 in whatever order during the 6 months; however, they must have been negatively affected by the current situation.
<br/><br/>
As for the reduction of salary, it must be in tandem with a reduction in work hours, i.e., a reduction of 20% of pay should correlate with a reduction of at least 20% or more in work hours. In addition, the maximum salary reduction should not exceed 40%.
<br/><br/>
We note that a company cannot make a salary reduction if it has benefited from any government subsidies in relation to those specific employees included in such subsidies, such as the recent state-sponsored subsidy scheme for KSA nationals.
<br/><br/>
Any reduction in salary and any granted leave must be made in written form, by signing an addendum to the employment contract with the concerned employees without invalidating the initial employment contract, which will remain applicable for all other terms.
<br/><br/>
Employers may not terminate their employees during the 6 months, i.e., between April and October 2020. They may only do so if they still cannot pay salaries after:
<ul>
 	<li>such a period has ended,</li>
 	<li>they have exhausted all the possible options such as reduction of salary, giving leaves, and</li>
 	<li>it is proved that they never benefited from any governmental help or subsidy during that period.</li>
</ul>
During those 6 months, the limitations of Article 74 of the KSA Labor Law still apply, which only recognizes termination where either the business is shutting or the business unit within which the employee works is closing.
<br/><br/>
An invalid dismissal during that period will risk an award of arbitrary dismissal compensation payable at the following rates – subject to any scale of compensation agreed in the employment contract which cannot be less than two months pay:
<ul>
 	<li>Unlimited term employment contract: 15 days\' wages for each year of service.</li>
 	<li>Limited-term employment contract: payment for the remaining period of the contract.</li>
</ul>
Employers are encouraged to have a clear plan as to how to deal with employees and their businesses during this period by following the guidelines of the MHRSD and the General Organization for Social Insurance (GOSI).
<br/><br/>
Employees, whether foreigners or Saudi nationals may always challenge any dismissal, or any decision taken contrary to Article 41 or to the Labor Law, by reverting to the MHRSD and to the labor courts.
<br/><br/>
Authored by Senior Associate, <a href="https://bsabh.com/lawyer/jean-abboud/">Jean Abboud</a>','post_date' => '2020-06-03 12:20:01','post_name' => 'saudi-arabian-labor-law-amended-to-cover-covid-19','post_parent' => '0','Image' => 'wp-content/uploads/2020/06/shutterstock_1641855286.jpg','Categories' => '61, 1696','Tags' => '118, 1598, 1672, 1867'),

            array('ID' => '4519','post_title' => 'Reasoned Awards and Expert opinions as a possible prevention to Article 257 being triggered','post_content' => '<h5><strong>Reasoned Awards and Expert opinions as a possible prevention to Article 257 being triggered </strong></h5>
<h5><strong></strong><strong><em>By Antonios Dimitracopoulos FCIArb</em></strong></h5>
It is fair to say that last year’s revision of Article 257 of the Penal Code has sent ripples throughout the arbitration community that are still felt, seven months later.
<br/><br/>
It was not long before visions of arbitrators and experts being locked behind bars for some alleged lack of impartiality flooded the UAE arbitration community, with many fearing the end this mode of dispute resolution in the UAE as we know it.
<br/><br/>
Others offered a more optimistic approach, of there being not much to worry about, suggesting that, surely, the actual jailing of arbitrators or experts requires far too many loops for a ‘bad loser’ of a party to jump over, before its guerilla tactics materialise.
<br/><br/>
However, both the optimists and the pessimists appear to have joined forces in trying to lobby for the repealing of Article 257 or perhaps its modification in a way that will help the arbitration community accept it in its day to day practice.
<br/><br/>
Whilst many have pondered over what lies ahead, it may be helpful to take a step back and consider what may have caused the change in legislation.
<br/><br/>
This could assist in working towards a limitation of the instances where the cause occurs, and hopefully of the instances where Article 257 bites.
<br/><br/>
Whilst it would be difficult to determine the exact train of thought that was followed by the legislator just before Article 257 was drafted, there is little doubt that some degree of indignation by the losing parties to arbitral proceedings must have been part of the force that fueled the change.
<br/><br/>
After all, if losing parties were to habitually exalt the fairness and even-handedness of tribunals’ decisions, there would hardly be any reason for them to even suspect lack of impartiality, let alone contemplate its criminal prosecution.
<br/><br/>
Hence, it is arguable that there may have been an angry drive behind what is a very widely worded and far reaching penal law provision.
<br/><br/>
If this assumption is correct, one would have to ask what could have fueled the possible outrage. To do that, one would have to look at a typical arbitral award, the basic structure of which is generally as follows:
<br/><br/>
It starts off, as is normal, with the formal details of the parties and their representatives, together with some procedural background as to the tribunal’s appointment, swiftly followed by a summary of the dispute, this usually being a verbatim replication of previous submissions.
<br/><br/>
If there are any jurisdictional or authority related matters, these are set out and possibly dealt with as preliminary issues.
<br/><br/>
There follows a list of what a tribunal understands to be the main issues in dispute.
<br/><br/>
Each issue is then dealt with, again by adopting a verbatim “copy and paste” approach of exactly what each party had to say, all taken from past pleadings.
<br/><br/>
After both parties’ position on a given issue has been duly repeated, the tribunal opines as to which view it prefers and delivers its decision on that issue, often with very little analysis and very little reasoning preceding such decision.
<br/><br/>
Even if some reasoning is set out, this is almost always disproportionately limited in length and depth when compared with the preceding views of the parties.
<br/><br/>
The process is repeated for each issue and then the summary of the decisions is listed in the dispositive part.
<br/><br/>
Almost always the overwhelming bulk of a typical arbitral award in the UAE (and possibly beyond) consists of the parties’ positions.
<br/><br/>
Only a very minor part of that voluminous award is original text of the tribunal’s own assessment of what the parties have argued.
<br/><br/>
This is so, even though many institutional rules do dictate that the award must be reasoned, for example DIAC Rule 37.5, ADCCAC Rule 28.6. DIFC-LCIA Rule 26.2 and ICC Rule 32.2.
<br/><br/>
There is no definition of what constitutes reasoning, or reasons, and tribunals often take a rather minimalistic view.
<br/><br/>
They tend to hold, for example, that a tribunal has simply not been convinced that a given position applies over another or that a certain burden of proof has or has not been discharged.
<br/><br/>
Tribunals consider this as sufficient reasoning and promptly move on to uphold or reject an elaborately expressed position (concerning a head of claim of possibly tens of millions of dirhams), usually in the space of just a few lines.
<br/><br/>
It is true that arbitrators are generally very cautious not to stray beyond what has been pleaded, lest they touch upon a topic or a concept on which the parties have not had an opportunity to express their position.
<br/><br/>
Were a tribunal to introduce arguments in its reasoning that the parties see for the first time in the body of the award, this may lead to its nullification. Therefore, as far as reasoning is concerned, this is a potential limitation within the arbitral process.
<br/><br/>
Independent expert witnesses often reach conclusions within their reports based on experience and understanding of “best practice” where no evidence corroborating their findings actually exists.
<br/><br/>
A tribunal is then asked to accept the findings of one expert witness or another, simply because they profess to be an authority on a given specialised and usually highly technical matter.
<br/><br/>
At this stage, it is relevant to consider how the structure and contents of an award affects the readers, the first of whom are of course the parties.
<br/><br/>
As is common practice, the preferred way of reading an award is backwards.
<br/><br/>
That is to say, by starting from the dispositive part and if a disappointing item is listed therein, the reader then tends to try to find in the preceding text of the award why the tribunal reached that disappointing conclusion.
<br/><br/>
However, in doing so, the reader is unlikely to discover any illuminating thought process that could perhaps have a cathartic effect on any frustration caused.
<br/><br/>
Rather, what the reader usually finds, is that all the hard work and deep thought process, intricately crafted pleadings and eloquent writing, was summarily dismissed in just a few short paragraphs, which are in turn shrouded under a mysterious veil of undisclosed arbitral thought process.
<br/><br/>
It is perhaps easier to understand at that stage that, if this experience of unexplained rejection is repeated often enough, accusations of bias or even prejudice may start to proliferate.
<br/><br/>
It may be that some may have decided that the time has come to do something about this and it may be that their frustration eventually found its way to the listening ears of legislators.
<br/><br/>
It may be that their frustration eventually took the form of Article 257.
<br/><br/>
Or perhaps none of this is true or even likely.
<br/><br/>
The fact remains however, that investing more in reasoning is only going to help the arbitral process and appease the losing party.
<br/><br/>
It may in fact help both parties feel that the superior knowledge, wisdom and objective outlook of the arbitrators and experts (which are the presumptions upon which they were appointed in the first place) is, in fact, very helpful in strategising future dispute resolution.
<br/><br/>
The extent of reasoning must be limited to what has already been pleaded to avoid any risk of the award being nullified or set aside.
<br/><br/>
However, such risk would be limited and the need for a reasoned award may be satisfied, if the reworded arguments put forward by one party are compared with those of another and are set out in original text forming part of the tribunal’s decision.
<br/><br/>
It would be difficult then to imagine a party wishing to trigger Article 257, because any public authority or prosecutor would have to first plough through the tribunal’s thought process before the net decision is fully understood, let alone criticised for lacking impartiality.
<br/><br/>
A thoroughly reasoned decision (or opinion in the case of an independent expert) no matter how debatable such reasoning is, would be unlikely to be viewed as lacking impartiality.
<br/><br/>
By contrast, a decision in an award that comes across as cryptic, with laconic wording, or an expert opinion absolute in its determination and with minimal insight to its thought process, is more likely to raise suspicions.
<br/><br/>
And if suspicions are raised, then the arbitral community is left nervous and apprehensive, regardless of whether an Article 257 conviction is eventually confirmed, proven beyond reasonable doubt or not.
<br/><br/>
An award that is devoid of even a semblance of reasoning, may not necessarily be unjust but may lead to a sense of injustice.
<br/><br/>
A sense of injustice may lead to a desire to seek justice and this is now possible through the more arbitration-specific option afforded by Article 257.
<br/><br/>
It is common ground in a court judgment that comparatively less time and text is devoted to what the parties’ positions are with the emphasis leaning more on reasoned analysis of arguments and of any authorities put forward.
<br/><br/>
One may argue that court judgments set out their reasoning in a far more detailed manner because they are liable to an appeal.
<br/><br/>
Hence, it is imperative that the appellant court is aware of the reasoning adopted by the lower court before it can properly either uphold or overturn its decision.
<br/><br/>
In addition, Judges are less limited by what authorities or principles they can invoke, whether those have been raised by the parties or not.
<br/><br/>
One may also argue that arbitral awards are not appealable and hence the need for extensive reasoning is not as intense.
<br/><br/>
However, given the alarming possibilities afforded by Article 257, it is likely that an arbitrator, if faced with its application, will in any event have to explain the reasoning behind an award at one stage or another, probably in front of a prosecutor and probably as part of a defence or a proclamation of innocence.
<br/><br/>
Rather than doing so after the event and for the sake of desperately trying to avoid a jail term, it may best to invest the time in advance and simply include such reasoning, even in limited form, as part of an award.
<br/><br/>
Awards could be drafted with that thought in mind, so that their reasoning appears transparent and having taken on board (as opposed to merely repeating) the parties’ positions.
<br/><br/>
This would be a step in the right direction, regardless of whether the concerted efforts to repeal Article 257 succeed.
<br/><br/>
Aside from it serving as a potential method to avoid a jail sentence, basic reasoning would seem to be a natural ingredient of any award, simply because it is a key to a sense of justice being conveyed.
<br/><br/>
Ultimately, a sense of justice is what any party finding itself in an arbitral process expects to experience, whether it is victorious or not.
<h5><a href="https://bsabh.com/wp-content/uploads/2017/06/Reasoned-Awards-and-Expert-opinions-as-a-possible-prevention-to-Article-257-being-triggered-1.pdf">Click here </a>to view the article in PDF.</h5>
<strong><em>June 2017</em></strong>
<br/><br/>
<strong><em></em></strong><strong><em>Antonios Dimitracopoulos FCIArb is a Partner at BSA Ahmad Bin Hezeem &amp; Associates, specialising in arbitration.</em></strong>
<table style="height: 141px;" border="0" width="620" cellspacing="0" cellpadding="0"><colgroup> <col width="136" /> <col width="193" /></colgroup>
<tbody>
<tr>
<td style="text-align: left;" height="15">Published:</td>
<td style="text-align: left;">June 2017</td>
</tr>
<tr>
<td style="text-align: left;" height="16">Title:</td>
<td style="text-align: left;"><strong>Reasoned Awards and Expert opinions as a possible prevention to Article 257 being triggered</strong></td>
</tr>
<tr>
<td style="text-align: left;" height="15">Practice:</td>
<td style="text-align: left;"><a href="https://bsabh.com/legal-practice-areas/insurance-reinsurance/"></a>
<p class="title"><a href="https://bsabh.com/legal-practice-areas/arbitration-dispute-resolution/">Arbitration &amp; Dispute Resolution</a></p>
</td>
</tr>
<tr>
<td style="text-align: left;" height="16">Authors:</td>
<td style="text-align: left;">
<h3><a href="https://bsabh.com/lawyer/antonios-dimitracopoulos/">Antonios Dimitracopoulos</a></h3>
</td>
</tr>
</tbody>
</table>','post_date' => '2017-06-12 15:32:45','post_name' => 'reasoned-awards-and-expert-opinions-as-a-possible-prevention-to-article-257-being-triggered-2','post_parent' => '0','Image' => 'wp-content/uploads/2017/06/shutterstock_1145274419.jpg','Categories' => '34, 106','Tags' => '88, 885'),

            array('ID' => '61576','post_title' => 'Oman - Legal Insight into Criminal Protection against Coronavirus (COVID-19)','post_content' => '<h4>In light of the continuing spread of novel coronavirus worldwide, and with efforts undertaken by the Sultanate of Oman to curb this unprecedented pandemic and try to contain and mitigate its effects, the leadership considered to strengthen the criminal protection and activate new legal rules that serve the public interest and complement the decisions of the Supreme Committee for Dealing with COVID-19 and other health instructions.</h4>
To this effect, Royal Decree No. (32/2020) was issued to amend certain provisions of the Law on the Control of Communicable Diseases promulgated by Royal Decree No. (73/92). The new Decree replaces some provisions and adds new ones.
<br/><br/>
The Royal Decree provides for a number of rights and responsibilities of treating doctors, patients or persons suspected of being infected with any communicable disease listed in Section I of the Schedule attached to the Law.
<br/><br/>
The Decree provides for that the doctor treating any person infected with any communicable disease listed in Section I of the Schedule attached to the Decree shall provide him with the necessary advice, make him aware of the nature of his disease and the methods of its transmission, and inform him of the measures and guidance that he must follow to limit the transmission of the infection to others.
<br/><br/>
In this respect, the law required that patients be made aware of his disease in order to take the necessary to protect others and in order to sense the severity of this disease and know the sanctions imposed in case of any violation of provisions of law. Sensitizing patients about the disease is paramount and ignorance might expose his life and lives of others to danger. This makes treating doctor the most competent person to provide medical advice and guidance.
<br/><br/>
In respect of rights of persons infected with any communicable diseases listed in Section I of the schedule attached to the Law:
<ol>
 	<li>They are entitled to receive medical care and treatment in government treatment facilities in accordance with the rules specified by the Minister of Health (article 5bis).</li>
 	<li>They are entitled to strict confidentiality and non-disclosure of their data and information except in cases stipulated under the law, or with their written consent. Therefore, it is prohibited to stigmatize those infected or divulge their places of residence or any information related to them (article 5bis1).</li>
</ol>
On the other hand, patients and persons suspected of being infected with a communicable disease listed in Section I of the Schedule attached to the law have obligations and responsibilities under the said Law, namely:
<ol>
 	<li>A person infected, or suspected of being infected with a communicable disease listed in section I of the schedule attached to the law shall immediately go to the closest health establishment to undertake medical examination, receive treatment and advice, be made aware of the dangers of the disease and the methods by which it is transmitted. If the said person (infected, or suspected to be infected) fails to do so, he shall be subject to criminal liability (as stated in article 5bis (3)).</li>
 	<li>The royal decree states that the person infected with any communicable disease listed in section I of the schedule attached to the law provides the health establishment that undertakes his treatment with all the information and data of persons who came into contact with him within the period prior to his illness as determined by the Ministry of Health. If the infected person fails to provide the health institution undertaking his treatment with such information, then he is deemed to have committed a crime punishable by law (as stated by article (5)bis (4)).</li>
 	<li>The law does not neglect the regulation of those coming to the Sultanate of Oman and requires that any person coming to the Sultanate, and who is aware that he is infected, or is suspected of being infected, with a communicable disease listed in section I of the schedule attached to the law shall inform the border crossing authorities of this immediately upon his arrival to them, and, if available, shall provide these authorities with all documents and records relating to his health condition. If he fails or refuses to do so, while he knows that he is infected, or suspected to be infected, he will be subject to criminal liability (as stated by article 5bis(5)).</li>
</ol>
In these continued efforts to combat the communicable diseases listed in section I of the schedule attached to the law, the Law does not ignore the scope of control and protection of those coming to the Sultanate of Oman and goes further to authorize The Ministry of Health to subject any person coming to the Sultanate from areas where a communicable disease, listed in section I of the schedule attached to the law, is spread to medical examination to ensure that he is free of the disease. The Ministry of Health may undertake appropriate procedures and measures including quarantining him and holding his luggage and personal belongings in the places it specifies, in coordination with competent bodies. (as stated in article 5bis (6)).
<ol>
 	<li>The law also provides for that a person infected, or suspected of being infected, with a communicable disease listed in section I of the schedule attached to the law shall abide by the instructions and guidance prescribed to him by the health establishment undertaking his treatment.</li>
</ol>
It is prohibited for the person infected with any of these diseases - upon his knowledge of the infection - to engage in any behavior that leads to transmitting the disease to others such as deliberately coming into contact with others, trying to spread the disease through bodily fluids (mucus, etc.) on surfaces and door-knobs and any high-touch surfaces (as stated in article 5bis (7)).
<br/><br/>
The law concludes with a general provision and criminalizes the disruption or refusal to implement the precautionary measures to curb the spread of the infection or the transmission of the same to other, including measures decided by the Supreme Committee for Dealing with COVID-19 or instructions and measures imposed by a competent authority such as Ministry of Health, such as wearing masks, curfew, etc. Criminalization and punishment also extend to any failure to collaborate with entities in charge of the implementation of such measures. The provision also includes the punishment of any behavior involving the disruption or refusal to implement the measures for curbing the spread of disease or its transmission (article 5bis (8)).
<br/><br/>
In the context of criminalization and punishment, the Law imposed sanctions and penalties commensurate with the severity of communicable diseases listed in the Schedule. The Royal Decree No. (32/2020) has therefore, replaced punishments stated in Law on the Control of Communicable Diseases promulgated by Royal Decree No. (73/92) with more severe punishments. Articles (19) and (20) were replaced accordingly.
<br/><br/>
Article (19) states: <em>“Whoever fails to report a communicable disease in accordance with the provisions of articles 2,3, and 5 of this law shall be punished by imprisonment for a period no less than 3 (three) months and not exceeding 1 (one) year, and a fine no less than 1,000 (one thousand) Rial Omani and not exceeding 10,000 (ten thousand) Rial Omani, or one of those two punishments.</em>
<em>Whoever violates the provisions of articles 5bis 3, 5bis 4, 5bis 5, 5bis 7, and 5bis 8 of this law shall be sentenced to the same penalty. If an alien is sentenced to a custodial penalty, his deportation from the country shall be adjudicated”.</em>
<br/><br/>
Article (20) states: <em>“Except for cases provided for in article 19 hereof, whoever violates any of the provisions of this law, or the ministerial decisions implementing it, shall be punished by imprisonment for a period no less than 1 (one) month and not exceeding 1 (one) year, and a fine no less than 500 (five hundred) Rial Omani and not exceeding 5,000 (five thousand) Rial Omani, or one of those two punishments. If an alien is sentenced to custodial penalty, his deportation from the country shall be adjudicated"</em>.
<br/><br/>
Finally, BSA Law firm calls upon community members and residents of Sultanate of Oman to show solidarity with measures and procedures of competent entities, respect and take warnings seriously and be open only to valuable information so that together we can stop the spread of this disease and contain the pandemic with fewer losses inshallah.
<br/><br/>
Lawyer Omar bin Khalid bin Saeed Al Kharousi
Al Rashdi & Al Barwani Advocates and Legal Consultants','post_date' => '2020-04-22 11:11:39','post_name' => 'oman-legal-insight-into-criminal-protection-against-coronavirus-covid-19','post_parent' => '0','Image' => 'wp-content/uploads/2020/04/shutterstock_535488232.jpg','Categories' => '31, 1696','Tags' => '54, 1672, 1828, 1837'),

            array('ID' => '55360','post_title' => 'New Rules Regulating the Employment of the UAE Nationals in the Private Sector','post_content' => 'On 04 April 2018, the Minister of Human Resources and Emiratization issued a groundbreaking Ministerial Decision No. 212 of 2018 on the regulations relating to the employment of UAE nationals in the private sector.
<br/><br/>
This decision replaced Ministerial Decision No. 293 of 2015 which set the rules and procedures for the employment of UAE nationals and Ministerial Decision No. 176 of 2009 which regulated the termination of UAE nationals in the private sector.
<br/><br/>
Ministerial Decision No. 212 of 2018 is a comprehensive policy that addresses the employment and termination of UAE nationals in the private sector including the creation of an efficient platform to enhance Emiratization. It further identified the procedures and steps to follow upon any employment of UAE nationals in the private sector.
<br/><br/>
There is no doubt that the decision will optimize the creation of a balanced work environment and foster the participation of UAE nationals in the private sector thereby enhancing their competitiveness and productivity.
<br/><br/>
As part of the changes introduced under the new decision, a special regulatory unit was created at the Ministry of Human Resources and Emiratization (The “Ministry”) to organize and secure the Emiratization process. This unit is expected to provide the right platform to ensure that UAE national employees are able to perform within a healthy and suitable work environment and that they can rely on a strong mechanism to address and solve employment related complaints.
<br/><br/>
Further, the decision outlined clearly the conditions under which an employment relation with a UAE national can be terminated. Employers in the private sector are now required to submit an exit interview at the expiration or termination of a work relationship with UAE national employees including the reasons for the termination of the relationship.
<br/><br/>
In all cases of termination of the employment of UAE nationals, employers must procure that their contributions and the contribution of the insureds (the UAE national employees) are fully paid to the pension and social security scheme approved in the country and in any other schemes required by the legal systems in force at the Ministry in favor of the UAE national.
<br/><br/>
Termination of UAE nationals can occur in any of the following conditions:
<br/><br/>
– If the contractual employment relation expired without being renewed;
– If the termination is based on Article 120 of the UAE Federal Law No. 8 of 1980 also known as the UAE Labor Law;
– If the relation is terminated amicably by the parties;
– If the relation is terminated by any of the parties pursuant to a notice with a period varying between one (1) and (3) months provided that it is not    less than three (3) months in limited contracts and where there is a disagreement between the parties over the calculation of the notice period.
<br/><br/>
Termination of UAE nationals is deemed as unlawful in any of the following conditions:
– If the termination is not based on Article 120 of the UAE Federal Law No. 8 of 1980 also known as UAE Labor Law
– If it is proven that the employer has retained a non-national employee to perform the same work as the UAE national employee, or if it has been  proved that the termination of the UAE national was for the purpose of replacing him/her with a non-national employee without any justification acceptable to the Ministry in both cases.
– If the reasons for the termination are not related to the employment relationship especially if it is due to the employee submitting a serious complaint to the competent authorities or filing a valid claim against the employer.
<br/><br/>
By virtue of the new decision, if the Ministry ascertained that the employment relationship has been terminated unlawfully by the employer or the employee and has not been able to settle the dispute and reinstate the UAE national to his/her work, the dispute shall be referred to the competent labor court after the expiry of 5 working days from the date of notifying the parties to resolve the dispute amicably without reaching a settlement.
<br/><br/>
In such circumstances, the Ministry is entitled to suspend the permits given to the employer to issue new work permits for a period not exceeding 6 months from the date when the UAE national’s claim is proven to be accurate or from the date on which a final judgment is issued.
<br/><br/>
The new decision entitled the Ministry to impose fines on employers equal to AED 20,000 per incident in any of the following cases:
<br/><br/>
a- Breach by the employer of the provisions of the new decision or any of the subsequent guidelines issued following its publication;
b- Unlawful termination of a UAE national and failure to reinstate an employee after being required to do so by the Ministry;
c- The Employer’s failure to maintain and settle its contributions due to the pension and social security as applicable in the country; or
d- Failing to fulfill Emiratization requirements in an effective manner.
<br/><br/>
At this stage, the Ministry is committed to provide training and development programmes to UAE nationals that will enable them to show their full potential, in addition to continuous training programs after employment to support their continuous progress and enable them to become future leaders with a high level of competence and commitment.
<br/><br/>
It is expected that the above changes will contribute to motivate UAE nationals and encourage them to work with private sector corporations as the measures implemented now can ensure to UAE nationals a stable, long term and secure employment relationship.
<br/><br/>
Emiratization is an important national issue and this new decision will support its effective implementation and the efforts pursued by the Ministry in the past years to optimize the employment of UAE nationals in the private sector. It will further promote the creation and training of qualified national cadres capable of taking the lead in the overall development of a sustainable economy.
<br/><br/>
&nbsp;','post_date' => '2018-12-11 15:35:31','post_name' => 'new-rules-regulating-the-employment-of-the-uae-nationals-in-the-private-sector','post_parent' => '0','Image' => 'wp-content/uploads/2018/12/shutterstock_592724405.jpg','Categories' => '61, 86','Tags' => '97, 1071'),

            array('ID' => '61141','post_title' => 'Cybersecurity Issues During COVID-19 Pandemic: A Brief Update on Key Issues','post_content' => '<h4><em>Public fear leads to hacking opportunities</em></h4>
The rise of COVID-19 has seen a significant change in the way many industries and corporations conduct their day to day work. While remote working arrangements are not new, we are seeing more employees than ever moving to such arrangements across the globe leading to many companies scrambling to set-up adequate IT systems and provide access to the basic work tools needed to conduct their jobs efficiently.
<br/><br/>
At the same time, we are seeing a significant increase in cybercrimes exploiting existing vulnerabilities as well as new vulnerabilities arising out of the increase in remote working arrangements. Cyber incidents have been tailored to the pandemic with malware circulating in the form of phone applications updating users on incidents of the virus as well as phishing emails claiming to be sent by major health organizations around the world.
<br/><br/>
<em>Important issues must be addressed</em>
<br/><br/>
It is important for companies to implement clear remote working policies and procedures. Some of the issues include the following:
<ul>
 	<li>Ensuring that all employee devices are up to date with anti-virus software and that all appropriate training has been provided to current and new joiners on how to identify phishing emails and what information should or should not be divulged over email.</li>
 	<li>Avoiding the opening or sending of documents that may contain sensitive materials while connected on personal devices or connected on public WiFi networks. Companies should already have clear protocols in place should they allow employees to use personal devices for business purposes.</li>
 	<li>Keeping a watchful eye on money transfers and always validating beneficiary details before making payments. This is a common and easy target for cyber-criminals and we expect invoice redirection and change of bank details to heavily increase during this period.</li>
 	<li>Allowing employees access to company-owned Virtual Private Networks and ensuring appropriate storage and back-up of all key digital files. This is especially an issue where companies use outdated VPN’s and do not utilize multi-factor authentication for log-in purposes.</li>
 	<li>Ensuring that any phone calls or video calls are taken and conducted in a private forum to ensure confidentiality of the conversations.</li>
 	<li>Dealing with confidential information in particular where such information may have been printed or provided in hard copy and where there is no easy access to appropriate facilities to dispose of such documents.</li>
</ul>
As highlighted earlier, many organizations are rapidly adopting new technology that either was not a necessity prior to COVID-19 or has now become accessible. The latter is particularly relevant as the Telecommunications Regulatory Authority (“<strong>TRA</strong>”) in the UAE has, on a temporary basis, allowed the usage of VoIP applications such as Zoom and Google Hangouts and VoIP features on communication platforms such as Microsoft Teams.
<br/><br/>
It is important that companies provide clear and accurate directions to all staff on how to use any new technology platforms to ensure that any potential exposure to cyberthreats is reduced. Issuing regular cybersecurity reminders to employees must become a necessity and it is vital that a clear strategy to respond to any potential threats is in place and tested.
<br/><br/>
<em>DFSA an early-mover in reducing threats</em>
<br/><br/>
It is timely that the Dubai Financial Services Authority (“<strong>DFSA</strong>”) launched in January of this year their new cyber threat intelligence platform with the aim to facilitate the detection of cyberattacks and ensure that all firms regulated and supervised by the DFSA have a platform on which to share any credible intelligence on potential cyber threats.
<br/><br/>
This platform is unique in the UAE and offers a good example of what could become a widely adopted model in several other free zones and jurisdictions in the Middle East. The threat of cyberattacks are especially concerning for regulated entities in the DFSA as they may be exposed to serious liability should a cyber attack occur and data is stolen or services are interrupted. Authorized individuals may also bear a certain amount of liability and face enforcement action by the DFSA in specific circumstances.
<br/><br/>
We will continue to monitor any relevant cybersecurity developments related to COVID-19.
<br/><br/>
Authored by Senior Associate <a href="https://bsabh.com/lawyer/nadim-bardawil/">Nadim Bardawil</a>','post_date' => '2020-03-26 10:54:17','post_name' => 'cybersecurity-issues-during-covid-19-pandemic-a-brief-update-on-key-issues','post_parent' => '0','Image' => 'wp-content/uploads/2020/03/shutterstock_1269361138.jpg','Categories' => '1048, 1696','Tags' => '96, 1672, 1756, 1759'),

            array('ID' => '61330','post_title' => 'Implementation of UAE Life Insurance Regulations Delayed','post_content' => '<h4>The UAE Life Insurance Regulations delayed by 6 months</h4>
The Insurance Authority issued the Board of Directors’ Decision No. (49) of 2019 Concerning Instructions for Life Insurance and Family Takaful Insurance (“Life Regulations”) in October 2019 with an implementation date 6 months from the date of gazette of the Life Regulations, which were due to come into force effective 16 April 2020.
<br/><br/>
The Insurance Authority Board of Directors’ Resolution No (15) of 2020 dated 30 March 2020, On the Amendment to Certain Provisions of the Board Resolution No 49 of 2019 On the Instructions as to Life Insurance and Family Takaful Insurance has delayed implementation of the Life Regulations by 6 months, which means the Life Regulations will be effective from 16 October 2020.
<br/><br/>
It is noteworthy that the Life Regulations were issued late last year after 2 years of consultation on the draft. While the majority of the Insurance sector hailed this as a welcome move, due to the drastic measures announced by the Life Regulations, such as caps on commissions and indemnity commissions, the insurance intermediaries felt that the Life Regulations would be counterproductive to the growth of the life insurance market in the UAE.
<br/><br/>
The Life Regulations bring in a mandatory free-look period of 30 days, enhanced disclosures to customers and complete transparency in terms of other charges and fees and would greatly cut down on mis-selling of life insurance products. However, given the unprecedented times we are living in, where insurance companies are faced by large and uncertain claims exposure, under health, travel and especially life insurance, the extension by the UAE Insurance Authority is a step in the right direction, as it will give insurance companies time to focus on dealing with other immediate concerns.
<br/><br/>
Authored by Senior Associate, <a href="https://bsabh.com/lawyer/anand-singh/">Anand Singh</a>','post_date' => '2020-04-02 07:52:58','post_name' => 'implementation-of-uae-life-insurance-regulations-delayed','post_parent' => '0','Image' => 'wp-content/uploads/2020/04/shutterstock_1363135319.jpg','Categories' => '37, 1696','Tags' => '125, 141, 1203, 1672, 1693'),
            array('ID' => '3925','post_title' => 'Implementation of the Value Added Tax in the United Arab Emirates','post_content' => '<h5>Introduction</h5>
In February 2016, the Ministry of Finance announced the introduction of Value Added Tax (“VAT”) in the UAE, and has ended a long period of speculation and discussion of “when” and “if” VAT would be introduced.
<br/><br/>
According to the announcement, the UAE will impose VAT at a rate of five percent on the sale of goods and on services as from 1 January 2018. The five percent rate has been agreed between all GCC countries, and the framework agreement on the implementation of VAT is expected for release in July this year. The implementation will take place throughout the GCC, however the other GCC countries have from 1 January 2018 until 1 January 2019 to also implement VAT. The amount generated from VAT revenue in the UAE is expected to be between AED8 and AED12billion in the first year.
<br/><br/>
According to the Minister of State for Financial Affairs, Obaid Humaid Al Tayer, there will be some exemptions from VAT: the GCC countries have agreed to approximately 100 staple food items that will be exempt, and have agreed to zero rate certain industries such as education, social services and health care. Furthermore, according to Younis Ak Khoury, undersecretary at UAE’s Ministry of Finance, the implementation of the VAT will take place in several phases. Thereby, VAT registration in Phase 1 will be obligatory for UAE companies that generate an annual revenue of AED 3,75 million and above. For UAE businesses with an annual revenue between AED 1,87 and AED 3,75 million VAT registration will be optional. With the roll-out of Phase 2, the registration will become obligatory for all businesses. The introduction of a value-added tax system in the UAE will not only affect consumers but also have a broader impact on business. The announcement of the VAT introduction date has put an end to the discussion about the possibility of VAT in the UAE, and raised new discussions about the readiness of businesses for the proposed date of 1 January 2018. Some business owners say they will require more time to implement the new system or to revamp their financial structures.
<br/><br/>
At the moment, it is uncertain exactly which VAT model will be implemented in the UAE and if every GCC country will apply the same model or if there will be different models to implement. We can mention three main VAT systems, namely the European, New Zealand and Japanese models.
<br/><br/>
The New Zealand model comes closest to resembling the ideal as it is levied at a single rate and is based on a relatively broad consumption of goods and services, extending through the retail stage of the economy with minimal exclusions. Most jurisdictions have adopted a European VAT model, however, this is marked by multiple rates and varying degrees of exemptions. In practice, no two VAT systems look exactly alike but show differences in rates, thresholds, exemptions, refunds and coverage.
<br/><br/>
The VAT system to be implemented by the GCC is still subject to speculation, but the following factors have to be considered:
<ul>
 	<li style="list-style-type: none;">
<ul>
 	<li style="list-style-type: none;">
<ul class="list">
 	<li>Ease of administration and efficiency of the system;</li>
 	<li>Economic impact; and</li>
 	<li>Possible adoption of VAT models implemented by other Arab countries or those in the Middle East and North Africa (MENA) region.</li>
</ul>
</li>
</ul>
</li>
</ul>
The goal of this article is not to analyze which of the models will be the best for the UAE but to start a discussion on the practical implementation of VAT and the day-to-day challenges that businesses may face in the UAE after 1 January 2018.
<h5>Preparing for VAT (“VAT-readiness”)</h5>
In order to deal with the challenges, businesses in the UAE should evaluate and review their daily business activities, with a focus on the following points.
<br/><br/>
<strong>Understanding:</strong> Conduct analysis of all operational processes and business domains in order to determine all areas that will be affected.
<br/><br/>
<strong>Preparation:</strong> Elaborate future implementation strategy, prepare a checklist.
<br/><br/>
<strong>IT Systems:</strong> Make sure your IT systems are VAT enabled, including enterprise resource planning (ERP), accounting and point-of-sales IT systems. The IT systems must be able for complete implementation of all legal regulations in order to comply with VAT requirements in daily business.
<br/><br/>
<strong>Current accounting:</strong> Keep your books in order and make sure they are up to date by 1 January 2018, ready for the change as the time of supply is critical to your VAT liability.
<br/><br/>
<strong>VAT accounting:</strong> Ensure you have all the correct accounts and VAT codes set up to accommodate the calculation of your responsibility to generate a VAT return, which will need to be submitted periodically.
<br/><br/>
<strong>VAT return:</strong> Make sure your incoming and outgoing invoices are VAT compliant, on both the supply and sales fronts.
<br/><br/>
<strong>Employees:</strong> Check that your staff is knowledgeable about VAT and update job descriptions to reflect VAT duties and responsibilities.
<br/><br/>
<strong>Public officer:</strong> Appoint an appropriate person to be the public officer representing the company before the VAT authorities and make sure the person is suitable and prepared for the role.
<br/><br/>
<strong>VAT guide:</strong> Prepare a policy manual for the business, to cover the company’s policy regarding VAT.
<br/><br/>
<strong>Contracts:</strong> Put resources in place to provide for VAT should any contract or project span into January 2018.
<h5>Compliance</h5>
As general custom around the world, the principles of self-declaration as well as of prepayment are expected to be applied in UAE. Companies, as registered taxpayers, shall be responsible for calculation of the VAT payable as part of a self-assessment process and execute a prepayment during a fiscal year. At the end of the assessment year the company shall submit an annual tax declaration.
<br/><br/>
It is expected that the duties and responsibilities of company management will incur certain liabilities, and the consequence of non-compliance, errors and omissions weighs heavily on the company’s managers.
<br/><br/>
Taking into consideration the abovementioned consequences of non-compliance, the coherent and correct implementation of the VAT process and legally compliant execution on daily basis is essential for the management of the company in order to avoid future errors and sanctions.
<h5>Conclusion</h5>
After implementation, VAT will affect practically all functions within a business, including IT, human resources, procurement, finance and marketing. VAT is applicable on goods and services at each stage of the supply chain with the ultimate burden being borne by the customer, at least in theory. If not applied correctly, VAT may become an additional cost to the business, and non-compliance with the tax law will lead to severe penalties.
<br/><br/>
Therefore, all businesses must undertake and review their current situation in terms of their suppliers and customers and if the former are registered or not. The timely training of employees who will be dealing with VAT issues is just as crucial as ensuring that all systems of the business enable VAT administration.
<br/><br/>
Further development and details are still awaited, however, it is advisable for all businesses to start the review and preparation process in a timely manner to be able to evaluate their company’s situation and apply the necessary changes – after all, to paraphrase W. Edwards Deming “if you can measure it, you can manage it”.
<br/><br/>
&nbsp;
<table width="742">
<tbody>
<tr>
<td style="text-align: left;" width="146">Published:</td>
<td style="text-align: left;" width="187"><strong>July 2016</strong></td>
</tr>
<tr>
<td style="text-align: left;">Practice:</td>
<td style="text-align: left;"><strong><a href="https://bsabh.com/legal-practice-areas/corporate-law-and-ma/" target="_blank" rel="noopener">Corporate and M&amp;A</a></strong></td>
</tr>
<tr>
<td style="text-align: left;">Publication:</td>
<td style="text-align: left;"><strong><a href="http://www.german-emirates-club.com/" target="_blank" rel="noopener">German Emirates Club</a></strong></td>
</tr>
<tr>
<td style="text-align: left;">Authors:</td>
<td style="text-align: left;"><a style="font-weight: bold;" href="https://bsabh.com/lawyer/john-peacock/" target="_blank" rel="noopener">John Peacock</a><strong>and</strong><strong><a href="https://bsabh.com/lawyer/svetlana-weiss/" target="_blank" rel="noopener">Svetlana Weiss-Friesen</a></strong></td>
</tr>
</tbody>
</table>
&nbsp;','post_date' => '2016-07-27 16:44:14','post_name' => 'implementation-of-the-value-added-tax-in-the-united-arab-emirates','post_parent' => '0','Image' => 'wp-content/uploads/2016/07/shutterstock_1011603964.jpg','Categories' => '31, 36, 86, 100, 103, 106','Tags' => '56, 95, 104, 127, 129, 709, 718, 719'),

            array('ID' => '61981','post_title' => 'Economic Substance Regulations for IP Rights Holders','post_content' => '<h4>Following a review conducted by the European Union on countries with low or nominal corporate tax, the committee prescribed the ‘Economic Substance Regulations’ and requested that the low tax jurisdictions review their own regulations and make efforts to ensure their compliance with best practices. The purpose of the regulations is to impede potentially harmful tax practices by requiring entities within low tax jurisdictions to demonstrate they carry out a substantial amount of economic activity in the country in which they are incorporated.</h4>
As a result, on 30 April 2019, the UAE Cabinet issued Cabinet of Ministers Resolution No 31 of 2019, which concerns economic substance regulations in the UAE, requiring all qualifying UAE entities that conduct ‘relevant activities’ to demonstrate their economic substance in the UAE on an annual basis.
Over the course of 2019, the Ministry of Finance published a series of guidelines, flow charts and information to assist UAE entities in assessing whether they would be considered subject to the new Economic Substance Regulations and if so, to advise of the applicable deadlines which they must adhere to.
<br/><br/>
Who is subject to the Economic Substance Regulations?
<br/><br/>
According to the relevant laws and regulations, the definition of an entity which falls within the scope of these guidelines is broad and is considered as any entity with a UAE trade license that carries out a ‘relevant activity’. The relevant activities are listed as follows:
<br/><br/>
● Banking Businesses
● Insurance Businesses
● Investment Fund Management Businesses
● Lease-Finance Businesses
● Headquarter Businesses
● Shipping Businesses
● Holding Company Businesses
● Intellectual Property Businesses
● Distribution and Service Centre Businesses
<br/><br/>
It is important to note that the regulations apply irrespective of whether the entity is in a free zone or is considered as a mainland entity. Entities which are excluded from the regulations are any that are directly or indirectly owned by the government.
<br/><br/>
What are the requirements?
<br/><br/>
In order to fulfil the economic substance requirements, a relevant UAE entity that conducts a ‘relevant activity’ must comply with the following:
<br/><br/>
• Conduct the \'core income-generating activity\' within the UAE;
• direct and manage the entity from the UAE;
• employ full-time staff in the UAE (or outsource to a UAE-based provider);
• incur operating expenditure in the UAE;
• retain adequate physical assets in the UAE.
<br/><br/>
Compliance with reporting requirements
<br/><br/>
Any Relevant Entity is required to notify their regulatory authority (i.e. the authority which issued their Trade License) and submit their annual report in line with the prescribed deadlines.
<br/><br/>
The annual reports must contain information concerning the relevant activity, the income, any assets or expenses and a declaration that the requirements of the Economic Substance Test have been met.
<br/><br/>
As each regulatory authority is empowered to decide whether an entity is in compliance with the Economic Substance Test, each authority may interpret the application of the test differently.
<br/><br/>
The aforementioned requirements are the general requirements that are applicable to relevant entities. However, where an entity is considered to conduct “high risk Intellectual Property activities’ they will be subject to more stringent requirements.
<br/><br/>
What does this mean for Intellectual Property?
<br/><br/>
A UAE entity is considered to conduct the relevant activity of Intellectual Property (IP) if it holds, exploits or receives income from any IP Assets.
<br/><br/>
The above does not apply to an entity that simply maintains its intellectual property portfolio as a supplementary part of their business activity. The majority of entities within the UAE will hold IP assets in the form of trademarks, patents, copyrights and trade secrets in order to protect their brand for the goods or services that they offer. However, simply owning IP assets does not equate to conducting the business activity of Intellectual Property for the purposes of the Economic Substance Regulations, unless an entity is using its IP assets to procure and benefit from a separate income.
<br/><br/>
In the event that an entity is using their Intellectual Property assets to procure a separate income by way of a licensing or franchise agreement and where they are in receipt of royalties, the entity will be required to declare ‘Intellectual Property’ as a relevant business activity to their regulatory authority and submit their reports demonstrating compliance with the Economic Substance requirements.
<br/><br/>
According to the applicable regulations, the core income-generating activity required will depend on the IP asset that is being exploited and how the asset is operating to procure income.
<br/><br/>
High Risk Intellectual Property Licensee
<br/><br/>
In accordance with the Economic Substance Test conditions, where an entity established in the UAE is a Licensee of Intellectual Property and is conducting the business activity of Intellectual Property within the UAE, it will need to consider whether it falls within the remit of a ‘High Risk’ Licensee and will be required to declare this accordingly.
<br/><br/>
A Licensee is considered a High Risk IP Licensee if it receives income from an Intellectual Property asset and meets all of the following three requirements:
<br/><br/>
1. The Licensee did not create the IP asset which it holds for the purpose of its business; and
2. The Licensee acquired the IP asset from either:
<ul>
 	<li>A group company; or</li>
 	<li>In consideration for funding research and development by another person situated in foreign jurisdiction; and</li>
</ul>
3. The Licensee licenses or has sold the IP Asset to one or more group companies, or otherwise earns separately identifiable income (e.g. royalties, licence fees) from a foreign group company in respect of the use or exploitation of the IP asset.
<br/><br/>
As per the regulations, ‘income derived from IP assets poses a greater risk of profit shifting as opposed to income derived from non-IP related activity’. Therefore, any Licensee which meets the above criteria of a High Risk IP Licensee will be presumed as automatically failing the Economic Substance Regulation Test. As a result, the UAE will automatically exchange information with the any concerned Foreign Competent Authorities to which the Parent Company, Ultimate Parent Company and/or Ultimate Beneficial Owner of the High Risk IP Licensee are residents of.
<br/><br/>
The presumption can then be rebutted by the concerned Licensee in the event that they are able to fulfil the enhanced economic substance requirements. This includes producing evidence of the following:
<br/><br/>
• that decision making is taking place with the UAE.
• that it has or had a high degree of control in developing the IP asset
• that it has a business plan demonstrating the reasons for holding IP assets in the UAE
• that it has adequate full-time employees permanently residing in the UAE
<br/><br/>
In the event that a High Risk IP Licensee is unable to meet the enhanced Economic Substance requirements, the Licensee will receive notice of the application an administrative penalty in addition to any actions it must undertake to satisfy the Economic Substance Test. The administrative penalty should not exceed fifty thousand Dirhams in the first instance. However, if the Licensee fails to meet the conditions of the test in the next financial year, the penalty will increase to up to three hundred thousand Dirhams.
<br/><br/>
The proper implementation of Economic Substance law in UAE will proof the country’s commitment to IP owners that incorporate their IP holding companies in UAE to be internationally accepted without busines difficulties. For instance, the issue resulted from a review of the UAE tax framework by the EU resulted in the UAE being included on the EU list of non-cooperative jurisdictions for tax purposes (EU Blacklist) should be resolved in the future. IP owners are invited to meet their regulatory commitment, make submission to authorities and disclose their assets/licenses in according to acceptable formalities. Holding companies, such as those at JAFZA or any other free zones, are being notified by their companies’ registrars and urged companies to complete and file the forms. This demonstrate a serious step is being taken to implement and enforce this law to clear UAE from the EU list and enrich the level of transparency in doing business in UAE as being the leading economic hub at the region.
<br/><br/>
For any queries or assistance with respect to plant varieties filing in the UAE and/or Middle East Region, please contact Partner and Head of Intellectual Property, <a href="https://bsabh.com/lawyer/munir-abdallah-suboh/">Munir Suboh</a> and Senior Associate, <a href="https://bsabh.com/lawyer/felicity-hammond/">Felicity Hammond</a>','post_date' => '2020-07-26 16:02:59','post_name' => 'economic-substance-regulations-for-ip-rights-holders','post_parent' => '0','Image' => 'wp-content/uploads/2020/07/shutterstock_115474381.jpg','Categories' => '86, 678','Tags' => '663, 923, 1735, 1885'),

            array('ID' => '57368','post_title' => 'Employment Law - An Overview','post_content' => 'When it come to UAE employment law, Federal Law No. 8/1980 (also know as the UAE Labour Code) or the Law Regulating Labour Relations the main law governing employee-employer relations in the private sector in the UAE. This law outlines the key employment aspects including working hours, annual leave and public holidays, sick leave, the employment of young people, maternity leave, employee health and safety, termination of employment and end of service gratuity. According to Article 3 of Federal Law No. 8/1980, this law applies to all employees working in the UAE, whether they are UAE nationals or expatriates. However, there are certain categories of employee who are exempt from it and may have to follow different regulations.
<br/><br/>
<strong>FEDERAL LAW NO. 8/1980 EXCEPTIONS</strong>
Federal Law No. 8/1980 provisions do not apply to a number of categories of employee including officials, employees and workers of the Federal Government, Governmental Departments of UAE Emirates, of the State, municipality officials, employees and workers and other officials working in Federal and local public Departments and organisations. Others exempt from this law include officials, employees and workers appointed by Governmental Federal and local projects; members of the Armed Forces, Police and Security services and domestic servants working in private residences (who are covered by Federal Law No. 10/ 2017 on support service workers).
<br/><br/>
There are also exemptions for workers employed in the agricultural or pastoral sectors, although not those employed in agricultural corporations engaged in processing these products or those permanently engaged in operating or repairing mechanical machines used in agriculture.
<br/><br/>
The Ministry of Human Resources and Emiratisation previously known as the Ministry of Labour is responsible for overseeing and administering employer-employee relationships and labour rights for the private sector.
<br/><br/>
<strong>FEDERAL DECREE LAW NO. 11/2008</strong>
In contrast, public sector employees are governed by the Federal Decree Law No. 11 /2008, as amended. These laws apply to the civil servants who earn their salaries from the Federal budget, and civil employees working at Federal authorities and corporations. The UAE government also introduced the Emiratisation programme in 2004 to encourage employment of its citizens in both the public and private sectors. This programme aims to reduce the UAE’s dependence on foreign workers and ensure that UAE citizens benefit from the country’s economic grow.
<br/><br/>
<strong>FREE ZONES</strong>
Workers employed in the UAE free zones are generally not governed by Federal Law No. 8/1980. Each free zone authority may have its own set of rules and regulations and their employees are subject to these. Workers in these free zones are sponsored by the respective free zone authority and not by their employer. In particular, Federal Law No. 8/1980 does not apply in the DIFC where employment is governed by the DIFC employment law, DIFC Law No. 3 /2012.
<br/><br/>
<strong>EMPLOYMENT CONTRACTS</strong>
Before starting work in the UAE, expatriates will need a work permit and residency visa to enable them to live and work in the country. These documents are obtained after signing the requisite employment contract with the sponsor. All employment contracts and any related amendments must be approved and registered with the Ministry. The standard forms and templates for job offers and employment contracts which comply with the basic requirements of Federal Law No. 8/1980 can be found on the Ministry’s web portal. Federal Law No. 8/1980 envisages two types of employment contracts, limited/fixed-term and unlimited/open-ended (terminable on notice) contracts.
<br/><br/>
Fixed-term contracts cannot exceed two years but can be renewed by mutual consent for a similar period. Employment contracts without a termination date are deemed an unlimited/open-ended contract. In general, employment contracts must specify their date, nature of the contract (fixed or open-ended), job designation, term of contract (for fixed-term contracts), location of employment, salary including the base salary (employee’s wage excluding all allowances) as this is used to calculate end of services benefits, holiday pay, overtime, compensation for employment-related death, injury or disease and allowances for transportation and housing, and any bonuses. If the position involves access to private or confidential information, the employer may include a confidentiality and non-compete clause in the employment contract.
<br/><br/>
However, non-compete clauses are carefully scrutinized before being enforced in the UAE and must be reasonable and limited in terms of the duration (a maximum of three years) and geographical scope as well as specific to the field or sub-field of employment in order to be enforceable. Nevertheless, the judges in UAE courts rarely enforce restrictive provisions for a period of more than one year and prefer to award monetary compensations for any such breach instead of granting injunctions.
<br/><br/>
They will also require evidence of the actual harm or damages suffered by the employer. Ministerial Resolution No. 764/2015 which came in effect on 1 January 2016 has also provided greater protection for expatriate employees by ensuring their employment contract is consistent with the terms originally offered to the employee. The contract cannot be altered unless the employee has agreed to such a change and even if changes are agreed they will be subject to the Ministry’s approval to ensure they are not detrimental to the employee.
<br/><br/>
Employees may also be subject to a probationary period of not more than six months, during which employment may be terminated without notice or severance benefits. For the purposes of termination after the probationary period, a distinction is made between contracts of employment for limited and unlimited periods. A contract for a limited term terminates at the end of the specified term unless the parties choose to renew it. A contract without a specific term, however, may be terminated if both parties agree to cancel it, or if one of the parties does so with 30 days prior notice (or less if the employee is engaged on a daily basis), or for reasons expressly permitted by Federal Law No. 8/1980.
<br/><br/>
<strong>MINIMUM WAGE</strong>
There is no minimum salary stipulated in Federal Law No. 8/1980, but it broadly mentions that salaries must cover employee’s basic needs. Article 63 of Federal Law No. 8/1980 mentions that the minimum wage and cost of living index is determined either in general or for a particular area or a particular profession by virtue of a decree and consent of the Cabinet.
<br/><br/>
<strong>WORKING HOURS</strong>
Under Article 65 of Federal Law No. 8/1980, the normal working hours for the private sector are eight hours a day or 48 hours per week and no more than five consecutive work hours are allowed without a rest period. Working hours for certain businesses, including hotels and cafes may be increased after the necessary approval from the Ministry. Governmental entities which are not governed by Federal Law No. 8/1980 operate for seven hours a day.
<br/><br/>
If the employee’s job demands working beyond normal working hours as overtime, they are entitled to pay equal to their normal working hours’ remuneration plus 25% but this can increase to 50% for overtime worked between 9:00 p.m. and 4:00am. It is important to note that these working time and overtime provisions do not apply to those with senior executive managerial supervisory positions who have the power of an employer over employees.
<br/><br/>
The Ministry categorises these as chairpersons and members of boards of directors, general managers, managers of departments, and individuals working in supervisory posts who have dedicated powers of authority over other employees. In addition, construction and industrial workers are not permitted to work during the hottest hours of the day during the summer.
<br/><br/>
Any employer found to have staff working during their designated break time will also be fined 5,000 AED per worker up to a maximum of 50,000 AED. Normal working hours are also reduced by two hours daily during the holy month of Ramadan. As Friday is the official weekend for all employees in the UAE, except daily wage workers, if they have to work on a Friday, they are entitled to their regular working hours’ pay, and an increase of not less than 50%.
<br/><br/>
<strong>LEAVE</strong>
Employees who have been employed for at least six months to one year are entitled to annual leave of two days per month, and 30 days per year if they have been employed for more than one year. The annual leave allowance is equal to the sum of the base salary plus housing allowance (if any) for the complete duration of the leave. Employees are also granted special leave without pay, of not more than 30 days to perform the Hajj once in their complete term of service. Employees are not entitled to any paid sick leave during their probation period. However, after this they are entitled to sick leave of up to 90 days per year subject to conditions stipulated in the law.They will receive full pay for the first 15 days, half pay for the next 30 days and no pay for the remaining 45 days.
<br/><br/>
However, they must provide evidence of their illness with an official medical certificate issued by the relevant governmental institution. Women working in the private sector are also entitled to 45 days maternity leave, inclusive of pre-natal and post-natal periods. They will receive full pay during this if they have completed at least one year of continuous service or half pay if they have not. A woman can also take an additional 100 consecutive or non-consecutive days of unpaid leave if they have an illness that prohibits them returning to work even after the 45-day period.
<br/><br/>
However, this illness must be certified by a physician to have been related to the maternity. These benefits are in addition to other benefits available under Federal Law No. 8/1980 such as the two additional half-hour breaks during the day for nursing a baby for 18 months from the birth of the child which are included in their working hours and are paid. Employees in the UAE are also entitled to paid leave on public holidays such as the Hijri New Year (1 day), Gregorian New Year (1 day), Eid Al Fitr (2 days), Arafat day and Eid Al Adha (3 days), Prophet Mohammed’s birthday (1 day), Isra and Miraj or Ascension Day (1 day), Commemoration or Martyr’s Day (1 day) and National Day (1 day).
<br/><br/>
<strong>WAGE PROTECTION</strong>
Under Ministerial Decree No. 739/2016 Concerning the Protection of Wages, all establishments registered in the Ministry must pay their employees’ wages on the due date through the Wages Protection System (WPS). This is designed to protect workers’ rights as it is administered by the Ministry and allows verification that wage payments are in line with the terms initially offered. Under the WPS system, employees’ salaries are transferred to their accounts in banks or financial institutions, authorised by the UAE Central Bank to provide this service. If there are any concerns or complaints on salary, employees can contact the Ministry or lodge a complaint through eNetwasal.
<br/><br/>
<strong>TERMINATION</strong>
Generally a limited contract can be terminated if the contract expires and is not renewed if both, the employer and employee mutually agree or if a worker commits a violation in Article 120 of Federal Law No. 8/1980. If either party wishes to singly terminate a fixed-term/limited employment contract for reasons other than those in Article 121 of Federal Law No. 8/1980, it should comply with the legal notice requirements. These cannot be less than one month or more than three months, and the parties must continue to honour their obligations during the notice period. As this sort of termination qualifies as an arbitrary termination, the party terminating the employment must under Article 116 of Federal Law No. 8/198 compensate the other party to the extent agreed as long as this does not exceed three months’ gross wages.
<br/><br/>
In the case of open-ended/unlimited contracts, employment can be terminated if both parties mutually agree or either party decides, at any time, to terminate the contract and abide by the legal notice requirements in Article 117 of Federal Law No. 8/1980 and continue to honour their obligations for the duration of the notice period, which cannot be less than one month or more than three months which means the employee will be entitled to full pay during the notice period calculated on the basis of their last remuneration and in return must perform their duties if the employer requests or if either party unilaterally terminates the contract, without complying with the legal notice and without default reasons the terminating party bears the legal consequences of early termination.
<br/><br/>
In addition, under Article 119 of Federal Law No. 8/1980 if an employer fails to give the employee notice of the termination, or reduces the notice period, they must pay the employee compensation equal to their remuneration for the entire notice period or the time it has been reduced. However, under Article 120 of Federal Law No. 8/1980 an employer can terminate any type of employment contract without notice if the employee adopts a false identity or nationality or provides forged documents or certificates or if they have a probation period and are dismissed before the end of it or if they have committed an error causing substantial material loss to the employer provided that they have advised the labour department of the incident within 48 hours of knowing about it or the employee has violated health and safety instructions involving the business provided that these are displayed in writing in conspicuous places or illiterate employees have been verbally informed of them.
<br/><br/>
This can also be the case if an employee fails to perform their basic duties under the employment contract and persists in violating them despite a formal investigation with them on this and if they have been warned of dismissal if this is repeated; or they divulge company secrets or are convicted in a final judgment by a competent court of an offence prejudicing honour, honesty or public morals. Dismissal is also possible if an employee is drunk or under the influence of prohibited drugs during work hours; assaults their employer, manager or colleagues in the course of their work, or is absent without lawful excuse for more than 20 intermittent days or for more than seven successive days in one year.
<br/><br/>
There are also a number of valid reasons for termination under Article 121 of Federal Law No. 8/1980 and under Article 2 of Ministerial Resolution No. 765 /2015 an employment relationship will be deemed to have been terminated if the employer breaches their legal (statutory or contractual) obligations including non-payment of wages for 60 days, if the employer or their legal representative assaults the employee, or an employee files a complaint that the employer has closed its business and is failing to employ them or following a final judgment which has been issued by the Courts in favour of an employee who has filed a labour complaint.
<br/><br/>
<strong>ARBITRARY DISMISSAL</strong>
Article 122 of Federal Law No. 8/1980 describes arbitrary termination as where an unlimited contract’s termination is irrelevant to the work, particularly if the reason is that the worker has submitted a serious complaint to the authorities or has instituted legal proceedings against the employer that have been proved to be valid. Grounds for termination based on business restructuring and redundancy, for unlimited employment contracts, may be held by UAE courts as being an action related to the business environment, and as such not an arbitrary action against a particular employee. As a result they may be found by a court to be justified.
<br/><br/>
If termination by the employer is deemed abusive by the court, the employer may be ordered to pay the employee additional compensation, without prejudice to the employee’s right to other post-termination entitlements. The court will assess this based on the nature of the work, the amount of prejudice the employee sustained and their period of service, after investigating the circumstances of the employment.
<br/><br/>
Compensation will not exceed three months’ remuneration calculated on the basis of the last remuneration the employee was entitled to. With limited contracts Article 115 of Federal Law No. 8/1980 states arbitrary termination by an employer is termination for reasons other than those specified in Article 120 and the employer must compensate the worker for any prejudice they have sustained but the compensation must not exceed the aggregate remuneration due for three months or the residual period of the contract whichever is less unless the contract contains a provision to the contrary.
<br/><br/>
It should be noted that business restructuring or redundancy is not included in the reasons in Article 120, and so termination of an employment contract concluded for a specified term because of this is likely to be deemed arbitrary.
<br/><br/>
<strong>END OF SERVICE DUES</strong>
An employee with an unlimited contract who has their employment terminated will be eligible for pay in lieu of noticeperiod, or any amount due in lieu of the notice or to work their notice. They may receive compensation for abusive dismissal if the contract was terminated by the employer for grounds not recognised by Federal Law No. 8/1980 as justifiable causes for termination but this will not exceed three months’ remuneration calculated on the basis of their last remuneration unless the contract so provides.
<br/><br/>
Those with a limited contract, can also receive compensation for abusive dismissal if the contract was terminated for grounds other than those in Article 120 and compensation will be either the aggregate remuneration due for three months or the residual period of the contract whichever is less, unless the contract provides to the contrary.
<br/><br/>
Overtime, unpaid salary, holiday pay, and repatriation expenses (either under Federal Law No. 8/1980 or the contract) will also be due, as will an end of service gratuity calculated on the basis of the period of service, if the employee has completed at least one year’s continuous service and was not terminated under Article 120 of Federal Law No. 8/1980. However, if an employee with a limited contract resigns before its expiry they will only be entitled to severance pay if they have more than five years’ continuous service.
<br/><br/>
<a href="https://bsabh.com/lawyer/asa-siddiqui/">Asma Siddiqui</a>, Associate atBSA article was originally published in Emirates Law Magazine. See the published article <a href="http://web.lexisnexis.fr/Fb/FB_DIJ_201812/6/">here</a>','post_date' => '2019-01-21 12:10:48','post_name' => 'employment-law-an-overview','post_parent' => '0','Image' => 'wp-content/uploads/2019/01/shutterstock_1032864043.jpg','Categories' => '61, 106','Tags' => '771, 967, 1172'),

            array('ID' => '61369','post_title' => 'Economic Relief Measures for the UAE during COVID-19','post_content' => '<h4>The UAE government has introduced certain measures that will offer economic relief to UAE businesses suffering under the strain of COVID-19 ramifications.</h4>
Although the UAE has not announced any direct relief from tax payment obligations similar to those introduced in KSA, the governments of both the Emirates of Dubai and Abu Dhabi have introduced financial assistance measures due to the current COVID-19 crisis.
<br/><br/>
<em><strong>THE EMIRATE OF DUBAI</strong></em>
<br/><br/>
In the Emirate of Dubai, the government has introduced the following measures:
<ul>
 	<li>An AED1,5 billion business stimulation package involving the reduction in fees payable by businesses for three months</li>
 	<li>Dubai Customs introduced the following measures which will be in place from 15 March 2020 till 30 June 2020:</li>
</ul>
<ol>
 	<li>A refund of 20% of all Customs Duty paid on goods at 5% which have been sold locally</li>
 	<li>The revokation of the AED50,000 cash or bank guarantees required to conduct customs broker activities and the refund of existing guarantees held to these brokers and clearing companies</li>
 	<li>The exemption from berthing fees as well as direct and indirect loading fees for boats that qualify as traditional wooden commercial boats registered in the UAE at either Dubai Creek or Hamriyah Port</li>
</ol>
<em><strong>THE EMIRATE OF ABU DHABI</strong></em>
<br/><br/>
In the Emirate of Abu Dhabi, the government has introduced:
<ul>
 	<li>With effect from 16 March 2020, a 16 point stimulus plan involving fee exemptions, fine waivers, SME support initiatives</li>
 	<li>An AED 5 billion water and electricity subsidy scheme</li>
 	<li>An AED 3 billion SME credit guarantee programme</li>
 	<li>The establishment of a AED 1 billion market maker fund to sustain a balanced supply and demand balance for stocks</li>
 	<li>The suspension of performance bonds for bidding procedures</li>
 	<li>A 25% reduction on new industrial land leasing fees</li>
 	<li>The waiver of industrial and commercial penalties</li>
 	<li>Cancellation of individual and commercial real estate registration fees</li>
 	<li>The payment of government approved invoices within 15 days</li>
 	<li>The suspension of Tourism and Municipality Fees for the tourism and entertainment sectors for 2020</li>
 	<li>Waiver of performance guarantees for start-ups for projects up till AED 50 million.</li>
</ul>
In addition to the government’s measures the private sector has also stepped up to the plate and numerous of the large property leasing companies have offered reduced and even exemptions from rental payments for commercial tenants that have had to shut down their businesses due to COVID-19. Various banks, property developers and even utility suppliers have also offered payment holidays and have deferred evictions and legal collections in the light of the current situation in the UAE relating to COVID-19.
<br/><br/>
Should you be experiencing financial pressures and would like to take advantage of any of the above measures, BSA is ready and eager to assist.
<div class="entry-content">
<br/><br/>
Authored byHead of Indirect Tax and Conveyancing,<a href="https://bsabh.com/lawyer/john-peacock/">John Peacock</a>
<br/><br/>
</div>','post_date' => '2020-04-02 10:07:06','post_name' => 'economic-relief-measures-for-the-uae-during-covid-19','post_parent' => '0','Image' => 'wp-content/uploads/2020/04/shutterstock_661369264.jpg','Categories' => '1, 31, 1696','Tags' => '56, 104, 1672, 1789'),

            array('ID' => '61279','post_title' => 'Dubai Notary Public Services to be Provided Remotely','post_content' => '<h4>A recent circular from the Notary Public in Dubai confirmed that due to COVID-19 all Notary Public services, in all branches, would cease up till the 9th April 2020. Up until this date it has been confirmed that certain Notary Public services may be conducted remotely.</h4>
The following Notary Services can be conducted remotely:
<br/><br/>
i) Power of Attorney notarization;
ii) Notarization of legal notices;
iii) Acknowledgments;
iv) Notarization of Local Service Agent Agreements;
v) Notarization of Memorandums of Association and addendums thereto for civil companies.
<br/><br/>
All services relating to commercial companies’ memoranda and addenda have been transferred to Dubai Economy and are no longer dealt with by the Dubai Notary Public.
<br/><br/>
The remote working times for the Dubai Notary Public will be from 8am till 4pm from Sunday to Thursday.
<br/><br/>
The service requires a subscription to BOTIM and the Notary office will contact the attestor to the document through this video connection to establish identity and knowledge of the document which must be sent to the Dubai Notary Public’s dedicated email address in pdf format with an approved reference to the remote signing on the bottom of each page. The fees will be payable by credit card and the courier will deliver the document at a cost of AED21 to your address.
<br/><br/>
There is therefore no need to wait for 9th April 2020 before important notarizations are attended to and BSA is able to assist you in finalizing the arrangements for notarial execution of your documents during this already stressful time.
<br/><br/>
Authored ByHead of Indirect Tax and Conveyancing, <a href="https://bsabh.com/lawyer/john-peacock/">John Peacock</a>','post_date' => '2020-03-30 16:24:43','post_name' => 'dubai-notary-public-services-to-be-provided-remotely','post_parent' => '0','Image' => 'wp-content/uploads/2020/03/shutterstock_1063275596.jpg','Categories' => '1696','Tags' => '104, 1672, 1708, 1774'),

            array('ID' => '1824','post_title' => 'Dubai Marina Torch Fire - What\'s Next? Who Pays?','post_content' => 'Non-fire retardant cladding poses an enormous risk for high-rises in the UAE, write <a href="https://bsabh.com/lawyer/barry-greenberg/">Barry Greenberg</a> and <a href="https://bsabh.com/lawyer/michael-kortbawi/">Michael Kortbawi</a> from BSA Ahmad Bin Hezeem &amp; Associates LLP
<br/><br/>
The recent fire at the Torch tower located in Dubai Marina raises several important concerns as to the safety of many of Dubai’s buildings, whether any measures can be taken to improve these structures’ safety, the costs thereof, and who would be responsible for bearing these costs. While there are immediate concerns, such as relocating those whose homes have been damaged, there are longer term wider considerations that should now be addressed.
<br/><br/>
The Torch is a “super-tall” structure; an 86 story, 352 meter high tower. When it opened in 2011, it was the tallest residential building in the world, but soon lost that title to, among other buildings, the Princess Tower directly opposite the street. Located in an area of the Marina known as “the tallest block in the world” due to its tightly clustered group of super-tall skyscrapers, the Torch and its neighboring towers are considered to be engineering marvels and are in many ways symbolic of Dubai’s rapid rise in international status.
<br/><br/>
In the very early morning hours of February 21, the Torch caught fire. While the exact cause is as yet unknown, speculation is that the conflagration was started by a barbeque or shisha coal left out on one of the building’s balconies. It reportedly started somewhere around the 50th floor on one of the building’s corners and raced up that corner, ultimately reaching the top of the building. Fanned by unusually strong winds, the fire spread rapidly and pieces of flaming debris fell from the building, causing another fire on the adjacent corner of the building, which likewise raced upwards approximately 20 stories.
<br/><br/>
Despite the horrific scene wherein two sides of the building burned high above the street, firefighters through their heroic efforts were able to extinguish the blaze, no lives were lost, and there were only a handful of minor injuries. And despite the visually disturbing images – dramatic video resembling scenes from the 1970’s disaster film The Towering <em>Inferno</em> was quickly transmitted around the world – the actual interior damage was minimal, with most residents able to reoccupy their apartments within days.
<h5>Not the first time</h5>
The Torch was not the first Dubai skyscraper to burn. In November 2012, Tamweel Tower located in Jumeirah Lakes Towers ignited due to a lit cigarette tossed into a pile of rubbish. The fire spread through the aluminum cladding affixed to the tower’s sides, ultimately reaching and consuming a large portion of the structure’s roof. Fortunately though, as with the Torch, no lives were lost.
<br/><br/>
Currently, over two years after this blaze, Tamweel remains unoccupied, the interior damage being much more significant than that sustained by the Torch and its repairs delayed, pending resolution of a dispute between the building’s Homeowners Association and its insurer.
<h5>Safety Issues – Aluminum Composite Cladding</h5>
A contributing hazard at Tamweel was identified as the non fire-resistant aluminum composite panels utilised to clad the exterior of the building. These cladding panels contain a potentially dangerous mix of aluminum and polyurethane. The flammable material is sandwiched between layers of aluminum, and when exposed to flame or even extreme heat, will ignite.
<br/><br/>
In some cases, this situation is exacerbated by the affixing of these panels to grid-work with air pockets beneath, allowing flames and heat to travel up both sides of the panels, thus resulting in a quicker ignition time. One engineer has compared the amount of flammable material contained in the cladding of a large building fitted with this type of composite, to that of a tanker delivering petrol to a filling station.
<br/><br/>
As a result of the Tamweel fire, and several other high rise conflagrations elsewhere in the UAE, regulations were enacted banning the use of non fire resistant composite panels of the type that contributed to Tamweel’s immolation.
<br/><br/>
The UAE updated its Fire and Life Safety Code in 2013 to require that exterior cladding is fire-resistant on all new buildings over 15 meters tall. Some of the individual emirates, including Abu Dhabi and Dubai, had been working towards amending their building codes even prior to the Tamweel fire, to prohibit the use of non fire resistant cladding.
<br/><br/>
These requirements, however, do not apply to already existing buildings. It has been estimated that there may be hundreds of high-rise buildings in the UAE that are clad with flammable composite material panels, placing each of those buildings at a higher risk of fire.
<br/><br/>
It is unclear at this time whether the exterior cladding tiles contributed to the Torch fire, as a forensics report has not been issued. However, if one observed the fire as it occurred, watched the video, observed the crumpled shards of aluminum littering the street outside the Torch immediately after the fire, or took a look at the burn pattern, the question that immediately comes to mind is how this fire was able to so quickly climb 30 stories up the side of the building, ignite on the opposite side, and climb up 20 or so floors from that location.
<br/><br/>
The answer that seems obvious is that the exterior building material was flammable.
<br/><br/>
It must be noted that the Torch management has recently stated that the building was constructed in full conformity with the building code. However, whether the building was up to code when completed in 2011 does not preclude that flammable cladding was utilised, as such may have been legal at the time.
<h5>Retrofitting of Existing Structures</h5>
The question that many building owners may now ask is if their buildings are constructed with this potentially dangerous cladding, and if so whether they may want to replace this material with a fire-resistant substitute.
<br/><br/>
However, the cost of this replacement in both material and labour will be prohibitive. Consider the largest buildings; scaffolding will need to be erected over 300 meters high and along the entirety of the building perimeter – or rope removal and replacement teams deployed – and thousands of square meters of panels will need replacing. The costs could easily run into the tens of millions of dirhams.
<br/><br/>
The owners should consider, however, before rejecting the idea of retrofitting due to cost factors, the potential cost of not retrofitting; the cost of doing nothing might be incurred not only in dirhams, but in human lives.
<br/><br/>
When considering what happened at Tamweel and the Torch, the lack of any fatalities can be viewed as fortunate, as no matter how efficient the evacuation and fire fighters efforts were, loss of life in a high rise fire is a great risk. And given how rapidly the flames spread in these very tall structures, a mass casualty event cannot be ruled out in the future. This is a nightmare scenario that no one wants to see.
<br/><br/>
The monetary cost of these events cannot be understated either.
<br/><br/>
Total loss of a supertall building – requiring demolition and replacement – is a distinct possibility in the event of a fire. Extensive refitting and need to obtain alternate residences for the occupants, as has occurred with Tamweel, is a probability. Owners now need to carefully consider these factors if they determine that their buildings have been fitted with these materials.
<h5>Who Pays?</h5>
Most of the residential buildings constructed prior to the enactment of more stringent building requirements have been fully turned over by their developers to their individual unit owners – and are now run by the building homeowners’ associations (HOA). It is these HOAs that will bear at least the initial cost of the retrofitting.
<br/><br/>
The issue then arises if this loss can be shifted back to the developer, general contractor, responsible subcontractor, supplier, or architect/engineer who specified this type of composite paneling.
<br/><br/>
This cost shift will depend less on whether the developer warranted the structure beyond the turnover date, but rather an analysis of whether prevailing law permits a claim of negligence for which recovery may be had under these circumstances. One of the issues here is that there is no actual “loss” in the cases of voluntary retrofitting. No damage has occurred and the cladding is performing as advertised. The retrofitting is only undertaken to prevent a future danger, albeit in light of recent events, a very foreseeable one.
<br/><br/>
However, UAE Civil Code Articles 880 – 883 imposes joint liability upon contractors and architects for a period of 10 years, “for any defect which threatens the stability or safety of the building”. There is a three year statute of limitations running from the time the defect is discovered. When the nature of the defects in these cladding tiles is “discovered” will be one of the major issues subject of dispute.
<br/><br/>
If the owners seek recovery from the developers, general contractor, designers, or other trades, this will trigger insurance claims by these parties. Their insurers will in turn need to determine if cover applies. Their first inquiry will be if there is an occurrence that would trigger coverage. An “occurrence” or an “accident”, depending on the policy wording, is normally required to trigger the duty to defend and indemnify, and the insurer will likely argue that the mere replacement of these composite panels is definitionally not an occurrence.
<br/><br/>
Additional issues will be whether the policy provides cover for completed operations, impaired products, the insured’s work, whether sufficient notice of claim was provided, application of aggregate limits, and what policy period is implicated in any claim. Insurers will resist these claims on many of these grounds and the insured will likely have to initiate litigation in order to have any chance to recover from its insurers.
<br/><br/>
Insurers, however, need to look at the long term view before reflexively denying these claims. Should a tall building burn in the future, the insurer will certainly bear the risk. Had they instead agreed to honour a replacement claim earlier – allowing the retrofit to take place – perhaps on a proportional share basis, the loss could have been significantly mitigated, if not averted in its entirety.
<br/><br/>
The underwriters also need to take notice; a retrofitted structure carries a far lesser risk than one clad in flammable material. Perhaps there can be a mutually beneficial market oriented solution arrived at between building owners, contractors, and insurers where retrofitting costs are equitably distributed, rather than resorting to litigation or a dismissal of the idea of retrofitting due to the aforementioned difficulties.
<br/><br/>
Given the stakes and uncertainly that these issues collectively represent, the fallout from the recent Torch fire may have only begun. While those directly affected residents will need to find new homes until repairs are made, the ripples from this event may spread out throughout the UAE.
<br/><br/>
The enormous risk posed by non fire retardant building cladding has once again been pushed into the forefront of the news, raising longer term considerations as to how best prevent reoccurrence of potentially worse tragedies.
<br/><br/>
<strong>To view published </strong><strong>article<a href="http://gulfbusiness.com/2015/03/dubai-marina-torch-fire-next-pays/#.VPVKmGdEjIV" target="_blank" rel="noopener"><span style="text-decoration: underline;"><span style="color: #006341; text-decoration: underline;">CLICK HERE </span></span></a></strong>
<table width="384">
<tbody>
<tr>
<td style="text-align: left;" width="146">Published:</td>
<td style="text-align: left;" width="187">March 2015</td>
</tr>
<tr>
<td style="text-align: left;">Publication:</td>
<td style="text-align: left;"><strong><span style="color: #006341;"><a href="http://gulfbusiness.com/" target="_blank" rel="noopener">Gulf Business</a></span></strong></td>
</tr>
<tr>
<td style="text-align: left;">Title:</td>
<td style="text-align: left;"><strong>Dubai Marina Torch Fire - What Next? Who Pays?</strong></td>
</tr>
<tr>
<td style="text-align: left;">Practice:</td>
<td style="text-align: left;"><a href="https://bsabh.com/legal-practice-areas/insurance-reinsurance/" target="_blank" rel="noopener">Insurance &amp; Reinsurance</a>, <a href="https://bsabh.com/legal-practice-areas/construction-law/" target="_blank" rel="noopener">Construction</a></td>
</tr>
<tr>
<td style="text-align: left;">Authors:</td>
<td style="text-align: left;"><em><strong><a href="https://bsabh.com/lawyer/michael-kortbawi/" target="_blank" rel="noopener"><span style="color: #006341;">Michael Kortbawi</span></a>, <a href="https://bsabh.com/lawyer/barry-greenberg/" target="_blank" rel="noopener"><span style="color: #006341;">Barry Greenberg</span></a></strong></em></td>
</tr>
</tbody>
</table>','post_date' => '2015-03-03 06:29:05','post_name' => 'dubai-marina-torch-fire-what-next-who-pays','post_parent' => '0','Image' => 'wp-content/uploads/2015/03/shutterstock_1040065087.jpg','Categories' => '35, 37, 86, 106','Tags' => '56, 65, 303, 300, 186, 199, 200, 201'),

            array('ID' => '61195','post_title' => 'Dubai Free Zones Council Announces a New Business Stimulus Package','post_content' => '<h4>Dubai Free Zones Council announced late yesterday a business stimulus package that complimented the previous packages announced by the UAE Government and the Emirate of Dubai. The package included deferral of rent payments for up to six months, reimbursement of guarantees and security deposits, facilitating financial payments through easy installments on a monthly basis, cancellation of penalties and allowing the transfer of labor between free zones provided necessary arrangements and contracts are in place with the employees.</h4>
Various free zones issued subsequent press releases to support the above including:
<ul>
 	<li><strong>DP World</strong> which includes also Jebel Ali Free Zone (JAFZA), National Industries Park (NIP) and Dubai Cars & Automotive Zone (DUCAMZ) confirming their support to the above and waiving fines on expired trade licenses for companies operating in the above free zones and waiver of license fees for new registration for the first year.</li>
 	<li><strong>DDA and TECOM Group</strong> announced a set of initiatives including deferral of payments, postponement of lease and registration fees for up to six months for new registrations, cancellation of fines on expired licenses and other financial and administrative fees, flexible fee payments and refunds of security deposits and guarantees.</li>
 	<li><strong>Dubai Aviation City Corporation and Dubai South</strong> issued a package of four initiatives including waiving fees on residential land and expired licenses, facilitating payments through monthly checks and exempting licensing fees for the first year. Dubai South further noted their readiness to issue additional offers in due course.</li>
 	<li><strong>DMCC</strong> joined all other free zones by confirming their commitment to offer a reduction of registration fees by 50% for new companies and 30% discounts for current companies on renewal and amendment fees.</li>
 	<li><strong>DIFC</strong> Governor issued this morning a press release confirming the center’s commitment to offer support during this time to its clients. The DIFC package included:</li>
</ul>
<ol>
 	<li>Waiver of annual licensing fees on new registrations during the next three months.</li>
 	<li>10% discount of renewal fees for existing license holders in the DIFC that are due to renew their licenses during the next three months.</li>
 	<li>Deferred payments in respect all properties owned by DIFC Investments for a period up to six months.</li>
 	<li>Reduction on property transfer fees from 5% to 4% for any sale of property (or any part thereof) that will take place within the three-month period.</li>
 	<li>DIFC will facilitate the free movement of labor in and out of the DIFC to other free zones, provided that the employers concerned have the necessary arrangements and contracts in place with their employees.</li>
</ol>
There is no doubt that this will bring huge comfort to free zones companies in Dubai and will allow them to be ready and able to survive the short-term impact of the COVID-19 outbreak. It is useful at this stage for all companies benefiting from these packages to undergo an internal review of their overall operations to make sure that they are aware of the business risks threating their continuity and future. This includes their overall liabilities, liquidity, relations with employees and relations with clients and suppliers. Maintaining transparency at this stage is key in ensuring that the trust factor between all of these parties is maintained. This in itself is sufficient to create a supportive environment for all businesses to pursue moving forward.
<br/><br/>
Authored by Partner <a href="https://bsabh.com/lawyer/rima-mrad/">Rima Mrad</a>','post_date' => '2020-03-29 12:34:32','post_name' => 'dubai-free-zones-council-announces-a-new-business-stimulus-package','post_parent' => '0','Image' => 'wp-content/uploads/2020/03/shutterstock_1662519004.jpg','Categories' => '31, 1696','Tags' => '97, 286, 1672, 1771'),

            array('ID' => '62149','post_title' => 'Do you have Debtors that have not paid you? Do you need extra cash in your Business?','post_content' => '<h3>During these trying times of Covid-19 and its impact on the financial affairs and revenue of numerous companies, an interesting method of recovering a little bit of extra cash is by applying the provisions of Article 64 of the Federal Law No (8) of 2017 on Value Added Tax (the “VAT Law”).</h3>
Article 64 of VAT Law makes provision for the recovery of output VAT charged by the supplier of the goods or services which is registered under the provisions of the VAT Law (the “Supplier”) and who has paid the output VAT to the Federal Tax Authority (the “FTA”) on the supply of goods or services that are considered to be classed as “bad debt”, subject to certain conditions imposed by the VAT Law being in place.
<br/><br/>
A “bad debt” can be considered to be any amount due from the recipient of goods or services (the “Customer”) which is not paid and the Customer who has rejected the payment for whatever reason, including a dispute arising from the terms of delivery, the product quality and even the discontinuation of business by the Customer.
<br/><br/>
To explain the background we advise that in the normal course of events:
<br/><br/>
- at the time of supply, the Supplier charges VAT to the Customer and the amount of VAT will be shown as output VAT in its VAT return;
- the amount of the due VAT is accordingly paid on or before the due payment date, after adjustment against Input VAT, to the FTA in the respective VAT return period;
- as a result of the above scenario, the Supplier can be placed in a financially onerous situation whereby the Supplier has paid the VAT to the FTA however the VAT has not yet been received by the Supplier from the Recipient.
<br/><br/>
Article 64 of the VAT Law provides that a Supplier can reduce the amount of its output VAT for amounts of VAT considered to be “bad debt” if the following conditions are satisfied by the Supplier:
<br/><br/>
- the Supplier has written off the receivable amount as a “bad debt” in the Supplier’s books of account;
- the Supplier has notified the Customer that the amount of consideration for the supply has been returned off; and
- the receivable amount is due to the Supplier for more than six (6) months old from the date of supply to the Customer.
<br/><br/>
In the event that the Supplier complies with ALL the requirements mentioned above, the Supplier can request the FTA for the “refund” through the adjustment of the output VAT amount in the tax return for the tax return period that the “bad debt” is written off.
<br/><br/>
The Customer’s responsibility in the event that the Supplier has written off any amount due to the Supplier by the Customer as “bad debt” and the abovementioned conditions have been met, the Customer must reduce its input tax by the amount of the VAT reflected in the letter to the Customer by the Supplier, by inclusion of this reduction in the tax return for the next tax return period.
<br/><br/>
Practically, the adjustments are reflected in the tax return as follows:
<br/><br/>
- in the case of the Supplier:
<br/><br/>
In the VAT Return form (VAT 201 form) there is a column named “Adjustments” under the section “VAT on sales and other output 1(a) to 1(g)g”. This column is used for reducing the output tax as a result of adjustment of bad debts. The amount entered should be only the VAT amount and should not be the sales value. This will be always a negative figure. This amount has to be deducted from the total of output VAT for the particular VAT Return period. Further, this adjustment amount of VAT on bad debts should be disclosed in accordance with per Emirate reporting as initially declared as output VAT.
<br/><br/>
- in the case of Customer:
<br/><br/>
If the Supplier considers an unpaid tax invoice as “bad debt” and the Supplier notifies the Customer thereof that the Supplier is going to recover the output tax through his VAT return, the Customer has the responsibility to make the adjustment in their VAT return form as well. The column shown under the name “Adjustment” in the section “VAT on Expenses and all other Inputs, line no. 9 – Standard Rated Expenses” has to be used for such adjustment. The VAT amount on the “bad debt” which the Customer has not paid but claimed as input earlier has to be deducted from the current period input tax.
<br/><br/>
Should you require assistance in putting this VAT relief mechanism into practice contact us at BSA to make it happen.
<br/><br/>
Author Head of Indirect Tax and Conveyancing, <a href="https://bsabh.com/lawyer/john-peacock/">John Peacock</a>','post_date' => '2020-09-06 15:06:43','post_name' => 'do-you-have-debtors-that-have-not-paid-you-and-you-need-a-little-extra-cash-in-your-business','post_parent' => '0','Image' => 'wp-content/uploads/2020/09/shutterstock_702204343.jpg','Categories' => '86, 949','Tags' => '104, 709, 1672, 1897'),
            array('ID' => '60838','post_title' => 'Coronavirus Outbreak – Impact on Regional (Re)insurance & Coverage Issues','post_content' => '<h4>World Health Organisation (WHO) have officially declared COVID-19 as a pandemic. BSA have already received several instructions regarding coverage advice related to matters arising from the coronavirus. While all efforts are being made by the WHO and Governments around the world to contain the coronavirus and look to develop a vaccine, world business and trade has been impacted with issues around supply chains and business interruption.</h4>
We anticipate that (re)insurers will have some part to play in terms of a shift of risk and loss from its insureds to them, as capacity-providers under certain insurance coverages. This will give the immediate need for policy coverage reviews including insuring clauses, type of insurance coverage and exclusions.
<br/><br/>
Insured businesses will need to carry out risk assessments of their current supply chains and look for alternative measures, review contractual provisions to explore whether any of those provisions provide protection such as a “force majeure” clause. In addition, all insurance coverage should be reviewed and analysed to explore whether and to what extent the policy would respond to the coronavirus outbreak.
<br/><br/>
From our experience, we have seen certain coverage terms that address several areas of coverage, which are broad in scope, where insureds’ have relied on these as comfort for an event such as the coronavirus outbreak. These could be, for example, “Properties All Risk” or “Financial Lines” insurance. However, these coverages may not be appropriate and may not respond to an event like the coronavirus outbreak as opposed to a more tailored insurance coverage, such as a business interruption insurance coverage. Generally, any business interruption coverage, in order to respond to any form of virus outbreak will need to include a “notifiable diseases” extension in the policy terms or additional endorsement. This is then normally triggered by Government/Ministerial Orders. Note, that many business interruption insurance policies do not include these provisions and specialist insurance advice should always be sought.
<br/><br/>
Companies with financial lines insurance coverage will need to look closely at their insurance terms while also taking measures to mitigate any exposure in taking or failing to take certain actions where those financial lines insurance policies would not respond, but for their failures in taking or not taking certain measures.
<br/><br/>
Medical and travel insurers will have been impacted by the coronavirus outbreak and those insurance providers will need to review current capacity with its reinsurance arrangements, where many of those policies will need to respond to claims unless otherwise excluded with the primary insurance coverage.
<br/><br/>
Finally, it is certain that the coronavirus will, without a doubt, trigger litigation, insurance claims and coverage disputes in 2020 and beyond and this will have implications for the global insurance market and capacity.
<br/><br/>
Authored by Partner and Head of Insurance/Reinsurance <a href="https://bsabh.com/lawyer/simon-isgar/">Simon Isgar</a>','post_date' => '2020-03-15 13:33:48','post_name' => 'coronavirus-outbreak-impact-on-regional-reinsurance-coverage-issues','post_parent' => '0','Image' => 'wp-content/uploads/2020/03/shutterstock_1273437736.jpg','Categories' => '37, 1696','Tags' => '125, 1057, 1672, 1693'),

            array('ID' => '61177','post_title' => 'Attorney General Announces Fines to Prevent Spread of COVID-19','post_content' => '<h4>The UAE Attorney General has issued resolution No. (38) of 2020 following Cabinet Decision No. 17 of 2020 regarding the implementation of regulations for spreading communicable diseases. The resolution covers 15 penalties, ranging from AED 500 to AED 50,000, which aim to curb the spread of the novel coronavirus in the UAE.</h4>
The Attorney General has clarified that the fine for not wearing medical masks in closed places can only be imposed on patients suffering from chronic diseases and on people who are suffering from symptoms of flu and cold and fail to maintain social distancing while among other people.
<br/><br/>
The following are the violations and associated fines as issued under the Attorney General’s resolution:
<ul>
 	<li>Fine for not complying with instructions of home quarantine and/or not following the guidelines under the Home Quarantine Guide– <strong>AED 50,000</strong></li>
 	<li>Fine for patients who refuse mandatory hospitalisation or fail to take the prescribed medicines despite being alerted – <strong>AED 50,000</strong></li>
 	<li>Fine for violating administrative closure of public places like shopping centers, malls, outdoor markets, gyms, public swimming pools, cinemas, clubs, parks and restaurants – <strong>AED 50,000</strong> (Additionally, there is a fine of <strong>AED 500</strong> for people caught visiting these public places)</li>
 	<li>Fine for organising social gatherings, meetings and public celebrations – <strong>AED 10,000</strong> (Additionally, there is a fine of <strong>AED 5,000</strong> for people attending the social gatherings and events)</li>
 	<li>Fine for not conducting a medical test upon request – <strong>AED 5,000</strong></li>
 	<li>Fine for violating precautionary measures set by the UAE Ministry of Health and Prevention by people coming from nations affected by communicable diseases – <strong>AED 2,000</strong></li>
 	<li>Fine for failure to observe health measures regarding regulation of roads, markets and other public places exempted from temporary closure – <strong>AED 3,000</strong></li>
 	<li>Fine for failure to dispose of clothes, luggage or any temporary structures proved to be contaminated, which can\'t be disinfected by the standard established methods – <strong>AED 3,000</strong></li>
 	<li>Fine for unnecessary visits to hospitals and other health facilities – <strong>AED 1,000</strong></li>
 	<li>Fine for exceeding the maximum number of allowed persons in a car (i.e. more than 3 persons in car) – <strong>AED 1,000</strong></li>
 	<li>Fine for not wearing medical masks indoors and failure to maintain social distancing by persons suffering from chronic disease or having symptoms of flu and cold – <strong>AED 1,000</strong></li>
 	<li>Fine for leaving home unnecessarily and without reason, except for important work or a genuine reason (purchase of essentials, medicines, etc.) – <strong>AED 2,000</strong></li>
 	<li>Fine for violating provisions of the law when burying or transporting the body of a person who died from a communicable disease – <strong>AED 3,000</strong></li>
 	<li>Fine for drivers failing to maintain hygiene and following sterilization procedures in public transportation – <strong>AED 5,000</strong></li>
 	<li>Fine for failure to take precautionary measures, failure for the crew of ships from the captain or shipping agent, as the case may be – <strong>AED 10,000</strong></li>
</ul>
The National Emergency, Crisis and Disasters Federal Prosecution has been entrusted with the task of implementation of the resolution and may seek assistance from local and public authorities as required.
The penalties shall be doubled in amount for repeat violators and if the violator commits a third offence, he will be referred to the National Emergency, Crisis and Disasters Federal Prosecution for appropriate action. Further, the violator must also bear the costs of any repairs for damages occurring due to the violation.
<br/><br/>
The resolution is a part of the government\'s measures to combat the spread of coronavirus and to protect the health of citizens and residents of the UAE by curbing unnecessary gathering and outings.
<br/><br/>
Authored By Associate <a href="https://bsabh.com/lawyer/swati-soni/">Swati Soni</a>','post_date' => '2020-03-29 10:31:23','post_name' => 'attorney-general-announces-fines-to-prevent-spread-of-covid-19','post_parent' => '0','Image' => 'wp-content/uploads/2020/03/shutterstock_400590481.jpg','Categories' => '33, 1696','Tags' => '56, 807, 1565, 1672, 1768'),

            array('ID' => '59483','post_title' => 'UAE Chapter on Sanctions','post_content' => '<span style="color: #53af32;"><strong>Overview</strong> </span>
<br/><br/>
<strong><span style="color: #53af32;">1.1 Describe your jurisdiction’s sanctions regime</span></strong>
<br/><br/>
The United Arab Emirates (“UAE”) has a complex sanctions regime based on a variety of sources. Sanctions are based on diverse interests, including political, economic and national security interests. Due to the rapidly changing nature of such interests, sanctions are susceptible to significant and constant changes. Sanctions in the UAE are usually imposed at a federal level, through a variety of methods, including, by way of example:
<br/><br/>
a. Adding sanctioned persons to local lists and the UN sanctions list: This is effected by issuing local terrorism lists (“Local Lists”) and a sanctions list (“Sanctions List”) pursuant to Cabinet Decision No. 20 of 2019 on the Regulation of Terrorism Lists and Implementation of Security Council Resolutions Related to the Prevention and Suppression of Terrorism and Cessation of Proliferation of Weapons and its Financing, and the Relevant Decisions (“Sanctions Regulation”).
<br/><br/>
b. Issuing specific laws addressing specific sanctions against a country or persons: This is effected by issuing federal laws specifically targeting certain entities and persons, such as Federal Law No. 15 of 1972 concerning the Boycott of Israel (“Israel Boycott Law”).
<br/><br/>
c. Others: Where sanctions are issued by inter-governmental organisations (“IGOs”) of which the UAE is a member, these sanctions are implemented by adding the sanctioned persons to the Sanctions List and/or issuing internal circulars to the relevant governmental entities. In addition to the above methods, multiples laws and regulations are regularly issued to additionally impose restrictions and require that persons in the UAE, particularly in financial and regulated industries, undertake implementation measures such as reporting requirements and client due diligence, in order to ensure compliance with UAE and international sanctions such as those of the UN, Office of Foreign Assets Control (“OFAC”) and European Union (“EU”), as applicable.
<br/><br/>
<strong><span style="color: #53af32;">1.2 What are the relevant government agencies that administer or enforce the sanctions regime?</span> </strong>
<br/><br/>
The UAE administers sanctions through different governmental entities, depending on the implementation measures required for the imposition of the relevant sanctions. Two main governmental authorities often used in the implementation of sanctions are the Executive Office of the Committee for Goods and Materials subject to Import and Export Control (“Office”) and UAE central bank (“Central Bank”):
<br/><br/>
<strong>1. Office</strong>: Federal Law No. 13 of 2007 concerning the Commodities subject to the Monitoring of Imports and Exports (“Commodities Law”) authorises the restriction or ban on import, export or re-export of goods deemed a threat to the UAE’s foreign policy.
<br/><br/>
<strong>2. Central Bank</strong>: The Central Bank is a fundamental authority that handles the compliance with sanctions as applicable to financial institutions in the UAE. Its authorities include the prohibition of transactions with sanctioned persons and the freezing of funds of sanctioned persons.
<br/><br/>
<strong><span style="color: #53af32;">2 Legal Basis/Sanctions Authorities </span></strong>
<br/><br/>
<strong><span style="color: #53af32;">2.1 What are the legal or administrative authorities for imposing sanctions?</span></strong>
The Supreme Council, the Supreme Council for National Security and UAE Cabinet are the ultimate governmental entities responsible for imposing sanctions. The Supreme Council for National Security proposes sanctions pursuant to Article 2 of the Sanctions Regulation. As illustrated under our answer to question 1.1 above, sanctions are often imposed by way of lists issued pursuant to the Sanctions Regulations, as well as other Federal Laws and Cabinet Decisions. The UAE may, in certain circumstances, implement sanctions issued by IGOs of which it is a member or otherwise by distributing circulars or making announcements to that effect.
<br/><br/>
<strong><span style="color: #53af32;">2.2 Does your jurisdiction implement United Nations sanctions? Describe that process. Are there any signiﬁcant ways in which your jurisdiction fails to implement United Nations sanctions?</span> </strong>
The UAE implements United Nations (“UN”) sanctions issued by the UN Sanctions Committee. Federal Law No. 20 of 2018 on the Criminalisation of Money Laundering and Combating the Financing of Terrorism and the Financing of Unlawful Organisations (“AMLCFT Law”) requires “Prompt application of the directives when issued by the competent authorities in the state for implementing the decisions by the UNSC under Ch. 7 of UN Convention for the Prohibition and Suppression of the Financing of Terrorism and Proliferation of weapons of mass destruction, and other directives”.
<br/><br/>
The Sanctions Regulations sets out the implementation framework of sanctions passed by the UN Sanctions Committee (“Sanctions List”). This framework includes the imposition of direct obligations on the Office (Article 10), financial institutions and Designated NonFinancial Businesses and Professions (“DNFBPs”) (Article 19) to implement the Sanctions List. DNFBPs consist of anyone conducting one or more of the commercial or professional activities listed in the implementing regulation of the AMLCFT Law, which include certain real estate brokers and agents, merchants of precious metals and precious stones, lawyers and providers of corporate services. The framework in the Sanctions Regulations further imposes an obligation on:
<br/><br/>
(1) the Office to circulate the Sanctions List to the concerned governmental entities as soon as updated; and
<br/><br/>
(2) the Central Bank and other UAE financial regulators to ensure financial institutions and DNFBPs comply with the Sanctions List. For instance, where applicable, the Sanctions List may be distributed to the Federal Authority for Nuclear Regulation which issues multiple regulations to control the export and import of nuclear material, nuclear-related items and nuclear-related dual-use items, and requires special authorisations for the conduct of certain activities. Similar obligations are imposed on financial institutions and DNFBPs pursuant to the AMLCFT Law.
<br/><br/>
The UAE has issued implementation reports with respect to its application of UN sanctions on certain countries, including North Korea, Somalia, Sudan, Iran and Libya. These reports are publicly accessible on the UN’s website and set out specific actions undertaken by the UAE. It is worth noting that in certain cases, sanctions imposed by the UN may have already been implemented by the UAE on other grounds and included in Local Lists, e.g. as a result of its membership in the Terrorist Financing Targeting Centre (“TFTC”).
<br/><br/>
<strong><span style="color: #53af32;">2.3 Is your country a member of a regional body that issues sanctions? If so: (a) does your country implement those sanctions? Describe that process; and (b) are there any signiﬁcant ways in which your country fails to implement these regional sanctions?</span> </strong>
<br/><br/>
The UAE is a member of three main regional bodies that issue sanctions – the Arab League, the TFTC and the Gulf Cooperation Council (“GCC”):
<br/><br/>
<strong>a. Arab League</strong>: The UAE implements sanctions adopted by the Arab League on an ad hoc basis. It has, for instance, implemented sanctions against the Republic of Syria, including by imposing a travel ban on transactions with Syria’s central bank, a freeze to all Syrian government assets, and an end to commercial exchanges with the Syrian government.
<br/><br/>
<strong>b. TFTC</strong>: Members of the TFTC consist of the United States and certain GCC countries. The UAE implements all sanctions designated by the TFTC by issuing the Terrorism Lists referred to above. TFTC-designated sanctions are also available on the US’s treasury government website.
<br/><br/>
<strong>c. GCC</strong>: The UAE is a member of the GCC, which consists of six member states. The Charter of the GCC sets up a framework that would permit the joint establishment of foreign policies and therefore issuance of sanctions. Although the GCC has, in the past, made announcements with respect to its members’ stance on foreign policy, it has not, at the date hereof, issued any sanctions as such.
<br/><br/>
<strong><span style="color: #53af32;">2.4 Does your jurisdiction maintain any lists of sanctioned individuals and entities? How are individuals and entities: a) added to those sanctions lists; and b) removed from those sanctions lists?</span></strong>
<br/><br/>
The UAE maintains two main lists of sanctioned individuals and entities:
<br/><br/>
<strong>a. Local Lists</strong>: These lists consist of local terrorism lists (“Local Lists”) issued pursuant to Federal Law No. 7 of 2014 on combating terrorism offences (“Anti-Terrorism Law”) and the Sanctions Regulations. Decisions of listing, removal and relisting on Local Lists enter into effect when issued by the UAE Cabinet and published in the Official Gazette. Such decisions are also published in audio-visual and print media of the UAE, in both Arabic and English.
<br/><br/>
<strong>b. Sanctions List</strong>: This list consists of the Sanctions List issued by the UN Security Council. Addition and removal from the Sanctions List is effected by the UN Security Council. The UAE may, in certain circumstances, circulate lists issued by IGOs of which it is a member or otherwise by distributing circulars or making announcements to that effect.
<br/><br/>
<strong><span style="color: #53af32;">2.5 Is there a mechanism for an individual or entity to challenge its addition to a sanctions list?</span> </strong>
<br/><br/>
The mechanisms available for an individual or entity to challenge its addition to a sanctions list are set out under the Sanctions Regulation:
<br/><br/>
<strong> a. Local Lists</strong>: In accordance with Article 6 of the Sanctions Regulation, a grievance can be filed to the Ministry of Justice. If the grievance is rejected or not responded to within 60 days, the complainant may appeal before the court concerned with state security crimes.
<br/><br/>
<strong>b. Sanctions List</strong>: In accordance with Article 15 of the Sanctions Regulation, the Office shall post on its official website the procedures of submission of applications for removal from the Sanctions List. The procedure varies depending on the nature of the sanction.
<br/><br/>
<strong><span style="color: #53af32;">2.6 How does the public access those lists?</span> </strong>
<br/><br/>
Changes to the Local Lists are published in the Official Gazette as well as in audio-visual and print media in both Arabic and English. The Sanctions List is available on the UN’s website.
<br/><br/>
<strong><span style="color: #53af32;">2.7 Does your jurisdiction maintain any comprehensive sanctions or embargoes against countries or regions?</span> </strong>
<br/><br/>
In addition to the sanctions referred to above, the UAE currently maintains comprehensive sanctions and embargoes against Qatar and Israel. a. Qatar: On 4 June 2017, the UAE imposed an economic and diplomatic embargo on Qatar which is still in effect. In addition to including key Qatari individuals in its Local Lists, the UAE undertook the following measures, among others: UAE officials’ announcement: The UAE authorities have announced that it imposed an embargo on Qatar and imposed sanctions on the Qatari government and certain Qatari companies and citizens.
<br/><br/>
The embargo includes:
<br/><br/>
(1) the blocking of all air and land links;
<br/><br/>
(2) restrictions in respect to some lending activities and exportation of some products; and
<br/><br/>
(3) the processing of certain payments in Qatari riyals. Circular: A circular was issued by the Federal Transport Authority – Land & Maritime, to all UAE “ports and ship agents”, ordering all UAE ports not to:
<br/><br/>
i. receive any Qatari-flagged vessel or vessel owned by Qatari companies or individuals;
<br/><br/>
ii. load or unload any cargo of Qatari origin in any port or water of the UAE; and
<br/><br/>
iii. allow ships to load any cargo of UAE origin to the state of Qatar.
<br/><br/>
Israel: In 1972, the UAE imposed a boycott on Israel. The scope of the boycott imposed was initially wide and included secondary and tertiary sanctions.
<br/><br/>
<strong><span style="color: #53af32;">2.8 Does your jurisdiction maintain any other sanctions?</span> </strong>
<br/><br/>
In addition to the above-mentioned sanctions, the UAE, in particular the Central Bank and regulated financial institutions in the UAE, also takes into consideration sanctions imposed by the EU and OFAC.
<br/><br/>
<strong><span style="color: #53af32;">2.9 What is the process for lifting sanctions?</span> </strong>
<br/><br/>
The process for lifting sanctions varies depending on the method of imposition as well as the nature of such sanction. With respect to the Local Lists, the Supreme Council for National Security periodically reviews the same in coordination with the Ministry of Justice and submits recommendations to the Ministry of Presidential Affairs. The latter then submits the application to the UAE Cabinet, including its opinion. With respect to sanctions issued by IGOs, sanctions are added and removed by the relevant IGO. The removal of such sanctions is then implemented by the UAE through different means, which may vary depending on the method by which the relevant sanction was imposed.
<br/><br/>
<span style="color: #53af32;"><strong>2.10 Does your jurisdiction have an export control regime that is distinct from sanctions?</strong> </span>
<br/><br/>
Although the export control regime of the UAE is distinct from sanctions, it plays an important role in enforcing sanctions where the same is with respect to the export or import of sanctioned products from sanctioned countries/persons. This is particularly illustrated by the significant role accorded to the Office with respect to implementing the Sanctions List, as well as the Commodities Law, which includes banning the import, export or re-export of goods deemed a threat to the UAE’s foreign policy (sanctions are often used as an instrument of foreign policy).
<br/><br/>
<span style="color: #53af32;"><strong>2.11 Does your jurisdiction have blocking statutes or other restrictions that prohibit adherence to other jurisdictions’ sanctions or embargoes?</strong> </span>
<br/><br/>
The UAE does not have blocking statutes or other restrictions prohibiting adherence to other jurisdictions’ sanctions or embargoes.
<br/><br/>
<span style="color: #53af32;"><strong>2.12 Does your jurisdiction impose any prohibitions or threaten any sanctions consequences for transactions that do not have a connection to that jurisdiction (sometimes referred to as “secondary sanctions”)?</strong> </span>
<br/><br/>
Under the Israel Boycott Law, the UAE imposed secondary and tertiary sanctions on Israel. However, in 1995, Cabinet Resolution 462/17M of 1995 was issued, reducing the scope to only primary sanctions. This Resolution was not publicised in the Gazette and to date, the Israel Boycott Law has not been amended.
<br/><br/>
<span style="color: #53af32;"><strong>3 Implementation of Sanctions Laws and Regulations</strong> </span>
<br/><br/>
<span style="color: #53af32;"><strong>3.1 What parties and transactions are subject to your jurisdiction’s sanctions laws and regulations? For example, do sanctions restrictions apply based on the nationality of the parties involved? Or the location where the transactions take place?</strong> </span>
<br/><br/>
The parties and transactions subject to UAE’s sanctions laws and regulations depend on the nature of and reasons for the sanctions. Certain sanctions are more narrowly targeted than others. With respect to sanctions targeted at specific individuals and organisations (e.g. under Local Lists), restrictions would not apply based on their nationality but rather identity or affiliations. With respect to more comprehensive sanctions targeted at governments, such sanctions often apply based on the nationality of persons involved, such as in the case of Qatar and Israel. Sanctions can also apply on the location where the transaction takes place; this is particularly relevant where sanctions are targeting trade with a certain country or the country imposing the sanction refuses to recognise or accept deals involving the currency of a certain country, as is the case with Iran and Qatar.
<br/><br/>
<span style="color: #53af32;"><strong>3.2 Are parties required to block or freeze funds or other property that violate sanctions prohibitions?</strong> </span>
<br/><br/>
Financial institutions and customs departments in the UAE are, in certain circumstances, required to block or freeze funds or other property that violate sanctions. Article 5 of the AMLCFT Law specifically accords the governor of the Central Bank or his delegate the right to freeze suspicious funds. Furthermore, Article 2 of the Commodities Law accords the customs departments the right to ban or restrict the import/export of commodities in case the “foreign policy of the State so requires”. Pursuant to Article 12 of the Sanctions Regulation, “no physical or moral person shall be allowed to make the Funds in its possession or under its managements, or any financial services or other, available directly or indirectly to or in favour of any person or organisation listed on the Sanctions List…”.
<br/><br/>
<span style="color: #53af32;"><strong>3</strong><strong><span style="color: #53af32;">.</span>3 Are there licences available that would authorise activities otherwise prohibited by sanctions?</strong> </span>
<br/><br/>
There are no licences available that would authorise activities otherwise prohibited by sanctions per se. However, special licences may be required to conduct activities more susceptible to the possible breach of sanctions; for example, pursuant to the Commodities Law, strategic goods and dual-use items, such as arms and military hardware, chemical and biological materials, cannot be exported or re-exported without a special licence.
<br/><br/>
<strong><span style="color: #53af32;">3.4 Are there any sanctions-related reporting requirements? When must reports be ﬁled and what information must be reported?</span></strong>
<br/><br/>
Multiple laws and regulations, including Article 15 of the AMLCFT Law, impose an obligation on financial institutions and DFNBPs to report to the relevant financial regulator any suspicion or any situation in which they have reasonable grounds to suspect a transaction or funds is related to a money-laundering crime, related predicate offences, financing of terrorism or illegal organisations.
<br/><br/>
Furthermore, Article 19 of the Sanctions Regulations imposes several reporting obligations on financial institutions and DFNBPs to the relevant financial regulator, including in the following cases: a. where it has frozen funds pursuant to issued sanctions; and b. where any of its former customers or an accidental customer dealt with is a person listed on the Sanctions List.
<br/><br/>
<strong><span style="color: #53af32;">3.5 How does the government convey its compliance expectations? Are certain entities required to maintain compliance programmes? What are the elements of a compliance programme required (or recommended) by the competent regulator(s)?</span> </strong>
<br/><br/>
The government conveys its compliance expectations by circulating circulars and directives as well as issuing laws and regulations. Article 16 of the AMLCFT Law requires financial institutions and DFNBPs to develop internal policies, controls and procedures to enable them to manage the risks identified and mitigate them. In financial free zones, compliance expectations are comprehensive and included in “Rulebooks”. Furthermore, Article 20 of the Sanctions Regulations imposes an obligation on financial regulators to take all measures to ensure financial institutions and DFNBPs comply with UN sanctions and apply administrative sanctions upon violation of such compliance. A common compliance policy required is the implementation of client due diligence and onboarding clearances to ensure that customers of such institutions are not subject to any sanctions.
<br/><br/>
<span style="color: #53af32;"><strong>4 Enforcement Criminal Enforcement </strong></span>
<br/><br/>
<span style="color: #53af32;"><strong>4.1 Are there criminal penalties for violating economics sanctions laws and/or regulations?</strong> </span>
<br/><br/>
There are criminal penalties for violating economic sanction laws and/or regulations where such violation also constitutes a crime under Federal Law No. 3 of 1987 on the issuance of the Penal Code (“Penal Code”) or other applicable laws, such as the Anti-Terrorism Law, Commodities Law, and AMLCFT Law. Article 20 of the Sanctions Regulations provides that any violation thereof is subject to the penal and administrative sanctions set forth under the AMLCFT Law.
<br/><br/>
<span style="color: #53af32;"><strong>4.2 Which government authorities are responsible for investigating and prosecuting criminal economic sanctions offences?</strong> </span>
<br/><br/>
The government authorities responsible for investigating criminal economic sanctions offences differ depending on the nature of the sanctions. As explained above, different government authorities are responsible for implementing different types of sanctions; these very same government authorities must investigate any potential breach by a person under its surveillance. For example, the Central Bank is responsible for investigating any breach of sanctions by financial institutions in the UAE, or the relevant customs department for any breach of sanctions by way of unauthorised exports/imports. These governmental authorities must then report to the executive board of the Supreme Council for National Security, which will further investigate the matter and may file a claim for prosecution of any person found to be in breach of sanctions through the judiciary system.
<br/><br/>
<span style="color: #53af32;"><strong>4.3 Is there both corporate and personal liability?</strong> </span>
<br/><br/>
There is both corporate and personal liability. This is expressly stated, among others, under Article 42 of the Anti-Terrorism Law and Article 4 of the AMLCFT Law. With respect to natural persons, imprisonment may be imposed in addition to or instead of fines.
<br/><br/>
<span style="color: #53af32;"><strong>4.4 What are the maximum ﬁnancial penalties applicable to individuals and legal entities convicted of criminal sanctions violations?</strong> </span>
<br/><br/>
Pursuant to Article 42 of the Anti-Terrorism Law, a maximum of AED 100 million shall be imposed upon a judicial person who violates criminal sanctions, unless a more severe penalty is imposed under the Penal Code. Article 162 of the Penal Code provides for a fine of not less than AED 1 million where a person imports/exports to an enemy country in time of war. Under the Commodities Law, an individual can be fined up to AED 500,000. Under the Central Bank Law, a financial institution disregarding instructions of the Central Bank not to deal with specific persons will be fined a minimum of AED 500,000 and a maximum of AED 10 million.
<br/><br/>
<strong><span style="color: #53af32;">4.5 Are there other potential consequences?</span> </strong>
<br/><br/>
Other potential consequences for breach of sanctions, where such breach constitutes crimes under the Anti-Terrorism Law or Penal Code, include imprisonment and capital punishment. The Commodities Law also provides for imprisonment for up to one year. Public prosecution may be involved if the issue relates to a crime punishable by law, such as felonies. The AMLCFT Law lists out potential consequences for breaches thereof, including:
<ul>
 	<li>banning the violator from working in the sector related to the violation for the period determined by the supervisory authority;</li>
 	<li>constraining the powers of the board members, supervisory or executive management members, managers or owners who are proven to be responsible for the violation;</li>
 	<li>arresting managers, board members and supervisory and executive management members who are proven to be responsible for the violation for a period to be determined by the supervisory authority or requesting their removal; and</li>
 	<li>cancelling the licence of the violator. Certain companies involved in money laundering and proliferation of dual-use/dangerous materials have had their trade licences revoked due to breach of the AMLCFT Law, Sanctions Regulations, Commodities Law as well as the Non-Proliferation Treaty and other UN resolutions.</li>
</ul>
<strong><span style="color: #53af32;">4.6 Are there civil penalties for violating economics sanctions laws and/or regulations?</span></strong>
<br/><br/>
There are civil penalties for violating economics sanctions laws and regulations. These penalties depend on the nature of the violation in question. Persons violating custom laws may find themselves fined or their assets seized and/or destroyed. Persons effecting wire transfers in breach of sanctions may, in certain cases, find access to their bank accounts blocked until an investigation is conducted by the Central Bank or other competent financial regulator.
<br/><br/>
<strong><span style="color: #53af32;">4.7 Which government authorities are responsible for investigating and enforcing civil economic sanctions violations?</span> </strong>
<br/><br/>
The government authority responsible for investigating and enforcing civil economic sanctions depends on the nature of the sanction that was breached. For instance, with respect to breach of custom laws, the relevant customs department is responsible, and with respect to breach of the Central Bank’s rules, the Central Bank is responsible.
<br/><br/>
<strong><span style="color: #53af32;">4.8 Is there both corporate and personal liability?</span> </strong>
<br/><br/>
There is both corporate and personal liability for civil economic sanctions violations. The laws and procedures applicable where civil economic sanctions are violated include the blocking of transactions and imposition of administrative fines by the competent authorities.
<br/><br/>
<span style="color: #53af32;"><strong>4.9 What are the maximum ﬁnancial penalties applicable to individuals and legal entities found to have violated economic sanctions?</strong> </span>
<br/><br/>
Administrative penalties may apply where persons are found to have violated economic sanctions; the penalty amount differs depending on the severity of the violation. With respect to customs offences, persons may be fined varying amounts depending on the offence and the value of the related goods.
<br/><br/>
<strong><span style="color: #53af32;">4.10 Are there other potential consequences?</span> </strong>
<br/><br/>
There is no limitation in principle to other potential consequences for a violation of civil economic sanctions. Other potential consequences may vary depending on the nature of the violation and required measures to avoid a breach of sanctions, including the seizing and destruction of assets and freezing of bank accounts.
<br/><br/>
<strong><span style="color: #53af32;">4.11 Describe the civil enforcement process, including the assessment of penalties. Are all resolutions by the competent authorities public?</span> </strong>
<br/><br/>
Assessment of penalties depends on the breach itself. Where the latter includes a transaction, the penalty can be linked to the value of the transaction. Should a matter be brought before the courts, the penalty assessment can also be left to the discretion of the judge. Not all resolutions by the competent authorities are public; certain penalties are imposed at their discretion and are based on the gravity of the violation.
<br/><br/>
<span style="color: #53af32;"><strong>4.12 Describe the appeal process. Have companies challenged penalty assessments in judicial proceedings?</strong> </span>
<br/><br/>
The appeal process for sanction penalties does not usually take place in judicial proceedings but rather consists of the submission of grievances and other administrative proceedings. Depending on such proceedings, it may be possible in certain cases to raise a claim and to appeal a decision before the courts. The UAE does not have a binding precedent system, therefore information regarding cases in the UAE is not always publicly available.
<br/><br/>
<strong><span style="color: #53af32;">4.13 Are criminal and civil enforcement only at the national level? Is there parallel state or local enforcement?</span> </strong>
<br/><br/>
Criminal and civil enforcement are at both the national and Emirati level. While the Central Bank administers its applicable laws at a national level, customs laws are often administered at an Emirati level; for example, with respect to Dubai by the Dubai Customs and with respect to Abu Dhabi by the Abu Dhabi Customs.
<br/><br/>
<span style="color: #53af32;"><strong>4.14 What is the statute of limitations for economic sanctions violations?</strong> </span>
<br/><br/>
Under Federal Law No. 5 of 1985 regarding civil transactions (“Civil Code”), the statute of limitation for civil claims is 15 years, unless otherwise expressly provided in a statute. With respect to money laundering or financing terrorism or crimes by illegal organisations, Article 29 provides that criminal cases are not subject to the statute of limitations and the sanctions shall not lapse with time or with the lapse of any related civil legal cases due to the statute of limitations.
<br/><br/>
<strong><span style="color: #53af32;">5 General</span> </strong>
<br/><br/>
<strong><span style="color: #53af32;">5.1 If not outlined above, what additional economic sanctions-related measures are proposed or under consideration?</span> </strong>
<br/><br/>
Additional economic sanctions-related measures are regularly proposed and under consideration; however, such information is not usually shared with the public until officially issued or announced. In other exceptional situations, the UAE may announce its intention to impose or comply with certain sanctions, such as its latest announcement to comply with existing and upcoming US sanctions on Iran.
<br/><br/>
<span style="color: #53af32;"><strong>5.2 Please provide information for how to obtain relevant economic sanctions laws, regulations, administrative actions, and guidance from the Internet. Are the materials publicly available in English?</strong> </span>
<br/><br/>
Many economic sanctions laws are available on the websites of the governmental entities issuing the same, sometimes both in English and Arabic. Economic sanctions laws and regulations can also be found in the Official Gazette of the UAE.
<br/><br/>
Published first by <a href="https://iclg.com/practice-areas/sanctions/united-arab-emirates">International Comparative Legal Guides</a>.
<br/><br/>
Read full publication <a href="https://bsabh.com/wp-content/uploads/2019/10/SAN20_Chapter-27-United-Arab-Emirates.pdf">here</a> .
<br/><br/>
Article authored by <a href="https://bsabh.com/lawyer/rima-mrad/">Rima Mrad</a>, Partner and <a href="https://bsabh.com/lawyer/tala-azar/">Tala Azar</a>, Associate.
<br/><br/>
','post_date' => '2019-10-20 17:49:22','post_name' => 'uae-chapter-on-sanctions','post_parent' => '0','Image' => 'wp-content/uploads/2019/10/shutterstock_117017602-1.jpg','Categories' => '31, 38, 106, 107, 1526','Tags' => '97, 1086, 1529'),

            array('ID' => '57329','post_title' => 'BSA in Collaboration With Zubair SEC to Hold Workshop and Legal Clinic For SMEs','post_content' => 'BSA Bin Shabib, Al Rashdi &amp; Al Barwani (BSA), and Zubair Small Enterprises Centre (Zubair SEC) will jointly hold a workshop and legal clinic on January 15, 2019. The legal clinic will offer free consultations for small and medium-sized enterprises (SMEs) operating in the Sultanate.
<br/><br/>
Commenting on the initiative, <a href="https://bsabh.com/lawyer/mundhir-albarwani/">Mundhir Al Barwani</a> said, “Over the past few years, we have received several legal issues related to emerging enterprises and businesses at BSA. A majority of these legal cases stemmed from the fact that owners were not familiar with the legal aspects and official requirements relating to their area of business or the inability to pay for expensive consultations. It is due to these cases that we came up with the idea for this social initiative, which will help create the perfect platform from which we could offer consultations free of charge, as part of our community activities. In order to carry out this initiative, we have signed a MoU with Zubair SEC - one of the leading support centres in the Sultanate - that has made significant strides in helping small businesses over the past few years.”
<br/><br/>
Ali Shaker, Advisor – Business Development &amp; Strategic Partnerships at Zubair SEC said, “As part of our continuous efforts to extend diverse services to entrepreneurs and SME owners, we are holding this workshop and free legal clinic for the first time in the Sultanate in partnership with BSA Bin Shabib, Al Rashdi &amp; Al Barwani (BSA), one of the leading legal firms in the region. It is a very good opportunity for SMEs, and we encourage those with business-related legal matters to sign up and make use of the offered services by a team of high-calibre lawyers who have profound experience and understanding of the local context and legal framework.”
<br/><br/>
The Workshop and Legal Clinic will be taking place on Tuesday, January 15 at Bait Al Zubair and will see the participation of 11 lawyers and legal experts from BSA. They will each offer an average of seven hours of free legal consultation for attendees who own SMEs in Oman. This will total up to 77 free hours of specialised legal consultations','post_date' => '2019-01-13 23:55:20','post_name' => 'bsa-in-collaboration-with-zubair-sec-to-hold-workshop-and-legal-clinic-for-smes','post_parent' => '0','Image' => 'wp-content/uploads/2019/01/shutterstock_407475370.jpg','Categories' => '103','Tags' => '1090, 1166'),

            array('ID' => '55370','post_title' => 'Introduction to Company Acquisitions in Kingdom of Saudi Arabia','post_content' => '<strong>Overview</strong>
<br/><br/>
The article examines the material legal process involved when undertaking a corporate acquisition transaction in Saudi Arabia (Acquisition Transaction).
<br/><br/>
The general process an Acquisition Transaction undergoes shares several similarities with similar types of transactions in other jurisdictions. Notwithstanding, the prevalent regulations and overriding legal system unique to KSA must be taken into consideration and applied accordingly to the various agreements and processes involved. Such will serve to mitigate any risks associated with any legal clauses being considered non-compliant with Saudi law and therefore nonenforceable in a Saudi court of law, arbitration panel, and/or local enforcement court.
<br/><br/>
In addition, the governing rules and regulations in KSA applicable to an Acquisition Transaction include restrictions in certain instances (depending on the business sector involved) on matters such as share transfers, non-competition, and foreign (i.e., non Saudi national) ownership that must be considered for the subject transaction to be allowed to legally close.
<br/><br/>
<strong>Definitions</strong>
<ul>
 	<li>Acquiring Entity: The corporate entity that encompasses another by absorbing it.</li>
 	<li>Acquisition: When two (or more) corporate entities are combining and/or transferring their respective ownerships and/or assets amongst one another.</li>
 	<li>Acquisition Transaction: Process involved when undertaking a corporate acquisition transaction in Saudi.</li>
 	<li>CMA: Capital Market Authority of Saudi.</li>
 	<li>Companies\' Regulations: The regulations in KSA issued in accordance with Saudi Arabia Royal</li>
 	<li>Decree No. M3/1437 on the Approval of the Companies Law*, and Saudi Arabia Cabinet Decision No. 30/1437 Approving the Companies Law in the Enclosed Form.</li>
 	<li>Company Law: Saudi Arabia Royal Decree No. M3/1437 on the Approval of the Companies Law*.</li>
 	<li>Competition Law: Saudi Arabia Royal Decree No. M25/1425 Relating to the Competition Law (as amended) and with its implementing regulations issued by Saudi Arabia Competition Council Decision No. 126/1435 On the Issuance of the Implementing Regulation of the Competition Law.</li>
 	<li>Foreign Investment Law: Saudi Arabia Royal Decree No. M1/1421 on the Approval of the Foreign Investment Law*.</li>
 	<li>JSC: Joint Stock Company organised and incorporated in accordance with the rules and regulations of Saudi.</li>
 	<li>LLC: Limited Liability Company organised and incorporated in accordance with the rules and regulations of Saudi.</li>
 	<li>LOI: Letter of intent.</li>
 	<li>Long Stop Date: The deadline by which time a specified action/task must be completed.</li>
 	<li>Merger: A transaction where two or more corporate entities essentially combine their management and/or operations and may involve incorporating an entirely new corporate entity to represent the interests of the merged companies.</li>
 	<li>Merger and Acquisition Regulations: Saudi Arabia Capital Market Authority Merger and Acquisition Regulations. The regulations in Saudi Arabia issued by the Board of the CMA pursuant to its Saudi Arabia Resolution No. 150/2007, dated 21/09/1428 H corresponding to 03/10/2007 G, based on the Capital Market Law issued by Saudi Arabia Royal Decree No. M30/1424, dated 02/06/1424 H, and amended by Saudi Arabia Resolution of the Board of the CMA No. 345/2018, dated 07/08/1439 H (corresponding to 23/04/2018G), based on the Capital Market Law and the Companies\' Regulations.</li>
 	<li>MOCI: <a href="https://mci.gov.sa/en/Pages/default.aspx">Ministry of Commerce and Investment</a> in Saudi.</li>
 	<li>SAGIA: <a href="https://sagia.gov.sa/en/">Saudi Arabian General Investment Authority</a> in Saudi.</li>
 	<li>SPA: Share purchase agreement or an asset purchase agreement, depending on the type of Acquisition Transaction at issue.</li>
 	<li>Target: When an acquisition encompasses one corporate entity by absorbing another.</li>
</ul>
&nbsp;
<br/><br/>
<strong>Practical Guidance</strong>
<br/><br/>
<strong>Merger vs Acquisition</strong>
<br/><br/>
The term M&amp;A is often used interchangeably both within the legal and business community. When an M&amp;A transaction is referred to, it is inferred that two (or more) corporate entities are combining and/or transferring their respective ownerships and/or assets amongst one another, often with the purpose of, amongst a myriad of other commercial factors, enhancing the prospects for rapid business growth and/or greater exposure within a particular geographical region. Delving a little more into detail however, a “merger” encompasses a separate type of transaction than an “acquisition”. An acquisition encompasses one corporate entity (Acquiring Entity) absorbing another (Target), either in a “friendly” or “hostile” manner, by way of purchasing the Target\'s assets or stocks and/or offering cash or security in the Acquiring Entity, with the acquiring entity\'s management often remaining as is. No new entity is formed to represent the interests of the Target. This Practice Note focuses on acquisitions. However, it is important to note that in today\'s corporate world, the processes that go into closing a merger transaction and those involving an acquisition often tend to overlap. This is going to depend on the specific commercial understandings reached between the respective parties (i.e., retaining key senior management personnel of the Target, retaining a Target\'s brand for purposes of capitalising on a trademark\'s goodwill, etc.).
<br/><br/>
<strong>Acquisition Transaction - overview</strong>
<br/><br/>
Within Saudi Arabia, the Companies\' Regulations stipulate that six different types of corporate entities may be incorporated by law. Acquisition Transactions often involve entities incorporated as holding companies, JSC\'s, or LLCs. Acquisition Transactions involving public companies that are officially listed on Tadawul (the Saudi stock exchange) are regulated by the CMA by way of the Merger and Acquisition Regulations and require a separate analysis to denote the requirements to finalise an acquisition process that a publicly listed Acquiring Entity and/or Target.
<br/><br/>
Legal process outline
<br/><br/>
From a legal perspective, an Acquisition Transaction involves a series of processes that must be followed to ensure that each of the Acquiring Entity and the Target obtain their rights as ultimately agreed upon between the parties, from conducting a thorough legal due diligence on the Target to the negotiation and eventual execution of several binding agreements. Although the exact type of agreements and sequence of the processes involved in an Acquisition Transaction may differ depending on the nature and understandings reached in connection with a specific transaction, Acquisition Transactions materially involve the following:
<br/><br/>
<strong>1. Nondisclosure/ confidentiality agreement</strong>
Prior to engaging in any serious negotiations and in the interest of protecting sensitive and confidential information made available to the counterparty as part of each party\'s assessment on whether to move forward, the parties would agree on and execute a nondisclosure or confidentiality agreement. The terms within would require the recipient of any information deemed confidential to maintain it as such for an agreed upon period. Such confidentiality terms may also be included in individual clauses found within a letter of intent / term sheet as discussed below.
<br/><br/>
<strong>2. Letter of intent / term sheet</strong>
The parties to an Acquisition Transaction often opt to enter into either an LOI or a term sheet. Both documents outline the basic terms and conditions initially agreed upon by the respective parties (i.e., consideration, conditions precedent, etc.), as well as stipulate that subsequent definitive agreements will be entered into following successful completion of the due diligence process (after which the enforceability of the LOI or term sheet will terminate). Materially, an LOI/term sheet would also include a binding clause indicating that each party commits to negotiate exclusively with the other for a set period of time.
<br/><br/>
<strong>3. Legal due diligence</strong>
Subsequent to negotiating and executing the above described documentation, the Acquiring Entity will circulate to the Target a legal due diligence request list. The list will include, amongst others, documentation detailing/related to the following:
<br/><br/>
- constitutional documents;
- entity organisational structure (branches, subsidiaries, etc.);
- board/shareholders\' resolutions;
- tangible and intangible assets;
- powers of attorney;
- employment;
- related party contracts/transactions;
- contracts with customers, suppliers, and/or other third parties;
- financial transactions; and taxes.
<br/><br/>
The above exercise results in a legal due diligence report being prepared for use by the Acquiring Entity to identify any legal risks associated with the Acquisition Transaction.
<br/><br/>
<strong>4. Share/asset purchase agreement</strong>
Assuming the Acquiring Entity is satisfied with the results of the legal due diligence report (as well as with the separate financial due diligence report) and opts to proceed with the Acquisition Transaction, the parties need to negotiate and execute an SPA share purchase agreement. The SPA is the primary definitive agreement associated with the sale of the targeted shares or assets. Its purpose is to document the terms of the transaction, specify each party\'s rights,obligations and/or liabilities and provide contractual protection against undisclosed risks and/or liabilities of any party. Although there is no officially prescribed legal form for an SPA to follow, the majority of SPAs are written in a manner that include terms and conditions associated with the following (without limitation and not meant to be exclusive) clauses:
<br/><br/>
- identity of the parties;
- preamble/background details of the Acquisition Transaction;
- definitions (including for any agreed upon Long Stop Date);
- number of shares/assets, and relevant details on each, at issue in the Acquisition
- Transaction;
- price and consideration for the shares or assets;
- manner in which closing of the Acquisition Transaction shall occur;
- conditions precedent;
- warranties, indemnities, and specified remedies;
- any limitations on liability;
- tax provisions;
- restrictive covenants;
- confidentiality; and
- general legal provisions such as further assignment rights, entire agreement, dispute resolution mechanism, and governing law.
<br/><br/>
<strong>5. Closing an Acquisition Transaction</strong>
Upon all conditions precedent outlined in the SPA having been completed to the satisfaction of the relevant party, and following the execution of the required corporate approval documents and associated powers of attorney, the parties proceed to close the Acquisition Transaction. If the Acquisition Transaction involves a share purchase, the details associated with the relevant closing mechanism depends on the legal form of the Target in KSA.
<ul>
 	<li>For an LLC, the parties would need to execute the amended and restated articles of association of the Target in front of the notary public at MOCI, with the subject articles thereafter notarised. The Target\'s commercial registration file at MOCI would also be amended accordingly to reflect the share purchase by the Acquiring Entity.</li>
 	<li>In the case of a JSC, there is no process involving the notary public at MOCI. The parties simply ensure that the Target\'s shareholder register is amended to reflect the share purchase following the issuance of the relevant share certificates to the Acquiring Entity.</li>
</ul>
<em>Other material considerations</em>
<br/><br/>
As part of an Acquisition Transaction in KSA, it is prudent for an Acquiring Entity to consider several additional issues that could very well factor into the legal risks associated with the transaction and whether it will be allowed to close from a regulatory standpoint. Additionally, the parties should seek independent financial, commercial, and tax advice to bring any related risks associated with the latter to light as well.
<br/><br/>
The below are not meant to be exhaustive, but should be considered as part of ongoing negotiations between the parties:
As part of an Acquisition Transaction in KSA, it is prudent for an Acquiring Entity to consider several additional issues that could very well factor into the legal risks associated with the transaction andwhether it will be allowed to close from a regulatory standpoint. Additionally, the parties should seek independent financial, commercial, and tax advice to bring any related risks associated with the latter to light as well.
<br/><br/>
The below are not meant to be exhaustive, but should be considered as part of ongoing negotiations between the parties:
<br/><br/>
<strong>1. Foreign ownership restrictions</strong>
KSA\'s Foreign Investment Law allows for the concept of foreign investment within the country. SAGIA, the government entity that regulates foreign investment in KSA, possesses the authority to set the level or percentage of foreign investment in corporate entities participating in various industries or activities. In the majority of cases, the ownership level allowed for foreign nationals in many activities or industries prove significantly higher (up to 100% in many cases) than for onshore foreign investment in other GCC countries.
<br/><br/>
Notwithstanding, SAGIA does publish a “negative list” that lists the activities or industries where foreign investment is not allowed and that are reserved for participation only for KSA nationals. SAGIA also sets conditions for certain types of foreign investment licenses where a KSA national partner with a minimum percentage ownership is required. For instance, such as one type of retail license, which requires a minimum Saudi ownership of 25%, and EPC contracting licenses, which also require a minimum Saudi ownership of 25%.
<br/><br/>
<strong>2. Restrictions on share transfers</strong>
If an Acquisition Transaction involves an LLC as the Target, the Companies\' Regulations stipulates that non transferring shareholders possess a preemptive right to obtain the sale shares at issue. Once notified by way of written notice containing the requisite transfer details, the non transferring shareholders have 30 days thereafter to exercise their right to acquire the sale shares at issue.
<br/><br/>
If the Target is a JSC, the Acquiring Entity should be aware that the Target\'s founding shareholders are statutorily unable to transfer any of their shares to a non shareholder third party for a period of two years.
<br/><br/>
<strong>3. Unfair competition</strong>
The Competition Law, in attempting to combat or control anticompetitive practices in KSA, stipulates that the General Authority for Competition in KSA must be notified (within a minimum of 60 days prior to the Acquisition Transaction\'s closing) in instances where an Acquisition Transaction results with an Acquiring Entity ultimately possessing a dominant position (assessed in part as 40% market share in KSA) in the market.
<br/><br/>
Such transactions are evaluated for “economic concentration” (through a set of various determining factors) by the said General Authority for Competition. “Economic concentration” is attained by transferring ownership (through an Acquisition Transaction, merger, or combination) of a Target\'s assets, rights, shares, interests, or obligations to an Acquiring Entity in KSA.
<br/><br/>
<strong>4. Material principles of Sharia law</strong>
An understanding of Sharia law as applied in Saudi Arabia and what elements of an agreement are considered enforceable or unenforceable in the country, proves vital when:
<br/><br/>
a. negotiating terms and conditions within the various agreements that encompass an Acquisition Transaction,
b. highlighting the relevant legal risks encountered as part of the legal due diligence exercise, or
c. assessing legal defences should a dispute occur between the respective parties.
<br/><br/>
We list below certain points of interest pertaining to Sharia law (i.e., Saudi law) that should be considered:
<ul>
 	<li>Specifically relating to payment terms in general, an interest payment provision is unenforceable under Saudi law. Substance prevails over form. Nomenclature is disregarded where it is designed to disguise the payment of interest and any failure to make a payment which is considered in effect to be one of interest, as well as not being enforceable, may not be regarded by a Saudi court and arbitration panel as constituting a breach of contract.</li>
 	<li>Provisions limiting liability may be held unenforceable by Saudi courts and arbitration panels on the basis that a party cannot limit or exclude liability for his own failure to honour the terms of the contract.</li>
 	<li>If damages are payable, the quantum of damages is assessed on the basis of actual loss and Saudi law will never award punitive or consequential damages.</li>
 	<li>Saudi law generally recognises liquidated damages. However, if liquidated damages are challenged, a Saudi court and arbitration panel may not enforce a contractual provision for the payment of liquidated damages where the agreed liquidated damages are excessive, notwithstanding that the parties agreed to such a provision. The court or arbitration panel would assess whether the liquidated damages were fair and reasonable and, based on their own independent investigation, would determine whether to enforce such liquidated damages.</li>
 	<li>Indemnities are subject to the same rules as apply to liquidated damages: the amount claimed must be certain and must reflect the actual provable direct loss suffered by the claimant. Further, a party is only liable to give compensation for losses that it has occasioned.</li>
 	<li>While Saudi law does not impose a duty on the parties to legal agreements to act reasonably or in a nonarbitrary manner, there is a general assumption that contracting parties will act reasonably in the implementation and operation of their contractual arrangements.</li>
 	<li>Saudi law does give effect to provisions, which allow a party to act in its sole or absolute discretion, subject to the implied standard of reasonableness that applies to each of the contracting parties in their performance of the agreement.</li>
 	<li>There is no remedy of specific performance. In the event of breach of a representation or warranty, the sole remedy would be monetary compensation for any actual direct provable loss.</li>
</ul>
The above is a contribution to the Lexis Nexis Gulf Legal Advisor project.','post_date' => '2018-12-12 13:06:55','post_name' => 'introduction-to-company-acquisitions-in-kingdom-of-saudi-arabia-business-law','post_parent' => '0','Image' => 'wp-content/uploads/2018/12/shutterstock_684671572-1.jpg','Categories' => '31, 86','Tags' => '701, 1020, 1073'),

            array('ID' => '61423','post_title' => '24-hour Curfew in Major Saudi Cities Announced','post_content' => '<h4>Saudi authorities imposed a 24-hour curfew in most Saudi cities, the capital Riyadh as well as in Jeddah, Dammam, Al-Khobar, Tabuk, Dhahran, Al-Hofuf, Ta\'if, Al-Qatif.</h4>
Residents can only leave their homes to get essential needs within their neighborhoods between 6am and 3pm.
<br/><br/>
Only two passengers, including the driver, may be allowed inside vehicles.
<br/><br/>
Travel between cities is prohibited.
<br/><br/>
The curfew decision excludes essential workers in public and private sectors such as medical facilities and pharmacies, grocery stores, gas and oil stations, banking services and maintenance and operation, plumbing, electrical and air conditioning technicians and water delivery services and sewage tanks.
<br/><br/>
Auhored by Senior Associate, <a href="https://bsabh.com/lawyer/jean-abboud/">Jean Abboud</a>','post_date' => '2020-04-08 09:04:54','post_name' => '24-hour-curfew-in-major-saudi-cities-announced','post_parent' => '0','Image' => 'wp-content/uploads/2020/04/shutterstock_331444775.jpg','Categories' => '31, 1696','Tags' => '163, 1598, 1672, 1804'),


        ];
        $randomImages = [
            'https://bsabh.com/uploads/articles-optimized/bGx9M4vJm8x97mx2NIWIr0nc35Q2x8DU.jpg',
            'https://bsabh.com/uploads/articles-optimized/YwqtsZgE01JDnApWFZGXBn00doL9pnhW.jpg',
            'https://bsabh.com/uploads/articles-optimized/iA7fpCyvhNSDLi7HiQyEBOFNwDTnCMdX.jpg',
            'https://bsabh.com/uploads/articles-optimized/OdjSC8W7yGNCo5W8TYUWzVnKbGlC0Zsz.jpg',
            'https://bsabh.com/uploads/articles-optimized/vn4FjoI2xg6F9MRCBnpTMOpmtEMm6GLR.jpg',
            'https://bsabh.com/uploads/articles-optimized/MIOWINkhDyqgylOCQdQ7eoi9jU5oz7Sk.jpg',
            'https://bsabh.com/uploads/articles-optimized/0Ek3PzzlpAnATIh1Wtiewebi9mM6UKyL.jpg',
            'https://bsabh.com/uploads/articles-optimized/5UOKFcUjWo6x0QcdmIDHi3zRyquEMEEY.jpg',
            'https://bsabh.com/uploads/articles-optimized/sc0Vr6zONTJ1DvjqN9dYAi4qbJWcM4y2.jpg',
            'https://bsabh.com/uploads/articles-optimized/PeYnzy1C2A1G0hNhjhN9eN77t4vPLCiv.jpg',
            'https://bsabh.com/uploads/articles-optimized/9iyHv53mUzY2XOZI5RjaTrdzAROB1B3G.jpg',
            'https://bsabh.com/uploads/articles-optimized/uTslbPRVNaZt9ivQ9c2q9pXKDLMC6AyE.jpg',
            'https://bsabh.com/uploads/articles-optimized/mQij2DTR4ZbYhbFp4EnsbYcxzdm4aqY9.jpg',
            'https://bsabh.com/uploads/articles-optimized/ia6XcVWEAmqPkAMRtm1Uc6dkGXQ4Dt3Q.jpg',
            'https://bsabh.com/uploads/articles-optimized/cA3sQjvzVlrgZ1GWUATOhQX4QvSpXYFT.jpg',
        ];

        foreach($data as $item){
            $new['title'] = $item['post_title'];
            $new['content'] = $item['post_content'];
            $new['date'] = $item['post_date'];
            $slug = $item['post_name'];

            $slugLoop = 1;

            if(Article::where('slug',$slug)->count()==0){

                $new['photo'] = $randomImages[rand(0,5)];
                $new['slug'] = $slug;

                $article = Article::create($new);

                if($article){
                    $article->categories()->sync(['3003']);
                }
            }


        }
    }
}
