<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call(UserSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(TagSeeder::class);
        $this->call(LocationSeeder::class);
        $this->call(TitleSeeder::class);
        $this->call(PracticeSeeder::class);
        $this->call(ServiceTypeSeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(LawyerSeeder::class);
        $this->call(ArticleSeeder::class);
        $this->call(ArticleTwoSeeder::class);
        $this->call(ArticleLawyerSeeder::class);
        $this->call(ServiceArticleSeeder::class);
        $this->call(ServiceLawyerSeeder::class);
        $this->call(TimelineSeeder::class);
        $this->call(HighlightArticleSeeder::class);
        $this->call(LawyerFilterSeeder::class);
        $this->call(PromoboxSeeder::class);
        $this->call(PageSectionSeeder::class);
        $this->call(ActivitySeeder::class);
        $this->call(AwardSeeder::class);
        $this->call(ValueSeeder::class);
        $this->call(CustomPageSeeder::class);
        $this->call(ContactTeamSeeder::class);
        $this->call(NewCategorySeeder::class);
        $this->call(TagConverter::class);
    }
}
