<?php
namespace Database\Seeders;
use App\Models\Article;
use App\Models\Lawyer;
use Illuminate\Database\Seeder;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class LawyerImageFixer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Lawyer::get();

        foreach($data as $lawyer){
            if($lawyer->photo){

                if(strpos($lawyer->photo, 'lawyers-optimized') !== false){

                } else {
                    if(strpos($lawyer->photo, 'https://bsabh.com/') !== false)
                        $lawyer->photo = trim($lawyer->photo, "https://bsabh.com/");

                    $image =  new UploadedFile( public_path($lawyer->photo), 'tmp.jpg', 'image/jpeg',null,true);

                    $destinationPath = 'public/uploads/lawyers-optimized';
                    $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

                    Image::make($image->getRealPath())->fit(350,350)->save($destinationPath.'/'.$newFileName);
                    $lawyer->photo = 'uploads/lawyers-optimized/'. $newFileName;

                    $lawyer->save();
                }
            }
        }
    }
}
