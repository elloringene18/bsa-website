<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLawyerFilterItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lawyer_filter_items', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->bigInteger('lawyer_id')->unsigned()->index();
            $table->foreign('lawyer_id')->references('id')->on('lawyers')->onDelete('cascade');
            $table->bigInteger('lawyer_filter_id')->unsigned()->index();
            $table->foreign('lawyer_filter_id')->references('id')->on('lawyer_filters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lawyer_filter_items');
    }
}
