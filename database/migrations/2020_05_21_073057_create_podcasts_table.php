<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePodcastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('podcasts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('excerpt')->nullable();
            $table->string('speaker')->nullable();
            $table->string('slug')->nullable();
            $table->string('description')->nullable();
            $table->date('date')->nullable()->default(null);
            $table->text('content')->nullable();
            $table->longText('audio_file');
            $table->longText('image');
            $table->tinyInteger('active')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('podcasts');
    }
}
