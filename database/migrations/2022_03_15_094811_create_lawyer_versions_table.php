<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLawyerVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lawyer_versions', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('slug')->nullable();
            $table->longText('content')->nullable();
            $table->string('photo')->nullable();
            $table->string('telephone')->nullable();
            $table->string('email')->nullable();
            $table->text('excerpt')->nullable();
            $table->tinyInteger('hidden')->default(0);
            $table->string('titles')->nullable();
            $table->string('practices')->nullable();
            $table->string('locations')->nullable();
            $table->string('articles')->nullable();
            $table->string('experiences')->nullable();
            $table->string('services')->nullable();
            $table->string('industries')->nullable();
            $table->string('quotes')->nullable();
            $table->string('tags')->nullable();
            $table->string('educations')->nullable();
            $table->bigInteger('lawyer_id')->unsigned()->index();
            $table->foreign('lawyer_id')->references('id')->on('lawyers')->onDelete('cascade');
            $table->integer('version');
            $table->tinyInteger('is_current')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lawyer_versions');
    }
}
