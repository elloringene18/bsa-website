<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class APIController extends Controller
{
    public function getTimes(){
        $date = new \DateTime();

        $date->setTimezone(new \DateTimeZone('Asia/Dubai'));
        $data['uae'] = $date->format('H:i');
        $date->setTimezone(new \DateTimeZone('Asia/Baghdad'));
        $data['baghdad'] = $date->format('H:i');
        $date->setTimezone(new \DateTimeZone('Asia/Beirut'));
        $data['beirut'] = $date->format('H:i');
        $date->setTimezone(new \DateTimeZone('Asia/Muscat'));
        $data['oman'] = $date->format('H:i');
        $date->setTimezone(new \DateTimeZone('Asia/Riyadh'));
        $data['riyadh'] = $date->format('H:i');

        return $data;
    }
}

?>
