<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\ArtistInteraction;
use App\Models\ContactEntry;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class FormController extends Controller
{
    public function __construct(Subscriber $model)
    {
        $this->model = $model;
    }

    public function subscribe(Request $request){
        $input = $request->input('email');

        if(Subscriber::where('email',$input)->count())
            return 'This email has already been registered.';

        $success = Subscriber::create(['email'=>$input]);

        if($success){
            $message = 'Thank you for subscribing to our newsletter.';
            $emailData = $input;

            $subject = 'BSA Website - Newsletter Subscription';

            try {
                Mail::send('mail.subscribe', ['data' => $emailData], function ($message) use ($subject) {
                    $message->from('bsa.comms@bsabh.com', 'BSA Website')->to('bsa.comms@bsabh.com', 'BSA')->subject($subject);
                });
            }
            catch (\Exception $e) {
                dd($e);
            }
        }

        return $message;
    }

    public function sendEmail($input){

        $emailData = [
            'name' => $input['name'],
            'contact' => $input['contact'],
            'message' => $input['message'],
        ];

        $subject = 'Murabbaa Website - Artwork Inquiry';
        $artist = Artist::find($input['artist_id']);

        try {
            Mail::send('mail.send-artist', ['data' => $emailData], function ($message) use ($input, $artist,$subject) {
                $message->from('Artists@ajmantourism.ae', 'Murabbaa Website')->to('gene@thisishatch.com', 'Gene')->subject($subject);
//                $message->from('info@ajmantourism.ae', $input['name'])->to('info@ajmantourism.ae', 'Ajman Tourism')->subject($subject);
            });
        }
        catch (\Exception $e) {
            return false;
        }

        return true;
    }

    public function contact(Request $request){

        $type = $request->input('source');

        if($type=='contact')
            $request->validate([
                'phone' => 'required|max:255',
                'name' => 'required|max:255',
                'email' => 'required|email|max:255',
                'message' => 'required|max:500',
                'captcha' => 'required|captcha'
            ]);
        else {
            Session::flash('error','You seem to have manipulated the form.');
            return redirect()->back();
        }

        $input = $request->except('_token','user_id','lang');

        if(
            strpos($input['name'], 'http') !== false || strpos($input['name'], 'www') !== false ||
            strpos($input['phone'], 'http') !== false || strpos($input['phone'], 'www') !== false ||
            strpos($input['email'], 'http') !== false || strpos($input['email'], 'www') !== false ||
            strpos($input['message'], 'http') !== false || strpos($input['message'], 'www') !== false ||
            strpos($input['captcha'], 'http') !== false || strpos($input['captcha'], 'www') !== false
        ) {
            Session::flash('error','Links are not allowed.');
            return redirect()->back();
        }

        $entry = ContactEntry::create(['ip'=>$request->ip(),'source'=>$type]);

        if($entry){

            foreach ($input as $key=>$item){
                $entry->items()->create(['key'=>$key,'value'=>$item]);
            }

            Session::flash('message','Thank you for your contacting us. We will get back to you soon.');

            $emailData = $input;

            $subject = 'BSA Website - Contact Form';

            try {
                Mail::send('mail.contact', ['data' => $emailData], function ($message) use ($subject) {
                    $message->from('bsa.comms@bsabh.com', 'BSA Website')->to('bsa.comms@bsabh.com', 'BSA')->subject($subject);
                });
            }
            catch (\Exception $e) {
                dd($e);
            }

            return redirect()->back();
        }


        Session::flash('error','An error has occurred. Please try again later.');

        return redirect()->back();
    }

    public function reloadCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }
}
