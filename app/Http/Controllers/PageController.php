<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index(){

        $news = Article::with('tags','categories')->whereHas('categories', function($query) {
            return $query->where('slug','news');
        })->where('hidden',0)->orderBy('date','DESC')->limit(8)->get();

        return view('home',compact('news'));
    }
    public function showPage($page=null){
        $lang = 'en';

        if(view()->exists($page)){
            return view($page,compact('lang'));
        } else if (!$page){
            return view('home',compact('lang'));
        } else {
            return view('404',compact('lang'));
        }

    }

    public function showPageAR($page=null){
        $lang = 'ar';

        if(view()->exists($page)){
            return view($page,compact('lang'));
        } else if (!$page){
            return view('home',compact('lang'));
        } else {
            return view('404',compact('lang'));
        }
    }

    public function showBlog(){
        $lang = 'en';

        return view('article', compact('lang'));
    }

    public function showBlogAR(){
        $lang = 'ar';

        return view('article', compact('lang'));
    }
}
