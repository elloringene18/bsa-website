<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Value;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ValueController extends Controller
{

    public function __construct(Value $model)
    {
        $this->model = $model;
        $this->pageslug = 'careers';
    }

    public function index(){
        $pageSlug = $this->pageslug;
        $data = Value::get();

        return view('admin.values.index', compact('data','pageSlug'));
    }

    public function edit($id){

        $pageSlug = $this->pageslug;
        $data = $this->model->find($id);

        if(!$data)
            return 'Post not found!';

        return view('admin.values.edit',compact('pageSlug','data'));
    }

    public function delete($id){

        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $post->delete();

        Session::flash('success','Item deleted successfully');

        return redirect('admin/careers/values');
    }


    public function update(Request $request){

        $id = $request->input('id');
        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $input = $request->except('_token','image');
        $files = $request->file();

        if(isset($files['image'])){
            $file = $files['image'];
            //Move Uploaded File
            $destinationPath = 'public/uploads/values';

            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
            Image::make($file->getRealPath())->fit(532, 463)->save($destinationPath.'/'.$newFileName);
            $input['image'] = 'uploads/values/'. $newFileName;
        }

        $post->update($input);

        Session::flash('success','Item saved successfully');
        return redirect('admin/careers/values/edit/'.$post->id);
    }

    public function create(){
        $pageSlug = $this->pageslug;

        return view('admin.values.create',compact('pageSlug'));
    }

    public function store(Request $request){

        $input = $request->except('_token','image');
        $files = $request->file();

        if(isset($files['image'])){
            $file = $files['image'];
            //Move Uploaded File
            $destinationPath = 'public/uploads/values';

            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
            Image::make($file->getRealPath())->fit(532, 463)->save($destinationPath.'/'.$newFileName);
            $input['image'] = 'uploads/values/'. $newFileName;
        }

        $post = $this->model->create($input);

        Session::flash('success','Item saved successfully');
        return redirect('admin/careers/values/edit/'.$post->id);
    }
}
