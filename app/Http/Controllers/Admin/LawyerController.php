<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\ArticleLawyer;
use App\Models\Lawyer;
use App\Models\LawyerQuote;
use App\Models\LawyerReel;
use App\Models\LawyerService;
use App\Models\LawyerTag;
use App\Models\LawyerTitle;
use App\Models\Location;
use App\Models\Service;
use App\Models\Tag;
use App\Models\Title;
use App\Services\CanCreateSlug;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class LawyerController extends Controller
{
    use CanCreateSlug;

    public function __construct(Lawyer $model)
    {
        $this->model = $model;
        $this->pageslug = 'lawyers';
    }

    public function index(){
        $pageSlug = $this->pageslug;
        $data = Lawyer::orderBy('name')->get();

        return view('admin.lawyers.index', compact('data','pageSlug'));
    }

    public function create(){
        $pageSlug = $this->pageslug;
        $tags = Tag::select('name','t_id')->get();
        $articles = Article::where('title','!=','')->select('title','id')->get();
        $services = Service::select('title','id')->get();
        $locations = Location::select('name','l_id')->get();
        $titles = Title::select('name','t_id')->get();

        return view('admin.lawyers.create',compact('pageSlug','services','tags','articles','locations','titles'));
    }

    public function store(Request $request){

        $input = $request->except('_token','image','title-main','title-sub','location');
        $files = $request->file();

        if(isset($files['photo'])){
            $file = $files['photo'];
            //Move Uploaded File
            $destinationPath = 'public/uploads/lawyers';

            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
            Image::make($file->getRealPath())->fit(350, 350)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/lawyers/'. $newFileName;
        }

        $input['name'] = $input['first_name'].' '.$input['last_name'];
        $input['slug'] = $this->generateSlug($input['name']);

        $newData = $this->model->create($input);

        $data = $request->only('title-main','title-sub','quotes','tags','location','articles');

        if($newData){

            LawyerTitle::create(['lawyer_id'=>$newData->id,'title_id'=>$data['title-main'],'primary'=>1]);

            if($data['title-sub'])
                LawyerTitle::create(['lawyer_id'=>$newData->id,'title_id'=>$data['title-sub']]);

            $newData->locations()->sync([$request->input('location')]);

            if($input['quotes']){
                if(count($input['quotes'])){
                    foreach ($input['quotes'] as $item){
                        if($item['content'])
                            $newData->quotes()->create($item);
                    }
                }
            }

            if($input['tags']){
                $tags = json_decode($input['tags'],true);
                if(is_array($tags)){
                    foreach ($tags as $item){
                        $newData->tags()->create(['tag_id' => $item['id']]);
                    }
                }
            }

            if($input['articles']){
                $articles = json_decode($input['articles'],true);
                if(is_array($articles)){
                    foreach ($articles as $item){
                        ArticleLawyer::create(['lawyer_id' => $newData->id,'article_id' => $item['id']]);
                    }
                }
            }

            if($input['services']){
                $services = json_decode($input['services'],true);
                if(is_array($services)){
                    $s = [];
                    foreach ($services as $item){
                        $s[] = $item['id'];
                    }

                    $newData->services()->sync($s);
                }
            }

            if($input['reels']){
                $services = json_decode($input['reels'],true);
                if(is_array($services)){
                    $s = [];
                    foreach ($services as $item){
                        $s[] = $item['id'];
                    }

                    $newData->reels()->sync($s);
                }
            }

            if($input['experiences']){
                if(count($input['experiences'])){
                    foreach ($input['experiences'] as $item){
                        if($item)
                            $newData->experience()->create(['content' => $item]);
                    }
                }
            }

            if($input['educations']){
                if(count($input['educations'])){
                    foreach ($input['educations'] as $item){
                        if($item)
                            $newData->education()->create(['content' => $item]);
                    }
                }
            }
        }

        Session::flash('success','Item saved successfully');
        return redirect('admin/lawyers/edit/'.$newData->id);
    }

    public function edit($id){

        $pageSlug = $this->pageslug;
        $data = $this->model->find($id);

        if(!$data)
            return 'Post not found!';

        $tags = Tag::select('name','t_id')->get();
        $articles = Article::where('title','!=','')->select('title','id')->get();
        $services = Service::select('title','id')->get();
        $locations = Location::select('name','l_id')->get();
        $titles = Title::select('name','t_id')->get();

        return view('admin.lawyers.edit',compact('pageSlug','data','pageSlug','services','tags','articles','locations','titles'));
    }


    public function update(Request $request){

        $id = $request->input('id');
        $post = $this->model->find($id);
        $input = $request->except('_token','image','title-main','title-sub','location');

        if(!$post)
            return 'Post not found!';

        $exists = Lawyer::where('slug',$input['slug'])->first();

        if($exists) {
            if($post->id != $exists->id) {
                Session::flash('error','This URL already exists in the site. Please use a different one.');
                return redirect()->back();
            }
        }

        $files = $request->file();

        if(isset($files['photo'])){
            $file = $files['photo'];
            //Move Uploaded File
            $destinationPath = 'public/uploads/lawyers';

            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
            Image::make($file->getRealPath())->fit(350, 350)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/lawyers/'. $newFileName;
        }

        $input['name'] = $input['first_name'].' '.$input['last_name'];

//        if($input['name']!=$post->name)
//            $input['slug'] = $this->generateSlug($input['name']);

        $post->update($input);

        $data = $request->only('title-main','title-sub','quotes','tags','location','articles');

        if($post){

            LawyerTitle::where('lawyer_id',$post->id)->delete();

            LawyerTitle::create(['lawyer_id'=>$post->id,'title_id'=>$data['title-main'],'primary'=>1]);

            if($data['title-sub'])
                LawyerTitle::create(['lawyer_id'=>$post->id,'title_id'=>$data['title-sub']]);

            $post->locations()->sync([$request->input('location')]);

            LawyerQuote::where('lawyer_id',$post->id)->delete();

            if($input['quotes']){
                if(count($input['quotes'])){
                    foreach ($input['quotes'] as $item){
                        if(isset($item['content']))
                            if($item['content'])
                                $post->quotes()->create($item);
                    }
                }
            }

            LawyerTag::where('lawyer_id',$post->id)->delete();
            if($input['tags']){
                $tags = json_decode($input['tags'],true);
                if(is_array($tags)){
                    foreach ($tags as $item){
                        $post->tags()->create(['tag_id' => $item['id']]);
                    }
                }
            }

            ArticleLawyer::where('lawyer_id',$post->id)->delete();
            if($input['articles']){
                $articles = json_decode($input['articles'],true);
                if(is_array($articles)){
                    foreach ($articles as $item){
                        ArticleLawyer::create(['lawyer_id' => $post->id,'article_id' => $item['id']]);
                    }
                }
            }

            LawyerService::where('lawyer_id',$post->id)->delete();
            if($input['services']){
                $services = json_decode($input['services'],true);
                if(is_array($services)){
                    $s = [];
                    foreach ($services as $item){
                        $s[] = $item['id'];
                    }

                    $post->services()->sync($s);
                }
            }

            LawyerReel::where('lawyer_id',$post->id)->delete();
            if($input['reels']){
                $services = json_decode($input['reels'],true);
                if(is_array($services)){
                    $s = [];
                    foreach ($services as $item){
                        $s[] = $item['id'];
                    }

                    $post->reels()->sync($s);
                }
            }

            $post->experience()->delete();
            if($input['experiences']){
                if(count($input['experiences'])){
                    foreach ($input['experiences'] as $item){
                        if($item)
                            $post->experience()->create(['content' => $item]);
                    }
                }
            }

            $post->education()->delete();
            if($input['educations']){
                if(count($input['educations'])){
                    foreach ($input['educations'] as $item){
                        if($item)
                            $post->education()->create(['content' => $item]);
                    }
                }
            }
        }

        Session::flash('success','Item updated successfully');
        return redirect('admin/lawyers/edit/'.$post->id);
    }

    public function delete($id){

        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $post->delete();

        Session::flash('success','Item deleted successfully');

        return redirect('admin/content/press-releases');
    }

}
