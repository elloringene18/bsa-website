<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Activity;
use App\Models\Article;
use App\Models\Category;
use App\Models\Lawyer;
use App\Models\Location;
use App\Models\Promo;
use App\Models\Service;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ActivityController extends Controller
{

    public function __construct(Activity $model)
    {
        $this->model = $model;
        $this->pageslug = 'about';
    }

    public function index(){
        $pageSlug = $this->pageslug;
        $data = Article::orderBy('date','DESC')->whereHas('categories',function($query){ return $query->where('slug','csr-activity'); })->simplePaginate(100);

        return view('admin.activities.index', compact('data','pageSlug'));
    }

    public function edit($id){
        $data = Article::find($id);

        if(!$data)
            return 'Post not found!';

        $pageSlug = $this->pageslug;
        $tags = Tag::select('name','t_id')->get();
        $articles = Article::where('title','!=','')->select('title','id')->get();
        $services = Service::select('title','id')->get();
        $locations = Location::select('name','l_id')->get();
        $lawyers = Lawyer::select('name','id')->get();
        $categories = Category::select('name','cat_id','id')->where('slug','csr-activity')->get();
        $promos = Promo::where('type','short')->select('title','id')->get();

        return view('admin.activities.edit',compact('data','categories','pageSlug','services','tags','articles','locations','lawyers','promos'));
    }

    public function delete($id){

        $post = Article::find($id);

        if(!$post)
            return 'Post not found!';

        $post->delete();

        Session::flash('success','Item deleted successfully');

        return redirect('admin/about/activities');
    }


    public function update(Request $request){

        $id = $request->input('id');
        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $input = $request->except('_token');

        $post->update($input);

        Session::flash('success','Item updated successfully');
        return redirect('admin/about/activities');
    }

    public function create(){
        $pageSlug = $this->pageslug;
        $tags = Tag::select('name','t_id')->get();
        $articles = Article::where('title','!=','')->select('title','id')->get();
        $services = Service::select('title','id')->get();
        $locations = Location::select('name','l_id')->get();
        $lawyers = Lawyer::select('name','id')->get();
        $categories = Category::select('name','cat_id','id')->where('slug','csr-activity')->get();
        $promos = Promo::where('type','short')->select('title','id')->get();

        return view('admin.activities.create',compact('categories','pageSlug','services','tags','articles','locations','lawyers','promos'));
    }

    public function store(Request $request){

        $input = $request->except('_token');

        $post = $this->model->create($input);

        Session::flash('success','Item saved successfully');
        return redirect('admin/about/activities');
    }
}
