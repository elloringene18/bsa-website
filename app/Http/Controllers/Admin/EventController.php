<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ArticleLawyer;
use App\Models\ArticlePromo;
use App\Models\ArticleRelated;
use App\Models\Category;
use App\Models\Lawyer;
use App\Models\LawyerTitle;
use App\Models\Location;
use App\Models\Promo;
use App\Models\Service;
use App\Models\Tag;
use App\Models\Title;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Article;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use App\Services\CanCreateSlug;

class EventController extends Controller
{
    use CanCreateSlug;

    public function __construct(Article $model)
    {
        $this->model = $model;
        $this->pageslug = 'articles';
    }

    public function index(){
        $pageSlug = $this->pageslug;

        $posts = $this->model->whereHas('categories',function ($query){ return $query->where('slug','events'); })->orderBy('date','DESC')->simplePaginate(15);
        return view('admin.events.index', compact('posts','pageSlug'));
    }


    public function create(){
        $pageSlug = $this->pageslug;
        $tags = Tag::select('name','t_id')->get();
        $articles = Article::where('title','!=','')->select('title','id')->get();
        $services = Service::select('title','id')->get();
        $locations = Location::select('name','l_id')->get();
        $lawyers = Lawyer::select('name','id')->get();
        $categories = Category::where('slug','events')->select('name','cat_id')->get();
        $promos = Promo::where('type','short')->select('title','id')->get();

        return view('admin.events.create',compact('categories','pageSlug','services','tags','articles','locations','lawyers','promos'));
    }

    public function store(Request $request){

        $input = $request->except('_token','photo');
        $files = $request->file();

        if(isset($files['photo'])){
            $file = $files['photo'];
            //Move Uploaded File
            $destinationPath = 'public/uploads/articles';

            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
            \Intervention\Image\Facades\Image::make($file->getRealPath())->fit(300, 200)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/articles/'. $newFileName;
        }

        if(isset($files['photo_full'])){
            $file = $files['photo_full'];
            //Move Uploaded File
            $destinationPath = 'public/uploads/articles';

            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
            \Intervention\Image\Facades\Image::make($file->getRealPath())->fit(1296, 332)->save($destinationPath.'/'.$newFileName);
            $input['photo_full'] = 'uploads/articles/'. $newFileName;
        }

        $input['slug'] = $this->generateSlug($input['title']);
        $input['date'] = Carbon::parse($input['date'])->format('Y-m-d');

        $newData = $this->model->create($input);

        $data = $request->only('tags','articles','category','promos');

        if($newData){

//            if($data['location'])
//                $newData->location()->create(['location_id' => $data['location']]);

            $newData->categories()->sync([$data['category']]);

            if(isset($input['tags'])){
                $tags = json_decode($input['tags'],true);
                if(is_array($tags)){
                    $t = [];
                    foreach ($tags as $item){
                        $t[] = $item['id'];
                    }
                    $newData->tags()->sync($t);
                }
            }

            if(isset($input['contacts'])){
                $contacts = json_decode($input['contacts'],true);
                if(is_array($contacts)){
                    foreach ($contacts as $item){
                        ArticleLawyer::create(['lawyer_id' => $item['id'],'article_id' => $newData->id]);
                    }
                }
            }

            if(isset($input['promos'])){
                $promos = json_decode($input['promos'],true);
                if(is_array($promos)){
                    foreach ($promos as $item){
                        ArticlePromo::create(['promo_id' => $item['id'],'article_id' => $newData->id]);
                    }
                }
            }

            if(isset($input['articles'])){
                $articles = json_decode($input['articles'],true);
                if(is_array($articles)){
                    foreach ($articles as $item){
                        ArticleRelated::create(['article_related_id' => $item['id'],'article_id' => $newData->id]);
                    }
                }
            }

            if(isset($input['services'])){
                $services = json_decode($input['services'],true);
                if(is_array($services)){
                    $s = [];
                    foreach ($services as $item){
                        $s[] = $item['id'];
                    }

                    $newData->services()->sync($s);
                }
            }
        }

        Session::flash('success','Item added successfully');
        return redirect('admin/events/edit/'.$newData->id);
    }

    public function edit($id){

        $data = $this->model->find($id);

        if(!$data)
            return 'Post not found!';

        $pageSlug = $this->pageslug;
        $tags = Tag::select('name','t_id')->get();
        $articles = Article::where('title','!=','')->select('title','id')->get();
        $services = Service::select('title','id')->get();
        $locations = Location::select('name','l_id')->get();
        $lawyers = Lawyer::select('name','id')->get();
        $categories = Category::where('slug','events')->select('name','cat_id','id')->get();
        $promos = Promo::where('type','short')->select('title','id')->get();

        return view('admin.events.edit',compact('data','categories','pageSlug','services','tags','articles','locations','lawyers','promos'));
    }

    public function delete($id){

        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $post->delete();

        Session::flash('success','Item deleted successfully');

        return redirect('admin/events');
    }


    public function update(Request $request){

        $id = $request->input('id');
        $newData = $this->model->find($id);

        if(!$newData)
            return 'Post not found!';


        $input = $request->except('_token','photo');
        $files = $request->file();

        if(isset($files['photo'])){
            $file = $files['photo'];
            //Move Uploaded File
            $destinationPath = 'public/uploads/articles';

            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
            \Intervention\Image\Facades\Image::make($file->getRealPath())->fit(300, 200)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/articles/'. $newFileName;
        }

        if(isset($files['photo_full'])){
            $file = $files['photo_full'];
            //Move Uploaded File
            $destinationPath = 'public/uploads/articles';

            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
            \Intervention\Image\Facades\Image::make($file->getRealPath())->fit(1296, 332)->save($destinationPath.'/'.$newFileName);
            $input['photo_full'] = 'uploads/articles/'. $newFileName;
        }


        if($newData->title != $input['title'])
            $input['slug'] = $this->generateSlug($input['title']);

        $input['date'] = Carbon::parse($input['date'])->format('Y-m-d');

        $newData->update($input);

        $data = $request->only('tags','location','articles','category');

        if($newData){

//            $newData->location()->delete();
//
//            if($data['location'])
//                $newData->location()->create(['location_id' => $data['location']]);

            $newData->categories()->sync([$data['category']]);

            if(isset($input['tags'])){
                $tags = json_decode($input['tags'],true);
                if(is_array($tags)){
                    $t = [];
                    foreach ($tags as $item){
                        $t[] = $item['id'];
                    }
                    $newData->tags()->sync($t);
                }
            }

            ArticleLawyer::where('article_id',$newData->id)->delete();

            if(isset($input['contacts'])){
                $contacts = json_decode($input['contacts'],true);
                if(is_array($contacts)){
                    foreach ($contacts as $item){
                        ArticleLawyer::create(['lawyer_id' => $item['id'],'article_id' => $newData->id]);
                    }
                }
            }

            ArticleRelated::where('article_id',$newData->id)->delete();
            if(isset($input['articles'])){
                $articles = json_decode($input['articles'],true);
                if(is_array($articles)){
                    foreach ($articles as $item){
                        ArticleRelated::create(['article_related_id' => $item['id'],'article_id' => $newData->id]);
                    }
                }
            }

            ArticlePromo::where('article_id',$newData->id)->delete();
            if(isset($input['promos'])){
                $promos = json_decode($input['promos'],true);
                if(is_array($promos)){
                    foreach ($promos as $item){
                        ArticlePromo::create(['promo_id' => $item['id'],'article_id' => $newData->id]);
                    }
                }
            }

            if(isset($input['services'])){
                $services = json_decode($input['services'],true);
                if(is_array($services)){
                    $s = [];
                    foreach ($services as $item){
                        $s[] = $item['id'];
                    }

                    $newData->services()->sync($s);
                }
            }
        }

        Session::flash('success','Item updated successfully');
        return redirect('admin/events/edit/'.$newData->id);
    }

}
