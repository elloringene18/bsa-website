<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BottomLink;
use App\Models\Value;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class BottomLinkController extends Controller
{

    public function __construct(BottomLink $model)
    {
        $this->model = $model;
        $this->pageslug = 'link';
    }

    public function index(){
        $pageSlug = $this->pageslug;
        $data = BottomLink::get();

        return view('admin.bottomlinks.index', compact('data','pageSlug'));
    }

    public function update(Request $request){
        $input = $request->input('link');

        BottomLink::truncate();

        foreach ($input as $item)
            if($item['title'] && $item['link'])
                BottomLink::create($item);

        Session::flash('success','Item update successfully');
        return redirect('admin/bottomlinks/');
    }

}
