<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Podcast;
use App\Models\Video;
use App\Services\CanCreateSlug;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class VideoController extends Controller
{
    use CanCreateSlug;

    public function __construct(Video $model)
    {
        $this->model = $model;
        $this->pageslug = 'media';
    }

    public function index(){
        $pageSlug = $this->pageslug;
        $data = $this->model->get();
        return view('admin.videos.index',compact('data','pageSlug'));
    }

    public function preview($id){
        $post = $this->model->find($id);
        $similar = Podcast::where('active',1)->where('slug','!=',$post->slug)->where('series',$post->series)->get();
        return view('pages.podcast-preview',compact('post','similar'));
    }

    public function single($slug){
        $post = Podcast::where('slug', $slug)->first();
        $page = $post->parent;
        $similar = Podcast::where('active',1)->where('slug','!=',$slug)->limit(2)->where('series',$post->series)->get();

        return view('pages.podcast',compact('page','post','similar'));
    }

    public function edit($id){
        $pageSlug = $this->pageslug;
        $data = $this->model->find($id);

        return view('admin.videos.edit',compact('data','pageSlug'));
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

    public function create(){
        $pageSlug = $this->pageslug;
        return view('admin.videos.create',compact('pageSlug'));
    }

    public function store(Request $request){

        $data = $request->except('images','external','buttonLink','others','video_url');
        $data['slug'] = $this->generateSlug($request->input('title'));
        $data['date'] = strtotime($request->input('date'));

//        // UPLOAD AUDIO FILE
//        $audio = $request->file('video_file');
//        $destinationPath = 'uploads/videos';
//        $audioName = Str::random('24').'.'.$audio->getClientOriginalExtension();
//        $audio->move('public/'.$destinationPath,$audioName);
//        // END OF AUDIO UPLOAD
//
//        $data['video_file'] = $destinationPath .'/'. $audioName;
//
//        $files = $request->file();
//
//        if(isset($files['image'])){
//            $file = $files['image'];
//            //Move Uploaded File
//            $destinationPath = 'public/uploads/videos';
//
//            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
//            Image::make($file->getRealPath())->fit(600, 600)->save($destinationPath.'/'.$newFileName);
//            $data['image'] = 'uploads/videos/'. $newFileName;
//        }

        parse_str( parse_url( $request->input('video_url'), PHP_URL_QUERY ), $my_array_of_vars );

        if(!isset($my_array_of_vars['v'])) {
            Session::flash('error','Invalid youtube video URL format');
            return redirect()->back();
        }

        $data['video_file'] = $my_array_of_vars['v'];

        $newPage = $this->model->create($data);

        return redirect()->to('admin/videos/edit/'.$newPage->id);
    }

    public function update(Request $request){

        $page = $this->model->find($request->input('id'));

        if(!$page)
            dd('Page does not exist');

        $data = $request->except('images','id','buttonLink');

        if($data['title'] != $page->title)
            $data['slug'] = $this->generateSlug($request->input('title'));

        $data['date'] = strtotime($request->input('date'));

        // UPLOAD AUDIO FILE
//        $audio = $request->file('video_file');
//
//        if($audio){
//            $destinationPath = 'uploads/videos';
//            $audioName = Str::random('24').'.'.$audio->getClientOriginalExtension();
//            $audio->move('public/'.$destinationPath,$audioName);
//            // END OF AUDIO UPLOAD
//
//            $data['video_file'] = $destinationPath .'/'. $audioName;
//        }
//
//        $files = $request->file();
//
//        if(isset($files['image'])){
//            $file = $files['image'];
//            //Move Uploaded File
//            $destinationPath = 'public/uploads/videos';
//
//            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
//            Image::make($file->getRealPath())->fit(600, 600)->save($destinationPath.'/'.$newFileName);
//            $data['image'] = 'uploads/videos/'. $newFileName;
//        }

        parse_str( parse_url( $request->input('video_url'), PHP_URL_QUERY ), $my_array_of_vars );

        if(!isset($my_array_of_vars['v'])) {
            Session::flash('error','Invalid youtube video URL format');
            return redirect()->back();
        }

        $data['video_file'] = $my_array_of_vars['v'];

        $page->update($data);


        return redirect()->to('admin/videos/edit/'.$page->id);
    }
}
