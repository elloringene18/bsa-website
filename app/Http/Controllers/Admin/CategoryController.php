<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Category;
use App\Services\CanCreateSlug;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    use CanCreateSlug;

    public function __construct(Category $model)
    {
        $this->model = $model;
        $this->pageslug = 'articles';
    }

    public function index(){
        $pageSlug = $this->pageslug;
        $data = $this->model->where('slug','!=','csr-activity')->get();

        return view('admin.categories.index', compact('pageSlug','data'));
    }

    public function create(){
        $pageSlug = $this->pageslug;
        $articles = Article::get();

        return view('admin.categories.create',compact('pageSlug','articles'));
    }

    public function store(Request $request){

        $input = $request->except('_token','articles');

        $exists = Category::where($input)->count();

        if($exists){
            Session::flash('error','This category already exists');
            return redirect()->back();
        }

        $input['slug'] = $this->generateSlug($input['name']);
        $input['cat_id'] = rand(10000,999999);

        $newData = $this->model->create($input);

        if($request->input('articles')){
            $articles = json_decode($request->input('articles'),true);
            foreach ($articles as $item){
                ArticleCategory::create(['article_id' => $item['id'],'category_id' => $newData->cat_id]);
            }
        }

        Session::flash('success','Item saved successfully');
        return redirect('admin/categories/edit/'.$newData->id);
    }

    public function edit($id){
        $pageSlug = $this->pageslug;
        $data = $this->model->find($id);

        if(!$data)
            return 'Post not found!';

        $pageSlug = 'articles';
        $articles = Article::get();

        return view('admin.categories.edit',compact('data','pageSlug','articles'));
    }

    public function update(Request $request){

        $id = $request->input('id');
        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $input = $request->except('_token','articles');

        $exists = Category::where($input)->count();

        if($exists && ($input['name']!=$post->name)){
            Session::flash('error','This category already exists');
            return redirect()->back();
        }

        if($input['name']!=$post->name)
            $input['slug'] = $this->generateSlug($input['name']);

        $post->update($input);

        $articles = json_decode($request->input('articles'),true);

        ArticleCategory::where(['category_id'=>$post->cat_id])->delete();

        foreach ($articles as $item){
            ArticleCategory::create(['article_id' => $item['id'],'category_id' => $post->cat_id]);
        }

        Session::flash('success','Item updated successfully');
        return redirect('admin/categories/edit/'.$post->id);
    }

    public function delete($id){

        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $post->delete();

        Session::flash('success','Item deleted successfully');

        return redirect('admin/categories/');
    }
}
