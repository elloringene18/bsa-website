<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Lawyer;
use App\Models\LawyerFilter;
use App\Models\Location;
use App\Models\Practice;
use App\Models\Service;
use App\Models\ServiceType;
use App\Models\Title;
use App\Services\CanCreateSlug;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LawyerFilterController extends Controller
{
    use CanCreateSlug;

    public function __construct(LawyerFilter $model)
    {
        $this->model = $model;
        $this->pageslug = 'lawyers';
    }

    public function index(){
        $pageSlug = $this->pageslug;

        $filtered = [];
        $data = [];

        if(isset($_GET['title']) || isset($_GET['location']) || isset($_GET['practice'])) {

            $q = Lawyer::query();

            $title = isset($_GET['title']) ? $_GET['title'] : null;
            $location = isset($_GET['location']) ? $_GET['location'] : null;
//            $practice = isset($_GET['practice']) ? $_GET['practice'] : null;
//            $industry = isset($_GET['industry']) ? $_GET['industry'] : null;
            $service = isset($_GET['service']) ? $_GET['service'] : null;

            $f['title_id'] = Title::where('slug',$_GET['title'])->first() ? Title::where('slug',$_GET['title'])->first()->t_id : null;
            $f['service_id'] = Service::where('slug',$_GET['service'])->first() ? Service::where('slug',$_GET['service'])->first()->id : null;
            $f['location_id'] = Location::where('slug',$_GET['location'])->first() ? Location::where('slug',$_GET['location'])->first()->l_id : null;

            $filter = LawyerFilter::where($f)->first();

            if(!$filter) { // NO WITH FILTER ON DATABASE

                if ($title) {
                    if ($title == "partner") {
                        $titles = [
                            'managing-partner',
                            'senior-partner',
                            'partner',
                            'head-of-indirect-tax-and-conveyancing',
                            'head-of-intellectual-property',
                            'head-of-office',
                            'head-of-department',
                            'head-of-spanish-and-latin-american-desk',
                            'head-of-corporate-department',
                            'head-of-corporate-practice',
                            'head-of-insurance',
                            'head-of-insurance',
                            'head-of-abu-dhabi-office',
                            'head-of-sharjah-northern-emirates-offices',
                            'head-of-lebanon-office',
                        ];
                    } elseif ($title == "associate") {
                        $titles = [
                            'associate',
                            'advocate',
                            'head-of-advocacy',
                        ];
                    } else {
                        $titles[0] = $title;
                    }


                    $titles = Title::whereIn('slug', $titles)->orderBy('id', 'ASC')->get();
                    $ids = [];

                    foreach ($titles as $title) {

                        $q = Lawyer::query();

                        $q->whereHas('titles', function ($query) use ($title) {
                            return $query->where('slug', $title->slug);
                        });

                        if ($location) {
                            $q->whereHas('locations', function ($query) use ($location) {
                                return $query->where('slug', $location);
                            });
                        }

                        //                    if($practice){
                        //                        $q->whereHas('practices', function ($query) use($practice) {
                        //                            return $query->where('slug',$practice);
                        //                        });
                        //                    }
                        //
                        //                    if($industry){
                        //                        $q->whereHas('industries', function ($query) use($industry) {
                        //                            return $query->where('slug',$industry);
                        //                        });
                        //                    }

                        if ($service) {
                            $q->whereHas('services', function ($query) use ($service) {
                                return $query->where('slug', $service);
                            });
                        }

                        $n = $q->where('hidden',0)->with('titles', 'practices', 'locations')->orderBy('last_name', 'ASC')->whereNotIn('id', $ids)->get();

                        $data[$title->slug] = $n;

                        foreach ($n as $i) {
                            $ids[] = $i->id;
                        }
                    }

                } else {

                    $ids = [];

//                    if ($service) {
//                        if ($service == 'arbitration-dispute-resolution') {
//                            $data[0][0] = Lawyer::with('titles', 'practices', 'locations')->whereNotIn('id', $ids)->find(10);
//                            $ids[] = 10;
//                        }
//                    }

                    $titles = Title::get();
                    foreach ($titles as $title) {

                        $q = Lawyer::query();

                        $q->whereHas('titles', function ($query) use ($title) {
                            return $query->where('slug', $title->slug);
                        });

                        if ($location) {
                            $q->whereHas('locations', function ($query) use ($location) {
                                return $query->where('slug', $location);
                            });
                        }

                        if ($service) {
                            $q->whereHas('services', function ($query) use ($service) {
                                return $query->where('slug', $service);
                            });
                        }

                        $n = $q->where('hidden',0)->with('titles', 'practices', 'locations')->whereNotIn('id', $ids)->orderBy('last_name', 'ASC')->get();

                        $data[$title->slug] = $n;

                        foreach ($n as $i) {
                            $ids[] = $i->id;
                        }
                    }

                }
                // WITH FILTER ON DATABASE
            } else {
                foreach($filter->items as $item){
                    $lawyer = $item->lawyer;
                    $lawyer->load('titles','practices','locations');

                    if($lawyer->hidden==0)
                        $filtered[] = $lawyer;
                }
            }

        } else {

            $filter = LawyerFilter::whereNull('title_id')->whereNull('service_id')->whereNull('location_id')->first();

            if(!$filter){
                $titles = [
                    'managing-partner',
                    'senior-partner',
                    'partner',
                ];

                $titles = Title::whereIn('slug',$titles)->orderBy('id','ASC')->get();
                $ids = [];

                foreach($titles as $title){

                    $q = Lawyer::query();

                    $q->whereHas('titles', function ($query) use($title){
                        return $query->where('slug',$title->slug);
                    });

                    $n = $q->where('hidden',0)->with('titles','practices','locations')->orderBy('last_name','ASC')->whereNotIn('id',$ids)->get();

                    $data[$title->slug] = $n;

                    foreach($n as $i){
                        $ids[] = $i->id;
                    }
                }
            } else {
                foreach($filter->items as $item){
                    $lawyer = $item->lawyer;
                    $lawyer->load('titles','practices','locations');

                    if($lawyer->hidden==0)
                        $filtered[] = $lawyer;
                }

            }
        }

        $titles = Title::get();
        $locations = Location::get();
        $practices = Practice::get();
        $lawyers = Lawyer::where('hidden',0)->get();

        $serviceTypes = ServiceType::get();
        $types = [];

        foreach ($serviceTypes as $type){
            $types[$type->name] = Service::where('service_type_id',$type->id)->select('slug','title','id')->get();
        }

        $currentFilter = $filter;

        return view('admin.lawyers.filters.editAll',compact('pageSlug','titles','types','locations','lawyers','data','filtered','currentFilter'));
    }

    public function create(){
        $pageSlug = $this->pageslug;
        $titles = Title::get();
        $locations = Location::get();
        $services = Service::get();
        $lawyers = Lawyer::get();

        $serviceTypes = ServiceType::get();
        $types = [];

        foreach ($serviceTypes as $type){
            $types[$type->name] = Service::where('service_type_id',$type->id)->select('slug','title','id')->get();
        }

        return view('admin.lawyers.filters.create',compact('pageSlug','titles','types','locations','lawyers'));
    }

    public function store(Request $request){

        $input = $request->except('_token','contacts');

        $exists = LawyerFilter::where($input)->count();

        if($exists){
            Session::flash('error','This filter already exists');
            return redirect()->back();
        }

        $newData = $this->model->create($input);

        $contacts = json_decode($request->input('contacts'),true);

        if(is_array($contacts)){
            if(count($contacts)){
                foreach ($contacts as $item){
                    $newData->items()->create(['lawyer_id' => $item['id']]);
                }
            }
        }

        Session::flash('success','Item saved successfully');
        return redirect('admin/lawyers/filters/edit/'.$newData->id);
    }

    public function edit($id){
        $pageSlug = $this->pageslug;
        $data = $this->model->find($id);

        if(!$data)
            return 'Post not found!';

        $pageSlug = 'lawyers';
        $titles = Title::get();
        $locations = Location::get();
        $services = Service::get();
        $lawyers = Lawyer::get();

        $serviceTypes = ServiceType::get();
        $types = [];

        foreach ($serviceTypes as $type){
            $types[$type->name] = Service::where('service_type_id',$type->id)->select('slug','title','id')->get();
        }

        return view('admin.lawyers.filters.edit',compact('data','pageSlug','titles','types','locations','lawyers'));
    }

    public function update(Request $request){

        $id = $request->input('id');

        if($id){
            $filter = $this->model->find($id);
        }
        else {
            $t = Title::where('slug',$request->input('title'))->first();
            $s = Service::where('slug',$request->input('service'))->first();
            $l = Location::where('slug',$request->input('location'))->first();

            $filter['title_id'] = $t ? $t->t_id : null;
            $filter['service_id'] = $s ? $s->id : null;
            $filter['location_id'] = $l ? $l->l_id : null;
            $filter = $this->model->create($filter);
        }

        $input = $request->except('_token','contacts','id');

        $contacts = json_decode($request->input('contacts'),true);

        $filter->items()->delete();

        if(is_array($contacts)){
            if(count($contacts)){
                foreach ($contacts as $item){
                    $filter->items()->create(['lawyer_id' => $item['id']]);
                }
            }
        }

        Session::flash('success','Item updated successfully');

        if($request->input('service') || $request->input('title') || $request->input('location'))
            return redirect('admin/lawyers/filters?title='.$request->input('title').'&service='.$request->input('service').'&location='.$request->input('location'));
        else
            return redirect('admin/lawyers/filters');
    }

    public function delete($id){

        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $post->delete();

        Session::flash('success','Item deleted successfully');

        return redirect('admin/lawyers/filters/');
    }
}
