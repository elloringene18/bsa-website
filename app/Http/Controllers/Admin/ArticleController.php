<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ArticleCategory;
use App\Models\ArticleLawyer;
use App\Models\ArticlePromo;
use App\Models\ArticleRelated;
use App\Models\ArticleVersion;
use App\Models\Category;
use App\Models\Lawyer;
use App\Models\LawyerTitle;
use App\Models\Location;
use App\Models\PageSection;
use App\Models\Promo;
use App\Models\Service;
use App\Models\Tag;
use App\Models\Title;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Article;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use App\Services\CanCreateSlug;

class ArticleController extends Controller
{
    use CanCreateSlug;

    public function __construct(Article $model)
    {
        $this->model = $model;
        $this->pageslug = 'articles';
    }

    public function cmetatags(){
        $articles = Article::whereNotNull('meta_title')->orWhereNotNull('meta_description')->get();
        $services = Service::whereNotNull('meta_title')->orWhereNotNull('meta_description')->get();
        $lawyers = Lawyer::whereNotNull('meta_title')->orWhereNotNull('meta_description')->get();

        $pages['home']['meta_title'] = PageSection::where('page','home')->where('slug','meta-title')->first()->content;
        $pages['home']['meta_description'] = PageSection::where('page','home')->where('slug','meta-description')->first()->content;

        $pages['about']['meta_title'] = PageSection::where('page','about')->where('slug','meta-title')->first()->content;
        $pages['about']['meta_description'] = PageSection::where('page','about')->where('slug','meta-description')->first()->content;

        $pages['locations']['meta_title'] = PageSection::where('page','locations')->where('slug','meta-title')->first()->content;
        $pages['locations']['meta_description'] = PageSection::where('page','locations')->where('slug','meta-description')->first()->content;

        $pages['our-people']['meta_title'] = PageSection::where('page','our-people')->where('slug','meta-title')->first()->content;
        $pages['our-people']['meta_description'] = PageSection::where('page','our-people')->where('slug','meta-description')->first()->content;

        $pages['careers']['meta_title'] = PageSection::where('page','careers')->where('slug','meta-title')->first()->content;
        $pages['careers']['meta_description'] = PageSection::where('page','careers')->where('slug','meta-description')->first()->content;

        $pages['contact']['meta_title'] = PageSection::where('page','contact')->where('slug','meta-title')->first()->content;
        $pages['contact']['meta_description'] = PageSection::where('page','contact')->where('slug','meta-description')->first()->content;

//        echo '<h1>Articles</h1>';
        foreach($articles as $item){
            echo '<h3 style="color: red">'.$item->title.'</h3>';
            echo '<p>Meta Title: '.$item->meta_title.'</p>';
            echo '<p>Meta Description: '.$item->meta_description.'</p>';
        }

//        echo '<hr/>';
//        echo '<h1>Services</h1>';
        foreach($services as $item){
            echo '<h3 style="color: red">'.$item->title.'</h3>';
            echo '<p>Meta Title: '.$item->meta_title.'</p>';
            echo '<p>Meta Description: '.$item->meta_description.'</p>';
        }

//        echo '<hr/>';
//        echo '<h1>Lawyers</h1>';
        foreach($lawyers as $item){
            echo '<h3 style="color: red">'.$item->name.'</h3>';
            echo '<p>Meta Title: '.$item->meta_title.'</p>';
            echo '<p>Meta Description: '.$item->meta_description.'</p>';
        }
//
//        echo '<hr/>';
//        echo '<h1>Pages</h1>';
        foreach($pages as $key=>$item){
            echo '<h3 style="color: red">'.$key.'</h3>';
            echo '<p>Meta Title: '.$item['meta_title'].'</p>';
            echo '<p>Meta Description: '.$item['meta_description'].'</p>';
        }
    }

    public function metatags(){

        $practices = array(
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Arbitration and Dispute Resolution Law Firm in Dubai | BSA','post_name' => 'arbitration-dispute-resolution'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Experienced Banking & Finance Law Firm in Dubai | BSA','post_name' => 'banking-finance-law'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Experienced Construction Law Firm in Dubai | BSA','post_name' => 'construction-law'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Employment Law Firm in Dubai | BSA, A Middle East Law Firm','post_name' => 'employment-law-firm'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Energy Law Firm in Dubai I BSA, A Middle East Law Firm','post_name' => 'energy-law'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Litigation Law Firm in Dubai | BSA, A Middle East Law Firm','post_name' => 'litigation-law-firm'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Expo 2020 Law Firm | Lawyers for Expo 2020 | BSA lawyers','post_name' => 'expo-2020'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Restructuring & Bankruptcy Law Firm in Dubai | BSA, A Middle East Law Firm','post_name' => 'restructuring-bankruptcy'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Cybersecurity Law Firm in Dubai | Cybersecurity Lawyers | BSA','post_name' => 'cybersecurity'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'VAT Implementation in Dubai I BSA, A Middle East Law Firm','post_name' => 'vat-implementation'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Technology, Media, and TMT Law Firm in Dubai I BSA, A Middle East Law Firm','post_name' => 'technology-media-and-telecommunications'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Screening and Due Diligence Law Firm in Dubai I BSA, A Middle East Law Firm','post_name' => 'screening-and-due-diligence'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Real Estate Law Firm in Dubai | BSA, A Middle East Law Firm','post_name' => 'real-estate-law'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Intellectual Property Law Firm in Dubai | BSA, A Middle East Law Firm','post_name' => 'intellectual-property-law'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Insurance & Reinsurance Law Firm in Dubai | BSA, A Middle East Law Firm','post_name' => 'insurance-reinsurance-law'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Corporate Law and M&A Firm in Dubai | BSA, A Middle East Law Firm','post_name' => 'corporate-law-and-ma'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Commercial Law Firm in Dubai I BSA','post_name' => 'commercial-law'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Maritime Law Firm in Dubai | BSA, A Middle East Law Firm','post_name' => 'international-trade-transport-and-maritime '),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Cybersecurity Law Firm in Dubai I BSA, A Middle East Law Firm','post_name' => 'cybersecurity'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Working closely with our Construction, Litigation and Banking and Finance Departments, our real estate attorneys offer a full range of legal services on local and regional property development.','post_name' => 'real-estate-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Our team of highly qualified litigation lawyers collectively speaks 16 languages and has rights of audience in all courts including the English Law-based DIFC Courts.','post_name' => 'litigation-law-firm'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With years of experience acting for clients in some of the region’s most important IP cases, we can ensure your interests are protected.','post_name' => 'intellectual-property-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With years of experience acting for clients in some of the region’s most important IP cases, we can ensure your interests are protected.','post_name' => 'insurance-reinsurance-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Middle East law firm BSA has top energy lawyers in Dubai, UAE, Saudi Arabia, Oman, Lebanon, Iraq.','post_name' => 'energy-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA can help you navigate every aspect of employment, labour and HR as well as providing related services such as due diligence, corporate support and litigation.','post_name' => 'employment-law-firm'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Cybersecurity is an omnipresent concern for all companies and organizations. In our increasingly interconnected and tech-driven world, businesses of all sizes are vulnerable to attacks from cybercriminals.','post_name' => 'cybersecurity'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With an experienced team that has worked on some of the most dynamic corporate law deals in the Middle East, we can help guide you through all aspects of corporate law.','post_name' => 'corporate-law-and-ma'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With years of experience working to protect our clients in this difficult regulatory area, we can provide our you with legally watertight and local law-compliant contractual arrangements.','post_name' => 'construction-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Benefitting from close working relationships with local authorities and regulatory bodies across the region, we provide innovative, commercially focused solutions.','post_name' => 'commercial-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Our specialist lawyers are highly skilled in contract law, drafting syndicated loans and security documentation as well as structuring and advising on investment funds.','post_name' => 'banking-finance-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With years of experience working on some of the most important regional and international arbitration cases, we can help you successfully navigate this area of law.','post_name' => 'arbitration-dispute-resolution'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'We help people do business all over the Middle East. As a Middle East law firm, we can help you negotiate the maze of regional regulations and ensure your business interests are protected at all times','post_name' => 'legal-practice-areas'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Our team of highly qualified litigation lawyers collectively speaks 16 languages and has rights of audience in all courts including the English Law-based DIFC Courts.','post_name' => 'international-trade-transport-and-maritime '),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'A family business is any business in which two or more members of the same family are involved and most of the ownership or control lies within such a family.','post_name' => 'family-business-regulations-in-the-uae'),
        );

        $lawyers = array(
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Yad is an Associate in the Corporate and M&A Practice in our Erbil office in Iraq. He has over eight years’ experience and specialises in taxation, corporate transactions, mergers and acquisitions, commercial matters, employment claims and contract review.','post_name' => 'yad-ezzuldin-dezay'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Swati is an Associate in the Litigation Practice in our DIFC office in Dubai. Fluent in English, Hindi and Panjabi, her expertise includes white collar crimes, bank frauds, bounced cheques, prevention of money laundering, criminal defamation, and matrimonial disputes.','post_name' => 'swati-soni'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Simon is a Partner and Head of Insurance at our Dubai office. Previously Head of Kennedys’ Corporate and Regulatory Insurance MENA practice, he is a qualified barrister who was called to the Bar in 1998, practising commercial and company law matters in the English courts.','post_name' => 'simon-isgar'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Shaaban is a Partner in the Litigation Practice in the DIFC office in Dubai. Practicing since 1991, he specialises in various types of litigation and dispute resolution including insurance and reinsurance, debt recovery, maritime, commercial, real estate.','post_name' => 'shaaban-metwally'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Salman is an Associate in the Corporate and M&A Practice in our DIFC office in Dubai. At BSA since 2014, he specialises in incorporating companies and has advised multinational clients on corporate structure.','post_name' => 'salman-madkour'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Robert is a Senior Associate with the Corporate, M&A and Real Estate Practices in our DIFC office in Dubai.','post_name' => 'robert-mitchley'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Rima is a Partner with the Corporate and M&A Practice in our DIFC offices in Dubai. She is an experienced corporate and insurance lawyer who has practiced in the UAE for over fourteen years.','post_name' => 'rima-mrad'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Ralph is a Senior Associate in the corporate practice based in the Sultanate of Oman (Muscat).','post_name' => 'ralph-hejaily'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Omar is an Associate in the Litigation Practice in our Muscat office in the Sultanate of Oman.','post_name' => 'omar-alkharoosi'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Nour is an Associate in the Insurance & Reinsurance Practice based in our DIFC office in Dubai. Fluent in English, Arabic and French, she represents insurance and reinsurance companies.','post_name' => 'nour-gemayel'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Nadia is an Associate within the Insurance Practice, based in Dubai.','post_name' => 'nadia-el-tannir'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Nadim is a Senior Associate both in our Corporate and TMT practices in our DIFC office in Dubai. He specialises in transactional corporate work across various industries including media, technology and healthcare.','post_name' => 'nadim-bardawil'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Munir joined BSA in 2018 as a Partner and Head of the Intellectual Property Practice in our Dubai office. Practicing in the Middle East since 2006, he has worked on numerous contentious and non-contentious IP works including trademarks, trade names, copyrights, patents, trade secrets and domain names.','post_name' => 'munir-abdallah-suboh'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Mundhir is a Partner in the Muscat office in the Sultanate of Oman. His experience covers high profile insurance and banking cases, in which he has acted for major international insurance and financial institutions.','post_name' => 'mundhir-albarwani'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Mundhir is an Associate in the Litigation Practice in our Muscat office in the Sultanate of Oman. A qualified Omani lawyer, he specialises in litigation and other forms of dispute resolution, with a particular expertise in civil, commercial, employment law and debt recovery.','post_name' => 'mundhir-al-rasbi'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Moustafa is a Litigation Associate in our DIFC office in Dubai. Practicing since 2003, he handles litigation cases before the Dubai Courts and Federal Courts, executing both registration and follow-up of cases for clients.','post_name' => 'moustafa-arfa'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The Head of our Sharjah and Northern Emirates offices. Mohammed specialises in arbitration, litigation and dispute resolution.','post_name' => 'mohd-nedal-dajani'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Mohammed is an Associate in the Litigation Practice in our Muscat office in the Sultanate of Oman. With expertise including commercial, civil, administrative and employment law, he represents clients in real estate, criminal, family and construction cases.','post_name' => 'mohammed-alghazali'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Mohammed is an Associate in the Corporate and M&A team in our DIFC office in Dubai. He advises private clients, real estate and property developers and organisations from the retail, lifestyle, leisure and hospitality sectors.','post_name' => 'mohammed-alahdal'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Michael is a Partner in the Corporate and M&A Practice in our DIFC offices in Dubai. With a strong focus on insurance and reinsurance, he has gained hands-on experience in the Middle East and North Africa region.','post_name' => 'michael-kortbawi'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Mahmoud is an Associate with the Litigation Practice in our DIFC office in Dubai. Practicing since 2005, he regularly advises financial institutions and private clients on various types of litigation and dispute resolution','post_name' => 'mahmoud-elgousi'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Lara is a Partner in the Corporate and Commercial Practice in our DIFC/ Dubai office in the UAE, specialising in energy matters','post_name' => 'lara-barbary'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Khulood is an Associate in the Litigation practice in our Muscat office in the Sultanate of Oman. Fluent in Arabic and English, she is qualified to draft legal documents and provide legal advice to both the public and private sectors.','post_name' => 'khulood-al-wahaibi'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Khalid is an Advocate in the Litigation Practice in our DIFC office in Dubai. With over 22 years’ experience, he has full rights of audience before the UAE Courts.','post_name' => 'khalid-bujsaim'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Khaled is an Associate in the Litigation practice in our DIFC office in Dubai. With over five years’ litigation experience practicing law in Egypt and the UAE, he specialises in employment disputes, commercial disputes, rental disputes and criminal cases.','post_name' => 'khaled-elgohari'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Jonathan Brown is Of Counsel within the Arbitration practice based in Dubai. Jonathan qualified as a Solicitor in 1991 and worked in the City of London before coming to Dubai in 1998 to head up the Contentious Business Practice of Clifford Chance.','post_name' => 'jonathan-brown'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Johnny is a Partner in our Lebanon office. She has a full range of legal services expertise in a variety of matters ranging from penal and civil law to commercial and business law, general corporate work, mergers and acquisitions and structuring foreign investments.','post_name' => 'johnny-el-hachem'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Based in Dubai, Jimmy is the Managing Partner of the firm. He specialises in corporate, commercial and real estate transactions and has almost two decades of experience in the region.','post_name' => 'jimmy-haoula'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Jean Abboud is a Senior Associate based in our Riyadh office with 9 years’ experience. He specializes in corporate and commercial law (foreign investment), securities, telecom, regulatory and corporate matters as well as projects.','post_name' => 'jean-abboud'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Hassan is an Associate with the Litigation Practice in our DIFC office in Dubai.','post_name' => 'hassan-el-shahat-mohamed'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Hassan is an Associate in the Litigation Practice in our DIFC office in Dubai. He specialises in various types of litigation, criminal cases and rent issues.','post_name' => 'hassan-el-agawani'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Haitham is an Associate in the Litigation Practice in our Muscat office in the Sultanate of Oman. As an Omani-qualified lawyer, he has rights of audience in civil, commercial and administrative courts.','post_name' => 'haitham-al-naabi'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Hadiel is an Associate with the Litigation Practice in our DIFC office in Dubai. With an undergraduate degree from the prestigious University of Sorbonne in Paris, Hadiel is involved in all matters related to dispute resolution.','post_name' => 'hadiel-hussien'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Habib is a Senior Associate with the Litigation Practice in our Abu Dhabi office. Practising in the UAE since 2000, he specialises in several types of litigation cases including civil, criminal, commercial and Sharia law.','post_name' => 'habib-al-hadi'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Felicity joined BSA Ahmad Bin Hezeem & Associates LLP as an Associate earlier this year in the Intellectual Property Department.','post_name' => 'felicity-hammond'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Elhag is an Associate in the Litigation team in our Abu Dhabi office. A member of the Sudan Bar Association since 1988, he has extensive knowledge of the legal systems in both the UAE and Sudan.','post_name' => 'elhag-mohamed-ali'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Dr. Ahmad is the Senior Partner in our Dubai office. He has over 24 years’ experience working in legal and judicial governmental institutions in Dubai.','post_name' => 'dr-ahmad-bin-hezeem-2'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Barry is a Senior Associate in the Restructuring & Bankruptcy, Corporate, and Insurance and Reinsurance practices in our DIFC office in Dubai.','post_name' => 'barry-greenberg'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Asim is a Partner and the Head of the Litigation practice in our Dubai office.','post_name' => 'asim-ahmed'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Ashraf is a Senior Associate in the Litigation practice in our Dubai office. With nearly 30 years’ experience practicing law, he joined BSA in 2017 and currently represents governments and high net worth individuals.','post_name' => 'ashraf-ibrahim'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Asma is an Associate in our Litigation practice, based in our DIFC office in Dubai. She represents corporate bodies and individuals and specialises in various litigation and alternative dispute resolution sectors.','post_name' => 'asa-siddiqui'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Arsalan is a Partner based in the Oman office. Practicing since 2009, Arsalan specializes in Islamic and conventional finance, bilateral and syndicated project finance, and acquisition finance.','post_name' => 'arsalan-tariq'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Antonios is a Partner, and the Head of Arbitration & Dispute Resolution, and Construction practices, based in our DIFC office in Dubai.','post_name' => 'antonios-dimitracopoulos'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Andrée practices as Government Relations Counsel from our DIFC offices in Dubai. As a qualified English solicitor, she has over twelve years’ experience, ten of which have been in Dubai, in private practice and in-house governmental roles.','post_name' => 'andree-hobson'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Anand is a Senior Associate in the Insurance & Reinsurance Practice based in our DIFC office in Dubai. With over 8 years of experience as a corporate insurance and regulatory lawyer having practiced in India and across the Middle East.','post_name' => 'anand-singh'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Amal assisted international and domestic Brand Owners in devising and implementing tailored strategies for trademark protection in the MENA region, with regard to prosecution, enforcement and litigation.','post_name' => 'amal-atieh'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Ali is an Associate in the Litigation practice based in our Abu Dhabi office. He joined BSA in 2013 and has over seven years’ legal experience gained from practicing law both in Egypt and the UAE.','post_name' => 'ali-abdelhalim'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Alaaeldin is an Associate in the Litigation Practice based in our DIFC office in Dubai. He represents clients in criminal litigation proceedings, civil disputes, rental disputes and employment matters.','post_name' => 'alaaeldin-ghoneim'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Akram is the Head of our Abu Dhabi office. An advocate with extensive experience in litigation, his diverse expertise includes advising clients on real estate matters, commercial issues and corporate disputes.','post_name' => 'akram-albaiti'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Ahmed is a Senior Associate in our Oman office, with over twenty years’ experience in corporate/commercial matters, litigious matters and arbitration.','post_name' => 'ahmed-al-taher-2'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Abdullah is a Partner in our Litigation practice based in our DIFC office in Dubai. Practicing since 2005, he specialises in various types of litigation and dispute resolution including medical malpractice, employment and debt recovery.','post_name' => 'abdullah-ishnaneh'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Abdelmuneem is a Senior Associate in our Muscat office. With over 20 years’ experience, he has worked on some of the biggest commercial, insurance, banking and dispute resolution matters in the Sultanate of Oman.','post_name' => 'abdul-moneem-al-rafei'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Abdulaziz is a Partner in our Muscat office. Experienced in litigation and dispute resolution, his expertise encompasses a wide range of industries including commercial, civil and administrative disputes.','post_name' => 'abdul-aziz-alrashdi'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Abdalla is an advocate based in Dubai. Abdalla has built a solid experience in different areas of litigation in the UAE and has the right of audience before the local courts.','post_name' => 'abdalla-alsuwaidi'),
        );

        $posts = array(
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Middle East law firm BSA can provide legal services to protect your interests during Expo 2020 in Dubai.','post_name' => 'expo-2020'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Specialist bankruptcy lawyers at Middle East law firm BSA expertly guide individuals and businesses through the ever-evolving insolvency regulations across the region.','post_name' => 'restructuring-bankruptcy'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'We help people do business all over the Middle East. We have offices in Dubai, UAE, Saudi Arabia, Oman, Lebanon and Iraq.','post_name' => 'sample-page-4'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Professionals from medicine, education and technology will be major beneficiaries of the UAE’s 10-year visa policy, which will come into effect in 2019, say legal experts.','post_name' => '10-year-visa-to-investors-skilled-professional-will-go-a-long-way-to-boost-investor-confidence'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The UAE cabinet has approved 122 economic activities across 13 sectors that will be eligible for up to 100% foreign ownership.','post_name' => '100-foreign-ownership-for-13-sectors-in-the-uae'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The maritime sector has always been significant to the growth of the UAE economy over the decades. This has been most apparent in Dubai where the creation of Jebel Ali Port and the Dubai Dry Docks.','post_name' => '100-foreign-ownership-for-the-maritime-sector'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With an international audience of more than 400 industry leaders, policymakers, academics and the global media assembled at the conference.','post_name' => '10th-annual-world-takaful-conference'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Saudi authorities imposed a 24-hour curfew in most Saudi cities, the capital Riyadh as well as in Jeddah, Dammam, Al-Khobar, Tabuk, Dhahran, Al-Hofuf, Ta’if, Al-Qatif.','post_name' => '24-hour-curfew-in-major-saudi-cities-announced'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The summit has speakers from local and international firms sharing their views and insights on Arbitration, ADR and Mediation from different regions.','post_name' => '2nd-annual-international-arbitration-summit'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'One of the most important points arising from the recent briefings on VAT by the Ministry of Finance isthe proposed rules regarding “group registrations”.','post_name' => 'how-vat-legislation-will-change-the-business-set-up-model-in-the-uae'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The conference programme looked at transforming insurance and re-Insurance strategies, enhance distribution channel strategies, learn the latest product innovations to help grow businesses.','post_name' => '4th-annual-middle-east-africa-insurance-summit'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The article examines the material legal process involved when undertaking a corporate acquisition transaction in Saudi Arabia (Acquisition Transaction).','post_name' => 'introduction-to-company-acquisitions-in-kingdom-of-saudi-arabia-business-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA Ahmad Bin Hezeem & Associates LLP seeks sustainable growth through the recruitment of talented lawyers, support staff and other professionals.','post_name' => 'careers'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'We help people do business all over the Middle East. We have offices in Dubai, UAE, Saudi Arabia, Oman, Lebanon and Iraq.','post_name' => 'about-us'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA can carry out due diligence to ensure your business is operating efficiently under the new Middle East VAT laws, advising on everything from basic procedures to filing your tax return.','post_name' => 'vat-implementation'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With a broad range of clients including technology vendors and public and private sector technology buyers, we can help you with every aspect of technology law in the Middle East.','post_name' => 'technology-media-and-telecommunications'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'New people represent a risk to every business. Whether you’re hiring staff or considering any kind of commercial partnership, working with the wrong people can cost you time, money, and even your company’s reputation.','post_name' => 'screening-and-due-diligence'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Working closely with our Construction, Litigation and Banking and Finance Departments, our real estate attorneys offer a full range of legal services on local and regional property development.','post_name' => 'real-estate-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Our team of highly qualified litigation lawyers collectively speaks 16 languages and has rights of audience in all courts including the English Law-based DIFC Courts.','post_name' => 'litigation-law-firm'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With years of experience acting for clients in some of the region’s most important IP cases, we can ensure your interests are protected.','post_name' => 'intellectual-property-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With years of experience acting for clients in some of the region’s most important IP cases, we can ensure your interests are protected.','post_name' => 'insurance-reinsurance-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Middle East law firm BSA has top energy lawyers in Dubai, UAE, Saudi Arabia, Oman, Lebanon, Iraq.','post_name' => 'energy-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA can help you navigate every aspect of employment, labour and HR as well as providing related services such as due diligence, corporate support and litigation.','post_name' => 'employment-law-firm'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Middle East lawyers BSA Law are authorized DIFC Corporate Service Providers for Special Purpose Companies operating in Dubai.','post_name' => 'difc-corporate-service-providers'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Cybersecurity is an omnipresent concern for all companies and organizations. In our increasingly interconnected and tech-driven world, businesses of all sizes are vulnerable to attacks from cybercriminals.','post_name' => 'cybersecurity'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With an experienced team that has worked on some of the most dynamic corporate law deals in the Middle East, we can help guide you through all aspects of corporate law.','post_name' => 'corporate-law-and-ma'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With years of experience working to protect our clients in this difficult regulatory area, we can provide our you with legally watertight and local law-compliant contractual arrangements.','post_name' => 'construction-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Benefitting from close working relationships with local authorities and regulatory bodies across the region, we provide innovative, commercially focused solutions.','post_name' => 'commercial-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Our specialist lawyers are highly skilled in contract law, drafting syndicated loans and security documentation as well as structuring and advising on investment funds.','post_name' => 'banking-finance-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With years of experience working on some of the most important regional and international arbitration cases, we can help you successfully navigate this area of law.','post_name' => 'arbitration-dispute-resolution'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'We help people do business all over the Middle East. As a Middle East law firm, we can help you negotiate the maze of regional regulations and ensure your business interests are protected at all times','post_name' => 'legal-practice-areas'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'When people criticise Dubai and the UAE, they often make sweeping statements about labour conditions and worker’s rights, but the UAE Labour Law actually protects workers in many ways.','post_name' => '7-things-you-might-not-realise-are-against-the-uaes-labour-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The conference was concluded by ceremonies honouring the winners of the 12th Middle East CEO Excellence Awards and 4th Middle East Future Leaders Excellence Awards.','post_name' => '5th-uae-government-future-leaders-conference'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The conference programme looked at updates in the Middle East insurance sector covering regulations and market changes, the compulsory health insurance in Dubai, UAE’s role as a financial hub for the insurance industry.','post_name' => '5th-annual-middle-east-africa-insurance-summit'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The United Arab Emirates has a complex sanctions regime based on a variety of sources. Sanctions are based on diverse interests, including political, economic and national security interests.','post_name' => 'uae-chapter-on-sanctions'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA Bin Shabib, Al Rashdi & Al Barwani (BSA), and Zubair Small Enterprises Centre (Zubair SEC) recently completed a workshop and legal clinic on January 15 at Bait Al Zubair.','post_name' => 'bsa-and-zubair-sec-successfully-concludes-legal-clinic-and-workshop-for-smes'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA Bin Shabib, Al Rashdi & Al Barwani (BSA), and Zubair Small Enterprises Centre (Zubair SEC) will jointly hold a workshop and legal clinic on January 15, 2019.','post_name' => 'bsa-in-collaboration-with-zubair-sec-to-hold-workshop-and-legal-clinic-for-smes'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'For legal advice on anything related to COVID-19, visit the Coronavirus Insight Centre from Middle East lawyers BSA Law or contact their experienced lawyers today.','post_name' => 'coronavirus_insight_centre'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'fractional-title-deed'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'beirut-blast-insurance'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => '100-foreign-ownership-ras-al-khaimah'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'DIFC Litigation is becoming a more popular choice for Companies and Individuals alike. It is fast becoming a preferred option for international companies who prefer a more familiar common law based system but don’t want to pay for expensive arbitration.','post_name' => 'difc-litigation'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA Ahmad Bin Hezeem & Associates LLP seeks sustainable growth through the recruitment of talented lawyers, support staff and other professionals.','post_name' => 'privacy-policy'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'We help people do business all over the Middle East. We have offices in Dubai, UAE, Saudi Arabia, Oman, Lebanon and Iraq.','post_name' => 'sample-page-2'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Our team of highly qualified litigation lawyers collectively speaks 16 languages and has rights of audience in all courts including the English Law-based DIFC Courts.','post_name' => 'maritime'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'We help people do business all over the Middle East. We have offices in Dubai, UAE, Saudi Arabia, Oman, Lebanon and Iraq.','post_name' => 'sample-page-3'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'We help people do business all over the Middle East. We have offices in Dubai, UAE, Saudi Arabia, Oman, Lebanon and Iraq.','post_name' => 'sample-page'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'uae-insurance-regulatory-round-up'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'We help people do business all over the Middle East. We have offices in Dubai, UAE, Saudi Arabia, Oman, Lebanon and Iraq.','post_name' => 'sample-vid'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'new-developments-foreign-direct-investment-in-the-uae'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'clarified-legal-landscape-for-ksa-offers-investors-potential-new-playground'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'recent-legal-developments-in-business-interruption-insurance-coverage-issues-and-wider-implications-perhaps-for-the-middle-east'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'real-estate-crowdfunding-as-potential-opportunity-for-investors-and-entrepreneurs'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'new-developments-foreign-direct-investment-in-the-uae'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'alcohol-and-trademarks-practice-in-uae'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'saudi-central-bank-launches-open-banking-initiative'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'dubai-art-scene-how-to-improve-the-picture'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'nuts-and-bolts-of-decision-no-26-of-2020-covering-small-claims-tribunals'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'draft-insurance-broker-regulations-what-could-change'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'uae-vat-laws-and-export-of-services'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'the-end-of-the-gcc-patent-what-next'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'dubai-rent-freeze'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'new-sme-market-conduct-regulation'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'pay-when-paid-clause-can-be-defeated'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'update-saudi-companies-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'A family business is any business in which two or more members of the same family are involved and most of the ownership or control lies within such a family.','post_name' => 'family-business-regulations-in-the-uae'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'On 13 June 2021 the UAE Central Bank issued a circular with reference no CBUAE/BSD/N/2021/3148 (“the Circular”) addressed to all financial institutions operating in the UAE and offering structured conventional life insurance and takaful investment and saving products (“the Products”). The Circular refers to Central Bank’s Ccircular of May 2017 which was addressed to banks and finance companies and which stopped such organizations from offering savings and investment, and non-capital guaranteed products, until the Central Bank issues a governance policy around the issuance of such products. The Circular Central Bank now provides that subject to a Licensed Financial Institution (“LFI”) fulfilling the requirements laid down by Central bBank, and after having obtained a no objection letter from the Central Bank, they these institutions can resume selling such Products.','post_name' => 'revised-central-bank-guidance-regarding-saving-and-investment-insurance'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA Ahmad Bin Hezeem & Associates is proud to have been named Regional Law Firm of the Year at the annual Law.com Legal Week Middle East Legal Awards.BSA is noted as having "an impressive regional footprint in the Middle East, which offers a full service to its clients, delivering excellence each time." The judges took into consideration our impressive flagship deals from the past 12 months','post_name' => 'bsa-named-regional-law-firm-of-the-year'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The Government of Oman has introduced new laws to promote the economy and to ensure minimal disruption to businesses during these difficult times. Many business owners and their in-house legal teams seek relief and protection from the implications of coronavirus. The recent measures issued by Omani authorities are an effort to ensure that business goes on as usual.','post_name' => 'new-laws-agile-teams'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'UAE has a new data law. Check out our article to see the newest additions and to be posted on the effects of this law.','post_name' => 'uae-project-of-the-50-new-data-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'This article looks at the concept of healthcare and medical trusts, as well as the UAE Trusts Law, focusing on medical insurers','post_name' => 'the-concept-of-healthcare-medical-trust'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The 3rd annual motor insurance conference, hosted by Emirates Insurance Association. Industry speakers will discuss \'Fair vs Aggressive Competition\'.','post_name' => 'emirates-insurance-association-third-annual-motor-conference'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Webinar discussing anti money laundering regulations and the impact on the UAE insurance industry, in association with Premium Insurance Magazine','post_name' => 'money-laundering-regulations'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'A seminar on restructuring & insolvency in the UAE, in association with the Oath Legal Magazine, focusing on the UAE Bankruptcy Law.','post_name' => 'insolvency-restructuring-in-the-uae-practical-considerations-for-corporates'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Emirates Maritime Arbitration Centre and DIFC Arbitration Institute merge into Dubai International Arbitration Centre as announced by HH Sheikh Mohammed','post_name' => 'emirates-maritime-arbitration-centre-and-difc-arbitration-institute-merge-into-dubai-international-arbitration-centre'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'A closer look at the UAE Health Data Law and how these impact the protection and Processing of the healthcare sectos\'s data and information','post_name' => 'the-new-uae-health-data-law-an-in-depth-look'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The following article discusses the implications of the Dubai Courts\' specialised money laundering court, as originally published in Arabian Business','post_name' => 'dubais-recently-established-money-laundering-court-explained'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Ahead of Expo 2020, we discuss all things related to renewable energy in the Middle East, including fossil fuels, wind, solar and hydrogen','post_name' => 'the-middle-east-plugged-into-renewable-energy'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'When can a landlord increase a tenant\'s rent or serve an eviction notice in the UAE? This article discusses a tenant\'s rights under the law.','post_name' => 'rent-increases-and-eviction-notices-what-are-your-rights'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The BSA Insurance Practice provide the UAE Chapter Chambers & Partners of the Insurance Litigation 2021 Guide, with an in-depth look at insurance ADR','post_name' => 'uae-insurance-litigation-2021'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Want to know your rights as a tenant in Dubai? What landlords can and can\'t do? Hazem Balbaa provides expert legal advisor for tenants renting in Dubai','post_name' => 'renting-in-dubai-here-are-your-rights-as-a-tenant'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Changes to the DIFC Employment Law serve to clarify ambiguities regarding the existing law and will increase the financial centre\'s attractiveness.','post_name' => 'difc-employment-law-new-amendments-explained'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Central Bank Digital Currencies (“CBDC") are a new form of digital currencies. Associate Omar Al Masri discusses CBDC\'s with Gulf Business','post_name' => 'central-bank-digital-currencies'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA will be hosting aspecial breakfast event at Dubai Expo 2020: \'The Future of FinTech: Bridging the divide between the unbanked and traditional finance.','post_name' => 'the-future-of-fintech'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'One of the most daunting prospects for an MEP sub-contractor is the possibility of being held ransom to a pay-when-paid clause.','post_name' => 'testing-the-legal-basis-of-the-lack-of-pursuit-argument'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'There are a number of precautions that ought to be taken into consideration prior to investing in a UAE business. Taronish Mistry speaks to Arabian Business','post_name' => 'precautions-to-consider-before-investing-in-a-uae-business'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Getting involved in fractional property ownership using tokens requires legal knowledge. Nadim Bardawil speaks to AMEinfo on energizing the market.','post_name' => 'considering-fractional-property-ownership-using-tokens'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'All UAE business owners and managers should be aware of the very recent decision of the Dubai Court of First Instance in the matter of the Bankruptcy of Marka Holdings PJSC, which is indicative of the risk that a company’s Managers and Directors may face in an underfunded bankruptcy under the provisions of the UAE Bankruptcy Law.','post_name' => 'marka-group-bankruptcy-update'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => ' The concept of tokenisation of real estate assets involves creating a virtual token that represents ownership of a particular type of asset','post_name' => 'tokenization-of-real-estate-assets-explained'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA present Market Watch - The Future of Finance, a report on digital transformation, innovation and the fintech ecosystem in the GCC. ','post_name' => 'bsa-fintech-report-2021-the-future-of-finance-in-the-gcc'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'There have recently been amendments to the UAE Civil Procedures Law and related regulations. Bassel Boutros examines the changes and their potential impact.','post_name' => 'keeping-it-civil-the-latest-key-amendments-to-the-uae-civil-procedures-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'In this article Musab Iftikhar, discusses the most significant highlights and changes made following changed to the SCA licensing regime','post_name' => 'the-uaes-new-sca-licensing-regime'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'UAE Franchise Laws and Regulations 2022 Guide covering common issues in franchise laws and regulations including competition law.','post_name' => 'iclg-uae-franchise-laws-and-regulations-2022'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'A raft of UAE anti-money laundering guidance has been published recently. Rima Mrad examines the key provisions and their impact.','post_name' => 'uae-anti-money-laundering'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The following UAE International Arbitration 2022 Q&A provides an overview of International Arbitration laws and regulations applicable in the UAE.','post_name' => 'legal-500-uae-international-arbitration-chapter'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The UAE Federal Decree-Law No. 9 of 2016, known as the UAE Bankruptcy Law, has been amended pursuant to Federal Decree-Law No. 35 of 2021','post_name' => 'uae-bankruptcy-law-amendments'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'In a post-Covid era, cybersecurity for SMEs is high on the agenda. Cyber-attacks have become the fastest growing crime with 50 percent of such attacks targeting SMEs','post_name' => 'cybersecurity-for-smes'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The interest in SPAC’s in the UAE and wider region is growing, as SPACs aim to acquire private companies and make them public.','post_name' => 'spacs-in-the-uae-how-does-it-work'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The practical obstacles in leading UAE construction claims, asthese are arguably the most complex claims in the legal sector.','post_name' => 'the-practical-obstacles-in-leading-uae-construction-claims'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The Future of Finance for SMEs looks at FinTech in the GCC region, including app-led societies, banks vs FinTech and innovation in regulation','post_name' => 'finance-for-smes'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Have SPAC listings hit ‘the Wall’? SPACs have been the subject of a drastic surge where they emerged as the preferred route for public fund-raising in 2020','post_name' => 'have-spac-listings-hit-the-wall'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'India has made the commitment of procuring 50% of its energy requirements from clean and renewable energy sources within this decade.','post_name' => 'india-renews-its-green-pledge-with-a-commitment-to-triple-renewable-energy-generation-by-2030'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The following Q&A, originally published for Lexis Middle East, answers important questions regarding the laws and regulations governing Banking in Oman. ','post_name' => 'banking-in-oman'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The following Q&A, originally published for Lexis Middle East, answers important questions regarding the laws and regulations governing Islamic Finance in Oman.','post_name' => 'islamic-finance-in-oman'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'On the 2nd of January 2022 the Federal Decree-Law No. 14/2020 will come into force, read the latest amendments in relation to bounced cheques in the UAE','post_name' => 'latest-amendments-in-relation-to-bounced-cheques-in-the-uae'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Middle East law firm BSA can provide legal services to protect your interests during Expo 2020 in Dubai.','post_name' => 'expo-2020'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Specialist bankruptcy lawyers at Middle East law firm BSA expertly guide individuals and businesses through the ever-evolving insolvency regulations across the region.','post_name' => 'restructuring-bankruptcy'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'We help people do business all over the Middle East. We have offices in Dubai, UAE, Saudi Arabia, Oman, Lebanon and Iraq.','post_name' => 'sample-page-4'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Professionals from medicine, education and technology will be major beneficiaries of the UAE’s 10-year visa policy, which will come into effect in 2019, say legal experts.','post_name' => '10-year-visa-to-investors-skilled-professional-will-go-a-long-way-to-boost-investor-confidence'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The UAE cabinet has approved 122 economic activities across 13 sectors that will be eligible for up to 100% foreign ownership.','post_name' => '100-foreign-ownership-for-13-sectors-in-the-uae'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The maritime sector has always been significant to the growth of the UAE economy over the decades. This has been most apparent in Dubai where the creation of Jebel Ali Port and the Dubai Dry Docks.','post_name' => '100-foreign-ownership-for-the-maritime-sector'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With an international audience of more than 400 industry leaders, policymakers, academics and the global media assembled at the conference.','post_name' => '10th-annual-world-takaful-conference'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Saudi authorities imposed a 24-hour curfew in most Saudi cities, the capital Riyadh as well as in Jeddah, Dammam, Al-Khobar, Tabuk, Dhahran, Al-Hofuf, Ta’if, Al-Qatif.','post_name' => '24-hour-curfew-in-major-saudi-cities-announced'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The summit has speakers from local and international firms sharing their views and insights on Arbitration, ADR and Mediation from different regions.','post_name' => '2nd-annual-international-arbitration-summit'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'One of the most important points arising from the recent briefings on VAT by the Ministry of Finance isthe proposed rules regarding “group registrations”.','post_name' => 'how-vat-legislation-will-change-the-business-set-up-model-in-the-uae'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The conference programme looked at transforming insurance and re-Insurance strategies, enhance distribution channel strategies, learn the latest product innovations to help grow businesses.','post_name' => '4th-annual-middle-east-africa-insurance-summit'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The article examines the material legal process involved when undertaking a corporate acquisition transaction in Saudi Arabia (Acquisition Transaction).','post_name' => 'introduction-to-company-acquisitions-in-kingdom-of-saudi-arabia-business-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Yad is an Associate in the Corporate and M&A Practice in our Erbil office in Iraq. He has over eight years’ experience and specialises in taxation, corporate transactions, mergers and acquisitions, commercial matters, employment claims and contract review.','post_name' => 'yad-ezzuldin-dezay'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Swati is an Associate in the Litigation Practice in our DIFC office in Dubai. Fluent in English, Hindi and Panjabi, her expertise includes white collar crimes, bank frauds, bounced cheques, prevention of money laundering, criminal defamation, and matrimonial disputes.','post_name' => 'swati-soni'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Simon is a Partner and Head of Insurance at our Dubai office. Previously Head of Kennedys’ Corporate and Regulatory Insurance MENA practice, he is a qualified barrister who was called to the Bar in 1998, practising commercial and company law matters in the English courts.','post_name' => 'simon-isgar'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Shaaban is a Partner in the Litigation Practice in the DIFC office in Dubai. Practicing since 1991, he specialises in various types of litigation and dispute resolution including insurance and reinsurance, debt recovery, maritime, commercial, real estate.','post_name' => 'shaaban-metwally'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Salman is an Associate in the Corporate and M&A Practice in our DIFC office in Dubai. At BSA since 2014, he specialises in incorporating companies and has advised multinational clients on corporate structure.','post_name' => 'salman-madkour'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Robert is a Senior Associate with the Corporate, M&A and Real Estate Practices in our DIFC office in Dubai.','post_name' => 'robert-mitchley'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Rima is a Partner with the Corporate and M&A Practice in our DIFC offices in Dubai. She is an experienced corporate and insurance lawyer who has practiced in the UAE for over fourteen years.','post_name' => 'rima-mrad'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Ralph is a Senior Associate in the corporate practice based in the Sultanate of Oman (Muscat).','post_name' => 'ralph-hejaily'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Omar is an Associate in the Litigation Practice in our Muscat office in the Sultanate of Oman.','post_name' => 'omar-alkharoosi'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Nour is an Associate in the Insurance & Reinsurance Practice based in our DIFC office in Dubai. Fluent in English, Arabic and French, she represents insurance and reinsurance companies.','post_name' => 'nour-gemayel'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Nadia is an Associate within the Insurance Practice, based in Dubai.','post_name' => 'nadia-el-tannir'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Nadim is a Senior Associate both in our Corporate and TMT practices in our DIFC office in Dubai. He specialises in transactional corporate work across various industries including media, technology and healthcare.','post_name' => 'nadim-bardawil'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Munir joined BSA in 2018 as a Partner and Head of the Intellectual Property Practice in our Dubai office. Practicing in the Middle East since 2006, he has worked on numerous contentious and non-contentious IP works including trademarks, trade names, copyrights, patents, trade secrets and domain names.','post_name' => 'munir-abdallah-suboh'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Mundhir is a Partner in the Muscat office in the Sultanate of Oman. His experience covers high profile insurance and banking cases, in which he has acted for major international insurance and financial institutions.','post_name' => 'mundhir-albarwani'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Mundhir is an Associate in the Litigation Practice in our Muscat office in the Sultanate of Oman. A qualified Omani lawyer, he specialises in litigation and other forms of dispute resolution, with a particular expertise in civil, commercial, employment law and debt recovery.','post_name' => 'mundhir-al-rasbi'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Moustafa is a Litigation Associate in our DIFC office in Dubai. Practicing since 2003, he handles litigation cases before the Dubai Courts and Federal Courts, executing both registration and follow-up of cases for clients.','post_name' => 'moustafa-arfa'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The Head of our Sharjah and Northern Emirates offices. Mohammed specialises in arbitration, litigation and dispute resolution.','post_name' => 'mohd-nedal-dajani'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Mohammed is an Associate in the Litigation Practice in our Muscat office in the Sultanate of Oman. With expertise including commercial, civil, administrative and employment law, he represents clients in real estate, criminal, family and construction cases.','post_name' => 'mohammed-alghazali'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Mohammed is an Associate in the Corporate and M&A team in our DIFC office in Dubai. He advises private clients, real estate and property developers and organisations from the retail, lifestyle, leisure and hospitality sectors.','post_name' => 'mohammed-alahdal'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Michael is a Partner in the Corporate and M&A Practice in our DIFC offices in Dubai. With a strong focus on insurance and reinsurance, he has gained hands-on experience in the Middle East and North Africa region.','post_name' => 'michael-kortbawi'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Mahmoud is an Associate with the Litigation Practice in our DIFC office in Dubai. Practicing since 2005, he regularly advises financial institutions and private clients on various types of litigation and dispute resolution','post_name' => 'mahmoud-elgousi'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Lara is a Partner in the Corporate and Commercial Practice in our DIFC/ Dubai office in the UAE, specialising in energy matters','post_name' => 'lara-barbary'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Khulood is an Associate in the Litigation practice in our Muscat office in the Sultanate of Oman. Fluent in Arabic and English, she is qualified to draft legal documents and provide legal advice to both the public and private sectors.','post_name' => 'khulood-al-wahaibi'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Khalid is an Advocate in the Litigation Practice in our DIFC office in Dubai. With over 22 years’ experience, he has full rights of audience before the UAE Courts.','post_name' => 'khalid-bujsaim'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Khaled is an Associate in the Litigation practice in our DIFC office in Dubai. With over five years’ litigation experience practicing law in Egypt and the UAE, he specialises in employment disputes, commercial disputes, rental disputes and criminal cases.','post_name' => 'khaled-elgohari'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Jonathan Brown is Of Counsel within the Arbitration practice based in Dubai. Jonathan qualified as a Solicitor in 1991 and worked in the City of London before coming to Dubai in 1998 to head up the Contentious Business Practice of Clifford Chance.','post_name' => 'jonathan-brown'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Johnny is a Partner in our Lebanon office. She has a full range of legal services expertise in a variety of matters ranging from penal and civil law to commercial and business law, general corporate work, mergers and acquisitions and structuring foreign investments.','post_name' => 'johnny-el-hachem'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Based in Dubai, Jimmy is the Managing Partner of the firm. He specialises in corporate, commercial and real estate transactions and has almost two decades of experience in the region.','post_name' => 'jimmy-haoula'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Jean Abboud is a Senior Associate based in our Riyadh office with 9 years’ experience. He specializes in corporate and commercial law (foreign investment), securities, telecom, regulatory and corporate matters as well as projects.','post_name' => 'jean-abboud'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Hassan is an Associate with the Litigation Practice in our DIFC office in Dubai.','post_name' => 'hassan-el-shahat-mohamed'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Hassan is an Associate in the Litigation Practice in our DIFC office in Dubai. He specialises in various types of litigation, criminal cases and rent issues.','post_name' => 'hassan-el-agawani'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Haitham is an Associate in the Litigation Practice in our Muscat office in the Sultanate of Oman. As an Omani-qualified lawyer, he has rights of audience in civil, commercial and administrative courts.','post_name' => 'haitham-al-naabi'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Hadiel is an Associate with the Litigation Practice in our DIFC office in Dubai. With an undergraduate degree from the prestigious University of Sorbonne in Paris, Hadiel is involved in all matters related to dispute resolution.','post_name' => 'hadiel-hussien'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Habib is a Senior Associate with the Litigation Practice in our Abu Dhabi office. Practising in the UAE since 2000, he specialises in several types of litigation cases including civil, criminal, commercial and Sharia law.','post_name' => 'habib-al-hadi'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Felicity joined BSA Ahmad Bin Hezeem & Associates LLP as an Associate earlier this year in the Intellectual Property Department.','post_name' => 'felicity-hammond'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Elhag is an Associate in the Litigation team in our Abu Dhabi office. A member of the Sudan Bar Association since 1988, he has extensive knowledge of the legal systems in both the UAE and Sudan.','post_name' => 'elhag-mohamed-ali'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Dr. Ahmad is the Senior Partner in our Dubai office. He has over 24 years’ experience working in legal and judicial governmental institutions in Dubai.','post_name' => 'dr-ahmad-bin-hezeem-2'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Barry is a Senior Associate in the Restructuring & Bankruptcy, Corporate, and Insurance and Reinsurance practices in our DIFC office in Dubai.','post_name' => 'barry-greenberg'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Asim is a Partner and the Head of the Litigation practice in our Dubai office.','post_name' => 'asim-ahmed'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Ashraf is a Senior Associate in the Litigation practice in our Dubai office. With nearly 30 years’ experience practicing law, he joined BSA in 2017 and currently represents governments and high net worth individuals.','post_name' => 'ashraf-ibrahim'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Asma is an Associate in our Litigation practice, based in our DIFC office in Dubai. She represents corporate bodies and individuals and specialises in various litigation and alternative dispute resolution sectors.','post_name' => 'asa-siddiqui'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Arsalan is a Partner based in the Oman office. Practicing since 2009, Arsalan specializes in Islamic and conventional finance, bilateral and syndicated project finance, and acquisition finance.','post_name' => 'arsalan-tariq'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Antonios is a Partner, and the Head of Arbitration & Dispute Resolution, and Construction practices, based in our DIFC office in Dubai.','post_name' => 'antonios-dimitracopoulos'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Andrée practices as Government Relations Counsel from our DIFC offices in Dubai. As a qualified English solicitor, she has over twelve years’ experience, ten of which have been in Dubai, in private practice and in-house governmental roles.','post_name' => 'andree-hobson'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Anand is a Senior Associate in the Insurance & Reinsurance Practice based in our DIFC office in Dubai. With over 8 years of experience as a corporate insurance and regulatory lawyer having practiced in India and across the Middle East.','post_name' => 'anand-singh'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Amal assisted international and domestic Brand Owners in devising and implementing tailored strategies for trademark protection in the MENA region, with regard to prosecution, enforcement and litigation.','post_name' => 'amal-atieh'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Ali is an Associate in the Litigation practice based in our Abu Dhabi office. He joined BSA in 2013 and has over seven years’ legal experience gained from practicing law both in Egypt and the UAE.','post_name' => 'ali-abdelhalim'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Alaaeldin is an Associate in the Litigation Practice based in our DIFC office in Dubai. He represents clients in criminal litigation proceedings, civil disputes, rental disputes and employment matters.','post_name' => 'alaaeldin-ghoneim'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Akram is the Head of our Abu Dhabi office. An advocate with extensive experience in litigation, his diverse expertise includes advising clients on real estate matters, commercial issues and corporate disputes.','post_name' => 'akram-albaiti'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Ahmed is a Senior Associate in our Oman office, with over twenty years’ experience in corporate/commercial matters, litigious matters and arbitration.','post_name' => 'ahmed-al-taher-2'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Abdullah is a Partner in our Litigation practice based in our DIFC office in Dubai. Practicing since 2005, he specialises in various types of litigation and dispute resolution including medical malpractice, employment and debt recovery.','post_name' => 'abdullah-ishnaneh'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Abdelmuneem is a Senior Associate in our Muscat office. With over 20 years’ experience, he has worked on some of the biggest commercial, insurance, banking and dispute resolution matters in the Sultanate of Oman.','post_name' => 'abdul-moneem-al-rafei'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Abdulaziz is a Partner in our Muscat office. Experienced in litigation and dispute resolution, his expertise encompasses a wide range of industries including commercial, civil and administrative disputes.','post_name' => 'abdul-aziz-alrashdi'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Abdalla is an advocate based in Dubai. Abdalla has built a solid experience in different areas of litigation in the UAE and has the right of audience before the local courts.','post_name' => 'abdalla-alsuwaidi'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA Ahmad Bin Hezeem & Associates LLP seeks sustainable growth through the recruitment of talented lawyers, support staff and other professionals.','post_name' => 'careers'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'We help people do business all over the Middle East. We have offices in Dubai, UAE, Saudi Arabia, Oman, Lebanon and Iraq.','post_name' => 'about-us'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA can carry out due diligence to ensure your business is operating efficiently under the new Middle East VAT laws, advising on everything from basic procedures to filing your tax return.','post_name' => 'vat-implementation'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With a broad range of clients including technology vendors and public and private sector technology buyers, we can help you with every aspect of technology law in the Middle East.','post_name' => 'technology-media-and-telecommunications'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'New people represent a risk to every business. Whether you’re hiring staff or considering any kind of commercial partnership, working with the wrong people can cost you time, money, and even your company’s reputation.','post_name' => 'screening-and-due-diligence'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Working closely with our Construction, Litigation and Banking and Finance Departments, our real estate attorneys offer a full range of legal services on local and regional property development.','post_name' => 'real-estate-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Our team of highly qualified litigation lawyers collectively speaks 16 languages and has rights of audience in all courts including the English Law-based DIFC Courts.','post_name' => 'litigation-law-firm'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With years of experience acting for clients in some of the region’s most important IP cases, we can ensure your interests are protected.','post_name' => 'intellectual-property-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With years of experience acting for clients in some of the region’s most important IP cases, we can ensure your interests are protected.','post_name' => 'insurance-reinsurance-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Middle East law firm BSA has top energy lawyers in Dubai, UAE, Saudi Arabia, Oman, Lebanon, Iraq.','post_name' => 'energy-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA can help you navigate every aspect of employment, labour and HR as well as providing related services such as due diligence, corporate support and litigation.','post_name' => 'employment-law-firm'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Middle East lawyers BSA Law are authorized DIFC Corporate Service Providers for Special Purpose Companies operating in Dubai.','post_name' => 'difc-corporate-service-providers'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Cybersecurity is an omnipresent concern for all companies and organizations. In our increasingly interconnected and tech-driven world, businesses of all sizes are vulnerable to attacks from cybercriminals.','post_name' => 'cybersecurity'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With an experienced team that has worked on some of the most dynamic corporate law deals in the Middle East, we can help guide you through all aspects of corporate law.','post_name' => 'corporate-law-and-ma'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With years of experience working to protect our clients in this difficult regulatory area, we can provide our you with legally watertight and local law-compliant contractual arrangements.','post_name' => 'construction-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Benefitting from close working relationships with local authorities and regulatory bodies across the region, we provide innovative, commercially focused solutions.','post_name' => 'commercial-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Our specialist lawyers are highly skilled in contract law, drafting syndicated loans and security documentation as well as structuring and advising on investment funds.','post_name' => 'banking-finance-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With years of experience working on some of the most important regional and international arbitration cases, we can help you successfully navigate this area of law.','post_name' => 'arbitration-dispute-resolution'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'We help people do business all over the Middle East. As a Middle East law firm, we can help you negotiate the maze of regional regulations and ensure your business interests are protected at all times','post_name' => 'legal-practice-areas'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'When people criticise Dubai and the UAE, they often make sweeping statements about labour conditions and worker’s rights, but the UAE Labour Law actually protects workers in many ways.','post_name' => '7-things-you-might-not-realise-are-against-the-uaes-labour-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The conference was concluded by ceremonies honouring the winners of the 12th Middle East CEO Excellence Awards and 4th Middle East Future Leaders Excellence Awards.','post_name' => '5th-uae-government-future-leaders-conference'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The conference programme looked at updates in the Middle East insurance sector covering regulations and market changes, the compulsory health insurance in Dubai, UAE’s role as a financial hub for the insurance industry.','post_name' => '5th-annual-middle-east-africa-insurance-summit'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The United Arab Emirates has a complex sanctions regime based on a variety of sources. Sanctions are based on diverse interests, including political, economic and national security interests.','post_name' => 'uae-chapter-on-sanctions'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA Bin Shabib, Al Rashdi & Al Barwani (BSA), and Zubair Small Enterprises Centre (Zubair SEC) recently completed a workshop and legal clinic on January 15 at Bait Al Zubair.','post_name' => 'bsa-and-zubair-sec-successfully-concludes-legal-clinic-and-workshop-for-smes'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA Bin Shabib, Al Rashdi & Al Barwani (BSA), and Zubair Small Enterprises Centre (Zubair SEC) will jointly hold a workshop and legal clinic on January 15, 2019.','post_name' => 'bsa-in-collaboration-with-zubair-sec-to-hold-workshop-and-legal-clinic-for-smes'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'For legal advice on anything related to COVID-19, visit the Coronavirus Insight Centre from Middle East lawyers BSA Law or contact their experienced lawyers today.','post_name' => 'coronavirus_insight_centre'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'fractional-title-deed'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'beirut-blast-insurance'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => '100-foreign-ownership-ras-al-khaimah'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'DIFC Litigation is becoming a more popular choice for Companies and Individuals alike. It is fast becoming a preferred option for international companies who prefer a more familiar common law based system but don’t want to pay for expensive arbitration.','post_name' => 'difc-litigation'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA Ahmad Bin Hezeem & Associates LLP seeks sustainable growth through the recruitment of talented lawyers, support staff and other professionals.','post_name' => 'privacy-policy'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'We help people do business all over the Middle East. We have offices in Dubai, UAE, Saudi Arabia, Oman, Lebanon and Iraq.','post_name' => 'sample-page-2'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Our team of highly qualified litigation lawyers collectively speaks 16 languages and has rights of audience in all courts including the English Law-based DIFC Courts.','post_name' => 'maritime'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'We help people do business all over the Middle East. We have offices in Dubai, UAE, Saudi Arabia, Oman, Lebanon and Iraq.','post_name' => 'sample-page-3'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'We help people do business all over the Middle East. We have offices in Dubai, UAE, Saudi Arabia, Oman, Lebanon and Iraq.','post_name' => 'sample-page'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'uae-insurance-regulatory-round-up'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'We help people do business all over the Middle East. We have offices in Dubai, UAE, Saudi Arabia, Oman, Lebanon and Iraq.','post_name' => 'sample-vid'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'new-developments-foreign-direct-investment-in-the-uae'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'clarified-legal-landscape-for-ksa-offers-investors-potential-new-playground'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'recent-legal-developments-in-business-interruption-insurance-coverage-issues-and-wider-implications-perhaps-for-the-middle-east'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'real-estate-crowdfunding-as-potential-opportunity-for-investors-and-entrepreneurs'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'new-developments-foreign-direct-investment-in-the-uae'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'alcohol-and-trademarks-practice-in-uae'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'saudi-central-bank-launches-open-banking-initiative'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'dubai-art-scene-how-to-improve-the-picture'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'nuts-and-bolts-of-decision-no-26-of-2020-covering-small-claims-tribunals'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'draft-insurance-broker-regulations-what-could-change'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'uae-vat-laws-and-export-of-services'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'the-end-of-the-gcc-patent-what-next'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'dubai-rent-freeze'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'new-sme-market-conduct-regulation'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'pay-when-paid-clause-can-be-defeated'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'With the aim of attracting investors to the Emirate, the Dubai Land Department (DLD) announced a ‘fractional title deed’ initiative.','post_name' => 'update-saudi-companies-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'A family business is any business in which two or more members of the same family are involved and most of the ownership or control lies within such a family.','post_name' => 'family-business-regulations-in-the-uae'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'On 13 June 2021 the UAE Central Bank issued a circular with reference no CBUAE/BSD/N/2021/3148 (“the Circular”) addressed to all financial institutions operating in the UAE and offering structured conventional life insurance and takaful investment and saving products (“the Products”). The Circular refers to Central Bank’s Ccircular of May 2017 which was addressed to banks and finance companies and which stopped such organizations from offering savings and investment, and non-capital guaranteed products, until the Central Bank issues a governance policy around the issuance of such products. The Circular Central Bank now provides that subject to a Licensed Financial Institution (“LFI”) fulfilling the requirements laid down by Central bBank, and after having obtained a no objection letter from the Central Bank, they these institutions can resume selling such Products.','post_name' => 'revised-central-bank-guidance-regarding-saving-and-investment-insurance'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA Ahmad Bin Hezeem & Associates is proud to have been named Regional Law Firm of the Year at the annual Law.com Legal Week Middle East Legal Awards.BSA is noted as having "an impressive regional footprint in the Middle East, which offers a full service to its clients, delivering excellence each time." The judges took into consideration our impressive flagship deals from the past 12 months','post_name' => 'bsa-named-regional-law-firm-of-the-year'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The Government of Oman has introduced new laws to promote the economy and to ensure minimal disruption to businesses during these difficult times. Many business owners and their in-house legal teams seek relief and protection from the implications of coronavirus. The recent measures issued by Omani authorities are an effort to ensure that business goes on as usual.','post_name' => 'new-laws-agile-teams'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'UAE has a new data law. Check out our article to see the newest additions and to be posted on the effects of this law.','post_name' => 'uae-project-of-the-50-new-data-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'This article looks at the concept of healthcare and medical trusts, as well as the UAE Trusts Law, focusing on medical insurers','post_name' => 'the-concept-of-healthcare-medical-trust'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The 3rd annual motor insurance conference, hosted by Emirates Insurance Association. Industry speakers will discuss \'Fair vs Aggressive Competition\'.','post_name' => 'emirates-insurance-association-third-annual-motor-conference'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Webinar discussing anti money laundering regulations and the impact on the UAE insurance industry, in association with Premium Insurance Magazine','post_name' => 'money-laundering-regulations'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'A seminar on restructuring & insolvency in the UAE, in association with the Oath Legal Magazine, focusing on the UAE Bankruptcy Law.','post_name' => 'insolvency-restructuring-in-the-uae-practical-considerations-for-corporates'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Emirates Maritime Arbitration Centre and DIFC Arbitration Institute merge into Dubai International Arbitration Centre as announced by HH Sheikh Mohammed','post_name' => 'emirates-maritime-arbitration-centre-and-difc-arbitration-institute-merge-into-dubai-international-arbitration-centre'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'A closer look at the UAE Health Data Law and how these impact the protection and Processing of the healthcare sectos\'s data and information','post_name' => 'the-new-uae-health-data-law-an-in-depth-look'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The following article discusses the implications of the Dubai Courts\' specialised money laundering court, as originally published in Arabian Business','post_name' => 'dubais-recently-established-money-laundering-court-explained'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Ahead of Expo 2020, we discuss all things related to renewable energy in the Middle East, including fossil fuels, wind, solar and hydrogen','post_name' => 'the-middle-east-plugged-into-renewable-energy'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'When can a landlord increase a tenant\'s rent or serve an eviction notice in the UAE? This article discusses a tenant\'s rights under the law.','post_name' => 'rent-increases-and-eviction-notices-what-are-your-rights'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The BSA Insurance Practice provide the UAE Chapter Chambers & Partners of the Insurance Litigation 2021 Guide, with an in-depth look at insurance ADR','post_name' => 'uae-insurance-litigation-2021'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Want to know your rights as a tenant in Dubai? What landlords can and can\'t do? Hazem Balbaa provides expert legal advisor for tenants renting in Dubai','post_name' => 'renting-in-dubai-here-are-your-rights-as-a-tenant'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Changes to the DIFC Employment Law serve to clarify ambiguities regarding the existing law and will increase the financial centre\'s attractiveness.','post_name' => 'difc-employment-law-new-amendments-explained'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Central Bank Digital Currencies (“CBDC") are a new form of digital currencies. Associate Omar Al Masri discusses CBDC\'s with Gulf Business','post_name' => 'central-bank-digital-currencies'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA will be hosting aspecial breakfast event at Dubai Expo 2020: \'The Future of FinTech: Bridging the divide between the unbanked and traditional finance.','post_name' => 'the-future-of-fintech'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'One of the most daunting prospects for an MEP sub-contractor is the possibility of being held ransom to a pay-when-paid clause.','post_name' => 'testing-the-legal-basis-of-the-lack-of-pursuit-argument'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'There are a number of precautions that ought to be taken into consideration prior to investing in a UAE business. Taronish Mistry speaks to Arabian Business','post_name' => 'precautions-to-consider-before-investing-in-a-uae-business'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Getting involved in fractional property ownership using tokens requires legal knowledge. Nadim Bardawil speaks to AMEinfo on energizing the market.','post_name' => 'considering-fractional-property-ownership-using-tokens'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'All UAE business owners and managers should be aware of the very recent decision of the Dubai Court of First Instance in the matter of the Bankruptcy of Marka Holdings PJSC, which is indicative of the risk that a company’s Managers and Directors may face in an underfunded bankruptcy under the provisions of the UAE Bankruptcy Law.','post_name' => 'marka-group-bankruptcy-update'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => ' The concept of tokenisation of real estate assets involves creating a virtual token that represents ownership of a particular type of asset','post_name' => 'tokenization-of-real-estate-assets-explained'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'BSA present Market Watch - The Future of Finance, a report on digital transformation, innovation and the fintech ecosystem in the GCC. ','post_name' => 'bsa-fintech-report-2021-the-future-of-finance-in-the-gcc'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'There have recently been amendments to the UAE Civil Procedures Law and related regulations. Bassel Boutros examines the changes and their potential impact.','post_name' => 'keeping-it-civil-the-latest-key-amendments-to-the-uae-civil-procedures-law'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'In this article Musab Iftikhar, discusses the most significant highlights and changes made following changed to the SCA licensing regime','post_name' => 'the-uaes-new-sca-licensing-regime'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'UAE Franchise Laws and Regulations 2022 Guide covering common issues in franchise laws and regulations including competition law.','post_name' => 'iclg-uae-franchise-laws-and-regulations-2022'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'A raft of UAE anti-money laundering guidance has been published recently. Rima Mrad examines the key provisions and their impact.','post_name' => 'uae-anti-money-laundering'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The following UAE International Arbitration 2022 Q&A provides an overview of International Arbitration laws and regulations applicable in the UAE.','post_name' => 'legal-500-uae-international-arbitration-chapter'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The UAE Federal Decree-Law No. 9 of 2016, known as the UAE Bankruptcy Law, has been amended pursuant to Federal Decree-Law No. 35 of 2021','post_name' => 'uae-bankruptcy-law-amendments'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'In a post-Covid era, cybersecurity for SMEs is high on the agenda. Cyber-attacks have become the fastest growing crime with 50 percent of such attacks targeting SMEs','post_name' => 'cybersecurity-for-smes'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The interest in SPAC’s in the UAE and wider region is growing, as SPACs aim to acquire private companies and make them public.','post_name' => 'spacs-in-the-uae-how-does-it-work'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The practical obstacles in leading UAE construction claims, asthese are arguably the most complex claims in the legal sector.','post_name' => 'the-practical-obstacles-in-leading-uae-construction-claims'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The Future of Finance for SMEs looks at FinTech in the GCC region, including app-led societies, banks vs FinTech and innovation in regulation','post_name' => 'finance-for-smes'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'Have SPAC listings hit ‘the Wall’? SPACs have been the subject of a drastic surge where they emerged as the preferred route for public fund-raising in 2020','post_name' => 'have-spac-listings-hit-the-wall'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'India has made the commitment of procuring 50% of its energy requirements from clean and renewable energy sources within this decade.','post_name' => 'india-renews-its-green-pledge-with-a-commitment-to-triple-renewable-energy-generation-by-2030'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The following Q&A, originally published for Lexis Middle East, answers important questions regarding the laws and regulations governing Banking in Oman. ','post_name' => 'banking-in-oman'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'The following Q&A, originally published for Lexis Middle East, answers important questions regarding the laws and regulations governing Islamic Finance in Oman.','post_name' => 'islamic-finance-in-oman'),
            array('meta_key' => '_yoast_wpseo_metadesc','meta_value' => 'On the 2nd of January 2022 the Federal Decree-Law No. 14/2020 will come into force, read the latest amendments in relation to bounced cheques in the UAE','post_name' => 'latest-amendments-in-relation-to-bounced-cheques-in-the-uae'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => '10 Year Visa To Investors, Skilled Professional Will Go A Long Way To Boost Investor Confidence','post_name' => '10-year-visa-to-investors-skilled-professional-will-go-a-long-way-to-boost-investor-confidence'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => '100% Foreign Ownership For 13 Sectors in the UAE - Invest in the UAE','post_name' => '100-foreign-ownership-for-13-sectors-in-the-uae'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => '100% Foreign Ownership for the Maritime Sector in the UAE - Corporate Law Firms','post_name' => '100-foreign-ownership-for-the-maritime-sector'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => '10th Annual World Takaful Conference in Dubai - BSA','post_name' => '10th-annual-world-takaful-conference'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => '24-hour Curfew in Major Saudi Cities Announced','post_name' => '24-hour-curfew-in-major-saudi-cities-announced'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => '2nd Annual International Arbitration Summit - BSA','post_name' => '2nd-annual-international-arbitration-summit'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'How VAT Legislation will change the business set up model in the UAE','post_name' => 'how-vat-legislation-will-change-the-business-set-up-model-in-the-uae'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => '4th Annual Middle East & Africa Insurance Summit - Insurance Law','post_name' => '4th-annual-middle-east-africa-insurance-summit'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Introduction to Company Acquisitions in Kingdom of Saudi Arabia','post_name' => 'introduction-to-company-acquisitions-in-kingdom-of-saudi-arabia-business-law'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => '7 things you might not realise are against the UAE’s Labour Law','post_name' => '7-things-you-might-not-realise-are-against-the-uaes-labour-law'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => '5th UAE Government Future Leaders Conference - Business intelligence in the UAE','post_name' => '5th-uae-government-future-leaders-conference'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => '5th Annual Middle East & Africa Insurance Summit - Insurance Law','post_name' => '5th-annual-middle-east-africa-insurance-summit'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'UAE Chapter on Sanctions - UAE Law','post_name' => 'uae-chapter-on-sanctions'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'BSA and Zubair SEC Successfully Concludes Legal Clinic and Workshop for SMEs','post_name' => 'bsa-and-zubair-sec-successfully-concludes-legal-clinic-and-workshop-for-smes'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'BSA in Collaboration With Zubair SEC to Hold Workshop and Legal Clinic For SMEs','post_name' => 'bsa-in-collaboration-with-zubair-sec-to-hold-workshop-and-legal-clinic-for-smes'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'fractional-title-deed'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'beirut-blast-insurance'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => '100-foreign-ownership-ras-al-khaimah'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'uae-insurance-regulatory-round-up'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'new-developments-foreign-direct-investment-in-the-uae'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'clarified-legal-landscape-for-ksa-offers-investors-potential-new-playground'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'recent-legal-developments-in-business-interruption-insurance-coverage-issues-and-wider-implications-perhaps-for-the-middle-east'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'real-estate-crowdfunding-as-potential-opportunity-for-investors-and-entrepreneurs'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'new-developments-foreign-direct-investment-in-the-uae'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'alcohol-and-trademarks-practice-in-uae'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'saudi-central-bank-launches-open-banking-initiative'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'dubai-art-scene-how-to-improve-the-picture'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'nuts-and-bolts-of-decision-no-26-of-2020-covering-small-claims-tribunals'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'draft-insurance-broker-regulations-what-could-change'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'uae-vat-laws-and-export-of-services'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'the-end-of-the-gcc-patent-what-next'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'dubai-rent-freeze'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'new-sme-market-conduct-regulation'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'pay-when-paid-clause-can-be-defeated'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Fractional Title Deed in Dubai for The National - Investments in Dubai','post_name' => 'update-saudi-companies-law'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'UAE New Data Law  ','post_name' => 'uae-project-of-the-50-new-data-law'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Healthcare and Medical trusts  ','post_name' => 'the-concept-of-healthcare-medical-trust'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Money laundering regulations ','post_name' => 'money-laundering-regulations'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Restructuring & Insolvency  ','post_name' => 'insolvency-restructuring-in-the-uae-practical-considerations-for-corporates'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'DIAC  ','post_name' => 'emirates-maritime-arbitration-centre-and-difc-arbitration-institute-merge-into-dubai-international-arbitration-centre'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'UAE Health Data Law  ','post_name' => 'the-new-uae-health-data-law-an-in-depth-look'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'New Money Laundering Court  ','post_name' => 'dubais-recently-established-money-laundering-court-explained'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Plug into Renewable Energy ','post_name' => 'the-middle-east-plugged-into-renewable-energy'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'UAE Civil Procedures Law','post_name' => 'keeping-it-civil-the-latest-key-amendments-to-the-uae-civil-procedures-law'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'SCA Licensing Regime  ','post_name' => 'the-uaes-new-sca-licensing-regime'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'UAE Franchise Laws  ','post_name' => 'iclg-uae-franchise-laws-and-regulations-2022'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'UAE Anti-Money Laundering  ','post_name' => 'uae-anti-money-laundering'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'UAE International Arbitration  ','post_name' => 'legal-500-uae-international-arbitration-chapter'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'UAE Bankruptcy Law  ','post_name' => 'uae-bankruptcy-law-amendments'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Cybersecurity for SMEs  ','post_name' => 'cybersecurity-for-smes'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'SPAC\'s in the UAE  ','post_name' => 'spacs-in-the-uae-how-does-it-work'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'UAE Construction claims  ','post_name' => 'the-practical-obstacles-in-leading-uae-construction-claims'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Finance for SMEs  ','post_name' => 'finance-for-smes'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'SPAC listings ','post_name' => 'have-spac-listings-hit-the-wall'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Renewable energy  ','post_name' => 'india-renews-its-green-pledge-with-a-commitment-to-triple-renewable-energy-generation-by-2030'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Banking in Oman  ','post_name' => 'banking-in-oman'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'Islamic Finance in Oman ','post_name' => 'islamic-finance-in-oman'),
            array('meta_key' => '_yoast_wpseo_title','meta_value' => 'bounced cheques in the UAE  ','post_name' => 'latest-amendments-in-relation-to-bounced-cheques-in-the-uae')
        );

        $counter = 1;
        $postsData = [];
        foreach ($posts as $item){
            $article = Article::where('slug',$item['post_name'])->first();

            if($article){
//                echo $counter.'. ('.$article->id.') '.$article->title.'<br/>';
                if($item['meta_key'] == '_yoast_wpseo_title')
                    $postsData['articles'][$article->id]['meta_title'] = $item['meta_value'];
                elseif ($item['meta_key'] == '_yoast_wpseo_metadesc'){
                    $postsData['articles'][$article->id]['meta_description'] = $item['meta_value'];
                }

                $postsData['articles'][$article->id]['title'] = $article->title;
                $counter++;
            }

        }

        // Lawyers
        $counter = 1;
        foreach ($lawyers as $item){
            $article = Lawyer::where('slug',$item['post_name'])->first();

            if($article){
                if($item['meta_key'] == '_yoast_wpseo_title')
                    $postsData['lawyers'][$article->id]['meta_title'] = $item['meta_value'];
                elseif ($item['meta_key'] == '_yoast_wpseo_metadesc'){
                    $postsData['lawyers'][$article->id]['meta_description'] = $item['meta_value'];
                }
                $postsData['lawyers'][$article->id]['title'] = $article->name;
                $counter++;
            }

        }

        // Lawyers
        $counter = 1;
        foreach ($practices as $item){
            $article = Service::where('slug',$item['post_name'])->first();

            if($article){
                if($item['meta_key'] == '_yoast_wpseo_title')
                    $postsData['services'][$article->id]['meta_title'] = $item['meta_value'];
                elseif ($item['meta_key'] == '_yoast_wpseo_metadesc'){
                    $postsData['services'][$article->id]['meta_description'] = $item['meta_value'];
                }
                $postsData['services'][$article->id]['title'] = $article->title;
                $counter++;
            }

        }
        $counter = 1;

        foreach($postsData as $key=>$type){
            foreach($type as $item){
                echo '<h3>'.$item['title'].'</h3>';
                echo '<p>- Meta Title: '.(isset($item['meta_title']) ? $item['meta_title'] : '').'</p>';
                echo '<p>- Meta Description: '.(isset($item['meta_description']) ? $item['meta_description'] : '').'</p>';
                echo '<hr/>';
                $counter++;
            }
        }
    }

    public function convert()
    {
        $data = array(
                1 => array('title' => 'Are You Complying or Not?', 'category' => 'LEGAL & REG'),
                2 => array('title' => '', 'category' => 'DELETE'),
                3 => array('title' => 'ECONOMIC SUBSTANCE RULES: HOW THEY CAN IMPACT YOUR BUSINESS IN THE UAE?', 'category' => 'LEGAL & REG'),
                4 => array('title' => 'Economic Substance Rules: How Can They Impact Your Business in the UAE?', 'category' => 'LEGAL & REG'),
                5 => array('title' => 'The DIFC Insolvency Law: A First-Of-Its-Kind Procedure for Insolvency in Accordance with International Best Practice', 'category' => 'LEGAL & REG'),
                6 => array('title' => 'Investing in KSA', 'category' => 'LEGAL & REG'),
                7 => array('title' => 'Late Appointment of Tribunal Experts and Reopening of Closed Arbitral Proceedings', 'category' => 'LEGAL & REG'),
                8 => array('title' => '100% Foreign Ownership For 13 Sectors in the UAE', 'category' => 'News'),
                9 => array('title' => 'Is There a Specific Legislation Regulating E-Signatures in the UAE?', 'category' => 'LEGAL & REG'),
                10 => array('title' => 'The Experts in Healthcare Law', 'category' => 'News'),
                11 => array('title' => 'Key Considerations For Multinational Business Looking to Setup in the UAE', 'category' => 'LEGAL & REG'),
                12 => array('title' => 'UAE Ministry of Economy Update Official Fees for Trademark Enforcement and Registration', 'category' => 'News'),
                13 => array('title' => 'Breakfast Seminar: An Insight Into the New Law on Ownership for Foreign Investors', 'category' => 'Events'),
                14 => array('title' => 'Khulood Al Wahaibi', 'category' => 'DELETE'),
                15 => array('title' => 'H.H. Sheikh Mohamed Bin Soud Al Khasimi, Issues RAK Law No. 1 of 2019, to Establish Commercial Court in RAK', 'category' => 'LEGAL & REG'),
                16 => array('title' => 'Civil Law Training', 'category' => 'Events'),
                17 => array('title' => 'Anti-counterfeiting Committee Engages High-Level Government Officials in United Arab Emirates', 'category' => 'News'),
                18 => array('title' => 'UAE Launches Long-Term Residency System for Non-Citizens', 'category' => 'News'),
                19 => array('title' => 'Oman Expat Recruitment Rules and Recent Restrictions', 'category' => 'LEGAL & REG'),
                20 => array('title' => 'Money Matters', 'category' => 'LEGAL & REG'),
                21 => array('title' => 'Economic Substance Rules', 'category' => 'LEGAL & REG'),
                22 => array('title' => 'Challenging a Tribunal Member in an Arbitration', 'category' => 'LEGAL & REG'),
                23 => array('title' => 'Global Reinsurance a Great Learning Tool for Middle East Markets', 'category' => 'News'),
                24 => array('title' => 'Launch of Joint Initiative to Streamline UAE\'s Real Estate Sector to Promote Future Investment', 'category' => 'News'),
                25 => array('title' => 'Arabian Business Interview: White Collar Crimes', 'category' => 'News'),
                26 => array('title' => 'UAE Mock Trial', 'category' => 'Events'),
                27 => array('title' => 'Professional Negligence Law Review: UAE Chapter 2019', 'category' => 'LEGAL & REG'),
                28 => array('title' => 'Wave of Transformation in Saudi Arabia', 'category' => 'News'),
                29 => array('title' => 'New Dubai RERA Law Issued', 'category' => 'News'),
                30 => array('title' => 'Newly Formed Dubai International Arbitration Centre Board of Trustees Holds First Meeting', 'category' => 'News'),
                31 => array('title' => 'Dubai Real Estate Institute and BSA Sign MoU to Boost Skill Development and Emiratisation in UAE’s Property Industry', 'category' => 'News'),
                32 => array('title' => '', 'category' => 'DELETE'),
                33 => array('title' => 'RAK Establishes Commercial Court', 'category' => 'News'),
                34 => array('title' => 'Dubai Government Expands Opportunities for the Private Sector', 'category' => 'News'),
                35 => array('title' => 'UAE Chapter on Sanctions', 'category' => 'LEGAL & REG'),
                36 => array('title' => 'Overview on Compliance & Sanctions in the UAE', 'category' => 'Events'),
                37 => array('title' => 'Comparison Between the UAE and USA Professional Negligence Law', 'category' => 'LEGAL & REG'),
                38 => array('title' => 'Tax & VAT in Business and Practice', 'category' => 'Events'),
                39 => array('title' => 'Intellectual Property Right Assignments', 'category' => 'LEGAL & REG'),
                40 => array('title' => 'Dr. Ahmad\'s Interview with Al Arabiya News Channel', 'category' => 'News'),
                41 => array('title' => 'UAE Leads for Ease of Doing Business', 'category' => 'News'),
                42 => array('title' => 'UAE Stays Top MENA Economy', 'category' => 'News'),
                43 => array('title' => 'UAE Federal Tax Authority Extends the Excise Tax', 'category' => 'News'),
                44 => array('title' => 'The Dispute Resolution Question Time 2019', 'category' => 'News'),
                45 => array('title' => 'The UAE\'s New Regulation on Medical Liability', 'category' => 'LEGAL & REG'),
                46 => array('title' => 'UAE: Fintech (2nd Edition)', 'category' => 'LEGAL & REG'),
                47 => array('title' => 'Dubai Issues New Law Covering Jointly Owned Properties', 'category' => 'News'),
                48 => array('title' => 'UAE Anti-corruption & Bribery 2019', 'category' => 'LEGAL & REG'),
                49 => array('title' => 'UAE: International Arbitration', 'category' => 'LEGAL & REG'),
                50 => array('title' => 'Employee Monitoring in the UAE', 'category' => 'LEGAL & REG'),
                51 => array('title' => 'Compensation in Injury Cases: Onshore', 'category' => 'LEGAL & REG'),
                52 => array('title' => 'Foreign Investment Shareholding', 'category' => 'LEGAL & REG'),
                53 => array('title' => 'Interpreting Dubai’s New Law Governing Jointly Owned Properties', 'category' => 'News'),
                54 => array('title' => 'The UAE’s New Regulation on Medical Liability and Its Impact on the Health System', 'category' => 'LEGAL & REG'),
                55 => array('title' => 'Tech Used to Entice Global Investors to Dubai\'s Property Market', 'category' => 'News'),
                56 => array('title' => 'A New Bankruptcy Regime for Oman', 'category' => 'LEGAL & REG'),
                57 => array('title' => 'Share and Share Alike', 'category' => 'LEGAL & REG'),
                58 => array('title' => 'Radical Change for the Life Insurance Market', 'category' => 'LEGAL & REG'),
                59 => array('title' => 'The Development of Competition Law in the Region', 'category' => 'LEGAL & REG'),
                60 => array('title' => 'UAE Insolvency Law: The Opportunity to Repay instead of Running Away', 'category' => 'LEGAL & REG'),
                61 => array('title' => 'The DIFC has Enhanced Its Regulations with a New Intellectual Property Law', 'category' => 'News'),
                62 => array('title' => 'How the UAE\'s New Life Regulations Affect Advisers and Customers', 'category' => 'LEGAL & REG'),
                63 => array('title' => 'The New Key Laws Introduced in the UAE to Support Businesses', 'category' => 'LEGAL & REG'),
                64 => array('title' => 'KSA – The Premier Hub for Startups in MENA by 2030', 'category' => 'LEGAL & REG'),
                65 => array('title' => 'New Foreign Capital Investment Law (“FCIL”) in Oman', 'category' => 'News'),
                66 => array('title' => 'Professional Fees Now Priority Debts Under Revised UAE Bankruptcy Law', 'category' => 'News'),
                67 => array('title' => 'UAE Civil Court Judgments Now Enforceable in India', 'category' => 'News'),
                68 => array('title' => 'UAE Insurance Regulatory Round-up 2019', 'category' => 'LEGAL & REG'),
                69 => array('title' => 'New Package of Fee Waivers for Various Government Services in the UAE', 'category' => 'LEGAL & REG'),
                70 => array('title' => 'Arsalan Tariq has been named Partner at BSA Al Rashdi & Al Barwani Advocates and Legal Consultants', 'category' => 'LEGAL & REG'),
                71 => array('title' => 'The New Foreign Capital Investment Law in Oman', 'category' => 'News'),
                72 => array('title' => 'UAE Bankruptcy Law Updates', 'category' => 'News'),
                73 => array('title' => 'BSA Shortlisted for 5 Categories at the Middle East Legal Awards', 'category' => 'LEGAL & REG'),
                74 => array('title' => 'BSA Ahmad Bin Hezeem & Associates LLP are Expanding at a Rapid Rate with Promotions and Strategic Additions Across the Firm', 'category' => 'Reports'),
                75 => array('title' => 'Dubai EXPO 2020 - Decennial Liability & Decennial Liability Insurance', 'category' => 'Reports'),
                76 => array('title' => 'New Mandated Health Insurance Law in Oman', 'category' => 'News'),
                77 => array('title' => 'What To Look Out For In 2020 - Are You Covered?', 'category' => 'LEGAL & REG'),
                78 => array('title' => 'Groundbreaking Changes for Foreign Direct Investment in the UAE', 'category' => 'Reports'),
                79 => array('title' => 'New Commercial Companies Law: Modernising Corporate Legalisation in Oman', 'category' => 'News'),
                80 => array('title' => 'UAE Compliance Update', 'category' => 'LEGAL & REG'),
                81 => array('title' => 'Do You Have Insurance Coverage for Coronavirus-Related Interruption to Your Business?', 'category' => 'LEGAL & REG'),
                82 => array('title' => 'Can Employers Terminate Employees Due to COVID-19?', 'category' => 'LEGAL & REG'),
                83 => array('title' => 'Central Bank Stimulus Package', 'category' => 'LEGAL & REG'),
                84 => array('title' => 'Dubai Courts Postpone all Judicial Hearings', 'category' => 'LEGAL & REG'),
                85 => array('title' => 'DIFC Courts to Operate Remotely', 'category' => 'LEGAL & REG'),
                86 => array('title' => 'DIFC Wills Service Centre to Stop Appointments', 'category' => 'LEGAL & REG'),
                87 => array('title' => 'Business Stimulus Packages, Force Majeure and Back to Business Post Coronavirus', 'category' => 'LEGAL & REG'),
                88 => array('title' => 'Coronavirus in UAE: Know Your Current Rights as an Employee', 'category' => 'Reports'),
                89 => array('title' => 'Virtual Will Registration Launched by DIFC Wills Service Centre', 'category' => 'LEGAL & REG'),
                90 => array('title' => 'Decree on the Regulation of Advertisements in Dubai', 'category' => 'News'),
                91 => array('title' => 'UAE Activates Remote Mode to Ensure Continuity of Services', 'category' => 'LEGAL & REG'),
                92 => array('title' => 'Property Law Amid COVID-19', 'category' => 'LEGAL & REG'),
                93 => array('title' => 'COVID-19 Boosts the Uptake of Tele Health/Medicine in UAE – What are the Legal Implications?', 'category' => 'LEGAL & REG'),
                94 => array('title' => 'Ministerial Resolution No. (279) of 2020 on Employment Stability in Private Sector for Non-UAE Nationals', 'category' => 'LEGAL & REG'),
                95 => array('title' => 'Saudi Arabia Announces USD 31.9 Billion Stimulus Package to Mitigate the Economic Crisis Caused by the COVID-19 Outbreak', 'category' => 'LEGAL & REG'),
                96 => array('title' => 'Government Relief Measures for KSA during COVID-19', 'category' => 'LEGAL & REG'),
                97 => array('title' => 'A Summary of Ministerial Resolutions No 279 and No 280', 'category' => 'LEGAL & REG'),
                98 => array('title' => 'UAE Ministry of Economy Responds by Reducing IP Fees', 'category' => 'Reports'),
                99 => array('title' => 'Cyber Insurance: Risks and Trends 2020', 'category' => 'Reports'),
                100 => array('title' => 'How to Register a Trademark in the Sultanate of Oman', 'category' => 'Reports'),
                101 => array('title' => 'The Dangers of Failing to Accurately Define the Terms Under which Contractual Payments are to be made', 'category' => 'Reports'),
                102 => array('title' => 'Insurance Brokers - Reduction in Capital Requirements', 'category' => 'Reports'),
                103 => array('title' => 'Mortgage Holidays in the UAE during the Coronavirus Pandemic', 'category' => 'LEGAL & REG'),
                104 => array('title' => 'Extension Granted for Filing of VAT Returns with FTA', 'category' => 'LEGAL & REG'),
                105 => array('title' => 'Remote Property Registration: Remote Process', 'category' => 'LEGAL & REG'),
                106 => array('title' => 'The Employer’s Authority to Organize Work Amidst the Coronavirus Crisis', 'category' => 'LEGAL & REG'),
                107 => array('title' => 'إضاءة قانونية في الحماية الجزائية ضد مرض كورونا )كوفيد 19)', 'category' => 'DELETE'),
                108 => array('title' => 'Electronic Insurance Regulations Issued', 'category' => 'LEGAL & REG'),
                109 => array('title' => 'Insurance Claims Management: Using the Most Appropriate Governing Law & Jurisdiction Clause in Reinsurance Contracts', 'category' => 'News'),
                110 => array('title' => '100% Foreign Ownership for the Maritime Sector in the UAE', 'category' => 'Reports'),
                111 => array('title' => 'DIFC Data Protection Law 2020', 'category' => 'News'),
                112 => array('title' => 'Health Insurance in the UAE - Capitation Schemes Confirmed Unlawful by the Insurance Authority', 'category' => 'News'),
                113 => array('title' => 'Dubai Drone Law announced to provide new opportunities and boost Dubai’s aviation sector', 'category' => 'News'),
                114 => array('title' => 'Virtual Hearings in arbitration: are they as effective as they are efficient?', 'category' => 'Reports'),
                115 => array('title' => 'Plant Varieties Protection in the UAE', 'category' => 'Reports'),
                116 => array('title' => 'Insurance Distribution in Europe & Wider Implications for the Middle East Insurance Market', 'category' => 'Reports'),
                117 => array('title' => 'UAE Insurance Authority & Securities & Commodities Authority Merge', 'category' => 'Reports'),
                118 => array('title' => 'Economic Substance Regulations for IP Rights Holders', 'category' => 'Reports'),
                119 => array('title' => 'Dubai Land Department (DLD) Announces the Fractional Title Deed initiative', 'category' => 'News'),
                120 => array('title' => 'Beirut Blast - You May Have Coverage for Damage', 'category' => 'Reports'),
                121 => array('title' => '100% Foreign Ownership now Allowed in Mainland Ras Al Khaimah', 'category' => 'Reports'),
                122 => array('title' => 'Aftermath of the Beirut Port Explosion: Between Law, Insurance & Sympathy', 'category' => 'Reports'),
                123 => array('title' => 'Arsalan Tariq moves to BSA’s Muscat office as a Partner and Head of the Corporate Department', 'category' => 'Reports'),
                124 => array('title' => '', 'category' => 'DELETE'),
                125 => array('title' => 'What kind of Legal Protection do Employees have against Salary Cuts and Redundancy in the UAE?', 'category' => 'Reports'),
                126 => array('title' => 'Brief on the new UAE Cabinet Decision No 58/2020 on the Regulation of the Procedures of Real Beneficiary', 'category' => 'News'),
                127 => array('title' => 'Recent Legal Developments in Business Interruption Insurance – Coverage Issues and Wider Implications perhaps for the Middle East', 'category' => 'Reports'),
                128 => array('title' => 'Pay-when paid clause defeated due to lack of pursuit', 'category' => 'Reports'),
                129 => array('title' => 'Legal Brief', 'category' => 'LEGAL & REG'),
                130 => array('title' => 'Bankruptcy Law Update', 'category' => 'LEGAL & REG'),
                131 => array('title' => 'BSA October Newsflash', 'category' => 'LEGAL & REG'),
                132 => array('title' => 'Dubai Introduces the Opportunity to Retire in the Sun', 'category' => 'LEGAL & REG'),
                133 => array('title' => 'BSA The View', 'category' => 'LEGAL & REG'),
                134 => array('title' => 'BSA The View', 'category' => 'LEGAL & REG'),
                135 => array('title' => 'JAFZA Announces Economic Substance Regulations Notification Deadline', 'category' => 'LEGAL & REG'),
                136 => array('title' => 'Economic Substance Report filing reminder', 'category' => 'LEGAL & REG'),
                137 => array('title' => 'Dubai steps up rollout of the Pfizer jab', 'category' => 'LEGAL & REG'),
                138 => array('title' => 'UAE Insurance Regulatory Round-up 2020', 'category' => 'News'),
                139 => array('title' => 'New Developments - Foreign Direct Investment in the UAE', 'category' => 'Reports'),
                140 => array('title' => 'Munir Suboh selected as a MENA Super 50 Lawyer', 'category' => 'Reports'),
                141 => array('title' => 'Meritas Capability Webinar: The Road to 2030 - An overlook on Saudi Arabia and its business, investment and legal implications', 'category' => 'Events'),
                142 => array('title' => 'Clarified legal landscape for KSA offers investors potential new playground', 'category' => 'News'),
                143 => array('title' => 'Recent Legal Developments in Business Interruption Insurance – Coverage Issues and Wider Implications perhaps for the Middle East', 'category' => 'News'),
                144 => array('title' => 'Real Estate Crowdfunding as Potential Opportunity for Investors and Entrepreneurs', 'category' => 'News'),
                145 => array('title' => 'New Developments - Foreign Direct Investment in the UAE', 'category' => 'Reports'),
                146 => array('title' => 'Alcohol and Trademarks Practice in UAE', 'category' => 'News'),
                147 => array('title' => 'Saudi Central Bank Launches Open Banking Initiative', 'category' => 'News'),
                148 => array('title' => 'Dubai Art Scene - How to Improve the Picture', 'category' => 'News'),
                149 => array('title' => 'Nuts and Bolts of Decision No. 26 of 2020 Covering Small Claims Tribunals', 'category' => 'News'),
                150 => array('title' => 'Draft Insurance Broker Regulations: What could change?', 'category' => 'News'),
                151 => array('title' => 'UAE VAT Laws and Export of Services', 'category' => 'News'),
                152 => array('title' => 'The end of the GCC Patent: what next?', 'category' => 'Reports'),
                153 => array('title' => 'Dubai Rent Freeze', 'category' => 'News'),
                154 => array('title' => 'New SME Market Conduct Regulation', 'category' => 'News'),
                155 => array('title' => 'Pay-when-paid clause can be defeated', 'category' => 'Reports'),
                156 => array('title' => 'Update Saudi Companies Law', 'category' => 'News'),
                157 => array('title' => 'Movables Pledge Law: New Executive Regulations', 'category' => 'News'),
                158 => array('title' => 'UAE Foundations in ADGM, DIFC & RAK ICC', 'category' => 'News'),
                159 => array('title' => 'Executive regulations for new Movables Pledge Law: What do they mean for the secured lenders?', 'category' => 'News'),
                160 => array('title' => 'How the new Consumer Protection Regulations impact insureds', 'category' => 'News'),
                161 => array('title' => 'Family Business Regulations in the UAE', 'category' => 'News'),
                162 => array('title' => 'Companies in violation of UBO Regulations may face up to AED 100,000 fine', 'category' => 'News'),
                163 => array('title' => 'Revised Central Bank Guidance regarding Saving and Investment Insurance', 'category' => 'News'),
                164 => array('title' => 'BSA Named Regional Law Firm of the Year', 'category' => 'News'),
                165 => array('title' => 'Cartels Practice Guide 2021', 'category' => 'Reports'),
                166 => array('title' => 'Important Change to Interest Rate by Dubai Court', 'category' => 'News'),
                167 => array('title' => 'GCC Restructuring & Insolvency Report', 'category' => 'Reports'),
                168 => array('title' => 'Recent Updates and Developments in UAE Intellectual Property Practice and Procedures', 'category' => 'News'),
                169 => array('title' => 'Health Insurance in the UAE and the Concepts of Medical Benefits Trusts', 'category' => 'News'),
                170 => array('title' => 'Legal 500: UAE Litigation Chapter 2021', 'category' => 'Reports'),
                171 => array('title' => 'DIFC Wills', 'category' => 'News'),
                172 => array('title' => 'New Laws, Agile Teams', 'category' => 'Reports'),
                173 => array('title' => 'What FIDIC\'s New Green Book Holds', 'category' => 'Reports'),
                174 => array('title' => 'The UAE\'s Foundations Regimes', 'category' => 'Reports'),
                175 => array('title' => 'Dubai DIFC\'s new IP regulations provide welcome clarity', 'category' => 'News'),
                176 => array('title' => 'How the UAE ITC Data Law impacts businesses', 'category' => 'News'),
                177 => array('title' => 'BSA named a top foreign firm for India related work', 'category' => 'News'),
                178 => array('title' => 'Insurance In UAE: Ready For The Big Leap', 'category' => 'Reports'),
                179 => array('title' => 'Bankruptcy Update: ‘Emergency Financial Crisis’ Period Ends', 'category' => 'News'),
                180 => array('title' => 'Avoiding Insolvency – the Role of Good Governance', 'category' => 'News'),
                181 => array('title' => 'BSA Bankruptcy Series', 'category' => 'News'),
                182 => array('title' => 'Explained: The UAE Golden, Green and Freelance Visas', 'category' => 'News'),
                183 => array('title' => 'UAE Project of the 50: New Data Law', 'category' => 'News'),
                184 => array('title' => 'The Concept of Healthcare / Medical Trust', 'category' => 'Reports'),
                185 => array('title' => 'Emirates Insurance Association: Third Annual Motor Conference', 'category' => 'Events'),
                186 => array('title' => 'AML Regulations and the Impact on the Insurance Industry', 'category' => 'Events'),
                187 => array('title' => 'Insolvency & Restructuring in the UAE: Practical Considerations for Corporates', 'category' => 'Events'),
                188 => array('title' => 'Emirates Maritime Arbitration Centre and DIFC Arbitration Institute merge into Dubai International Arbitration Centre', 'category' => 'News'),
                189 => array('title' => 'The New UAE Health Data Law - an In-Depth Look', 'category' => 'Reports'),
                190 => array('title' => 'Dubai\'s recently established money laundering court explained', 'category' => 'News'),
                191 => array('title' => 'The Middle East: Plugged into Renewable Energy', 'category' => 'News'),
                192 => array('title' => 'Rent increases and eviction notices: what are your rights?', 'category' => 'News'),
                193 => array('title' => 'Chambers & Partners: UAE Insurance Litigation 2021', 'category' => 'Reports'),
                194 => array('title' => 'DIFC Employment Law\'s new Amendments Explained', 'category' => 'News'),
                195 => array('title' => 'Renting in Dubai? Here are your rights as a tenant', 'category' => 'Reports'),
                196 => array('title' => 'Central Bank Digital Currencies', 'category' => 'News'),
                197 => array('title' => 'Testing the legal basis of the lack-of-pursuit argument', 'category' => 'News'),
                198 => array('title' => 'The Future of FinTech', 'category' => 'Events'),
                199 => array('title' => 'Precautions to consider prior to investing in a UAE business', 'category' => 'News'),
                200 => array('title' => 'Considering fractional property ownership using tokens?', 'category' => 'News'),
                201 => array('title' => 'Marka Group Bankruptcy Update', 'category' => 'News'),
                202 => array('title' => 'Tokenization of real estate assets: explained', 'category' => 'News'),
                203 => array('title' => 'The Future of Finance: Digital transformation, innovation, and the FinTech ecosystem in the GCC', 'category' => 'Reports'),
                204 => array('title' => 'Keeping it Civil: The latest key amendments to the UAE Civil Procedures Law', 'category' => 'Reports'),
                205 => array('title' => 'The UAE\'s New SCA Licensing Regime', 'category' => 'Reports'),
                206 => array('title' => 'ICLG: UAE Franchise Laws and Regulations 2022', 'category' => 'Reports'),
                207 => array('title' => 'A Clear Score Sheet: The key provisions of recent AML Guidance', 'category' => 'Reports'),
                208 => array('title' => 'Legal 500: UAE International Arbitration Chapter', 'category' => 'Reports'),
                209 => array('title' => 'UAE Bankruptcy Law Amendments: Directors\' and Managers\' Personal Liability', 'category' => 'News'),
                210 => array('title' => 'White paper: Cybersecurity for SMEs in a Post-Covid era', 'category' => 'Reports'),
                211 => array('title' => 'SPAC\'s in the UAE: How it works', 'category' => 'News'),
                212 => array('title' => 'The practical obstacles in leading UAE construction claims', 'category' => 'News'),
                213 => array('title' => 'SME 10x: The Future of Finance for SMEs', 'category' => 'News'),
                214 => array('title' => 'Have SPAC listings hit ‘the wall’?', 'category' => 'News'),
                215 => array('title' => 'The Future of Finance: Digital transformation, innovation, and the FinTech ecosystem in the GCC', 'category' => 'Reports'),
                216 => array('title' => 'India renews its Green pledge with a commitment to triple Renewable Energy generation by 2030', 'category' => 'News'),
                217 => array('title' => 'The Laws and Regulations governing Banking in Oman', 'category' => 'Reports'),
                218 => array('title' => 'The Laws and Regulations governing Islamic Finance in Oman', 'category' => 'Reports'),
                219 => array('title' => 'Latest amendments in relation to bounced cheques in the UAE', 'category' => 'News'),
                220 => array('title' => 'Oman Firm of the Year: The Oath Legal Awards', 'category' => 'News'),
                221 => array('title' => 'Modernized System of Intellectual Property Protection in the UAE', 'category' => 'News'),
                222 => array('title' => '2021 in Review: The LEGAL & REGulatory Changes Impacting Intellectual Property in the UAE', 'category' => 'News'),
                223 => array('title' => '2021 in Review: The LEGAL & REGulatory Changes Impacting Bankruptcy and Insolvency in the UAE', 'category' => 'News'),
                224 => array('title' => '2021 in Review: The LEGAL & REGulatory Changes Impacting FinTech in the UAE', 'category' => 'News'),
                225 => array('title' => '2021 in Review: The LEGAL & REGulatory Changes Impacting Commercial Companies in the UAE', 'category' => 'News'),
                226 => array('title' => '2021 in Review: The LEGAL & REGulatory Changes Impacting Insurance This Year', 'category' => 'News'),
                227 => array('title' => '2021 in Review: The LEGAL & REGulatory Changes Impacting Employment in the UAE', 'category' => 'News'),
                228 => array('title' => 'Supporting the French Business Council in Erbil, Iraq.', 'category' => 'News'),
                229 => array('title' => 'BSA Erbil Office Expands', 'category' => 'News'),
                230 => array('title' => 'BSA Earn Recertification in Meritas', 'category' => 'News'),
                231 => array('title' => 'Corporate Counsel Middle East Legal Forum 2014', 'category' => 'Events'),
                232 => array('title' => '9th Annual World Takaful Conference', 'category' => 'Events'),
                233 => array('title' => 'In-House Congress Dubai', 'category' => 'Events'),
                234 => array('title' => '4th Annual Middle East & Africa Insurance Summit', 'category' => 'Events'),
                235 => array('title' => '2014 Q1 Review – Sector Spotlight On The Current Climate In Dubai', 'category' => 'LEGAL & REG'),
                236 => array('title' => 'A Lucrative Market – Growth of the Middle East Reinsurance Market', 'category' => 'LEGAL & REG'),
                237 => array('title' => 'Rebuilding Trust Between The Contractors and Developers In The UAE', 'category' => 'LEGAL & REG'),
                238 => array('title' => 'Compulsory Retention Rates Across The MENA Region', 'category' => 'LEGAL & REG'),
                239 => array('title' => 'Sound Consumer Protection – A Serious Issue In The UAE and GCC', 'category' => 'LEGAL & REG'),
                240 => array('title' => 'Counting The Costs – The Abu Dhabi Government Has Amended Its Judicial Fees Regime', 'category' => 'News'),
                241 => array('title' => 'The Dubai International Financial Centre Arbitration Law', 'category' => 'LEGAL & REG'),
                242 => array('title' => 'Islamic Finance: A Practical Guide', 'category' => 'Reports'),
                243 => array('title' => 'Insurance and Reinsurance In The MENA Region: A Legal and Regulatory Guide', 'category' => 'Reports'),
                244 => array('title' => 'M&A in the Middle East: A Practical Regional Guide', 'category' => 'Reports'),
                245 => array('title' => 'Getting The Deal Through: Real Estate', 'category' => 'Reports'),
                246 => array('title' => 'Insurance & Reinsurance Law & Regulation - Jurisdictional Comparisons: First Edition', 'category' => 'Reports'),
                247 => array('title' => 'Contract Watch: Forfeiture Clauses', 'category' => 'LEGAL & REG'),
                248 => array('title' => 'Tax matters In The UAE For French Individuals and Companies', 'category' => 'LEGAL & REG'),
                249 => array('title' => 'Saudi Arabia: Government Paves Way For New Centre', 'category' => 'LEGAL & REG'),
                250 => array('title' => 'Messages To Take Home From Dubai’s Arbitration Week', 'category' => 'News'),
                251 => array('title' => 'Corporate Governance Laws', 'category' => 'LEGAL & REG'),
                252 => array('title' => 'Insurance Regulations In The GCC - Risky Business', 'category' => 'LEGAL & REG'),
                253 => array('title' => 'Lloyd’s and The DIFC – Making It A Suitable Match', 'category' => 'LEGAL & REG'),
                254 => array('title' => 'BSA, JAFZA and Dumon & Partners Host Reception in Paris, To Celebrate the French Emirati Friendship', 'category' => 'LEGAL & REG'),
                255 => array('title' => 'A Long Awaited Initiative: Measures To Enhance The Regulatory Framework In The UAE', 'category' => 'LEGAL & REG'),
                256 => array('title' => 'Building A Stronger Reinsurance Industry', 'category' => 'LEGAL & REG'),
                257 => array('title' => 'Paving the Future Path of Arab Women', 'category' => 'LEGAL & REG'),
                258 => array('title' => 'Regulatory Enhancements Push UAE Industry Forward', 'category' => 'LEGAL & REG'),
                259 => array('title' => 'Back To Basics: Sukuk Structures', 'category' => 'LEGAL & REG'),
                260 => array('title' => 'BSA advises Dar Al Sharia on the set up of their DIFC entity', 'category' => 'News'),
                261 => array('title' => 'Bancassurance in the GCC: The Need for Change', 'category' => 'LEGAL & REG'),
                262 => array('title' => 'New Qatar Banking Law Paves Way For A Single Financial Regulator', 'category' => 'LEGAL & REG'),
                263 => array('title' => 'Country Feature - UAE: Exciting Growth Opportunities', 'category' => 'LEGAL & REG'),
                264 => array('title' => 'BSA Advises Ingen Ideas on Acquisition by Foster Wheeler AG', 'category' => 'News'),
                265 => array('title' => 'A Matter Of Interpretation … Decisions On Arbitration Clauses In the UAE', 'category' => 'LEGAL & REG'),
                266 => array('title' => 'Tax Regime In The United Arab Emirates', 'category' => 'LEGAL & REG'),
                267 => array('title' => 'Sector Feature - Project and Infrastructure Finance: A Growing Trend', 'category' => 'LEGAL & REG'),
                268 => array('title' => 'Drafting Dispute Resolution Clauses in the UAE', 'category' => 'LEGAL & REG'),
                269 => array('title' => 'Strengthening Ties between The UK and Dubai', 'category' => 'LEGAL & REG'),
                270 => array('title' => 'Compelling Ingenuity in a Legal Framework', 'category' => 'LEGAL & REG'),
                271 => array('title' => 'Mandatory Healthcare In The MENA Region', 'category' => 'LEGAL & REG'),
                272 => array('title' => 'UAE - The Laws You Need To Know', 'category' => 'LEGAL & REG'),
                273 => array('title' => 'MENA Insurance Potential and The Regulatory Role', 'category' => 'LEGAL & REG'),
                274 => array('title' => 'M&A In The Insurance Sector', 'category' => 'LEGAL & REG'),
                275 => array('title' => 'UAE Country Report: Dubai A Global Capital For Sukuk', 'category' => 'LEGAL & REG'),
                276 => array('title' => 'Country Report - UAE: Leading The Way In The Middle East', 'category' => 'LEGAL & REG'),
                277 => array('title' => 'Country Report: Status Evaluation Of The Islamic Finance Industry In Lebanon', 'category' => 'LEGAL & REG'),
                278 => array('title' => 'UAE Sector Feature: DFM Issues New Standard For Issuing, Acquiring, and Trading Sukuk', 'category' => 'News'),
                279 => array('title' => 'UAE Country Report: SCA Sukuk Announcement', 'category' => 'LEGAL & REG'),
                280 => array('title' => 'Country Report - UAE: New Standards for Sukuk', 'category' => 'LEGAL & REG'),
                281 => array('title' => 'Country Report: Unpopularity of Islamic Banking In Lebanon', 'category' => 'LEGAL & REG'),
                282 => array('title' => 'UAE Country report: Dubai Continues Its Ambitious Growth Path', 'category' => 'LEGAL & REG'),
                283 => array('title' => 'UAE Country Report : 2014 The Year For Takaful', 'category' => 'LEGAL & REG'),
                284 => array('title' => 'Staying Connected in KSA', 'category' => 'LEGAL & REG'),
                285 => array('title' => 'Oil Contracts and Policy in Iraq', 'category' => 'LEGAL & REG'),
                286 => array('title' => 'UAE Brokers Face Tougher Insurance Authority Rules', 'category' => 'LEGAL & REG'),
                287 => array('title' => 'Interview with CEO Report 2013 - Jimmy Haoula', 'category' => 'News'),
                288 => array('title' => 'Dispute Resolution in Dubai', 'category' => 'LEGAL & REG'),
                289 => array('title' => 'MENA Insurance Live 2014', 'category' => 'Events'),
                290 => array('title' => '2nd Annual International Arbitration Summit', 'category' => 'Events'),
                291 => array('title' => 'Emirates Insurance Authority Seminar', 'category' => 'Events'),
                292 => array('title' => 'World FM Congress 2014', 'category' => 'Events'),
                293 => array('title' => 'BSA speak on Alternative risk transfer (ART) at MENA IR breakfast briefing', 'category' => 'Events'),
                294 => array('title' => 'BSA and ISFIN host Islamic business networking reception in Dubai.', 'category' => 'Events'),
                295 => array('title' => 'Antonios Dimitracopoulos talks at Society of Construction Law (Gulf) Event', 'category' => 'Events'),
                296 => array('title' => '5th Annual Middle East & Africa Insurance Summit', 'category' => 'Events'),
                297 => array('title' => 'Middle East: UAE Report - The Boom Is Back', 'category' => 'LEGAL & REG'),
                298 => array('title' => 'The Draft Investor Protection Law: Pros and Cons', 'category' => 'LEGAL & REG'),
                299 => array('title' => 'Proposed Bankruptcy Law; Legal Sustainability or Loophole?', 'category' => 'LEGAL & REG'),
                300 => array('title' => '9th Annual In-House Congress Middle East', 'category' => 'Events'),
                301 => array('title' => 'Professional Liability: A Prudent Business Plan', 'category' => 'LEGAL & REG'),
                302 => array('title' => 'Where Are Insurance Regulations Headed?', 'category' => 'LEGAL & REG'),
                303 => array('title' => 'Dubai Marina Torch Fire - What\'s Next? Who Pays?', 'category' => 'LEGAL & REG'),
                304 => array('title' => 'Prevention Is Better Than Cure - anti-Corruption In The Region', 'category' => 'LEGAL & REG'),
                305 => array('title' => 'How to Navigate Conflict of Laws for International Employees - UAE', 'category' => 'LEGAL & REG'),
                306 => array('title' => 'Insurance Authority Prudential Regulations', 'category' => 'LEGAL & REG'),
                307 => array('title' => 'International investments in UAE - Leading the way in the Middle East', 'category' => 'Events'),
                308 => array('title' => 'International Corporate Procedures (ICP): UAE update 79', 'category' => 'LEGAL & REG'),
                309 => array('title' => 'Towards Developing A Global Islamic Economy', 'category' => 'LEGAL & REG'),
                310 => array('title' => 'The UAE Progresses To Be A Global Hub For The Islamic Economy', 'category' => 'LEGAL & REG'),
                311 => array('title' => 'Belgian Economic Mission to the UAE - breakfast in presence of HRH Princess Astrid, Representative of His Majesty the King and Ministers', 'category' => 'Events'),
                312 => array('title' => 'Jafza Road Show in Paris', 'category' => 'Events'),
                313 => array('title' => 'New Federal Commercial Companies Law in the UAE', 'category' => 'News'),
                314 => array('title' => 'BSA in Association With Jebel Ali Free Zone (JAFZA) Hosts a Business Road-show For 300 Leading French Companies', 'category' => 'Events'),
                315 => array('title' => 'Belgian Economic Mission to the UAE - seminar in the presence of HRH Princess Astrid of Belgium, Representative of His Majesty the King and Ministers', 'category' => 'Events'),
                316 => array('title' => 'Guide To Employment Law On A Business Sale: UAE Chapter', 'category' => 'Reports'),
                317 => array('title' => 'The UAE — A key Participant In The Islamic Finance Economy', 'category' => 'Reports'),
                318 => array('title' => '10th Annual World Takaful Conference', 'category' => 'Events'),
                319 => array('title' => 'Student lecture at American university in Dubai (AUD)', 'category' => 'Events'),
                320 => array('title' => 'Tightening The Screw - Insurance Authority Prudential Regulations', 'category' => 'LEGAL & REG'),
                321 => array('title' => 'UAE Practice On Restrictive Provisions and Penalty Clauses', 'category' => 'LEGAL & REG'),
                322 => array('title' => 'Speaking at Interpol regional conference', 'category' => 'Events'),
                323 => array('title' => 'BSA Launch Event: Introducing New Senior Partner, Dr. Ahmad Bin Hezeem', 'category' => 'News'),
                324 => array('title' => 'Recent Islamic Finance Developments In The UAE', 'category' => 'LEGAL & REG'),
                325 => array('title' => 'Senior Associate, Barry Greenberg attends Insurance Leaders Forum 2015', 'category' => 'Events'),
                326 => array('title' => 'Attending MEED Construction Leadership Summit 2015', 'category' => 'Events'),
                327 => array('title' => 'Proposals For A Subcontractor’s Escape From A Conditional Clause', 'category' => 'LEGAL & REG'),
                328 => array('title' => 'Water Supply Augmentation Project: Electronic Trading System', 'category' => 'LEGAL & REG'),
                329 => array('title' => 'BSA Supports Dubai Autism Centre Offering Pro Bono Legal Services', 'category' => 'News'),
                330 => array('title' => 'Visit to Dubai Autism Centre', 'category' => 'Events'),
                331 => array('title' => 'Insurance Regulation – Finding a Balance', 'category' => 'LEGAL & REG'),
                332 => array('title' => 'Radio Interview - Cyber laws in the UAE', 'category' => 'Events'),
                333 => array('title' => 'BSA Client Event Report: Moving Forward', 'category' => 'News'),
                334 => array('title' => 'Legal update: Insurance Authority Resolution', 'category' => 'LEGAL & REG'),
                335 => array('title' => 'Special Report: UAE Is Hot Right Now', 'category' => 'News'),
                336 => array('title' => 'Panel member at the PGA East - New York event', 'category' => 'Events'),
                337 => array('title' => 'Saudi Arabia Considers Allowing Full Foreign Ownership Of Retail and Wholesale Business', 'category' => 'LEGAL & REG'),
                338 => array('title' => 'BSA Announces Paris Tie-Up', 'category' => 'News'),
                339 => array('title' => '5th UAE Government Future Leaders Conference', 'category' => 'Events'),
                340 => array('title' => 'BSA Annonce Son Développement à Paris', 'category' => 'News'),
                341 => array('title' => 'Iran - Early Bird Benefits', 'category' => 'LEGAL & REG'),
                342 => array('title' => 'Insurers should Focus On Core Strengths', 'category' => 'LEGAL & REG'),
                343 => array('title' => 'Speaking Event by Insurance Business Group (IBG)', 'category' => 'Events'),
                344 => array('title' => 'Legal seminar by the faculty of law - University of Dubai', 'category' => 'Events'),
                345 => array('title' => 'UAE Insurance Brokerage Regulations – Challenges in Reforming the Marketplace', 'category' => 'LEGAL & REG'),
                346 => array('title' => 'Speaking engagement at the Islamic Insurer’s Association of London (IIAL)', 'category' => 'Events'),
                347 => array('title' => 'Optimal Successions in Dubai & the UAE', 'category' => 'Events'),
                348 => array('title' => 'Legal seminar at the Abu Dhabi Chamber', 'category' => 'Events'),
                349 => array('title' => 'Awarding Legal Costs In Arbitration: A UAE Perspective', 'category' => 'LEGAL & REG'),
                350 => array('title' => 'New Year\'s Eve: The Dubai Address Fire', 'category' => 'LEGAL & REG'),
                351 => array('title' => 'BSA\'s Firm Focus', 'category' => 'LEGAL & REG'),
                352 => array('title' => 'Growth Of The Islamic Banking Sector In The UAE Is On Track', 'category' => 'Reports'),
                353 => array('title' => 'Medical Data Protection', 'category' => 'LEGAL & REG'),
                354 => array('title' => 'The UAE Islamic Finance Industry Hits The Ground Running In 2016', 'category' => 'News'),
                355 => array('title' => 'DIFC Insurance Regulations – Proposed Amendments', 'category' => 'Events'),
                356 => array('title' => 'MENAIR Insurance Awards 2016', 'category' => 'News'),
                357 => array('title' => 'The Oath Middle East Legal Awards 2015', 'category' => 'Events'),
                358 => array('title' => 'In-House Congress Dubai 2016', 'category' => 'Events'),
                359 => array('title' => 'Structuring real estate ownership in Dubai: Pre-conference seminar', 'category' => 'Events'),
                360 => array('title' => 'Structuring real estate ownership in Dubai - Conference (Part 1)', 'category' => 'Events'),
                361 => array('title' => 'Structuring real estate ownership in Dubai - Conference (Part 2)', 'category' => 'Events'),
                362 => array('title' => 'Mena Insurance Awards 2016', 'category' => 'Events'),
                363 => array('title' => 'Grand opening of the 34th Annual Exhibition - Noah\'s Ark', 'category' => 'Events'),
                364 => array('title' => 'CIS GBF 2016', 'category' => 'Events'),
                365 => array('title' => 'SAGIA Implements A Fast Track Registration Process In A Bid To Attract Sizable Foreign Investments', 'category' => 'Reports'),
                366 => array('title' => 'Saudi Arabia Revamps Its Company Law', 'category' => 'LEGAL & REG'),
                367 => array('title' => 'When Your Rented Apartment Goes Up In Smoke', 'category' => 'LEGAL & REG'),
                368 => array('title' => 'D&O Liability Insurance Conference', 'category' => 'Events'),
                369 => array('title' => 'INTAX Expo Global Summit 2016', 'category' => 'Events'),
                370 => array('title' => 'Takaful in Iran: New Frontier or False Dawn?', 'category' => 'News'),
                371 => array('title' => 'Corporate seminar in Prague', 'category' => 'Events'),
                372 => array('title' => 'Women entrepreneurs seminar in Stockholm', 'category' => 'News'),
                373 => array('title' => 'Guest speaker at British University in Dubai (BUiD)', 'category' => 'Events'),
                374 => array('title' => 'Porte Ouverte Sur l’Iran', 'category' => 'News'),
                375 => array('title' => 'Mergers and Acquisitions - The Legal Considerations', 'category' => 'News'),
                376 => array('title' => 'Corporate Counsel Middle East Awards 2016', 'category' => 'Events'),
                377 => array('title' => 'The UAE Sets Up National Regulatory Shariah Authority', 'category' => 'Events'),
                378 => array('title' => 'VAT In The UAE and GCC', 'category' => 'News'),
                379 => array('title' => 'Business Forum in Turkey', 'category' => 'News'),
                380 => array('title' => 'BSA Advises GCC Sushi Shop Franchisee On Buyout', 'category' => 'News'),
                381 => array('title' => 'Gulf Shipping Insurance Trends in 2016', 'category' => 'Events'),
                382 => array('title' => 'Insurance event in London', 'category' => 'Events'),
                383 => array('title' => 'European and Middle East Guide To Litigation, Arbitration and ADR', 'category' => 'News'),
                384 => array('title' => 'Implementation of the Value Added Tax in the United Arab Emirates', 'category' => 'News'),
                385 => array('title' => 'B-2-B Dispute Resolution', 'category' => 'News'),
                386 => array('title' => 'Insurance Trends In The Middle East', 'category' => 'Reports'),
                387 => array('title' => 'New Measures Seek To Address Fire Safety Risks At Dubai’s High-Rise Towers', 'category' => 'News'),
                388 => array('title' => 'Key legal aspects of real estate', 'category' => 'Events'),
                389 => array('title' => 'UPDATE ON BANKRUPTCY LAW PROJECT IN THE UNITED ARAB EMIRATES', 'category' => 'LEGAL & REG'),
                390 => array('title' => 'New UAE Funds Regulation and its Effect on Insurers', 'category' => 'News'),
                391 => array('title' => '', 'category' => 'DELETE'),
                392 => array('title' => 'Update on Bankruptcy Law Project in the United Arab Emirates', 'category' => 'News'),
                393 => array('title' => 'Measuring the Damage', 'category' => 'News'),
                394 => array('title' => 'Mortgage Basics When Building a Home', 'category' => 'News'),
                395 => array('title' => 'UAE\'s First VAT\'S App', 'category' => 'LEGAL & REG'),
                396 => array('title' => 'Michael Kortbawi Presented The Keynote Speech At The UAE Run-Off Forum 2016', 'category' => 'News'),
                397 => array('title' => 'TENANTS’ RIGHTS IN THE EVENT OF THE FIRE', 'category' => 'DELETE'),
                398 => array('title' => 'Tenants Rights In The Event Of Fire', 'category' => 'News'),
                399 => array('title' => 'John Peacock presented on the implementation of the Value Added Tax in the United Arab Emirates on the 1st of January 2018', 'category' => 'DELETE'),
                400 => array('title' => 'The Islamic Economy Problems & Potential', 'category' => 'LEGAL & REG'),
                401 => array('title' => 'BSA: Proud sponsor of a flagship conference on International Arbitration', 'category' => 'Events'),
                402 => array('title' => 'Lost in Translation: Avoiding Regulatory Pitfalls', 'category' => 'LEGAL & REG'),
                403 => array('title' => 'The Judicial System of the Kingdom of Saudi Arabia', 'category' => 'LEGAL & REG'),
                404 => array('title' => 'International Comparative Legal Guide – Real Estate 2017', 'category' => 'Reports'),
                405 => array('title' => 'Lost in Translation', 'category' => 'LEGAL & REG'),
                406 => array('title' => 'Implementation of VAT in UAE: 10 things you need to know', 'category' => 'LEGAL & REG'),
                407 => array('title' => 'Here’s something you may not have realised about sick leave in the UAE', 'category' => 'LEGAL & REG'),
                408 => array('title' => 'Gearing up for VAT', 'category' => 'DELETE'),
                409 => array('title' => 'Gearing up for VAT', 'category' => 'DELETE'),
                410 => array('title' => 'Gearing up for VAT', 'category' => 'Events'),
                411 => array('title' => 'Dubai’s new standard tenancy contract: Points to consider', 'category' => 'LEGAL & REG'),
                412 => array('title' => 'BSA Ahmed Bin Hezeem & Associates Celebrates Its 17th Anniversary on the 4th April 2017', 'category' => 'News'),
                413 => array('title' => 'DIFC Employment law Protection For Employees', 'category' => 'LEGAL & REG'),
                414 => array('title' => 'How VAT Legislation will change the business set up model in the UAE', 'category' => 'LEGAL & REG'),
                415 => array('title' => 'Consumer Centred - Lexis Middle East Law Alert, November/December 2016', 'category' => 'LEGAL & REG'),
                416 => array('title' => 'Consumer Centred - Lexis Middle East Law -November/December 2016', 'category' => 'LEGAL & REG'),
                417 => array('title' => 'Consumer Centred: Barry Greenberg', 'category' => 'LEGAL & REG'),
                418 => array('title' => 'Case Focus: DIFC Small Claims Tribunal case decided', 'category' => 'LEGAL & REG'),
                419 => array('title' => 'Dr. Ahmad’s interview with Emirates Law p48-51', 'category' => 'LEGAL & REG'),
                420 => array('title' => 'The Road To Rescue and Recovery', 'category' => 'LEGAL & REG'),
                421 => array('title' => 'Building fires send insurance cost through the roof', 'category' => 'LEGAL & REG'),
                422 => array('title' => 'Exclusive Interview with Rima Mrad, Partner at BSA Ahmad Bin Hezeem & Associates LLP.', 'category' => 'News'),
                423 => array('title' => 'This is going to change how you take sick leave in Dubai', 'category' => 'LEGAL & REG'),
                424 => array('title' => 'If You Are Asked To Vacate', 'category' => 'LEGAL & REG'),
                425 => array('title' => 'Employee Benefits and Services, Nadim Bardawil\'s Interview with Dubai Eye', 'category' => 'News'),
                426 => array('title' => 'What Are The Laws When It Comes To Charity and Volunteering In The UAE', 'category' => 'LEGAL & REG'),
                427 => array('title' => 'Warning to Contractors Dealing with GCC Governments', 'category' => 'LEGAL & REG'),
                428 => array('title' => 'Halal Certification And The Need To Set Unified International Standards', 'category' => 'LEGAL & REG'),
                429 => array('title' => 'Overview of the Proceedings and Reasons for Delay within the Court of Cassation', 'category' => 'LEGAL & REG'),
                430 => array('title' => 'UAE: Proposed data protection amendments “seek to close loopholes”', 'category' => 'LEGAL & REG'),
                431 => array('title' => 'UAE chapter of the ICLG Trade Marks Guide 2017', 'category' => 'LEGAL & REG'),
                432 => array('title' => '7 things you might not realise can get you into trouble in the UAE', 'category' => 'LEGAL & REG'),
                433 => array('title' => 'the thing about...Ahmad Bin Hezeem', 'category' => 'LEGAL & REG'),
                434 => array('title' => 'Abu Dhabi Global Market Two Tier Structure', 'category' => 'LEGAL & REG'),
                435 => array('title' => 'BSA Ahmad Bin Hezeem & Associates LLP Signs an Agreement with JAFZA to Empower Emiratisation under Tumoohi Project', 'category' => 'News'),
                436 => array('title' => 'Introduction Of Value Added Tax (VAT) In The Kingdom Of Saudi Arabia', 'category' => 'LEGAL & REG'),
                437 => array('title' => 'BSA & Insurance Authority', 'category' => 'Events'),
                438 => array('title' => 'Is eating in public during the day over Ramadan a crime in the UAE?', 'category' => 'LEGAL & REG'),
                439 => array('title' => 'Lexis Nexis Middle East Law Alerts Case Focus on DIFC Case No. 006/2016', 'category' => 'LEGAL & REG'),
                440 => array('title' => 'Lexis Nexis Middle East Law Alerts Interview with Michael Kortbawi for Practitioner Perspective', 'category' => 'LEGAL & REG'),
                441 => array('title' => 'Fines, jail for rogue driving in Oman.', 'category' => 'LEGAL & REG'),
                442 => array('title' => 'UAE health care law opens new vistas', 'category' => 'LEGAL & REG'),
                443 => array('title' => 'Three ways businesses can limit their legal exposure in the UAE', 'category' => 'LEGAL & REG'),
                444 => array('title' => '', 'category' => 'DELETE'),
                445 => array('title' => 'Reasoned Awards and Expert opinions as a possible prevention to Article 257 being triggered', 'category' => 'LEGAL & REG'),
                446 => array('title' => 'BSA Ahmad Bin Hezeem & Associates LLP under the patronage of the UAE Insurance Authority hosted an exclusive discussion on the latest insurance regulations development', 'category' => 'News'),
                447 => array('title' => 'The Limits Of A Rent Increase', 'category' => 'LEGAL & REG'),
                448 => array('title' => 'How reasoned arbitration awards and expert opinions may help prevent Article 257 of the Penal Code being triggered', 'category' => 'LEGAL & REG'),
                449 => array('title' => 'Saudi Arabia: GAZT Issue Draft VAT Law', 'category' => 'LEGAL & REG'),
                450 => array('title' => 'Is it legal for companies in Dubai to send me spam text messages', 'category' => 'LEGAL & REG'),
                451 => array('title' => 'How VAT legislation will change business set up models in the UAE', 'category' => 'LEGAL & REG'),
                452 => array('title' => 'What is the Jointly Owned Real Property Law?', 'category' => 'LEGAL & REG'),
                453 => array('title' => 'London Fire: 95 U.K. Buildings Fail Fire Safety Tests', 'category' => 'News'),
                454 => array('title' => 'Introduction to Corporate Governance Regimes in Oman: Examining Directors Duties', 'category' => 'LEGAL & REG'),
                455 => array('title' => 'Legal inheritance for non-muslim expatriates', 'category' => 'LEGAL & REG'),
                456 => array('title' => 'Protecting patient data amidst the rise of health tourism', 'category' => 'LEGAL & REG'),
                457 => array('title' => 'Insurance Changes on the Way in the UAE', 'category' => 'LEGAL & REG'),
                458 => array('title' => 'UAE: New insurance regulations on the drawing board', 'category' => 'LEGAL & REG'),
                459 => array('title' => '7 things you might not realise are against the UAE’s Labour Law', 'category' => 'LEGAL & REG'),
                460 => array('title' => '“What are the tenants right after a residential fire”', 'category' => 'LEGAL & REG'),
                461 => array('title' => 'Q&A on Skyscraper Towers in Dubai', 'category' => 'LEGAL & REG'),
                462 => array('title' => 'All you need to know about the UAE’s tax administration system', 'category' => 'LEGAL & REG'),
                463 => array('title' => 'Is your personal data safe from hackers? New law proposed to protect Oman', 'category' => 'News'),
                464 => array('title' => 'Key new regulations implemented under Saudi’s new Crown Prince', 'category' => 'LEGAL & REG'),
                465 => array('title' => 'Country Comparative Legal Guides UAE: Insurance & Reinsurance', 'category' => 'LEGAL & REG'),
                466 => array('title' => 'In Sickness and in Health UAE Health Care Law', 'category' => 'LEGAL & REG'),
                467 => array('title' => 'Compulsory Ways', 'category' => 'LEGAL & REG'),
                468 => array('title' => '', 'category' => 'DELETE'),
                469 => array('title' => 'Dubai property buyers must take care to avoid surprises', 'category' => 'LEGAL & REG'),
                470 => array('title' => 'Legislation that leads to more IPOs', 'category' => 'DELETE'),
                471 => array('title' => 'Legislation that leads to more IPOs', 'category' => 'LEGAL & REG'),
                472 => array('title' => 'Affordable Dispute Resolution Forum in Dubai', 'category' => 'LEGAL & REG'),
                473 => array('title' => 'Those who seek addiction treatment in Dubai won’t be prosecuted', 'category' => 'LEGAL & REG'),
                474 => array('title' => 'Adding Value: Everything you need to know about VAT in the GCC', 'category' => 'LEGAL & REG'),
                475 => array('title' => 'Common Reporting Standards in the UAE', 'category' => 'LEGAL & REG'),
                476 => array('title' => 'Sheikh Mohammed Issues New Endowment Law for Dubai', 'category' => 'News'),
                477 => array('title' => 'Is Your Building Eco Friendly?', 'category' => 'LEGAL & REG'),
                478 => array('title' => 'What you need to know about writing a will in Dubai', 'category' => 'LEGAL & REG'),
                479 => array('title' => 'The Legal 500 & IHL Comparative Guide Legal Guide to Arbitration', 'category' => 'LEGAL & REG'),
                480 => array('title' => 'Insurance Hike Expected For Dubai High-Rises After Fires', 'category' => 'News'),
                481 => array('title' => 'BSA is a sponsor of the Oman Business Law Forum which is organized by Lexis Nexis', 'category' => 'Events'),
                482 => array('title' => 'UAE Fire Premiums To Rise', 'category' => 'News'),
                483 => array('title' => 'VAT is happening in the Free Zone', 'category' => 'LEGAL & REG'),
                484 => array('title' => 'Run-Off: Why it Matters', 'category' => 'LEGAL & REG'),
                485 => array('title' => 'Le régime juridique régissant la succession des expatriés non-musulmans aux Emirats arabes unis', 'category' => 'LEGAL & REG'),
                486 => array('title' => 'New VAT Implications Impacting The Region – Key Measures To Consider For Insurers In The GCC', 'category' => 'LEGAL & REG'),
                487 => array('title' => 'BSA Legal Clinic - Free Legal Advice for a Day!', 'category' => 'News'),
                488 => array('title' => 'Rima Mrad, Partner, attends GCC to Paris Legal Mission', 'category' => 'News'),
                489 => array('title' => 'BSA and DLD ‘Legal Clinic’ a Huge Success', 'category' => 'News'),
                490 => array('title' => 'Ready, Steady, Go: The Rise of Blockchain in the UAE', 'category' => 'LEGAL & REG'),
                491 => array('title' => 'The Digital Dawn', 'category' => 'LEGAL & REG'),
                492 => array('title' => 'ICLG UAE Real Estate 2018 - A practical cross-border insight into real estate law', 'category' => 'Reports'),
                493 => array('title' => 'Explained: Dubai’s new off-plan property purchase law', 'category' => 'LEGAL & REG'),
                494 => array('title' => 'New Partner in litigation for the BSA Paris office', 'category' => 'DELETE'),
                495 => array('title' => 'New VAT Implications Impacting The Region - Key Measures To Consider For Insurers In The GCC', 'category' => 'LEGAL & REG'),
                496 => array('title' => 'The Franchise Law Master Class, February 2018', 'category' => 'Events'),
                497 => array('title' => 'The Real Estate Law Master Class, February 2018', 'category' => 'Events'),
                498 => array('title' => 'Guide to Employee Non-Compete Agreements in EMEA', 'category' => 'LEGAL & REG'),
                499 => array('title' => 'Overview of Employee Monitoring in UAE', 'category' => 'LEGAL & REG'),
                500 => array('title' => 'Renowned IP expert joins BSA as Partner and Head of IP', 'category' => 'News'),
                501 => array('title' => 'Aviation Contracts: Consumer Rights, and Transfer and Registry', 'category' => 'LEGAL & REG'),
                502 => array('title' => 'The Impact of Cryptocurrencies and Blockchain in UAE', 'category' => 'LEGAL & REG'),
                503 => array('title' => 'BSA announces the launch of Spanish and Latin American Desk', 'category' => 'DELETE'),
                504 => array('title' => 'Setting up a company in the UAE', 'category' => 'LEGAL & REG'),
                505 => array('title' => 'Guidelines on UAE Insolvency Law', 'category' => 'LEGAL & REG'),
                506 => array('title' => 'New regulations from National Media Council on electronic media', 'category' => 'News'),
                507 => array('title' => 'Why Dubai destroyed fake iPhones, Rolex, LVs worth millions', 'category' => 'Events'),
                508 => array('title' => 'Comparative Analysis - Bankruptcy Laws in UAE and KSA', 'category' => 'LEGAL & REG'),
                509 => array('title' => 'Attend "The Essentials of VAT" workshop, April 2018', 'category' => 'Events'),
                510 => array('title' => 'Cancellation for non-use of registered trademarks', 'category' => 'LEGAL & REG'),
                511 => array('title' => 'BSA advises Chantier Naval Couach on their strategic agreements', 'category' => 'News'),
                512 => array('title' => 'Meet BSA\'s Paris team', 'category' => 'DELETE'),
                513 => array('title' => 'When homeowners refuse to pay service charges', 'category' => 'LEGAL & REG'),
                514 => array('title' => 'Insights for employers to implement policy on social media', 'category' => 'LEGAL & REG'),
                515 => array('title' => 'Setting up a business in Oman', 'category' => 'LEGAL & REG'),
                516 => array('title' => 'Setting up a business in KSA', 'category' => 'LEGAL & REG'),
                517 => array('title' => 'Jimmy Haoula in Gulf News on VAT and Owner\'s Associations', 'category' => 'LEGAL & REG'),
                518 => array('title' => 'ICLG Insurance & Reinsurance 2018 Legal Guide', 'category' => 'LEGAL & REG'),
                519 => array('title' => 'UAE now allows for multiple employer contracts', 'category' => 'News'),
                520 => array('title' => 'Our children are the future - BSA on the CSR road again', 'category' => 'News'),
                521 => array('title' => 'Dubai Chamber & BSA workshop on important legal aspects of VAT', 'category' => 'News'),
                522 => array('title' => 'What is property flipping?', 'category' => 'LEGAL & REG'),
                523 => array('title' => 'New law in UAE establishing Financial Audit Authority', 'category' => 'News'),
                524 => array('title' => 'Step by step guide to UAE VAT registration', 'category' => 'LEGAL & REG'),
                525 => array('title' => 'BSA to host a Mock Trial explaining UAE court processes', 'category' => 'Events'),
                526 => array('title' => 'BSA advises Louis Dreyfus Armateur on an agreement with EGA', 'category' => 'News'),
                527 => array('title' => 'Getting The Deal Through - Distribution and Agency 2018', 'category' => 'Reports'),
                528 => array('title' => 'VAT: The UAE versus the KSA way', 'category' => 'LEGAL & REG'),
                529 => array('title' => 'Does your company have to give you an annual flight back home?', 'category' => 'LEGAL & REG'),
                530 => array('title' => 'BSA Hosts UAE’s First Ever Mock Trial', 'category' => 'News'),
                531 => array('title' => 'The Enactment of a New Dubai Law Concerning the Dubai Health Authority', 'category' => 'News'),
                532 => array('title' => 'The Impact of Dubai Fires: Fire Damage and Tenant’s Insurance – What Every Landlord & Tenant Needs to Know', 'category' => 'LEGAL & REG'),
                533 => array('title' => 'Levées de fonds en cryptomonnaies (ICO) : anticipation des risques contentieux', 'category' => 'LEGAL & REG'),
                534 => array('title' => 'The New UAE Arbitration Law: it is not what it says but how you read it', 'category' => 'LEGAL & REG'),
                535 => array('title' => 'BSA to host the second Mock Trial at the DIFC Courts', 'category' => 'Events'),
                536 => array('title' => 'New Insurance Authority regulation to impact insurance consultants', 'category' => 'News'),
                537 => array('title' => 'Professional Negligence Law Review - UAE Chapter', 'category' => 'Reports'),
                538 => array('title' => 'The Legal 500: 2nd Edition Insurance & Reinsurance Comparative Guide', 'category' => 'LEGAL & REG'),
                539 => array('title' => 'Legal 500: Bribery & Corruption Country Comparative Guide', 'category' => 'Reports'),
                540 => array('title' => 'VAT: Till Slips and TRNS', 'category' => 'LEGAL & REG'),
                541 => array('title' => 'Tax Groups & Registrations - Here\'s What You Need To Know', 'category' => 'LEGAL & REG'),
                542 => array('title' => 'Social Media Policies', 'category' => 'LEGAL & REG'),
                543 => array('title' => 'UAE VAT Return Deadline on 28 June 2018', 'category' => 'News'),
                544 => array('title' => 'In Better Health', 'category' => 'LEGAL & REG'),
                545 => array('title' => 'BSA Ahmad Bin Hezeem & Associates LLP appoints Head of Corporate for both Riyadh and Abu Dhabi', 'category' => 'News'),
                546 => array('title' => 'UAE Business Brief - Patents, Trademarks & Counterfeiting', 'category' => 'LEGAL & REG'),
                547 => array('title' => 'UAE Leasing Property and Maintenance Responsibilities', 'category' => 'LEGAL & REG'),
                548 => array('title' => 'Commentary on the Saudi Arabian Bankruptcy Law', 'category' => 'LEGAL & REG'),
                549 => array('title' => 'Omani Consumer Law: 5 things you might not know', 'category' => 'LEGAL & REG'),
                550 => array('title' => 'Comparative Legal Guide on UAE Litigation', 'category' => 'LEGAL & REG'),
                551 => array('title' => 'Bankruptcy reforms \'will spur Saudi Arabia investment\'', 'category' => 'News'),
                552 => array('title' => 'Trusts & Firm Foundation', 'category' => 'LEGAL & REG'),
                553 => array('title' => 'From Generation to Generation', 'category' => 'LEGAL & REG'),
                554 => array('title' => 'Bankruptcy Proceedings in Oman', 'category' => 'LEGAL & REG'),
                555 => array('title' => 'Q&A: Legal Considerations for Construction in Saudi Arabia', 'category' => 'LEGAL & REG'),
                556 => array('title' => 'UAE Update on Trademark Protection & Online Enforcement in Dubai', 'category' => 'News'),
                557 => array('title' => 'Updates on the New Security Regime Over Movable Property in the UAE', 'category' => 'LEGAL & REG'),
                558 => array('title' => 'ICLG Data Protection 2018: UAE Chapter', 'category' => 'Reports'),
                559 => array('title' => '\'Protect Yourself by Modifying Your Status\'', 'category' => 'News'),
                560 => array('title' => 'Cybercrime New Amendment Law in the UAE', 'category' => 'News'),
                561 => array('title' => 'Fintech and The Accelerator Culture in the UAE', 'category' => 'LEGAL & REG'),
                562 => array('title' => 'BREXIT, Market Access and the DIFC', 'category' => 'LEGAL & REG'),
                563 => array('title' => 'An End To Harassment?', 'category' => 'LEGAL & REG'),
                564 => array('title' => 'Six Things That Can Get you Arrested in Oman', 'category' => 'LEGAL & REG'),
                565 => array('title' => 'The DIFC Franchise Law Master Class, September 2018', 'category' => 'Events'),
                566 => array('title' => 'The Real Estate Law Master Class, September & October 2018', 'category' => 'Events'),
                567 => array('title' => 'Abu Dhabi: The New Dual Licenses Initiative', 'category' => 'News'),
                568 => array('title' => 'Insurance Distribution in Europe & widers Implications for the Middle East', 'category' => 'News'),
                569 => array('title' => 'BSA Announces its Second Legal Clinic', 'category' => 'Events'),
                570 => array('title' => 'Oman New Expat Recruitment Rules', 'category' => 'News'),
                571 => array('title' => 'UAE issues Federal Law No.10 of 2018 on ‘Netting’', 'category' => 'News'),
                572 => array('title' => 'The UAE Foreign Direct Investment Law', 'category' => 'News'),
                573 => array('title' => 'DIFC Announces New Laws & Regulations', 'category' => 'News'),
                574 => array('title' => 'UAE Approves Long-Term Visa System', 'category' => 'News'),
                575 => array('title' => 'The Legal 500 & The In-House Lawyer Comparative Legal Guide UAE: Bribery & Corruption', 'category' => 'Reports'),
                576 => array('title' => 'New Abu Dhabi Tenancy Decision: What You Need to Know', 'category' => 'Reports'),
                577 => array('title' => 'Clarifications Where the VAT Law Seems Unclear', 'category' => 'Reports'),
                578 => array('title' => 'New Rules Regulating the Employment of the UAE Nationals in the Private Sector', 'category' => 'Reports'),
                579 => array('title' => 'UAE Corporate Law: A shifting Landscape', 'category' => 'Reports'),
                580 => array('title' => 'Introduction to Company Acquisitions in Kingdom of Saudi Arabia', 'category' => 'Reports'),
                581 => array('title' => 'Down With The Details', 'category' => 'Reports'),
                582 => array('title' => 'Joint Property Development Agreement', 'category' => 'Reports'),
                583 => array('title' => 'On Firm Foundations', 'category' => 'LEGAL & REG'),
                584 => array('title' => 'The Comparative Legal Guide to Arbitration: UAE Chapter', 'category' => 'Reports'),
                585 => array('title' => 'BSA Legal Clinic Returns to Dubai', 'category' => 'News'),
                586 => array('title' => 'BSA Signs MoU with Zubair SEC to Host Workshop and Legal Clinic', 'category' => 'News'),
                587 => array('title' => 'Public Benefits from Free Legal Clinic', 'category' => 'News'),
                588 => array('title' => 'BSA Ahmad Bin Hezeem & Associates Appoints New Head of Insurance in Dubai', 'category' => 'News'),
                589 => array('title' => 'BSA Highlights Latest Updates in the Omani Penal Code 2018', 'category' => 'LEGAL & REG'),
                590 => array('title' => 'Comparative Legal Guide on Fintech: UAE Chapter', 'category' => 'Reports'),
                591 => array('title' => 'Dubai Customs Announces New Inventory Management System Requirements', 'category' => 'News'),
                592 => array('title' => 'BSA Proudly Supports Dubai Autism Center', 'category' => 'News'),
                593 => array('title' => 'Long-term UAE Visa Set To Benefit Professionals in', 'category' => 'LEGAL & REG'),
                594 => array('title' => '10 Year Visa To Investors, Skilled Professional Will Go A Long Way To Boost Investor Confidence', 'category' => 'News'),
                595 => array('title' => 'New Insurance Law Opens Up Opportunities', 'category' => 'LEGAL & REG'),
                596 => array('title' => 'New Insurance Law Opens Up Opportunities', 'category' => 'LEGAL & REG'),
                597 => array('title' => 'BSA In Collaboration With Zubair SEC To Host Legal Clinic In Oman', 'category' => 'Events'),
                598 => array('title' => 'Middle East Legal Insight: UAE Netting Law', 'category' => 'LEGAL & REG'),
                599 => array('title' => 'BSA in Collaboration With Zubair SEC to Hold Workshop and Legal Clinic For SMEs', 'category' => 'News'),
                600 => array('title' => 'Employment Law - An Overview', 'category' => 'LEGAL & REG'),
                601 => array('title' => 'Understanding How it Works', 'category' => 'LEGAL & REG'),
                602 => array('title' => 'BSA and Zubair SEC Successfully Concludes Legal Clinic and Workshop for SMEs', 'category' => 'News'),
                603 => array('title' => 'Spanish Companies Look Towards The UAE For Expansion Opportunities', 'category' => 'News'),
                604 => array('title' => 'Interview With Simon Isgar', 'category' => 'News'),
                605 => array('title' => 'BSA Concluded A Successful Workshop at Zubair SEC', 'category' => 'News'),
                606 => array('title' => 'Streamlining DIFC Registration and Regulation', 'category' => 'LEGAL & REG'),
                607 => array('title' => 'Company Secretary Services Is Now Offered In BSA AD Office', 'category' => 'News'),
                608 => array('title' => 'BSA is Now Offering Company Secretary Services', 'category' => 'News'),
                609 => array('title' => 'A Practical Guide to Saudi Arabia\'s Anti Money Laundering Law', 'category' => 'LEGAL & REG'),
                610 => array('title' => 'Regulatory Alert: UAE Insurance Authority Issues List of Administrative Fines', 'category' => 'News'),
                611 => array('title' => 'Legal Considerations In An Increasingly Diverse Defence Market', 'category' => 'News'),
                612 => array('title' => '', 'category' => 'DELETE'),
                613 => array('title' => 'UAE\'s Draft Life Insurance Regulations - What You Can Expect', 'category' => 'LEGAL & REG'),
                614 => array('title' => 'BSA & Dubai Chamber Host Bankruptcy Workshop', 'category' => 'News'),
                615 => array('title' => 'Potential 100% Ownership for Business Investors in Oman', 'category' => 'LEGAL & REG'),
                616 => array('title' => 'UAE Legal Update - Regulatory Alert Healthcare', 'category' => 'News'),
                617 => array('title' => 'UAE Legal Update - Regulatory Alert (2) Healthcare & Data Privacy', 'category' => 'Reports'),
                618 => array('title' => 'IFRS 17 & UAE Regulatory Landscape', 'category' => 'Reports'),
                619 => array('title' => 'Abu Dhabi Social Support Authority', 'category' => 'News'),
                620 => array('title' => 'Potential 100% Ownership for Business Investors in Oman', 'category' => 'News'),
                621 => array('title' => 'Global Healthcare Providers Brace for New UAE Data Privacy Law', 'category' => 'News'),
                622 => array('title' => 'Challenges Facing Green and Socially Responsible Investments', 'category' => 'LEGAL & REG'),
                623 => array('title' => 'Distribution & Agency: UAE Chapter 2019', 'category' => 'Reports'),
                624 => array('title' => 'Oman – Mandatory Health Insurance is on its way', 'category' => 'News'),
                625 => array('title' => 'BSA and SAGIA Promote Foreign Investment in Saudi Arabia Throughout the UK and Ireland', 'category' => 'News'),
                626 => array('title' => 'Bancassurance Regulations in the GCC', 'category' => 'LEGAL & REG'),
                627 => array('title' => 'BSA Wins Regional Law Firm Of The Year At The ACC Middle East Legal Awards', 'category' => 'News'),
                628 => array('title' => 'An Introduction To Bankruptcy In The UAE', 'category' => 'LEGAL & REG'),
                629 => array('title' => 'Cartels: UAE Chapter', 'category' => 'LEGAL & REG')
        );

        $cats = [];

        foreach($data as $id=>$item){
            if(!in_array($item['category'],$cats))
                $cats[$item['category']][] = $item;

            $article = Article::find($id);

            if($article){

                if($item['category']=="DELETE"){
                    $article->delete();
                } else {
                    if($item['category']=="LEGAL & REG"){
                        $catId = 3003;}
                    elseif($item['category']=="Events"){
                        $catId = 102;}
                    elseif($item['category']=="Reports"){
                        $catId = 3001;}
                    else {
                        $catId = 103;
                    }

                    $oldCats = ArticleCategory::where('article_id',$article->id)->get();

                    foreach($oldCats as $oc)
                        $oc->delete();

                    $article->categories()->sync([$catId]);
                }
            }

        }

//        dd($cats);
    }

    public function index(){
        $pageSlug = $this->pageslug;
//        $data = $this->model->get();
//
//        echo '<table>';
//        foreach($data as $article) {
//            $mainCats = ['news','events','publications-2'];
//            $c = [];
//
//            $articleCats = $article->categories;
//            foreach($articleCats as $cat)
//                if(in_array($cat->slug,$mainCats))
//                    $c[] = $cat->name;
//
//            echo '<tr>';
//            echo'<td>'.$article->id.'</td>';
//            echo'<td>'.$article->title.'</td>';
//            echo'<td>';
//                foreach($article->categories as $category)
//                    echo $category->name.' ';
//            echo '</td>';
//            echo'<td>';
//                for($x=0;$x<count($c);$x++)
//                    echo $c[$x].' ';
//            echo '</td>';
//            echo'</tr>';
//        }
//
//        echo '</table>';

        $posts = $this->model->orderBy('date','DESC')->whereHas('categories',function($query){ return $query->where('slug','!=','events'); })->where('slug','!=','csr-activity')->simplePaginate(100);
        return view('admin.articles.index', compact('posts','pageSlug'));
    }


    public function create(){
        $pageSlug = $this->pageslug;
        $tags = Tag::select('name','t_id')->get();
        $articles = Article::where('title','!=','')->select('title','id')->get();
        $services = Service::select('title','id')->get();
        $locations = Location::select('name','l_id')->get();
        $lawyers = Lawyer::select('name','id')->get();
        $categories = Category::select('name','cat_id')->where('slug','!=','events')->where('slug','!=','csr-activity')->get();
        $promos = Promo::where('type','short')->select('title','id')->get();

        return view('admin.articles.create',compact('categories','pageSlug','services','tags','articles','locations','lawyers','promos'));
    }

    public function store(Request $request){

        $input = $request->except('_token','photo','location');
        $files = $request->file();

        if(isset($files['photo'])){
            $file = $files['photo'];
            //Move Uploaded File
            $destinationPath = 'public/uploads/articles';

            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
            \Intervention\Image\Facades\Image::make($file->getRealPath())->fit(300, 200)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/articles/'. $newFileName;
        }

        if(isset($files['photo_full'])){
            $file = $files['photo_full'];
            //Move Uploaded File
            $destinationPath = 'public/uploads/articles';

            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
            \Intervention\Image\Facades\Image::make($file->getRealPath())->fit(1296, 332)->save($destinationPath.'/'.$newFileName);
            $input['photo_full'] = 'uploads/articles/'. $newFileName;
        }

        $input['slug'] = $this->generateSlug($input['title']);
        $input['date'] = Carbon::parse($input['date'])->format('Y-m-d');

        $newData = $this->model->create($input);

        $data = $request->only('tags','location','articles','category','promos');

        if($newData){

//            if($data['location'])
//                $newData->location()->create(['location_id' => $data['location']]);

            $newData->categories()->sync([$data['category']]);

            if(isset($input['tags'])){
                $tags = json_decode($input['tags'],true);
                if(is_array($tags)){
                    $t = [];
                    foreach ($tags as $item){
                        $t[] = $item['id'];
                    }
                    $newData->tags()->sync($t);
                }
            }

            if(isset($input['contacts'])){
                $contacts = json_decode($input['contacts'],true);
                if(is_array($contacts)){
                    foreach ($contacts as $item){
                        ArticleLawyer::create(['lawyer_id' => $item['id'],'article_id' => $newData->id]);
                    }
                }
            }

            if(isset($input['promos'])){
                $promos = json_decode($input['promos'],true);
                if(is_array($promos)){
                    foreach ($promos as $item){
                        ArticlePromo::create(['promo_id' => $item['id'],'article_id' => $newData->id]);
                    }
                }
            }

            if(isset($input['articles'])){
                $articles = json_decode($input['articles'],true);
                if(is_array($articles)){
                    foreach ($articles as $item){
                        ArticleRelated::create(['article_related_id' => $item['id'],'article_id' => $newData->id]);
                    }
                }
            }

            if(isset($input['services'])){
                $services = json_decode($input['services'],true);
                if(is_array($services)){
                    $s = [];
                    foreach ($services as $item){
                        $s[] = $item['id'];
                    }

                    $newData->services()->sync($s);
                }
            }
        }

        Session::flash('success','Item added successfully');
        return redirect('admin/articles/edit/'.$newData->id);
    }

    public function edit($id){

        $data = $this->model->find($id);

        if(!$data)
            return 'Post not found!';

        $pageSlug = $this->pageslug;
        $tags = Tag::select('name','t_id')->get();
        $articles = Article::where('title','!=','')->select('title','id')->get();
        $services = Service::select('title','id')->get();
        $locations = Location::select('name','l_id')->get();
        $lawyers = Lawyer::select('name','id')->get();
        $categories = Category::select('name','cat_id','id')->where('slug','!=','events')->where('slug','!=','csr-activity')->get();
        $promos = Promo::where('type','short')->select('title','id')->get();

        return view('admin.articles.edit',compact('data','categories','pageSlug','services','tags','articles','locations','lawyers','promos'));
    }

    public function DELETE($id){

        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $post->DELETE();

        Session::flash('success','Item DELETEd successfully');

        return redirect('admin/articles');
    }

    public function versions($id){

        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $versions = $post->versions;

        return view('admin.articles.versions',compact('versions','post'));
    }


    public function checkcats(){

        $categories = Category::get();

        foreach($categories as $category){
            $count = Article::whereHas('categories',function($query) use($category) { return $query->where('cat_id',$category->cat_id); })->count();

            if($count)
                echo '<p>'.$category->name.': '.$count.'</p>';
            else
                $category->delete();
        }
    }

    public function compare($article_id,$version_id){

        $current = ArticleVersion::where('article_id',$article_id)->where('is_current',1)->first();
        $previous = ArticleVersion::where('id',$version_id)->first();

        if(!$current)
            return 'Post not found!';

        if(!$previous)
            return 'Version not found!';

        return view('admin.articles.compare',compact('current','previous'));
    }

    public function restore($version_id){

        $version = ArticleVersion::find($version_id);
        $article = Article::find($version->article_id);

        $article->title = $version->title;
        $article->slug = $version->slug;
        $article->excerpt = $version->excerpt;
        $article->content = $version->content;
        $article->date = $version->date;
        $article->photo = $version->photo;
        $article->photo_full = $version->photo_full;
        $article->thumbnail = $version->thumbnail;
        $article->hidden = $version->hidden;
        $article->event_time = $version->event_time;
        $article->event_location = $version->event_location;
        $article->meta_title = $version->meta_title;
        $article->meta_description = $version->meta_description;
        $article->meta_keywords = $version->meta_keywords;

        $newC = null;
        if($version->categories){
            $cats = json_decode($version->categories,true);

            if(is_array($cats)){

                foreach($cats as $c){
                    if(Category::where('cat_id',$c)->count())
                        $newC[] = $c;
                }
            }

        }
        $article->categories()->sync($newC);

        $newT= null;
        if($version->tags){
            $tags = json_decode($version->tags,true);

            if(is_array($tags)){
                foreach($tags as $t){
                    if(Tag::where('t_id',$t)->count())
                        $newT[] = $t;
                }

            }
        }
        $article->tags()->sync($newT);

        ArticleLawyer::where('article_id',$article->id)->DELETE();
        if($version->lawyers){
            $contacts = json_decode($version->lawyers,true);
            if(is_array($contacts)){
                foreach ($contacts as $item){
                    ArticleLawyer::create(['lawyer_id' => $item,'article_id' => $article->id]);
                }
            }
        }

        ArticlePromo::where('article_id',$article->id)->DELETE();
        if($version->promos){
            $promos = json_decode($version->promos,true);
            if(is_array($promos)){
                foreach ($promos as $item){
                    ArticlePromo::create(['promo_id' => $item,'article_id' => $article->id]);
                }
            }
        }

        if($version->articles){
            $articles = json_decode($version->articles,true);
            if(is_array($articles)){
                ArticleRelated::where('article_id',$article->id)->DELETE();
                foreach ($articles as $item){
                    ArticleRelated::create(['article_related_id' => $item,'article_id' => $article->id]);
                }
            } else {
                ArticleRelated::where('article_id',$article->id)->DELETE();
            }
        }

        $newS= null;
        if($version->services){
            $services = json_decode($version->services,true);

            if(is_array($services)){
                foreach($services as $s){
                    if(Service::where('id',$s)->count())
                        $newS[] = $s;
                }
            }
        }
        $article->services()->sync($newS);

        $article->save();

        $oldVersions = ArticleVersion::where('article_id',$article->id)->where('id','!=',$version->id)->get();

        foreach($oldVersions as $old){
            $old->is_current = 0;
            $old->save();
        }

        $version->update(['is_current'=>1]);

        Session::flash('success','Version successfully restored.');

        return redirect('admin/articles/edit/'.$article->id);
    }


    public function update(Request $request){

        $id = $request->input('id');
        $newData = $this->model->find($id);

        if(!$newData)
            return 'Post not found!';

        if(!$this->createVersion($newData)){
            Session::flash('error','Failed to create a backup.');
            return redirect()->back();
        }

        $input = $request->except('_token','photo','location');
        $files = $request->file();

        $exists = Article::where('slug',$input['slug'])->first();

        if($exists) {
            if($newData->id != $exists->id) {
                Session::flash('error','This URL already exists in the site. Please use a different one.');
                return redirect()->back();
            }
        }

        if(isset($files['photo'])){
            $file = $files['photo'];
            //Move Uploaded File
            $destinationPath = 'public/uploads/articles';

            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
            \Intervention\Image\Facades\Image::make($file->getRealPath())->fit(300, 200)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/articles/'. $newFileName;
        }

        if(isset($files['photo_full'])){
            $file = $files['photo_full'];
            //Move Uploaded File
            $destinationPath = 'public/uploads/articles';

            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
            \Intervention\Image\Facades\Image::make($file->getRealPath())->fit(1296, 332)->save($destinationPath.'/'.$newFileName);
            $input['photo_full'] = 'uploads/articles/'. $newFileName;
        }

        $input['date'] = Carbon::parse($input['date'])->format('Y-m-d');

        $newData->update($input);

        $data = $request->only('tags','location','articles','category');

        if($newData){

//            $newData->location()->DELETE();
//
//            if($data['location'])
//                $newData->location()->create(['location_id' => $data['location']]);

            $newData->categories()->sync([$data['category']]);

            $t = [];
            if(isset($input['tags'])){
                $tags = json_decode($input['tags'],true);
                if(is_array($tags)){
                    foreach ($tags as $item){
                        $t[] = $item['id'];
                    }
                    $newData->tags()->sync($t);
                }
            }
            $newData->tags()->sync($t);

            ArticleLawyer::where('article_id',$newData->id)->DELETE();

            if(isset($input['contacts'])){
                $contacts = json_decode($input['contacts'],true);
                if(is_array($contacts)){
                    foreach ($contacts as $item){
                        ArticleLawyer::create(['lawyer_id' => $item['id'],'article_id' => $newData->id]);
                    }
                }
            }

            ArticleRelated::where('article_id',$newData->id)->DELETE();
            if(isset($input['articles'])){
                $articles = json_decode($input['articles'],true);
                if(is_array($articles)){
                    foreach ($articles as $item){
                        ArticleRelated::create(['article_related_id' => $item['id'],'article_id' => $newData->id]);
                    }
                }
            }

            ArticlePromo::where('article_id',$newData->id)->DELETE();
            if(isset($input['promos'])){
                $promos = json_decode($input['promos'],true);
                if(is_array($promos)){
                    foreach ($promos as $item){
                        ArticlePromo::create(['promo_id' => $item['id'],'article_id' => $newData->id]);
                    }
                }
            }

            $s = [];
            if(isset($input['services'])){
                $services = json_decode($input['services'],true);
                if(is_array($services)){
                    $s = [];
                    foreach ($services as $item){
                        $s[] = $item['id'];
                    }

                }
            }
            $newData->services()->sync($s);
        }

        if(!$this->createVersion($newData)){
            Session::flash('error','Failed to create a backup.');
            return redirect()->back();
        }

        Session::flash('success','Item updated successfully');
        return redirect()->back();
    }

    public function createVersion($data){

        $target = $data;
        $input = null;
        $categories = null;
        $lawyers = null;
        $tags = null;
        $services = null;
        $locations = null;
        $promos = null;
        $related = null;

        if($data->categories)
            $categories[] = $data->categories->cat_id;

        $input['categories'] = $categories ? json_encode($categories) : null;

        if($data->tags)
            foreach($data->tags as $item)
                $tags[] = $item->t_id;

        $input['tags'] = $tags ? json_encode($tags) : null;

        if($data->lawyer)
            foreach($data->lawyer as $item)
                $lawyers[] = $item->id;

        $input['lawyers'] = $lawyers ? json_encode($lawyers) : null;

        if($data->services)
            foreach($data->services as $item)
                $services[] = $item->id;

        $input['services'] = $services ? json_encode($services) : null;

        if($data->promos)
            foreach($data->promos as $item)
                $promos[] = $item->id;

        $input['promos'] = $promos ? json_encode($promos) : null;

        if($data->articles)
            foreach($data->articles as $item)
                $related[] = $item->id;

        $input['related'] = $related ? json_encode($related) : null;

        foreach($data->toArray() as $key=>$row)
            if( $key != 'services' && $key != 'id' && $key != 'created_at' && $key != 'updated_at' )
                $input[$key] = $row;

        // Duplicate Check
        $query = ArticleVersion::query();

        foreach($input as $key=>$t)
            $query->where($key,$t);

//        $looper = 1;
//        while($looper<count($input)){
//            $query = ArticleVersion::query();
//            $x = 0;
//
//            foreach($input as $key=>$t){
//                $query->where($key,$t);
//                $x++;
//
//                if($x==$looper){
//                    break;
//                }
//            }
//
//            echo $key.' = '.$t . '| '.$query->count().'</br>';
//            if($query->count()==0)
//                dd('this');
//
//            $looper++;
//        }

        $exists = $query->orderBy('id','DESC')->first();
        $newVersion = null;

        if(!$exists && ArticleVersion::where('article_id',$target->id)->count() == 0){
            $input['version'] = ArticleVersion::where('article_id',$target->id)->count()+1;
            $input['is_current'] = 1;
            $newVersion = $target->versions()->create($input);
        }
        elseif($exists && $exists->version != ArticleVersion::where('article_id',$target->id)->count()){
            $input['version'] = ArticleVersion::where('article_id',$target->id)->count()+1;
            $input['is_current'] = 1;
            $newVersion = $target->versions()->create($input);
        }
        elseif(!$exists){
            $input['version'] = ArticleVersion::where('article_id',$target->id)->count()+1;
            $input['is_current'] = 1;
            $newVersion = $target->versions()->create($input);
        }

        if($newVersion){
            $oldVersions = ArticleVersion::where('article_id',$target->id)->where('id','!=',$newVersion->id)->get();
            foreach($oldVersions as $old){
                $old->is_current = 0;
                $old->save();
            }
        }

        return true;
    }

}
