<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ArticlePromo;
use App\Models\Category;
use App\Models\Lawyer;
use App\Models\LawyerService;
use App\Models\Promo;
use App\Models\Service;
use App\Models\ServicePromo;
use App\Models\ServiceType;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Models\Article;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use App\Services\CanCreateSlug;

class ServiceController extends Controller
{
    use CanCreateSlug;

    public function __construct(Service $model)
    {
        $this->model = $model;
        $this->pageslug = 'services';
    }

    public function index(){
        $pageSlug = $this->pageslug;
        $data = ServiceType::with('services')->get();

        return view('admin.services.index', compact('data','pageSlug'));
    }


    public function create(){
        $pageSlug = $this->pageslug;
        $categories = Category::select('name','cat_id')->get();
        $lawyers = Lawyer::select('name','id')->get();
        $tags = Tag::select('name','t_id')->get();
        $articles = Article::where('title','!=','')->select('title','id')->get();
        $types = ServiceType::select('name','id')->get();
        $promos = Promo::where('type','short')->select('title','id')->get();

        return view('admin.services.create',compact('pageSlug','categories','lawyers','tags','articles','types','promos'));
    }

    public function store(Request $request){

        $input = $request->except('_token');

        $data['title'] = $input['name'];
        $data['slug'] = $this->generateSlug($input['name']);
        $data['content'] = $input['content'];
        $data['service_type_id'] = $input['service_type_id'];
        $data['hidden'] = $input['hidden'];
        $data['meta_title'] = $input['meta_title'];
        $data['meta_description'] = $input['meta_description'];

        $newData = $this->model->create($data);

        if($newData){
            if($input['quotes']){
                if(count($input['quotes'])){
                    foreach ($input['quotes'] as $item){
                        if($item['content'])
                            $newData->quotes()->create($item);
                    }
                }
            }

            if(isset($input['contacts'])){
                $contacts = json_decode($input['contacts'],true);
                if(is_array($input['contacts']))
                    foreach ($contacts as $item){
                        $newData->contacts()->create(['lawyer_id' => $item['id']]);
                    }
            }


            if(isset($input['team'])){
                $team = json_decode($input['team'],true);
                if(is_array($input['team']))
                foreach ($team as $item){
                    LawyerService::create(['lawyer_id' => $item['id'],'service_id' => $newData->id]);
                }
            }

            if(isset($input['tags'])){
                $tags = json_decode($input['tags'],true);
                if(is_array($input['tags']))
                foreach ($tags as $item){
                    $newData->tags()->create(['tag_id' => $item['id']]);
                }
            }

            if(isset($input['articles'])){
                $articles = json_decode($input['articles'],true);
                if(is_array($input['articles']))
                foreach ($articles as $item){
                    $newData->articles()->create(['article_id' => $item['id']]);
                }
            }

            if(isset($input['promos'])){
                $promos = json_decode($input['promos'],true);
                if(is_array($input['promos']))
                if(is_array($promos)){
                    foreach ($promos as $item){
                        ServicePromo::create(['promo_id' => $item['id'],'service_id' => $newData->id]);
                    }
                }
            }
        }

        Session::flash('success','Item saved successfully');
        return redirect('admin/services/edit/'.$newData->id);
    }

    public function edit($id){

        $pageSlug = $this->pageslug;
        $data = $this->model->find($id);

        if(!$data)
            return 'Post not found!';

        $categories = Category::select('name','cat_id')->get();
        $lawyers = Lawyer::select('name','id')->get();
        $tags = Tag::select('name','t_id')->get();
        $articles = Article::where('title','!=','')->select('title','id')->get();
        $types = ServiceType::select('name','id')->get();
        $teamData = LawyerService::where('service_id',$data->id)->get();
        $promos = Promo::where('type','short')->select('title','id')->get();
        $team = [];

        if($teamData){
            foreach ($teamData as $td)
                $team[] = Lawyer::find($td->lawyer_id);
        }

        return view('admin.services.edit',compact('team','pageSlug','categories','lawyers','tags','articles','types','data','promos'));
    }

    public function delete($id){

        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $post->delete();

        Session::flash('success','Item deleted successfully');

        return redirect('admin/content/press-releases');
    }


    public function update(Request $request){

        $id = $request->input('id');
        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $input = $request->except('_token');

        $data['title'] = $input['name'];

        $data['slug'] = $input['slug'];
        $data['content'] = $input['content'];
        $data['service_type_id'] = $input['service_type_id'];
        $data['hidden'] = $input['hidden'];
        $data['meta_title'] = $input['meta_title'];
        $data['meta_description'] = $input['meta_description'];

        $post->update($data);

        $post->quotes()->delete();

        if($input['quotes']){
            if(count($input['quotes'])){
                foreach ($input['quotes'] as $item){
                    if($item['content'])
                        $post->quotes()->create($item);
                }
            }
        }

        $post->contacts()->delete();

        if(isset($input['contacts'])){
            $contacts = json_decode($input['contacts'],true);
            foreach ($contacts as $item){
                $post->contacts()->create(['lawyer_id' => $item['id']]);
            }
        }

        LawyerService::where('service_id',$post->id)->delete();

        if(isset($input['team'])){
            $contacts = json_decode($input['team'],true);
            foreach ($contacts as $item){
                LawyerService::create(['lawyer_id' => $item['id'],'service_id' => $post->id]);
            }
        }

        $post->tags()->delete();

        if(isset($input['tags'])){
            $tags = json_decode($input['tags'],true);
            foreach ($tags as $item){
                $post->tags()->create(['tag_id' => $item['id']]);
            }
        }

        $post->articles()->delete();

        if(isset($input['articles'])){
            $articles = json_decode($input['articles'],true);
            foreach ($articles as $item){
                $post->articles()->create(['article_id' => $item['id']]);
            }
        }

        ServicePromo::where('service_id',$post->id)->delete();
        if(isset($input['promos'])){
            $promos = json_decode($input['promos'],true);
            if(is_array($promos)){
                foreach ($promos as $item){
                    ServicePromo::create(['promo_id' => $item['id'],'service_id' => $post->id]);
                }
            }
        }

        Session::flash('success','Item updated successfully');
        return redirect('admin/services/edit/'.$post->id);
    }

}
