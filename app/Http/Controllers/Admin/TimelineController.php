<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;
use App\Models\Lawyer;
use App\Models\Service;
use App\Models\ServiceType;
use App\Models\Tag;
use App\Models\Timeline;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class TimelineController extends Controller
{

    public function __construct(Timeline $model)
    {
        $this->model = $model;
        $this->pageslug = 'about';
    }

    public function index(){
        $pageSlug = $this->pageslug;
        $data = Timeline::get();

        return view('admin.timelines.index', compact('data','pageSlug'));
    }

    public function edit($id){

        $pageSlug = $this->pageslug;
        $data = $this->model->find($id);

        if(!$data)
            return 'Post not found!';

        $categories = Category::select('name','cat_id')->get();
        $lawyers = Lawyer::select('name','id')->get();
        $tags = Tag::select('name','t_id')->get();
        $articles = Article::where('title','!=','')->select('title','id')->get();
        $types = ServiceType::select('name','id')->get();

        return view('admin.timelines.edit',compact('pageSlug','categories','lawyers','tags','articles','types','data'));
    }

    public function delete($id){

        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $post->delete();

        Session::flash('success','Item deleted successfully');

        return redirect('admin/content/press-releases');
    }


    public function update(Request $request){

        $id = $request->input('id');
        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $input = $request->except('_token','image');
        $files = $request->file();

        if(isset($files['image'])){
            $file = $files['image'];
            //Move Uploaded File
            $destinationPath = 'public/uploads/timelines';

            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
            Image::make($file->getRealPath())->fit(532, 463)->save($destinationPath.'/'.$newFileName);
            $input['image'] = 'uploads/timelines/'. $newFileName;
        }

        $input['date'] = Carbon::parse($input['date'])->format('Y-m-d');
        $post->update($input);

        Session::flash('success','Item saved successfully');
        return redirect('admin/about/timelines/edit/'.$post->id);
    }

    public function create(){
        $pageSlug = $this->pageslug;
        $categories = Category::select('name','cat_id')->get();
        $lawyers = Lawyer::select('name','id')->get();
        $tags = Tag::select('name','t_id')->get();
        $articles = Article::where('title','!=','')->select('title','id')->get();
        $types = ServiceType::select('name','id')->get();

        return view('admin.timelines.create',compact('pageSlug','categories','lawyers','tags','articles','types'));
    }

    public function store(Request $request){

        $input = $request->except('_token','image');
        $files = $request->file();

        if(isset($files['image'])){
            $file = $files['image'];
            //Move Uploaded File
            $destinationPath = 'public/uploads/timelines';

            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
            Image::make($file->getRealPath())->fit(532, 463)->save($destinationPath.'/'.$newFileName);
            $input['image'] = 'uploads/timelines/'. $newFileName;
        }

        $input['date'] = Carbon::parse($input['date'])->format('Y-m-d');
        $post = $this->model->create($input);

        Session::flash('success','Item saved successfully');
        return redirect('admin/about/timelines/edit/'.$post->id);
    }
}
