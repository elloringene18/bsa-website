<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Location;
use App\Services\CanCreateSlug;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class LocationController extends Controller
{
    use CanCreateSlug;

    public function __construct(Location $model)
    {
        $this->model = $model;
        $this->pageslug = 'locations';
    }

    public function index(){
        $pageSlug = $this->pageslug;
        $data = Location::get();

        return view('admin.locations.index', compact('data','pageSlug'));
    }

    public function edit($id){
        $pageSlug = $this->pageslug;
        $data = $this->model->find($id);

        if(!$data)
            return 'Post not found!';

        return view('admin.locations.edit',compact('pageSlug','data'));
    }

    public function delete($id){

        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $post->delete();

        Session::flash('success','Item deleted successfully');

        return redirect('admin/content/press-releases');
    }


    public function update(Request $request){

        $id = $request->input('id');
        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $input = $request->except('_token','image');

        if($input['name']!=$post->name)
            $input['slug'] = $this->generateSlug($input['name']);

        $post->update($input);

        Session::flash('success','Item updated successfully');
        return redirect('admin/locations/edit/'.$post->id);
    }

    public function create(){
        $pageSlug = $this->pageslug;
        return view('admin.locations.create',compact('pageSlug'));
    }

    public function store(Request $request){

        $input = $request->except('_token','image');

        $input['slug'] = $this->generateSlug($input['name']);
        $input['l_id'] = rand(1,999999);

        $post = $this->model->create($input);

        Session::flash('success','Item saved successfully');
        return redirect('admin/locations/edit/'.$post->id);
    }
}
