<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Award;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class AwardController extends Controller
{

    public function __construct(Award $model)
    {
        $this->model = $model;
        $this->pageslug = 'about';
    }

    public function index(){
        $pageSlug = $this->pageslug;
        $data = Award::get();

        return view('admin.awards.index', compact('data','pageSlug'));
    }

    public function edit($id){

        $pageSlug = $this->pageslug;
        $data = $this->model->find($id);

        if(!$data)
            return 'Post not found!';

        return view('admin.awards.edit',compact('pageSlug','data'));
    }

    public function delete($id){

        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $post->delete();

        Session::flash('success','Item deleted successfully');

        return redirect('admin/about/awards');
    }


    public function update(Request $request){

        $id = $request->input('id');
        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $input = $request->except('_token','image');
        $files = $request->file();

        if(isset($files['image'])){
            $file = $files['image'];
            //Move Uploaded File
            $destinationPath = 'public/uploads/awards';

            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
            Image::make($file->getRealPath())->fit(532, 463)->save($destinationPath.'/'.$newFileName);
            $input['image'] = 'uploads/awards/'. $newFileName;
        }

        $post->update($input);

        Session::flash('success','Item saved successfully');
        return redirect('admin/about/awards/edit/'.$post->id);
    }

    public function create(){
        $pageSlug = $this->pageslug;

        return view('admin.awards.create',compact('pageSlug'));
    }

    public function store(Request $request){

        $input = $request->except('_token','image');
        $files = $request->file();

        if(isset($files['image'])){
            $file = $files['image'];
            //Move Uploaded File
            $destinationPath = 'public/uploads/awards';

            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
            Image::make($file->getRealPath())->fit(532, 463)->save($destinationPath.'/'.$newFileName);
            $input['image'] = 'uploads/awards/'. $newFileName;
        }

        $post = $this->model->create($input);

        Session::flash('success','Item saved successfully');
        return redirect('admin/about/awards/edit/'.$post->id);
    }
}
