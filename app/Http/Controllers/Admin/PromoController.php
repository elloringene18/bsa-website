<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Lawyer;
use App\Models\LawyerService;
use App\Models\Promo;
use App\Models\Service;
use App\Models\ServiceType;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Models\Article;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use App\Services\CanCreateSlug;

class PromoController extends Controller
{
    use CanCreateSlug;

    public function __construct(Promo $model)
    {
        $this->model = $model;
        $this->pageslug = 'promos';
    }

    public function index(){
        $pageSlug = $this->pageslug;
        $data = Promo::get();

        return view('admin.promos.index', compact('data','pageSlug'));
    }

    public function create(){
        $pageSlug = $this->pageslug;
        $categories = Category::select('name','cat_id')->get();
        $lawyers = Lawyer::select('name','id')->get();
        $tags = Tag::select('name','t_id')->get();
        $articles = Article::where('title','!=','')->select('title','id')->get();
        $types = ServiceType::select('name','id')->get();

        return view('admin.promos.create',compact('pageSlug','categories','lawyers','tags','articles','types'));
    }

    public function store(Request $request){
        $data = $request->except('image','_token');
        $files = $request->file();

        if(isset($files['image'])){
            $file = $files['image'];
            //Move Uploaded File
            $destinationPath = 'public/uploads/promos';

            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
            \Intervention\Image\Facades\Image::make($file->getRealPath())->fit(600, 600)->save($destinationPath.'/'.$newFileName);
            $data['image'] = 'uploads/promos/'. $newFileName;
        }

        $data['slug'] = $this->generateSlug($data['title']);
        $newData = $this->model->create($data);

        Session::flash('success','Item saved successfully');
        return redirect('admin/promos/edit/'.$newData->id);
    }

    public function edit($id){

        $pageSlug = $this->pageslug;
        $data = $this->model->find($id);

        if(!$data)
            return 'Post not found!';

        $categories = Category::select('name','cat_id')->get();
        $lawyers = Lawyer::select('name','id')->get();
        $tags = Tag::select('name','t_id')->get();
        $articles = Article::where('title','!=','')->select('title','id')->get();
        $types = ServiceType::select('name','id')->get();
        $teamData = LawyerService::where('service_id',$data->id)->get();
        $team = [];

        if($teamData){
            foreach ($teamData as $td)
                $team[] = Lawyer::find($td->lawyer_id);
        }

        Session::flash('success','Item updated successfully');
        return view('admin.promos.edit',compact('team','pageSlug','categories','lawyers','tags','articles','types','data'));
    }


    public function update(Request $request){

        $id = $request->input('id');
        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $data = $request->except('image','_token');
        $files = $request->file();

        if(isset($files['image'])){
            $file = $files['image'];
            //Move Uploaded File
            $destinationPath = 'public/uploads/promos';

            $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();
            \Intervention\Image\Facades\Image::make($file->getRealPath())->fit(600, 600)->save($destinationPath.'/'.$newFileName);
            $data['image'] = 'uploads/promos/'. $newFileName;
        }

        $post->update($data);

        Session::flash('success','Item updated successfully');
        return redirect('admin/promos/edit/'.$post->id);
    }

    public function delete($id){

        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $post->delete();

        Session::flash('success','Item deleted successfully');

        return redirect('admin/content/press-releases');
    }


}
