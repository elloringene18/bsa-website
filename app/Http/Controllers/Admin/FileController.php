<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class FileController extends Controller
{

    public function __construct(File $model)
    {
        $this->model = $model;
        $this->pageslug = 'files';
    }

    public function index(){
        $posts = $this->model->get();
        $pageSlug = $this->pageslug;
        return view('admin.files.index', compact('posts','pageSlug'));
    }

    public function delete($id){

        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $post->delete();

        Session::flash('success','Item deleted successfully');

        return redirect('admin/files/');
    }

    public function create(){
        $pageSlug = $this->pageslug;
        return view('admin.files.create',compact('pageSlug'));
    }

    public function store(Request $request){
        $file = $request->file('file');
        //Move Uploaded File
        $destinationPath = 'public/uploads/files';

        $newFileName = str_replace(" ", "_", $file->getClientOriginalName());

        if(file_exists( public_path() . '/uploads/files/'.$newFileName)){
            $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $newFileName = str_replace(" ", "_", $file_name).'-'.rand(1,1000).'.'.$file->getClientOriginalExtension();
        }

        $upload['path'] = 'uploads/files';
        $upload['original_name'] = $file->getClientOriginalName();
        $upload['file_name'] = $newFileName;
        $upload['mime_type'] = $file->getClientOriginalExtension();

        $file->move($destinationPath,$newFileName);

        $this->model->create($upload);

        Session::flash('success','Item uploaded successfully');
        return redirect('admin/files');
    }
}
