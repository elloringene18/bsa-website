<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CustomPage;
use App\Services\CanCreateSlug;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CustomPageController extends Controller
{
    use CanCreateSlug;

    public function __construct(CustomPage $model)
    {
        $this->model = $model;
        $this->pageslug = 'pages';
    }

    public function index(){
        $pageSlug = $this->pageslug;
        $data = CustomPage::get();

        return view('admin.pages.index', compact('data','pageSlug'));
    }

    public function edit($id){
        $pageSlug = $this->pageslug;
        $data = $this->model->find($id);

        if(!$data)
            return 'Post not found!';

        return view('admin.pages.edit',compact('pageSlug','data'));
    }

    public function delete($id){

        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $post->delete();

        Session::flash('success','Item deleted successfully');

        return redirect('admin/pages');
    }


    public function update(Request $request){

        $id = $request->input('id');
        $post = $this->model->find($id);

        if(!$post)
            return 'Post not found!';

        $input = $request->except('_token');

        if($input['title']!=$post->title)
            $input['slug'] = $this->generateSlug($input['title']);

        $post->update($input);

        Session::flash('success','Item updated successfully');
        return redirect('admin/pages/edit/'.$post->id);
    }

    public function create(){
        $pageSlug = $this->pageslug;
        return view('admin.pages.create',compact('pageSlug'));
    }

    public function store(Request $request){

        $input = $request->except('_token');

        $input['slug'] = $this->generateSlug($input['title']);

        $post = $this->model->create($input);

        Session::flash('success','Item saved successfully');
        return redirect('admin/pages/edit/'.$post->id);
    }
}
