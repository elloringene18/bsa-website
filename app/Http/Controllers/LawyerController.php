<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Lawyer;
use App\Models\LawyerFilter;
use App\Models\LawyerIndustry;
use App\Models\LawyerService;
use App\Models\Location;
use App\Models\Practice;
use App\Models\Service;
use App\Models\ServiceArticles;
use App\Models\ServiceType;
use App\Models\Title;
use Illuminate\Http\Request;

class LawyerController extends Controller
{
    public function __construct(Lawyer $model)
    {
        $this->model = $model;
    }

    public function index(){

        $filtered = [];
        $data = [];

        if(isset($_GET['title']) || isset($_GET['location']) || isset($_GET['practice'])) {

            $q = Lawyer::query();

            $title = isset($_GET['title']) ? $_GET['title'] : null;
            $location = isset($_GET['location']) ? $_GET['location'] : null;
//            $practice = isset($_GET['practice']) ? $_GET['practice'] : null;
//            $industry = isset($_GET['industry']) ? $_GET['industry'] : null;
            $service = isset($_GET['service']) ? $_GET['service'] : null;

            $f['title_id'] = Title::where('slug',$_GET['title'])->first() ? Title::where('slug',$_GET['title'])->first(): null;
            $f['service_id'] = Service::where('slug',$_GET['service'])->first() ? Service::where('slug',$_GET['service'])->first() : null;
            $f['location_id'] = Location::where('slug',$_GET['location'])->first() ? Location::where('slug',$_GET['location'])->first() : null;

            $filter = LawyerFilter::where($f)->first();

            if(!$filter) { // NO WITH FILTER ON DATABASE

                if ($title) {
                    if ($title == "partner") {
                        $titles = [
                            'managing-partner',
                            'senior-partner',
                            'partner',
                            'head-of-indirect-tax-and-conveyancing',
                            'head-of-intellectual-property',
                            'head-of-office',
                            'head-of-department',
                            'head-of-spanish-and-latin-american-desk',
                            'head-of-corporate-department',
                            'head-of-corporate-practice',
                            'head-of-insurance',
                            'head-of-insurance',
                            'head-of-abu-dhabi-office',
                            'head-of-sharjah-northern-emirates-offices',
                            'head-of-lebanon-office',
                        ];
                    } elseif ($title == "associate") {
                        $titles = [
                            'associate',
                            'advocate',
                            'head-of-advocacy',
                        ];
                    } else {
                        $titles[0] = $title;
                    }


                    $titles = Title::whereIn('slug', $titles)->orderBy('id', 'ASC')->get();
                    $ids = [];

                    foreach ($titles as $title) {

                        $q = Lawyer::query();

                        $q->whereHas('titles', function ($query) use ($title) {
                            return $query->where('slug', $title->slug);
                        });

                        if ($location) {
                            $q->whereHas('locations', function ($query) use ($location) {
                                return $query->where('slug', $location);
                            });
                        }

                        //                    if($practice){
                        //                        $q->whereHas('practices', function ($query) use($practice) {
                        //                            return $query->where('slug',$practice);
                        //                        });
                        //                    }
                        //
                        //                    if($industry){
                        //                        $q->whereHas('industries', function ($query) use($industry) {
                        //                            return $query->where('slug',$industry);
                        //                        });
                        //                    }

                        if ($service) {
                            $q->whereHas('services', function ($query) use ($service) {
                                return $query->where('slug', $service);
                            });
                        }

                        $n = $q->where('hidden',0)->with('titles', 'practices', 'locations')->orderBy('last_name', 'ASC')->whereNotIn('id', $ids)->get();

                        $data[$title->slug] = $n;

                        foreach ($n as $i) {
                            $ids[] = $i->id;
                        }
                    }

                } else {

                    $ids = [];

//                    if ($service) {
//                        if ($service == 'arbitration-dispute-resolution') {
//                            $data[0][0] = Lawyer::with('titles', 'practices', 'locations')->whereNotIn('id', $ids)->find(10);
//                            $ids[] = 10;
//                        }
//                    }

                    $titles = Title::get();
                    foreach ($titles as $title) {

                        $q = Lawyer::query();

                        $q->whereHas('titles', function ($query) use ($title) {
                            return $query->where('slug', $title->slug);
                        });

                        if ($location) {
                            $q->whereHas('locations', function ($query) use ($location) {
                                return $query->where('slug', $location);
                            });
                        }

                        if ($service) {
                            $q->whereHas('services', function ($query) use ($service) {
                                return $query->where('slug', $service);
                            });
                        }

                        $n = $q->where('hidden',0)->with('titles', 'practices', 'locations')->whereNotIn('id', $ids)->orderBy('last_name', 'ASC')->get();

                        $data[$title->slug] = $n;

                        foreach ($n as $i) {
                            $ids[] = $i->id;
                        }
                    }

                }
            // WITH FILTER ON DATABASE
            } else {
                foreach($filter->items as $item){
                    $lawyer = $item->lawyer;
                    $lawyer->load('titles','practices','locations');

                    if($lawyer->hidden==0)
                        $filtered[] = $lawyer;
                }
            }

        } else {

            $filter = LawyerFilter::whereNull('title_id')->whereNull('service_id')->whereNull('location_id')->first();

            if(!$filter){
                $titles = [
                    'managing-partner',
                    'senior-partner',
                    'partner',
                ];

                $titles = Title::whereIn('slug',$titles)->orderBy('id','ASC')->get();
                $ids = [];

                foreach($titles as $title){

                    $q = Lawyer::query();

                    $q->whereHas('titles', function ($query) use($title){
                        return $query->where('slug',$title->slug);
                    });

                    $n = $q->where('hidden',0)->with('titles','practices','locations')->orderBy('last_name','ASC')->whereNotIn('id',$ids)->get();

                    $data[$title->slug] = $n;

                    foreach($n as $i){
                        $ids[] = $i->id;
                    }
                }
            } else {
                foreach($filter->items as $item){
                    $lawyer = $item->lawyer;
                    $lawyer->load('titles','practices','locations');

                    if($lawyer->hidden==0)
                        $filtered[] = $lawyer;
                }

            }
        }

        $titles = Title::get();
        $locations = Location::get();
        $practices = Practice::get();
        $lawyers = Lawyer::where('hidden',0)->get();

        $serviceTypes = ServiceType::get();
        $types = [];

        foreach ($serviceTypes as $type){
            $types[$type->name] = Service::where('service_type_id',$type->id)->select('slug','title','id')->where('hidden',0)->get();
        }

        return view('dynamic.our-people', compact('filtered','data','titles','locations','practices','types','lawyers'));
    }

    public function filter(Request $request){
        $titles = Title::get();
        $locations = Location::get();
        $practices = Practice::get();

        return view('dynamic.our-people', compact('titles','locations','practices'));
    }

    public function showAllList(){

        $data = [];
        $services = Service::get();

        foreach ($services as $service){
            $lawyers = LawyerService::where('service_id',$service->id)->get();

            foreach($lawyers as $lawyer){
                $d = Lawyer::find($lawyer->lawyer_id);
                $data['services'][$service->title][] = $d->name;
            }
        }

        $industries = Service::get();

        foreach ($industries as $industry){
            $lawyers = LawyerIndustry::where('service_id',$industry->id)->get();

            foreach($lawyers as $lawyer){
                $d = Lawyer::find($lawyer->lawyer_id);
                $data['industries'][$industry->title][] = $d->name;
            }
        }

        dd($data);
    }

    public function show($slug){
        $data = Lawyer::with('titles','practices','locations')->where('slug',$slug)->where('hidden',0)->first();
        $news = [];

        if($data)
            return view('dynamic.people', compact('data','news'));

        return view('404');
    }

    public function preview($slug){
        $data = Lawyer::with('titles','practices','locations')->where('slug',$slug)->first();
        $news = [];

        return view('dynamic.people', compact('data','news'));
    }
}
