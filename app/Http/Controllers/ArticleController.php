<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleLawyer;
use App\Models\Category;
use App\Models\Lawyer;
use App\Models\Service;
use App\Models\ServiceType;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function __construct(Article $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $news = $this->model->with('tags','categories')->whereHas('categories', function($query) {
            return $query->where('slug','news');
        })->where('hidden',0)->limit(8)->orderBy('date','DESC')->get();

        $reports = $this->model->with('tags','categories')->whereHas('categories', function($query) {
            return $query->where('slug','reports');
        })->where('hidden',0)->limit(8)->orderBy('date','DESC')->get();

        $updates = $this->model->with('tags','categories')->whereHas('categories', function($query) {
            return $query->where('slug','regulatory-and-legal-updates');
        })->where('hidden',0)->limit(8)->orderBy('date','DESC')->get();

        $pastevents = $this->model->with('tags','categories')->whereHas('categories', function($query) {
            return $query->where('slug','events');
        })->where('hidden',0)->limit(8)->orderBy('date','DESC')->whereDate('date','<', Carbon::now())->get();

        $upcomingevents = $this->model->with('tags','categories')->whereHas('categories', function($query) {
            return $query->where('slug','events');
        })->where('hidden',0)->limit(8)->orderBy('date','DESC')->whereDate('date','>=', Carbon::now())->get();

        return view('dynamic.knowledge-hub',compact('pastevents','upcomingevents','news','reports','updates'));
    }

    public function slug($slug)
    {

        $yearStart = isset($_GET['year']) ? ($_GET['year'] ? Carbon::parse('01-01-'.$_GET['year']) : null ) : null;

        $currentDate = $yearStart;
        $dotPosts = [];

//        for($x=0;$x<12;$x++){
//
//            $posts = $this->model->whereHas('categories', function($query) use($slug) {
//                return $query->where('slug',$slug);
//            })->whereDate('date','>=',$currentDate->format('Y-m-d'))->whereDate('date','<=',$currentDate->addMonth()->format('Y-m-d'))->get();
//
//            $dotPosts[$x] = $posts;
//        }

        $service = isset($_GET['service']) ? $_GET['service'] : null;

        if($service){

            if($currentDate)
                $data = $this->model->with('tags','categories')->whereHas('categories', function($query) use($slug) {
                    return $query->where('slug',$slug);
                })->whereHas('services', function($query) use($service) {
                    return $query->where('slug',$service);
                })->where('hidden',0)->whereDate('date','>=',$currentDate->format('Y-m-d'))->whereDate('date','<=',$currentDate->lastOfYear()->format('Y-m-d'))->orderBy('date','DESC')->paginate(12);
            else
                $data = $this->model->with('tags','categories')->whereHas('categories', function($query) use($slug) {
                    return $query->where('slug',$slug);
                })->whereHas('services', function($query) use($service) {
                    return $query->where('slug',$service);
                })->where('hidden',0)->orderBy('date','DESC')->paginate(12);

        } else {

            if($currentDate)
                $data = $this->model->with('tags','categories')->whereHas('categories', function($query) use($slug) {
                    return $query->where('slug',$slug);
                })->where('hidden',0)->whereDate('date','>=',$currentDate->format('Y-m-d'))->whereDate('date','<=',$currentDate->lastOfYear()->format('Y-m-d'))->orderBy('date','DESC')->paginate(12);
            else
                $data = $this->model->with('tags','categories')->whereHas('categories', function($query) use($slug) {
                    return $query->where('slug',$slug);
                })->where('hidden',0)->orderBy('date','DESC')->paginate(12);
        }

        $category = Category::where('slug',$slug)->first();
        $tags = Tag::orderBy('name','ASC')->get();

        $serviceTypes = ServiceType::get();
        $types = [];

        foreach ($serviceTypes as $type){
            $types[$type->name] = Service::where('service_type_id',$type->id)->select('slug','title','id')->where('hidden',0)->get();
        }

        return view('dynamic.articles',compact('data','category','tags','types'));
    }

    public function searchTag()
    {
        $tag = $_GET['s'];
        $data = $this->model->with('tags','categories')->whereHas('tags', function($query) use($tag) {
            return $query->where('name',$tag);
        })->where('hidden',0)->orderBy('date','DESC')->paginate(12);

        $category = Category::where('slug','news')->first();
        $tags = Tag::orderBy('name','ASC')->get();

        return view('dynamic.articles',compact('data','category','tags'));
    }

    public function article($category,$slug)
    {
        $data = $this->model->with('services','categories','tags','lawyer')->where('slug',$slug)->where('hidden',0)->first();

        if(!$data)
            return view('404');

        $category = Category::where('slug',$category)->first();

        if($category->slug == 'events')
            $similar = $this->model->with('categories','tags')->where('id','!=',$data->id)->whereHas('categories',function ($query){
                return $query->where('slug','events');
            })->orderBy('date','DESC')->limit(8)->get();
        else
            $similar = $this->model->with('categories','tags')->where('id','!=',$data->id)->whereHas('categories', function($query){
                return $query->where('slug','!=','events');
            })->whereHas('services',function ($query) use($data){
                $servs = [];
                if($data->services){
                    foreach ($data->services as $service)
                        $servs[] = $service->id;
                }
                return $query->whereIn('service_id',$servs);
            })->orderBy('date','DESC')->limit(8)->get();

        return view('dynamic.article',compact('data','category','similar'));
    }

    public function preview($category,$slug)
    {
        $data = $this->model->with('categories','tags','lawyer')->where('slug',$slug)->first();

        if(!$data)
            return view('404');

        $category = Category::where('slug',$category)->first();

        $similar = $this->model->with('categories','tags')->where('id','!=',$data->id)->orderBy('date','DESC')->limit(8)->get();

        return view('dynamic.article',compact('data','category','similar'));
    }

    public function attachLawyers()
    {
        $data = Article::with('tags')->get();

        foreach($data as $article){
            foreach($article->tags as $tag) {
                $lawyer = Lawyer::where('name',$tag->name)->first();
                if($lawyer){
                    $exists = ArticleLawyer::where('article_id',$article->id)->where('lawyer_id',$lawyer->id)->first();

                    if(!$exists)
                        $article->lawyer()->create(['lawyer_id'=>$lawyer->id]);
                }
            }
        }
    }
}
