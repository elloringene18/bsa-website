<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Lawyer;
use App\Models\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function __construct(Service  $model)
    {
        $this->model = $model;
    }

    public function show($slug)
    {
        $data = $this->model->where('slug',$slug)->where('hidden',0)->first();

        if(!$data)
            return view('404');

        $partners = Lawyer::whereHas('titles', function ($query){
                return $query->where('slug','partner');
            })->inRandomOrder()->limit(8)->get();

        return view('dynamic.service',compact('data','partners'));
    }


    public function preview($slug)
    {
        $data = $this->model->where('slug',$slug)->first();

        if(!$data)
            return view('404');

        $partners = Lawyer::whereHas('titles', function ($query){
            return $query->where('slug','partner');
        })->inRandomOrder()->limit(8)->get();

        return view('dynamic.service',compact('data','partners'));
    }
}
