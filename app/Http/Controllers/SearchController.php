<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Lawyer;
use App\Models\Service;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(){
        $keyword = $_GET['keyword'];

        $data['articles'] = Article::search($_GET['keyword'], null, true)->orderBy('date','DESC')->paginate(10);

        return view('search',compact('data','keyword'));
    }
    public function searchAll(){
        $keyword = $_GET['keyword'];

        $data['articles'] = Article::where('hidden',0)->search($_GET['keyword'], null, true, true)->orderBy('date','DESC')->get();
        $data['lawyers'] = Lawyer::where('hidden',0)->search($_GET['keyword'], null, true)->get();
        $data['services'] = Service::search($_GET['keyword'], null, true)->where('hidden',0)->get();

        return view('search-full',compact('data','keyword'));
    }

}
