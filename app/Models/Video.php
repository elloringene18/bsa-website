<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Video extends Model
{

    use SearchableTrait;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'title' => 10,
            'title_ar' => 10,
            'content' => 1,
            'content_ar' => 1,
            'speaker' => 1,
            'speaker_ar' => 1,
            'series' => 1,
            'series_ar' => 1,
            'additional_content_top' => 1,
            'additional_content_bottom' => 1
        ]
    ];

    protected $fillable = [
        'title',
        'excerpt',
        'speaker',
        'series',
        'slug',
        'description',
        'date',
        'content',
        'active',
        'video_file',
        'image',
    ];

    public $dates = ['date'];
    public $timestamps = false;
}
