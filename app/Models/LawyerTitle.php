<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LawyerTitle extends Model
{
    use HasFactory;

    protected $fillable = ['lawyer_id','title_id','primary'];
}
