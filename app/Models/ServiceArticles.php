<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceArticles extends Model
{
    use HasFactory;

    protected $fillable = ['article_id','service_id'];

    public function service(){
        return $this->hasOne('App\Models\Service','service_id','id');
    }
}
