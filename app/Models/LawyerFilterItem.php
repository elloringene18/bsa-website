<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LawyerFilterItem extends Model
{
    use HasFactory;

    protected $fillable = ['lawyer_id','lawyer_filter_id'];

    public function lawyer(){
        $this->hasOne('App\Models\Lawyer');
    }

    public function getLawyerAttribute(){
        return Lawyer::find($this->lawyer_id);
    }
}
