<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LawyerFilter extends Model
{
    use HasFactory;

    protected $fillable = ['title_id','service_id','location_id'];

    public function getTitleAttribute(){
        return Title::where('t_id',$this->title_id)->first();
    }

    public function getServiceAttribute(){
        return Service::find($this->service_id);
    }

    public function getLocationAttribute(){
        return Location::where('l_id',$this->location_id)->first();
    }

    public function items(){
        return $this->hasMany('App\Models\LawyerFilterItem','lawyer_filter_id','id');
    }

    public function getItemsAttribute(){
        return $this->items()->get();
    }
}
