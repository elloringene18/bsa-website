<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleVersion extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'excerpt',
        'content',
        'date',
        'photo',
        'photo_full',
        'thumbnail',
        'hidden',
        'categories',
        'tags',
        'lawyers',
        'services',
        'locations',
        'related',
        'promos',
        'article_id',
        'event_time',
        'event_location',
        'version',
        'is_current',
        'meta_title',
        'meta_description',
        'meta_keywords',
    ];
}
