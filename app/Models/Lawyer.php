<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Lawyer extends Model
{
    use HasFactory;

    use SearchableTrait;

    protected $fillable = ['name','first_name','last_name','slug','content','photo','email','telephone','excerpt','hidden','meta_title','meta_description'];
    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'name' => 1,
        ]
    ];

    public function locations(){
        return $this->belongsToMany('App\Models\Location','lawyer_locations','lawyer_id','location_id','','l_id');
    }

    public function practices(){
        return $this->belongsToMany('App\Models\Practice','lawyer_practices','lawyer_id','practice_id','','p_id');
    }

    public function titles(){
        return $this->belongsToMany('App\Models\Title','lawyer_titles','lawyer_id','title_id','','t_id');
    }

    public function tags(){
        return $this->hasMany('App\Models\LawyerTag');
    }

    public function articles(){
        return $this->hasMany('App\Models\LawyerArticle');
    }

    public function services(){
        return $this->belongsToMany('App\Models\Service','lawyer_services');
    }

    public function reels(){
        return $this->belongsToMany('App\Models\Service','lawyer_reels');
    }

    public function quotes(){
        return $this->hasMany('App\Models\LawyerQuote');
    }

//    public function industries(){
//        return $this->belongsToMany('App\Models\Service','lawyer_industries');
//    }

    public function news(){
        return $this->hasOne('App\Models\ArticleLawyer');
    }

    public function experience(){
        return $this->hasMany('App\Models\LawyerExperience');
    }

    public function education(){
        return $this->hasMany('App\Models\LawyerEducation');
    }

    public function getNewsAttribute(){
        $data = Article::whereHas('lawyer',function ($query){
           return $query->where('lawyer_id',$this->id);
        })->whereHas('categories',function ($query){
           return $query->where('slug','news');
        })->where('hidden',0)->get();

        return $data;
    }

    public function getExperienceAttribute(){
        return $this->experience()->get();
    }

    public function getServicesAttribute(){
        return $this->services()->where('hidden',0)->get();
    }

    public function getReelsAttribute(){
        return $this->reels()->where('hidden',0)->get();
    }

    public function getServicesByTypeAttribute(){
        $types = ServiceType::get();
        $data = [];
        $data['total'] = 0;

        foreach ($types as $type){
            $services = $this->services()->whereHas('type',function ($query) use($type){
                return $query->where('slug',$type->slug);
            })->where('hidden',0)->get();

            $data['data'][$type->slug] = $services;

            $data['total'] += count($services);
        }

        return $data;
    }

    public function getIndustriesAttribute(){
        return $this->industries()->get();
    }

    public function getPhotoUrlAttribute(){
        if (strpos($this->photo, 'http') !== false) {
            return $this->photo;
        } else {
            if($this->photo)
                return asset($this->photo);
            else
                return asset('img/profiles/blank.jpg');
        }
    }

    public function getPublicationsAttribute(){
        $data = Article::whereHas('lawyer',function ($query){
           return $query->where('lawyer_id',$this->id);
        })->whereHas('categories',function ($query){
           return $query->where('slug','publications-2');
        })->where('hidden',0)->get();

        return $data;
    }

    public function getInsightsAttribute(){

        $articles = count($this->articles);
        $tagged = count($this->tagArticles);
        $services = $this->reels()->pluck('service_id');

        if(!count($services))
            $services = $this->services()->pluck('service_id');

        $toget = 12 - $articles;
        $data = [];

        if($toget>0){
            $data = Article::whereHas('services',function ($query) use($services){
                return $query->whereIn('service_id',$services);
            })->orderBy('date','DESC')->limit($toget)->get();
        }

//        $data[0] = Article::whereHas('lawyer',function ($query){
//           return $query->where('lawyer_id',$this->id);
//        })->whereHas('categories',function ($query){
//           return $query->whereIn('slug',['publications-2','news']);
//        })->where('hidden',0)->orderBy('date','DESC')->get();
//
//        $lits = [5,24,6,7];
//
//        if(in_array($this->id,$lits)){
//
//            $service = [6];
//            $data[1] = Article::whereHas('categories',function ($query){
//                return $query->whereIn('slug',['news','publications-2']);
//            })->whereHas('services',function ($query) use($service){
//                return $query->whereIn('service_id',$service);
//            })->where('hidden',0)->orderBy('date','DESC')->limit(8)->get();
//
//        } else {
//            $data[1] = Article::whereHas('categories',function ($query){
//                return $query->whereIn('slug',['news','publications-2']);
//            })->whereHas('services',function ($query) use($service){
//                return $query->whereIn('service_id',$service);
//            })->where('hidden',0)->orderBy('date','DESC')->limit(8)->get();
//        }

        return $data;
    }

    public function getTagsAttribute(){
        $data = [];

        foreach ($this->tags()->get() as $item){
            $d = Tag::where('t_id',$item->tag_id)->first();
            if($d)
                $data[] = $d;
        }
        return $data;
    }

    public function getArticlesAttribute(){
        $data = [];

        $data = Article::whereHas('lawyer',function($query){
            return $query->where('lawyer_id',$this->id);
        })->where('hidden',0)->orderBy('date','DESC')->get();

        return $data;
    }

    public function getPrimaryTitleAttribute(){
        $data = $this->titles()->where('primary',1)->first();

        if(!$data)
            $data = $this->titles()->first()->name;

        return $data;
    }

    public function getSubTitleAttribute(){
        $data = $this->titles()->where('primary',0)->first();
        return $data;
    }

    public function getTagArticlesAttribute(){
        $data = [];

        $tags = $this->tags()->get();

        if($tags){
            foreach ($tags as $tag){

                $articles = Article::whereHas('tags',function($query) use($tag){
                    return $query->where('t_id',$tag->tag_id);
                })->where('hidden',0)->orderBy('date','DESC')->limit(10)->get();

                foreach ($articles as $item){
                    $data[] = $item;
                }
            }
        }

        return $data;
    }

    public function getLocationAttribute(){
        $data = [];

        return $this->locations()->first();
    }
}
