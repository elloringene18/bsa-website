<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleLawyer extends Model
{
    use HasFactory;

    protected $fillable = ['lawyer_id','article_id'];

    public $timestamps = false;
}
