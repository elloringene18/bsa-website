<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Service extends Model
{
    use HasFactory;
    use SearchableTrait;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'title' => 10,
            'content' => 1,
        ]
    ];

    protected $fillable = ['title','slug','content','date','photo','thumbnail','service_type_id','hidden','meta_title','meta_description'];

    public function articles(){
        return $this->hasMany('App\Models\ServiceArticles');
    }

    public function type(){
        return $this->hasOne('App\Models\ServiceType','id','service_type_id');
    }

    public function contacts(){
        return $this->hasMany('App\Models\ServiceContact');
    }

    public function tags(){
        return $this->hasMany('App\Models\ServiceTag');
    }

    public function quotes(){
        return $this->hasMany('App\Models\ServiceQuote');
    }

    public function promos(){
        return $this->belongsToMany('App\Models\Promo','service_promos');
    }

    public function team(){
        return $this->hasMany('App\Models\ServiceTeam');
    }

    public function getArticlesAttribute(){
        $data = [];

        $data = Article::whereHas('services',function($query){
            return $query->where('slug',$this->slug);
        })->where('hidden',0)->get();

        return $data;
    }

    public function getTagArticlesAttribute(){
        $data = [];

        $tags = $this->tags()->get();

        if($tags){
            foreach ($tags as $tag){

                $articles = Article::whereHas('tags',function($query) use($tag){
                    return $query->where('t_id',$tag->tag_id);
                })->where('hidden',0)->orderBy('date','DESC')->limit(10)->get();

                foreach ($articles as $item){
                    $data[] = $item;
                }
            }
        }

        return $data;
    }

    public function date_compare($a, $b)
    {
        $t1 = strtotime($a['date']);
        $t2 = strtotime($b['date']);
        return $t1 - $t2;
    }

    public function getPromosAttribute(){
        $data = [];
        $data = $this->promos()->get();
        return $data;
    }

    public function getContactsAttribute(){
        $data = [];

        $titles = Title::get();
        $contacts = $this->contacts()->pluck('lawyer_id');
        $ids = [];

//        foreach($titles as $title){
//            $q = Lawyer::query();
//
//            $q->whereHas('titles', function ($query) use($title){
//                return $query->where('slug',$title->slug);
//            })->whereIn('id',$contacts)->whereNotIn('id',$ids)->orderBy('name','ASC');
//
//            $f = $q->where('hidden',0)->with('titles','practices','locations')->orderBy('last_name','ASC')->get();
//
//            if(count($f))
//                $data[$title->slug] = $f;
//
//            foreach ($f as $i)
//                $ids[] = $i->id;
//        }

        foreach($contacts as $item){
            $item = Lawyer::where('id',$item)->where('hidden',0)->with('titles','practices','locations')->first();

            if($item)
                $data[] = $item;
        }

        return $data;
    }

    public function getTagsAttribute(){
        $data = [];

        foreach ($this->tags()->get() as $item){
            $d = Tag::where('t_id',$item->tag_id)->first();
            if($d)
                $data[] = $d;
        }
        return $data;
    }

    public function getTeamAttribute(){
        $data = [];

        foreach ($this->team()->get() as $item){
            $d = Lawyer::find($item->lawyer_id);
            if($d)
                $data[] = $d;
        }

        return $data;
    }
}
