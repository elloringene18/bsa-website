<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LawyerArticle extends Model
{
    use HasFactory;
    protected $fillable = ['article_id'];

    public function lawyer(){
        return $this->hasOne('App\Models\Lawyer','lawyer_id','id');
    }
}
