<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['name','slug','cat_id'];

    public function articles(){
        return $this->belongsToMany('App\Models\Article','article_categories','category_id','article_id', 'cat_id', '');
    }
}
