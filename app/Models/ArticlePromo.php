<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticlePromo extends Model
{
    use HasFactory;

    protected $fillable = ['promo_id','article_id'];
}
