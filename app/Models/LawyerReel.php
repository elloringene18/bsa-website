<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LawyerReel extends Model
{
    use HasFactory;

    protected $fillable = ['service_id','lawyer_id'];
}
