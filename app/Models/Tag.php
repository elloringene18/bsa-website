<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    protected $fillable = ['name','slug','t_id'];

    public function lawyers(){
        return $this->belongsToMany('App\Models\Lawyer','lawyer_tags','tag_id','lawyer_id','t_id','');
    }

    public function services(){
        return $this->belongsToMany('App\Models\Service','service_tags','tag_id','service_id','t_id','');
    }

    public function articles(){
        return $this->belongsToMany('App\Models\Article','article_tags','tag_id','article_id','t_id','');
    }

}
