<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LawyerTag extends Model
{
    use HasFactory;
    protected $fillable = ['lawyer_id','tag_id'];

    public function tags(){
        return $this->belongsToMany('App\Models\Tag','lawyer_tags','lawyer_id','tag_id','','t_id');
    }
}
