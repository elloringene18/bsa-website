<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Article extends Model
{
    use HasFactory;
    use SearchableTrait;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'title' => 10,
            'content' => 1,
        ]
    ];

    protected $fillable = ['title','slug','excerpt','content','date','photo','photo_full','hidden','event_time','event_location','meta_title','meta_description','meta_keywords','rsvp_text','rsvp_link'];

    public $timestamps = ['date'];

    public function categories(){
        return $this->belongsToMany('App\Models\Category','article_categories','article_id','category_id','','cat_id');
    }

    public function versions(){
        return $this->hasMany('App\Models\ArticleVersion');
    }

    public function tags(){
        return $this->belongsToMany('App\Models\Tag','article_tags','article_id','tag_id','','t_id');
    }

    public function lawyer(){
        return $this->hasMany('App\Models\ArticleLawyer');
    }

    public function location(){
        return $this->hasMany('App\Models\ArticleLocation');
    }

    public function promos(){
        return $this->belongsToMany('App\Models\Promo','article_promos');
    }

    public function services(){
        return $this->belongsToMany('App\Models\Service','service_articles','article_id');
    }

    public function articles(){
        return $this->belongsToMany('App\Models\Article','article_related','article_related_id');
    }

//    public function lawyers(){
//        return $this->belongsToMany('App\Models\Article','lawyer_articles','article_id');
//    }

    public function getLawyerAttribute(){
        $row = $this->lawyer()->get();

        $data = [];
        foreach($row as $item)
            $data[] = Lawyer::find($item->lawyer_id);

        return $data;
    }

    public function getTagsAttribute(){
        $data = [];

        foreach ($this->tags()->get() as $item){
            $d = Tag::where('t_id',$item->t_id)->first();
            if($d)
                $data[] = $d;
        }

        return $data;
    }

    public function getLocationAttribute(){
        $data = [];

        $data= $this->location()->first();

        return $data;
    }

//    public function getCategoriesAttribute(){
//        $data = [];
//
//        $data= $this->categories()->get();
//
//        return $data;
//    }

    public function getCategoriesAttribute(){
        $data = [];

        $data= $this->categories()->first();

        return $data;
    }

    public function getPromosAttribute(){
        $data = [];
        $data = $this->promos()->get();
        return $data;
    }

    public function getArticlesAttribute(){
        $data = [];

        $data = Article::whereHas('articles',function($query){
            return $query->where('article_id',$this->id);
        })->get();


        return $data;
    }

    public function getTagArticlesAttribute(){
        $data = [];

        $tags = $this->tags()->get();

        if($tags){
            foreach ($tags as $tag){
                $articles = Article::whereHas('tags',function($query) use($tag){
                    return $query->where('t_id',$tag->t_id);
                })->orderBy('date','DESC')->whereNotIn('id',[$this->id])->limit(10)->get();

                foreach ($articles as $item){
                    if(count($data)<10){
                        $data[] = $item;
                    }
                }
            }
        }

        return $data;
    }


}
