<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LawyerVersion extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'first_name',
        'last_name',
        'slug',
        'content',
        'photo',
        'telephone',
        'email',
        'excerpt',
        'hidden',
        'titles',
        'practices',
        'locations',
        'articles',
        'experiences',
        'services',
        'industries',
        'quotes',
        'tags',
        'educations',
        'lawyer_id',
        'version',
        'is_current',
    ];

}
