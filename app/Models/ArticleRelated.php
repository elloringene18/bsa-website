<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleRelated extends Model
{
    use HasFactory;

    protected $fillable = ['article_related_id','article_id'];
}
