<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceTag extends Model
{
    use HasFactory;

    protected $fillable = ['service_id','tag_id'];

    public function tags(){
        return $this->belongsToMany('App\Models\Tag','service_tags','service_id','tag_id','','t_id');
    }
}
