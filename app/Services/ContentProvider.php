<?php

namespace App\Services;
use App\Models\Activity;
use App\Models\Article;
use App\Models\Award;
use App\Models\Benefit;
use App\Models\BottomLink;
use App\Models\ClientCategory;
use App\Models\HighlightArticle;
use App\Models\HomeContent;
use App\Models\HomeIcon;
use App\Models\HomeItem;
use App\Models\Leadership;
use App\Models\Location;
use App\Models\Page;
use App\Models\PageSub;
use App\Models\PageSection;
use App\Models\Podcast;
use App\Models\Promo;
use App\Models\Service;
use App\Models\Team;
use App\Models\Timeline;
use App\Models\Value;
use App\Models\Video;
use Carbon\Carbon;
use Database\Seeders\BenefitSeed;
use Database\Seeders\HomeIconSeeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Psy\Util\Str;
use Ramsey\Uuid\Type\Time;

class ContentProvider
{
    public function getPageSections($slug){
        $data = PageSection::where('page',$slug)->get();

        $sections = [];
        foreach ($data as $item){
            $sections[$item->slug] = $item->content;
        }

        return $sections;
    }

    public function getPageSection($slug){
        $data = PageSection::where('slug',$slug)->first();

        if($data)
            return $data->content;

        return [];
    }

    public function getArticlesFromJSON($json){
        $data = json_decode($json,true);

        if(is_array($data)){
            $s = [];

            foreach ($data as $item){
                $s[] = Article::find($item['id']);
            }

            return $s;
        }

        return [];
    }

    public function getActivities(){
        return Article::whereHas('categories',function ($query){ return $query->where('slug','csr-activity'); })->get();
    }

    public function getServicesByType($type){
        $data = Service::whereHas('type',function($query) use($type) {
            return $query->where('slug',$type);
        })->where('hidden',0)->orderBy('title',"ASC")->get();

        return $data;
    }

    public function getAwards(){
        return Award::get();
    }

    public function getPromoboxes(){
        $data = Promo::get();
        return $data;
    }

    public function getPromoboxById($id){
        $data = Promo::find($id);
        return $data;
    }

    public function getArticles(){
        $data = Article::select('id','title')->get();
        return $data;
    }

    public function getValues(){
        $data = Value::get();
        return $data;
    }

    public function getTimeline(){
        $data = Timeline::orderBy('date','ASC')->get();
        return $data;
    }

    public function getPodcasts(){
        $data = Podcast::orderBy('date','ASC')->get();
        return $data;
    }

    public function getBottomLinks(){
        $data = BottomLink::get();
        return $data;
    }

    public function getVideos(){
        $data = Video::orderBy('date','ASC')->get();
        return $data;
    }

    public function getHighlightArticles(){
        $ids = HighlightArticle::get()->pluck('article_id');

        $data = Article::with('tags','categories')->whereIn('id',$ids)->where('hidden',0)->orderBy('date','DESC')->get();

        return $data;
    }

    public function getLocations(){
        $data = Location::get();
        return $data;
    }

    public function getSection($page,$sectionId){
        $data = PageSection::where('id',$sectionId)->whereHas('page',function($q) use ($page) {
            return $q->where('slug',$page);
        })->get();

        $rets = [];

        foreach($data as $item){
            $rets[$item->slug]['name'] = $item->title_en;
            $rets[$item->slug]['name_en'] = $item->title_en;
            $rets[$item->slug]['name_ar'] = $item->title_ar;
            $rets[$item->slug]['de'] = $item->content;
            $rets[$item->slug]['ar'] = $item->content_ar;
            $rets[$item->slug]['en'] = $item->content_en;
            $rets[$item->slug]['type'] = $item->type;
            $rets[$item->slug]['slug'] = $item->slug;
        }

        return $rets;
    }

    public function getSettings(){
        $data = PageContent::whereHas('page',function($q) {
            return $q->where('slug','settings');
        })->select('name','content','content_ar','slug','type')->get();

        $rets = [];

        foreach($data as $item){
            $rets[$item->slug] = $item->content;
        }

        return $rets;
    }

}
