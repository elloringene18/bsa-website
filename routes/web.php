<?php

use App\Models\Category;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/userTimeline', function()
{
//    $querier = \Atymic\Twitter\Facade\Twitter::forApiV2()
//        ->getQuerier();
//
//    $result = $querier
//        ->withOAuth2Client()
//        ->get("tweets/search/recent", ["from" => 'bsa_bh','max_results'=>5]);

    $userId = '1371472393';

    $params = [
        'max_results' => '5',
        'place.fields' => 'country,name',
        'tweet.fields' => 'text,attachments,entities,created_at',
        'media.fields' => 'preview_image_url,url',
        'expansions' => 'attachments.media_keys',
        \Atymic\Twitter\Contract\Twitter::KEY_RESPONSE_FORMAT => \Atymic\Twitter\Contract\Twitter::RESPONSE_FORMAT_JSON,
    ];

    return Twitter::userTweets($userId, $params);
});

Route::post('/contact','FormController@contact');
Route::get('/reload-captcha', 'FormController@reloadCaptcha');
Route::get('/contact-bsa',function (){
   return redirect('contact-us');
});
Route::post('/subscribe','FormController@subscribe');
Route::get('/show-all-lists','LawyerController@showAllList');

Route::post('/careers/submit','ApplicationController@store');
Route::post('/subscribe','FormController@subscribe');

Route::get('/legal-practice-areas/{slug}','ServiceController@show');
Route::get('/legal-practice-areas/{slug}/preview','ServiceController@preview');
Route::get('/knowledge-hub','ArticleController@index');
Route::get('/knowledge-hub/news/search-tag','ArticleController@searchTag');
Route::get('/knowledge-hub/{slug}','ArticleController@slug');
Route::get('/knowledge-hub/{category}/{slug}','ArticleController@article');
Route::get('/knowledge-hub/{category}/{slug}/preview','ArticleController@preview');

Route::get('/our-people','LawyerController@index');
Route::get('/lawyer/{slug}','LawyerController@show');
Route::get('/lawyer/{slug}/preview','LawyerController@preview');

Route::get('/attachLawyers','ArticleController@attachLawyers');

Route::get('/search','SearchController@search');
Route::get('/search-full','SearchController@searchAll');

Route::get('/about-us/csr-activities/{slug}',function ($slug){
    $data = \App\Models\Article::where('slug',$slug)->first();
    $similar = \App\Models\Article::whereHas('categories',function ($query){ return $query->where('slug','csr-activity'); })->get();
    return view('csr-activity',compact('data','similar'));
});

Route::get('admin/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('admin/login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('get-times', 'APIController@getTimes');

Route::group(['prefix' => 'admin','middleware' => 'auth'], function() {

    Route::group(['prefix' => 'contacts','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\FormController@index');
        Route::get('/view/{id}', 'Admin\FormController@show');
    });

    Route::group(['prefix' => 'sections','middleware' => 'auth'], function() {
        Route::get('/edit/{id}', 'Admin\SectionController@edit');
        Route::post('/update', 'Admin\SectionController@update');
        Route::get('/delete/{id}', 'Admin\SectionController@delete');
        Route::get('/delete-slide/{id}', 'Admin\SectionController@deleteSlide');
    });

    Route::group(['prefix' => 'home','middleware' => 'auth'], function() {
        Route::get('/icons', 'Admin\HomeController@icons');
        Route::post('/icons/update', 'Admin\HomeController@update');
    });

    Route::group(['prefix' => 'about','middleware' => 'auth'], function() {

        Route::group(['prefix' => 'timelines','middleware' => 'auth'], function() {
            Route::get('/', 'Admin\TimelineController@index');
            Route::get('/create', 'Admin\TimelineController@create');
            Route::post('/store', 'Admin\TimelineController@store');
            Route::post('/update', 'Admin\TimelineController@update');
            Route::get('/edit/{id}', 'Admin\TimelineController@edit');
            Route::get('/delete/{id}', 'Admin\TimelineController@delete');
        });

        Route::group(['prefix' => 'awards','middleware' => 'auth'], function() {
            Route::get('/', 'Admin\AwardController@index');
            Route::get('/create', 'Admin\AwardController@create');
            Route::post('/store', 'Admin\AwardController@store');
            Route::post('/update', 'Admin\AwardController@update');
            Route::get('/edit/{id}', 'Admin\AwardController@edit');
            Route::get('/delete/{id}', 'Admin\AwardController@delete');
        });

        Route::group(['prefix' => 'activities','middleware' => 'auth'], function() {
            Route::get('/', 'Admin\ActivityController@index');
            Route::get('/create', 'Admin\ActivityController@create');
            Route::post('/store', 'Admin\ActivityController@store');
            Route::post('/update', 'Admin\ActivityController@update');
            Route::get('/edit/{id}', 'Admin\ActivityController@edit');
            Route::get('/delete/{id}', 'Admin\ActivityController@delete');
        });
    });

    Route::group(['prefix' => 'careers','middleware' => 'auth'], function() {

        Route::group(['prefix' => 'values','middleware' => 'auth'], function() {
            Route::get('/', 'Admin\ValueController@index');
            Route::get('/create', 'Admin\ValueController@create');
            Route::post('/store', 'Admin\ValueController@store');
            Route::post('/update', 'Admin\ValueController@update');
            Route::get('/edit/{id}', 'Admin\ValueController@edit');
            Route::get('/delete/{id}', 'Admin\ValueController@delete');
        });

    });

    Route::group(['prefix' => 'articles','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\ArticleController@index');
        Route::get('/create', 'Admin\ArticleController@create');
//        Route::get('/convert', 'Admin\ArticleController@convert');
//        Route::get('/check-cats', 'Admin\ArticleController@checkcats');
        Route::get('/metatags', 'Admin\ArticleController@metatags');
        Route::get('/cmetatags', 'Admin\ArticleController@cmetatags');
        Route::post('/store', 'Admin\ArticleController@store');
        Route::post('/update', 'Admin\ArticleController@update');
        Route::get('/edit/{id}', 'Admin\ArticleController@edit');
        Route::get('/versions/{id}', 'Admin\ArticleController@versions');
        Route::get('/versions/{article_id}/compare/{verson_id}', 'Admin\ArticleController@compare');
        Route::get('/versions/{id}/restore', 'Admin\ArticleController@restore');
        Route::get('/delete/{id}', 'Admin\ArticleController@delete');
    });

    Route::group(['prefix' => 'events','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\EventController@index');
        Route::get('/create', 'Admin\EventController@create');
        Route::post('/store', 'Admin\EventController@store');
        Route::post('/update', 'Admin\EventController@update');
        Route::get('/edit/{id}', 'Admin\EventController@edit');
        Route::get('/delete/{id}', 'Admin\EventController@delete');
    });

    Route::group(['prefix' => 'bottomlinks','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\BottomLinkController@index');
        Route::post('/update', 'Admin\BottomLinkController@update');
    });

    Route::group(['prefix' => 'pages','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\CustomPageController@index');
        Route::get('/create', 'Admin\CustomPageController@create');
        Route::post('/store', 'Admin\CustomPageController@store');
        Route::post('/update', 'Admin\CustomPageController@update');
        Route::get('/edit/{id}', 'Admin\CustomPageController@edit');
        Route::get('/delete/{id}', 'Admin\CustomPageController@delete');
    });

    Route::group(['prefix' => 'podcasts','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\PodcastController@index');
        Route::get('/create', 'Admin\PodcastController@create');
        Route::post('/store', 'Admin\PodcastController@store');
        Route::post('/update', 'Admin\PodcastController@update');
        Route::get('/edit/{id}', 'Admin\PodcastController@edit');
        Route::get('/delete/{id}', 'Admin\PodcastController@delete');
    });

    Route::group(['prefix' => 'videos','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\VideoController@index');
        Route::get('/create', 'Admin\VideoController@create');
        Route::post('/store', 'Admin\VideoController@store');
        Route::post('/update', 'Admin\VideoController@update');
        Route::get('/edit/{id}', 'Admin\VideoController@edit');
        Route::get('/delete/{id}', 'Admin\VideoController@delete');
    });

    Route::group(['prefix' => 'promos','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\PromoController@index');
        Route::get('/create', 'Admin\PromoController@create');
        Route::post('/store', 'Admin\PromoController@store');
        Route::post('/update', 'Admin\PromoController@update');
        Route::get('/edit/{id}', 'Admin\PromoController@edit');
        Route::get('/delete/{id}', 'Admin\PromoController@delete');
    });

    Route::group(['prefix' => 'sections','middleware' => 'auth'], function() {
        Route::post('/update', 'Admin\SectionController@update');
        Route::get('/delete/{id}', 'Admin\SectionController@delete');
        Route::get('/delete-slide/{id}', 'Admin\SectionController@deleteSlide');
        Route::get('/{id}', 'Admin\SectionController@edit');
    });

    Route::group(['prefix' => 'categories','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\CategoryController@index');
        Route::get('/create', 'Admin\CategoryController@create');
        Route::post('/store', 'Admin\CategoryController@store');
        Route::post('/update', 'Admin\CategoryController@update');
        Route::get('/edit/{id}', 'Admin\CategoryController@edit');
        Route::get('/delete/{id}', 'Admin\CategoryController@delete');
    });

    Route::group(['prefix' => 'tags','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\TagController@index');
        Route::get('/create', 'Admin\TagController@create');
        Route::post('/store', 'Admin\TagController@store');
        Route::post('/update', 'Admin\TagController@update');
        Route::get('/edit/{id}', 'Admin\TagController@edit');
        Route::get('/delete/{id}', 'Admin\TagController@delete');
        Route::get('/convert', 'Admin\TagController@convert');
    });

    Route::group(['prefix' => 'services','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\ServiceController@index');
        Route::get('/create', 'Admin\ServiceController@create');
        Route::post('/store', 'Admin\ServiceController@store');
        Route::post('/update', 'Admin\ServiceController@update');
        Route::get('/edit/{id}', 'Admin\ServiceController@edit');
        Route::get('/delete/{id}', 'Admin\ServiceController@delete');
        Route::get('/delete-item/{id}', 'Admin\ServiceController@deleteItem');
    });

    Route::group(['prefix' => 'locations','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\LocationController@index');
        Route::get('/create', 'Admin\LocationController@create');
        Route::post('/store', 'Admin\LocationController@store');
        Route::post('/update', 'Admin\LocationController@update');
        Route::get('/edit/{id}', 'Admin\LocationController@edit');
        Route::get('/delete/{id}', 'Admin\LocationController@delete');
    });

    Route::group(['prefix' => 'files','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\FileController@index');
        Route::get('/edit/{id}', 'Admin\FileController@edit');
        Route::post('/update', 'Admin\FileController@update');
        Route::get('/create/', 'Admin\FileController@create');
        Route::post('/store/', 'Admin\FileController@store');
        Route::get('/delete/{id}', 'Admin\FileController@delete');
    });

    Route::group(['prefix' => 'lawyers','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\LawyerController@index');
        Route::get('/edit/{id}', 'Admin\LawyerController@edit');
        Route::post('/update', 'Admin\LawyerController@update');
        Route::get('/create/', 'Admin\LawyerController@create');
        Route::post('/store/', 'Admin\LawyerController@store');
        Route::get('/delete/{id}', 'Admin\LawyerController@delete');

        Route::group(['prefix' => 'filters','middleware' => 'auth'], function() {
            Route::get('/', 'Admin\LawyerFilterController@index');
            Route::get('/edit/{id}', 'Admin\LawyerFilterController@edit');
            Route::post('/update', 'Admin\LawyerFilterController@update');
            Route::get('/create/', 'Admin\LawyerFilterController@create');
            Route::post('/store/', 'Admin\LawyerFilterController@store');
            Route::get('/delete/{id}', 'Admin\LawyerFilterController@delete');
        });

        Route::group(['prefix' => 'titles','middleware' => 'auth'], function() {
            Route::get('/', 'Admin\TitleController@index');
            Route::get('/edit/{id}', 'Admin\TitleController@edit');
            Route::post('/update', 'Admin\TitleController@update');
            Route::get('/create/', 'Admin\TitleController@create');
            Route::post('/store/', 'Admin\TitleController@store');
            Route::get('/delete/{id}', 'Admin\TitleController@delete');
        });
    });
});

Route::get('/','PageController@index');

Route::get('/{page}', function ($page) {

    if($page=='implementation-')
        return redirect('implementation-of-the-value-added-tax-in-the-united-arab-emirates');
    elseif($page=='high-risk-intellectual-property-licensee')
        return redirect('economic-substance-regulations-for-ip-rights-holders');
    elseif($page=='employment')
        return redirect('employment-law-an-overview');
    elseif($page=='dubai-marina-torch-fire-what-next')
        return redirect('dubai-marina-torch-fire-what-next-who-pays');
    elseif($page=='dt_team')
        return redirect('our-people');
    elseif($page=='dt_slideshow')
        return redirect('our-people');
    elseif($page=='dt_slideshow')
        return redirect('our-people');
    elseif($page=='dr-ahmad-bin-hezeem')
        return redirect('lawyer/dr-ahmad-bin-hezeem-2');
    elseif($page=='contact')
        return redirect('contact-us');
    elseif($page=='59483-2')
        return redirect('uae-chapter-on-sanctions');
    elseif($page=='57329-2')
        return redirect('bsa-in-collaboration-with-zubair-sec-to-hold-workshop-and-legal-clinic-for-smes');
    elseif($page=='55370')
        return redirect('introduction-to-company-acquisitions-in-kingdom-of-saudi-arabia-business-law');
    elseif($page=='events')
        return redirect('knowledge-hub/events');
    elseif($page=='new-rules-regulating-the-')
        return redirect('new-rules-regulating-the-employment-of-the-uae-nationals-in-the-private-sector');

    if(view()->exists($page)){
        return view($page);
    } else {
        $article = \App\Models\Article::where('slug',$page)->first();

        if($article){
            $article->load('categories','tags','lawyer');
            $data = $article;

            $similar = \App\Models\Article::with('categories','tags')->where('id','!=',$data->id)->limit(8)->get();

            return view('dynamic.article',compact('data','similar'));
        } else {
            $data = \App\Models\CustomPage::where('slug',$page)->first();

            if($data)
                return view('dynamic.custom-page',compact('data'));
        }
    }


    return view('404');
});

//Route::get('/{page}', function ($page) {
//    if(view()->exists($page)){
//        return view($page);
//    }
//    return view('404');
//});

