@extends('partials.master')

@if($pageContent['meta-title'])
    @section('page-title'){{ $pageContent['meta-title'] }} | @endsection
@endif

@if($pageContent['meta-description'])
    @section('page-description'){{ $pageContent['meta-description'] }}@endsection
@endif

@section('css')
    <link rel="stylesheet" href="css/news.css?v=<?php echo rand(1,99999); ?>">
    <link rel="stylesheet" href="css/inner.css?v=<?php echo rand(1,99999); ?>">
    <style>

        .article-title {
            margin-top: 15px;
        }

        @media only screen and (max-width: 991px){
        }

        @media only screen and (max-width: 767px){

        }

        @media only screen and (max-width: 767px){

        }

        @media only screen and (max-width: 520px){
        }

    </style>
@endsection

@section('content')

<section id="inner" class="mb-5">
    <div class="container">

        <h1 class="page-title">Looking for someone in particular?</h1>

        <hr class="green"/>

        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
                <div class="custom-select">
                    <select>
                        <option value="latest">Position</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="custom-select">
                    <select>
                        <option value="latest">Intellectual Property</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="custom-select">
                    <select>
                        <option value="latest">Location</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 mt-5 mb-5">

                <p class="headline"><b>We help people do business all over the Middle East.</b> It doesn’t matter what sector you operate
in or whether you’re a fledgling local business or a multi-national corporate.</p>
                <p class="headline">
As a regional law firm with offices in Dubai, UAE, Saudi Arabia, Oman, Lebanon and Iraq,
we can help you negotiate the maze of local regulations and <b>ensure your business interests
are protected at all times.</b></p>

            </div>
        </div>

        <div class="row">
            <div class="col-md-3 article">
                <span class="border-top"></span>
                <a href="{{ url('people') }}"><img src="img/profile.png" width="100%" class="article-thumb"></a>
                <h3 class="article-title">Dr. Ahmad Bin Hezeem</h3>
                <span class="article-tag">Senior Partner</span>
            </div>
            <div class="col-md-3 article">
                <span class="border-top"></span>
                <a href="{{ url('people') }}"><img src="img/profile.png" width="100%" class="article-thumb"></a>
                <h3 class="article-title">Dr. Ahmad Bin Hezeem</h3>
                <span class="article-tag">Senior Partner</span>
            </div>
            <div class="col-md-3 article">
                <span class="border-top"></span>
                <a href="{{ url('people') }}"><img src="img/profile.png" width="100%" class="article-thumb"></a>
                <h3 class="article-title">Dr. Ahmad Bin Hezeem</h3>
                <span class="article-tag">Senior Partner</span>
            </div>
            <div class="col-md-3 article">
                <span class="border-top"></span>
                <a href="{{ url('people') }}"><img src="img/profile.png" width="100%" class="article-thumb"></a>
                <h3 class="article-title">Dr. Ahmad Bin Hezeem</h3>
                <span class="article-tag">Senior Partner</span>
            </div>
            <div class="col-md-3 article">
                <span class="border-top"></span>
                <a href="{{ url('people') }}"><img src="img/profile.png" width="100%" class="article-thumb"></a>
                <h3 class="article-title">Dr. Ahmad Bin Hezeem</h3>
                <span class="article-tag">Senior Partner</span>
            </div>
            <div class="col-md-3 article">
                <span class="border-top"></span>
                <a href="{{ url('people') }}"><img src="img/profile.png" width="100%" class="article-thumb"></a>
                <h3 class="article-title">Dr. Ahmad Bin Hezeem</h3>
                <span class="article-tag">Senior Partner</span>
            </div>
            <div class="col-md-3 article">
                <span class="border-top"></span>
                <a href="{{ url('people') }}"><img src="img/profile.png" width="100%" class="article-thumb"></a>
                <h3 class="article-title">Dr. Ahmad Bin Hezeem</h3>
                <span class="article-tag">Senior Partner</span>
            </div>
            <div class="col-md-3 article">
                <span class="border-top"></span>
                <a href="{{ url('people') }}"><img src="img/profile.png" width="100%" class="article-thumb"></a>
                <h3 class="article-title">Dr. Ahmad Bin Hezeem</h3>
                <span class="article-tag">Senior Partner</span>
            </div>
        </div>

    </div>
</section>


<section id="where" class="pb-5 mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-6 relative">
                <span class="medium">
                    BSA is<br/>
                    different<br/>
                    because<br/>
                     we are
                    <img src="{{ asset('') }}/img/ellipse-1.png" class="ellipse where">
                </span>
            </div>
            <div class="col-md-6">
                <ul class="timezone small clearfix">
                    <li>Attentive to every detail</li>
                    <li>Modern, progressive and commercially focused</li>
                    <li>Focused on excellence</li>
                    <li class="active">Innovative in everything we do</li>
                    <li>Fluent in a range of 22 languages including Arabic, English and French</li>
                </ul>
            </div>
        </div>
    </div>
</section>


<section id="" class="pb-5 mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-4 mt-5 uppercase">Contact us about this practice area</h5>
            </div>
            <form class="form">
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" name="" class="form-control" placeholder="Name" name="name">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <input type="text" name="" class="form-control" placeholder="Phone" name="phone">
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="" class="form-control" placeholder="Email" name="email">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <textarea class="form-control" placeholder="Message" name="message" rows="10"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <input type="submit" class="line-bt" value="SEND MESSAGE" />
                    </div>
                </div>
            </form>

        </div>
    </div>
    </div>
</section>



@endsection()

@section('js')
@endsection
