@extends('partials.master')

@section('css')
    <link rel="stylesheet" href="{{ asset('') }}/css/inner.css?v=<?php echo rand(1,99999); ?>">
    <link rel="stylesheet" href="{{ asset('') }}/css/news.css?v=<?php echo rand(1,99999); ?>">
    <style>
        .cropphoto {
            max-height: 200px;
            overflow: hidden;
            display: block;
        }

        .article {
            border-top: 1px solid #ccc;
            padding-top: 15px;
            padding-left: 20px;
        }

        @media only screen and (max-width: 991px){
            .article-thumb {
                margin-bottom: 0;
            }
        }

        @media only screen and (max-width: 767px){
            .cropphoto {
                display: block;
                max-height: 600px;
            }
        }
    </style>
@endsection


@section('content')
    <section id="inner" class="mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="position-relative">
                        <h5 class="section-heading">Search
                            <a href="#" id="searchBt"></a></h5>
                        <hr class="black mt-3"/>
                        <div id="searchbox">
                            <a href="#" id="closeSearch"></a>
                            <form action="{{url('search-full')}}" method="get">
                                <input type="text" id="searchfield" name="keyword">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if(count($data['lawyers']))
            <div class="container">
                <div class="row mt-5">
                    <div class="col-md-12">
                        <div class="row">
                            <h3>Lawyers</h3>

                            @foreach($data['lawyers'] as $item)
                                <div class="col-md-3 article" style="border:0;">
                                    <span class="border-top"></span>
                                    <a href="{{ url('lawyer/'.$item->slug) }}" class="photo">
                                        <img src="{{ $item->photoUrl }}" width="100%" class="article-thumb">
                                    </a>

                                    <h3 class="article-title">{{ $item->name }}</h3>
                                    <span class="article-tag">
                                    <?php $titleLoop = 1; ?>
                                        @foreach($item->titles()->get() as $title)
                                            {{ $title->name }}{{ $item->titles()->count() > $titleLoop ? ', ' : ''}}
                                            <?php $titleLoop++; ?>
                                        @endforeach
                                </span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if(count($data['services']))
            <div class="container">
                <div class="row mt-5">
                    <div class="col-md-12">
                        <div class="row">
                            <h3>Services</h3>
                            @foreach($data['services'] as $item)
                                <div class="col-md-12 article">
                                    <a href="{{ url('/legal-practice-areas/'.$item->slug) }}"><p>{{$item->title}}</p></a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if(count($data['articles']))
            <div class="container">
                <div class="row mt-5">
                    <div class="col-md-12">
                        <div class="row">
                            <h3>Articles</h3>
                            @foreach($data['articles'] as $item)
                                <div class="row article">
                                    <div class="col-lg-3 col-md-6">
                                        <a href="article.php" class=""><img src="{{ $item->photo }}" width="100%" class="article-thumb"></a>
                                    </div>
                                    <div class="col-lg-9 col-md-6">
                                        <a href="{{ url('knowledge-hub/news/'.$item->slug) }}"><h3 class="article-title">{{$item->title}}</h3></a>
                                        <p class="article-excerpt">
                                            @if($item->excerpt)
                                                {{ \Illuminate\Support\Str::words(strip_tags($item->excerpt), 20) }}
                                            @else
                                                {{ \Illuminate\Support\Str::words(strip_tags($item->content), 20) }}
                                            @endif
                                        </p>
                                        <a href="{{ url('knowledge-hub/news/'.$item->slug) }}" class="readmore"></a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endif


    </section>

@endsection()

@section('js')
    <script src="{{ asset('') }}/js/news.js?v=<?php echo rand(1,99999); ?>"></script>
@endsection
