@extends('partials.master')

@section('css')
    <link rel="stylesheet" href="css/article.css?v=<?php echo rand(1,99999); ?>">
    <style>
        .contact-head {
            color: #006341;
            text-transform: uppercase;
            font-weight: bold;
            padding-bottom: 15px;
            margin-bottom: 15px;
            border-bottom: 1px solid #000;
        }

        .contact p {
            font-size: 14px;
            margin-bottom: 3px;
        }
        .content p:first-of-type {
            font-weight: bold;
        }

    </style>
@endsection

@section('content')

<section id="inner" class="mb-5">
    <div class="container">
        <p class="page-tag"><span class="tag-dot green"></span> Senior Partner</p>

        <h1 class="page-title">Dr. Ahmad Bin Hezeem</h1>

        <hr class="black"/>


        <div class="row mt-5">
            <div class="col-lg-3 col-md-3">
                <img src="img/profile.png" width="100%" class="mb-3">
            </div>
            <div class="col-lg-7 col-md-9 content">
                <p>Dr. Ahmad is the Senior Partner in our Dubai office. He has over 24 years’ experience working in legal and judicial governmental institutions in Dubai. His litigation experience is expansive, covering civil law, criminal law, family and private clients and breach of contracts.</p>
                <p>For nine years, he served at Dubai Police in various positions of law enforcement, as well as teaching at the Dubai Police Academy. In 2005, he moved away from teaching to serve at the Executive Office of H.H. Sheikh Mohammad Bin Rashid Al Maktoum as the Deputy Director General of the Ruler Court of Dubai Government.</p>
                <p>Dr. Ahmad is well versed with the legal system in the UAE. As the Director General of the Dubai Courts, he engaged in the daily operations of the courts and judicial institutions at local and federal level for over eight years.</p>
                <p>He is also a former member of strategic governmental bodies such as the Executive Council of Dubai, The Judicial Council of Dubai, The Federal Judicial Council, and the Dubai Judicial Institution’s Board.</p>
                <p>In 2019, Dr. Ahmad was appointed by H.H. Sheikh Mohammad Bin Rashid Al Maktoum as a Deputy Chairman for the newly formed board of trustees of the Dubai International Arbitration Centre (DIAC).</p>

                <blockquote>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ut ipsum sit amet mi mattis interdum. </blockquote>
            </div>
            <div class="col-lg-2 col-md-12 contact">
                <h5 class="contact-head">contact info</h5>
                <p><b>Phone:</b> +971 4 528 5555</p>
                <p><b>Email:</b> ahmad.hezeem@bsabh.com</p>
                <p><b>Location:</b> Dubai, UAE</p>
                <p><b>Practice Area:</b> Litigation</p>
            </div>
        </div>
    </div>
</section>
<section id="highlights" class="mb-5">
    <div class="container">
        <h5 class="section-heading">
            News Articles
            <a href="#" class="highlights-next carousel-arrows next"></a>
            <a href="#" class="highlights-prev carousel-arrows prev"></a>
        </h5>

        <div class="highlights-carousel">
            <div>
                <div class="bordertop article">
                    <span class="article-tag">bankruptcy, news, uae.</span>
                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>
                    <p class="article-excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    <a href="article.php" class="readmore"></a>
                </div>
            </div>
            <div>
                <div class="bordertop article">
                    <span class="article-tag">bankruptcy, news, uae.</span>
                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>
                    <p class="article-excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    <a href="article.php" class="readmore"></a>
                </div>
            </div>
            <div>
                <div class="bordertop article">
                    <span class="article-tag">bankruptcy, news, uae.</span>
                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>
                    <p class="article-excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    <a href="article.php" class="readmore"></a>
                </div>
            </div>
            <div>
                <div class="bordertop article">
                    <span class="article-tag">bankruptcy, news, uae.</span>
                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>
                    <p class="article-excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <a href="article.php" class="readmore"></a>
                </div>
            </div>
            <div>
                <div class="bordertop article">
                    <span class="article-tag">bankruptcy, news, uae.</span>
                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>
                    <p class="article-excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <a href="article.php" class="readmore"></a>
                </div>
            </div>
            <div>
                <div class="bordertop article">
                    <span class="article-tag">bankruptcy, news, uae.</span>
                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>
                    <p class="article-excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <a href="article.php" class="readmore"></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-4 uppercase">Contact us</h5>
            </div>
            <form class="form">
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" name="" class="form-control" placeholder="Name" name="name">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <input type="text" name="" class="form-control" placeholder="Phone" name="phone">
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="" class="form-control" placeholder="Email" name="email">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <textarea class="form-control" placeholder="Message" name="message" rows="10"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <input type="submit" class="line-bt" value="SEND MESSAGE" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection()

@section('js')
    <script src="{{ asset('') }}/js/home.js?v=<?php echo rand(1,99999); ?>"></script>
@endsection
