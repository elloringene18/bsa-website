@extends('partials.master')
@section('page-title')Contact Us | @endsection
@section('css')
    <link rel="stylesheet" href="css/article.css?v=<?php echo rand(1,99999); ?>">
    <style type="text/css">
        .greenbox .social {
            margin-bottom: 0;
            position: absolute;
            bottom: 30px;
            right: 80px;
        }
        .greenbox .social a {
            color: #006341;
        }
    </style>
@endsection

@section('content')

    @inject('contentService', 'App\Services\ContentProvider')
    <?php $pageContent = $contentService->getPageSections('contact'); ?>

    <section id="inner" class="mb-5">
        <div class="container">

            <h1 class="page-title">Content not found</h1>

            <hr class="green"/>

            <div class="row">
                <div class="col-md-12">
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <h5>The content you are trying to view is currently under maintenance or has been deleted.<br/><br/> Please come back later.</h5>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                </div>
            </div>

        </div>
    </section>

@endsection()

@section('js')
@endsection
