@extends('partials.master')

@section('css')
    <link rel="stylesheet" href="css/article.css?v=<?php echo rand(1,99999); ?>">
    <style>
        .service .title {
            font-size: 100px;
            font-weight: bold;
            text-transform: uppercase;
            background-image: url(img/service-arrow.png);
            background-position: center left;
            background-repeat: no-repeat;
            padding-left: 100px;
            background-size: 80px auto;
            float: left;
        }
        .service:hover .title {
            background-size: 60px auto !important;
        }

        .service  {
            border-bottom: 1px solid #006341;
            padding-bottom: 20px;
            float: left;
            width: 100%;
        }

        .service ul {
            padding: 0;
            margin-bottom: 0;
            float: left;
            width: 100%;
            display: none;
        }

        .service:hover ul  {
            display: block;
        }


        .service:hover .title {
            background-image: url(img/service-arrow-open.png);
        }

        .service ul li {
            display: inline-block;
            border-radius: 30px;
            border:  1px solid #51AD32;
            margin-right: 20px;
            padding: 15px 20px;
            margin-bottom: 20px;
            cursor: pointer;
            transition: backround-color 300ms, color 300ms;
            float: left;
        }

        .service ul li:hover {
            background-color:  #ADD0A8;
            border: 1px solid #ADD0A8;
            color: #fff;
        }

        .service ul li .number {
            display: inline-block;
            font-size: 14px;
            color: #006341;
            transition: color 300ms;
        }

        .service ul li a {
            transition: color 300ms;
        }

        .service ul li:hover .number, .service ul li:hover a {
            color: #fff;
        }

        .service ul li  h5 {
            display: inline-block;
            margin-left: 10px;
            margin-bottom: 0;
            font-size: 14px;
        }

        .service.even .title, .service.even ul li {
            float: right;
        }

        @media only screen and (max-width: 991px){
            .service .title {
                font-size: 70px;
                padding-left: 90px;
                background-size: 70px auto !important;
                float: left;
            }
            .service:hover .title {
                background-size: 50px auto !important;
            }

        }

        @media only screen and (max-width: 767px){

            .service .title {
                font-size: 60px;
                padding-left: 90px;
                background-size: 70px auto !important;
                float: left !important;
            }

            .service.even .title, .service.even ul li {
                float: left;
            }

            .service:hover .title {
                background-size: 50px auto !important;
            }

            .service {
                padding-top: 20px;
            }
        }

        @media only screen and (max-width: 767px){

            .service .title {
                font-size: 40px;
                padding-left: 60px;
                background-size: 40px auto !important;
                float: left !important;
            }

            .service:hover .title {
                background-size: 30px auto !important;
            }

            .service.even .title, .service.even ul li {
                float: left;
            }

            .service {
                padding-top: 20px;
            }
        }

        @media only screen and (max-width: 520px){

            .service .title {
                font-size: 30px;
                padding-left: 50px;
                background-size: 30px auto !important;
            }

            .service:hover .title {
                background-size: 20px auto !important;
            }

            .service ul {
                margin-top: 10px;
                display: block !important;
            }

            .service ul li {
                width:  100%;
            }

        }

    </style>
@endsection

@section('content')

<section id="inner" class="mb-5">
    <div class="container">

        <h1 class="page-title">Services</h1>

        <hr class="green"/>

        <div class="row">
            <div class="col-md-12">
                <div class="services">
                    <div class="service">
                        <div class="title">services</div>
                        <ul class="tracking-in-contract">
                            <li><a href="{{  url('legal-practice-areas/arbitration-dispute-resolution') }}"><span class="number">01</span><h5>Arbitration</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/banking-finance-law') }}"><span class="number">02</span><h5>Banking & Finance</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/commercial-law') }}"><span class="number">03</span><h5>Commercial</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/corporate-law-and-ma') }}"><span class="number">04</span><h5>Corporate & M&A</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/cybersecurity') }}"><span class="number">05</span><h5>Cyber Security & Data Protection</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/environment-social-and-governance') }}"><span class="number">06</span><h5>Environment, Social & Governance</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/employment-law-firm') }}"><span class="number">07</span><h5>Employment</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/intellectual-property-law') }}"><span class="number">08</span><h5>Intellectual Property</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/litigation-law-firm') }}"><span class="number">08</span><h5>Litigation</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/regulatory-and-compliance') }}"><span class="number">10</span><h5>Regulatory & Compliance</h5></a></li>
                        </ul>
                    </div>
                    <div class="service even">
                        <div class="title">industries</div>
                        <ul class="tracking-in-contract">
                            <li><a href="{{  url('legal-practice-areas/construction-law') }}"><span class="number">01</span><h5>Construction and Engineering</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/education') }}"><span class="number">02</span><h5>Education</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/energy-law') }}"><span class="number">03</span><h5>Energy</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/regulatory-and-compliance-1') }}"><span class="number">04</span><h5>Financial Institutions</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/insurance-reinsurance-law') }}"><span class="number">05</span><h5>Insurance</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/international-trade-transport-and-maritime') }}"><span class="number">06</span><h5>International Trade, Transport & Maritime</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/leisure-and-hospitality') }}"><span class="number">07</span><h5>Leisure & Hospitality</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/life-sciences-and-healthcare') }}"><span class="number">08</span><h5>Life Sciences & Healthcare</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/real-estate-law') }}"><span class="number">09</span><h5>Real Estate</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/retail-and-consumer-goods') }}"><span class="number">10</span><h5>Retail & Consumer Goods</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/technology-media-and-telecommunications') }}"><span class="number">11</span><h5>Technology, Media & Telecommunications (TMT)</h5></a></li>
                        </ul>
                    </div>
                    <div class="service">
                        <div class="title">groups</div>
                        <ul class="tracking-in-contract">
                            <li><a href="{{  url('legal-practice-areas/family-owned-businesses') }}"><span class="number">01</span><h5>Family-owned Businesses</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/high-net-worth-individuals') }}"><span class="number">02</span><h5>High Net Worth Individuals</h5></a></li>
                            <li><a href="{{  url('legal-practice-areas/private-client') }}"><span class="number">03</span><h5>Private Client</h5></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection()

@section('js')
@endsection
