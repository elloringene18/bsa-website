@extends('partials.master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $pageContent = $contentService->getPageSections('contact'); ?>

@if($pageContent['meta-title'])
    @section('page-title'){{ $pageContent['meta-title'] }} | @endsection
@endif

@if($pageContent['meta-description'])
    @section('page-description'){{ $pageContent['meta-description'] }}@endsection
@endif

@section('css')
    <link rel="stylesheet" href="css/article.css?v=<?php echo rand(1,99999); ?>">
    <style type="text/css">
        .greenbox .social {
            margin-bottom: 0;
            position: absolute;
            bottom: 30px;
            right: 80px;
        }
        .greenbox .social a {
            color: #006341;
        }
    </style>
@endsection

@section('content')


<section id="inner" class="mb-5">
    <div class="container">

        <h1 class="page-title">Get in touch</h1>

        <hr class="green"/>

        <div class="row">
            <div class="col-md-12">
                <img src="{{  asset('img/contact-us-banner.png') }}" width="100%" class="mt-3 mb-5">
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-4">{{ $pageContent['form-heading'] }}</h5>
            </div>
            @include('partials.contact-form')

        </div>
    </div>
</section>

<section class="pb-5 mt-5">
    <div class="container">
        <div class="row">
            <?php $promobox = $contentService->getPromoboxById($pageContent['promobox']); ?>
            @include('partials.long-promoboxes')
        </div>
    </div>
</section>

@endsection()

@section('js')
    <script>
        $('#reload').click(function () {
            $.ajax({
                type: 'GET',
                url: 'reload-captcha',
                success: function (data) {
                    $(".captcha span").html(data.captcha);
                }
            });
        });
    </script>
@endsection
