@extends('partials.master')

@if($pageContent['meta-title'])
    @section('page-title'){{ $pageContent['meta-title'] }} | @endsection
@endif

@if($pageContent['meta-description'])
    @section('page-description'){{ $pageContent['meta-description'] }}@endsection
@endif

@section('css')
    <link rel="stylesheet" href="css/inner.css?v=<?php echo rand(1,99999); ?>">
    <link rel="stylesheet" href="css/news.css?v=<?php echo rand(1,99999); ?>">

    <style>
        #banner {
            height: 700px;
            width: 100%;
            position: relative;
            overflow: hidden;
        }

        #banner .lines {
            position: absolute;
            width: 100%;
            height: 100%;
            background-image: url({{ asset('img/knowledge-lines.png') }});
            background-repeat: no-repeat;
            background-size: auto 100%;
            background-repeat: no-repeat;
            top: 0;
        }

        #banner .bg {
            position: absolute;
            width: 4020px;
            height: 3200px;
            background-image: url({{ asset('img/knowledge-bg.png') }});
            background-repeat: no-repeat;
            background-size: 4020px auto;
            background-position: center;
            top: -300px;
        }

        #banner h1 {
            font-size: 120px;
            margin-top: 300px;
            font-weight: bold;
        }

        #inner {
            background-position: top center;
            background-size: 100% auto;
            background-repeat: repeat-x;
            margin-top: 0;
            padding-top: 190px;
        }

        .service .title {
            font-size: 70px;
            line-height: 70px;
            font-weight: bold;
            text-transform: uppercase;
            background-image: url({{ asset('img/service-arrow.png') }});
            background-position: top left;
            background-repeat: no-repeat;
            padding-left: 100px;
            background-size: 80px auto;
            float: left;
            cursor: pointer;
        }

        .service .title.right {
            background-image: url({{ asset('img/service-arrow-right.png') }});
            background-position: top right;
            padding-left: 0;
            padding-right: 100px;
            text-align: right;
            float: right;
        }

        .service .title.right:hover {
            background-image: url({{ asset('img/service-arrow-right-open.png') }});
        }

        .service:hover .title {
            background-size: 60px auto !important;
        }

        .service  {
            padding-bottom: 20px;
            padding-top: 20px;
            float: left;
            width: 100%;
        }

        .service:hover .title {
            background-image: url({{ asset('img/service-arrow-open.png') }});
        }

        .service h5 {
            display: inline-block;
            margin-left: 10px;
            margin-bottom: 0;
            font-size: 14px;
        }

        .service.even .title, .service.even ul li {
            float: right;
        }

        #where .medium {
            position: relative;
            width: auto;
            float: left;
        }

        .timezone.small {
            margin-top: 0;
            padding-left: 0;
        }

        .ellipse.where {
            right: -20px;
            top: -190px;
        }

        .event .title {
            float: left;
            width: 70%;
            margin-top: 10px;
        }

        .event .date {
            float: right;
            width: 30%;
            text-align: right;
            background-image: url({{ asset('img/event-arrow.png') }});
            background-position: right center;
            background-repeat: no-repeat;
            padding-right: 30px;
            background-size: 20px auto;
        }

        .timezone.small li {
            padding: 5px 0;
            margin: 0;
        }

        .timezone.small .img {
            display: none;
            margin-top: 20px;
            padding-left: 10%;
            font-weight: normal;
            color: #000;
        }

        .timezone.small .img p {
            line-height: 26px;
        }

        .timezone .event.active .date {
            display: none;
        }

        .timezone.small li.active .img {
            float: left;
        }

        #subscribeForm input[type=submit] {
            border: 1px solid #54AB3B;
            width: 170px;
            text-align: center;
            background-color: transparent;
            height: 36px;
            line-height: 36px;
            float: right;
            color: inherit;
            transition: color 300ms, background-color 300ms;
        }

        @media only screen and (max-width: 1200px){
            .service .title {
                background-size: 50px auto !important;
                font-size: 40px;
                line-height: 50px;
                min-height: 80px;
            }

        }

        @media only screen and (max-width: 991px){
            .service .title {
                padding-left: 90px;
                background-size: 70px auto !important;
                float: left;
            }
            .service:hover .title {
                background-size: 50px auto !important;
            }
        }

        @media only screen and (max-width: 767px){

            .service .title {
                font-size: 60px;
                padding-left: 90px;
                background-size: 70px auto !important;
                float: left !important;
            }

            .service.even .title {
                float: left;
            }

            .service:hover .title {
                background-size: 50px auto !important;
            }

            .service .title.right {
                background-image: url({{ asset('img/service-arrow.png') }});
                background-position: top left;
                background-repeat: no-repeat;
                padding-left: 60px;
                background-size: 80px auto;
                float: left;
                padding-right: 0;
                text-align: left;
            }

            .service {
                padding-top: 0px;
                padding-bottom: 0;
            }
        }

        @media only screen and (max-width: 767px){

            .service .title {
                font-size: 40px;
                padding-left: 60px;
                background-size: 40px auto !important;
                float: left !important;
            }

            .service:hover .title {
                background-size: 30px auto !important;
            }

            .service.even .title {
                float: left;
            }

            .service {
                padding-top: 20px;
            }
        }

        @media only screen and (max-width: 520px){

            .service .title {
                font-size: 30px;
                padding-left: 40px;
                background-size: 30px auto !important;
                min-height: 29px;
                background-position: 0 10px;
            }

            .service .title.right {
                padding-left: 40px;
                background-position: 0 10px;
            }
            .ellipse.where {
                top: -50px;
            }
        }
    </style>
@endsection


@section('content')

<section id="banner">
    <div class="container">
        <h1>KNOWLEDGE<br/>HUB</h1>
    </div>
    <div class="bg" id="hole"></div>
    <div class="lines"></div>
</section>


<section id="inner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="position-relative">
                    <h5 class="section-heading">Insights
                        <a href="#" id="searchBt"></a></h5>
                    <hr class="black mt-3"/>
                    <div id="searchbox">
                        <a href="#" id="closeSearch"></a>
                        <form>
                            <input type="text" id="searchfield">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row mt-md-5 mt-sm-0">
            <div class="col-md-6">
                <div class="services">
                    <div class="service">
                        <a href="{{ url('knowledge-hub/news') }}"><div class="title">News</div></a>
                    </div>
                    <div class="service">
                        <a href="{{ url('knowledge-hub/publications-2') }}"><div class="title">Publications</div></a>
                    </div>
                    <div class="service">
                        <a href="{{ url('knowledge-hub/events') }}"><div class="title">Events</div></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="services">
                    <div class="service">
                        <a href="{{ url('knowledge-hub/covid-19') }}"><div class="title  right">Coronavirus Insight Centre</div></a>
                    </div>
                </div>
            </div>
        </div>
    </div>


<section id="highlights" class="mb-5 mt-5">
    <div class="container">
        <h5 class="section-heading">
            Latest News
        </h5>
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3 article">
                        <span class="border-top"></span>
                        <a href="{{ url('/article') }}"><img src="img/article-thumb.png" width="100%" class="article-thumb"></a>
                        <span class="article-tag">bankruptcy, news, uae.</span>
                        <h3 class="article-title">Lorem Ipsum sit Amet.</h3>
                    </div>
                    <div class="col-md-3 article">
                        <span class="border-top"></span>
                        <a href="{{ url('/article') }}"><img src="img/article-thumb-2.png" width="100%" class="article-thumb"></a>
                        <span class="article-tag">bankruptcy, news, uae.</span>
                        <h3 class="article-title">Lorem Ipsum sit Amet.</h3>
                    </div>
                    <div class="col-md-3 article">
                        <span class="border-top"></span>
                        <a href="{{ url('/article') }}"><img src="img/article-thumb-3.png" width="100%" class="article-thumb"></a>
                        <span class="article-tag">bankruptcy, news, uae.</span>
                        <h3 class="article-title">Lorem Ipsum sit Amet.</h3>
                    </div>
                    <div class="col-md-3 article">
                        <span class="border-top"></span>
                        <a href="{{ url('/article') }}"><img src="img/article-thumb.png" width="100%" class="article-thumb"></a>
                        <span class="article-tag">bankruptcy, news, uae.</span>
                        <h3 class="article-title">Lorem Ipsum sit Amet.</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    --}}
{{--    <div class="container">--}}
{{--        <h5 class="section-heading">--}}
{{--            Highlights--}}
{{--            <a href="#" class="highlights-next carousel-arrows next"></a>--}}
{{--            <a href="#" class="highlights-prev carousel-arrows prev"></a>--}}
{{--        </h5>--}}

{{--        <div class="highlights-carousel">--}}
{{--            <div>--}}
{{--                <div class="bordertop article">--}}
{{--                    <span class="article-tag">bankruptcy, news, uae.</span>--}}
{{--                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>--}}
{{--                    <p class="article-excerpt">--}}
{{--                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>--}}
{{--                    <a href="article.php" class="readmore"></a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div>--}}
{{--                <div class="bordertop article">--}}
{{--                    <span class="article-tag">bankruptcy, news, uae.</span>--}}
{{--                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>--}}
{{--                    <p class="article-excerpt">--}}
{{--                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>--}}
{{--                    <a href="article.php" class="readmore"></a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div>--}}
{{--                <div class="bordertop article">--}}
{{--                    <span class="article-tag">bankruptcy, news, uae.</span>--}}
{{--                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>--}}
{{--                    <p class="article-excerpt">--}}
{{--                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>--}}
{{--                    <a href="article.php" class="readmore"></a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div>--}}
{{--                <div class="bordertop article">--}}
{{--                    <span class="article-tag">bankruptcy, news, uae.</span>--}}
{{--                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>--}}
{{--                    <p class="article-excerpt">--}}
{{--                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}
{{--                    <a href="article.php" class="readmore"></a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div>--}}
{{--                <div class="bordertop article">--}}
{{--                    <span class="article-tag">bankruptcy, news, uae.</span>--}}
{{--                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>--}}
{{--                    <p class="article-excerpt">--}}
{{--                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}
{{--                    <a href="article.php" class="readmore"></a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div>--}}
{{--                <div class="bordertop article">--}}
{{--                    <span class="article-tag">bankruptcy, news, uae.</span>--}}
{{--                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>--}}
{{--                    <p class="article-excerpt">--}}
{{--                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}
{{--                    <a href="article.php" class="readmore"></a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
</section>

{{--<section id="lastestarticles" class="mb-5">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-lg-8 col-md-12">--}}
{{--                <h5 class="section-heading">Latest Articles <a href="news.php" class="viewAll">SEE ALL</a> </h5>--}}
{{--                <div class="bordertop">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-md-12">--}}
{{--                            <div class="row article">--}}
{{--                                <div class="col-lg-3 col-md-6">--}}
{{--                                    <a href="article.php"><img src="{{ asset('') }}/img/article-thumb-2.png" width="100%" class="article-thumb"></a>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-9 col-md-6">--}}
{{--                                    <span class="article-tag">bankruptcy, news, uae.</span>--}}
{{--                                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>--}}
{{--                                    <p class="article-excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}
{{--                                    <a href="article.php" class="readmore"></a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="row article">--}}
{{--                                <div class="col-lg-3 col-md-6">--}}
{{--                                    <a href="article.php"><img src="{{ asset('') }}/img/article-thumb.png" width="100%" class="article-thumb"></a>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-9 col-md-6">--}}
{{--                                    <span class="article-tag">bankruptcy, news, uae.</span>--}}
{{--                                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>--}}
{{--                                    <p class="article-excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}
{{--                                    <a href="article.php" class="readmore"></a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="row article">--}}
{{--                                <div class="col-lg-3 col-md-6">--}}
{{--                                    <a href="article.php"><img src="{{ asset('') }}/img/article-thumb-3.png" width="100%" class="article-thumb"></a>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-9 col-md-6">--}}
{{--                                    <span class="article-tag">bankruptcy, news, uae.</span>--}}
{{--                                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>--}}
{{--                                    <p class="article-excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}
{{--                                    <a href="article.php" class="readmore"></a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-lg-4 col-md-12">--}}
{{--                <h5 class="section-heading">MEDIA <a href="#" class="viewAll">SEE ALL</a></h5>--}}
{{--                <div class="bordertop pl-0">--}}
{{--                    <div class="podcast clearfix">--}}
{{--                        <button class="podcast control"></button>--}}
{{--                        <h3 class="podcast-title">Lorem ipsum / Video</h3>--}}
{{--                        <span class="podcast-time">24:13</span>--}}
{{--                    </div>--}}
{{--                    <div class="podcast clearfix">--}}
{{--                        <button class="podcast control"></button>--}}
{{--                        <h3 class="podcast-title">Lorem ipsum / Podcast</h3>--}}
{{--                        <span class="podcast-time">24:13</span>--}}
{{--                    </div>--}}
{{--                    <div class="podcast clearfix">--}}
{{--                        <button class="podcast control"></button>--}}
{{--                        <h3 class="podcast-title">Lorem ipsum / Video</h3>--}}
{{--                        <span class="podcast-time">24:13</span>--}}
{{--                    </div>--}}
{{--                    <div class="podcast clearfix">--}}
{{--                        <button class="podcast control"></button>--}}
{{--                        <h3 class="podcast-title">Lorem ipsum / Podcast</h3>--}}
{{--                        <span class="podcast-time">24:13</span>--}}
{{--                    </div>--}}
{{--                    <div class="podcast clearfix">--}}
{{--                        <button class="podcast control"></button>--}}
{{--                        <h3 class="podcast-title">Lorem ipsum / Video</h3>--}}
{{--                        <span class="podcast-time">24:13</span>--}}
{{--                    </div>--}}
{{--                    <div class="podcast clearfix">--}}
{{--                        <button class="podcast control"></button>--}}
{{--                        <h3 class="podcast-title">Lorem ipsum / Podcast</h3>--}}
{{--                        <span class="podcast-time">24:13</span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-12">--}}
{{--                <hr class="black"/>--}}
{{--                <a href="" class="line-bt">VIEW ALL</a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}

<section id="where" class="pb-5 mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span class="medium">
                    Save the date, <br/>
                    See you there
                    <img src="{{ asset('') }}/img/ellipse-1.png" class="ellipse where">
                </span>
            </div>
            <div class="col-md-8 offset-md-4 pt-5">
                <h5 class="section-heading">
                    UPCOMING EVENTS <a href="#" class="viewAll">SEE ALL</a>
                </h5>
                <ul class="timezone small clearfix bordertop">
                    <li class="event">
                        <div class="row ">
                            <a href="{{  url('article') }}">
                                <div class="col-md-12"><div class="title">Event Lorem Ipsum</div> <div class="date">November 24</div></div>
                                <div class="col-md-12 img">
                                    <p><b>Location:</b> Dubai (DIFC)</p>
                                    <p><b>Description:</b> We are seeking to recruit aself-motivated and driven Court Advocate to join our firm. UAE Nationals only.</p>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li class="event">
                        <div class="row ">
                            <a href="{{  url('article') }}">
                                <div class="col-md-12"><div class="title">Event Lorem Ipsum</div> <div class="date">November 24</div></div>
                                <div class="col-md-12 img">
                                    <p><b>Location:</b> Dubai (DIFC)</p>
                                    <p><b>Description:</b> We are seeking to recruit aself-motivated and driven Court Advocate to join our firm. UAE Nationals only.</p>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li class="event">
                        <div class="row ">
                            <a href="{{  url('article') }}">
                                <div class="col-md-12"><div class="title">Event Lorem Ipsum</div> <div class="date">November 24</div></div>
                                <div class="col-md-12 img">
                                    <p><b>Location:</b> Dubai (DIFC)</p>
                                    <p><b>Description:</b> We are seeking to recruit aself-motivated and driven Court Advocate to join our firm. UAE Nationals only.</p>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li class="event">
                        <div class="row ">
                            <a href="{{  url('article') }}">
                                <div class="col-md-12"><div class="title">Event Lorem Ipsum</div> <div class="date">November 24</div></div>
                                <div class="col-md-12 img">
                                    <p><b>Location:</b> Dubai (DIFC)</p>
                                    <p><b>Description:</b> We are seeking to recruit aself-motivated and driven Court Advocate to join our firm. UAE Nationals only.</p>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li class="event">
                        <div class="row ">
                            <a href="{{  url('article') }}">
                                <div class="col-md-12"><div class="title">Event Lorem Ipsum</div> <div class="date">November 24</div></div>
                                <div class="col-md-12 img">
                                    <p><b>Location:</b> Dubai (DIFC)</p>
                                    <p><b>Description:</b> We are seeking to recruit aself-motivated and driven Court Advocate to join our firm. UAE Nationals only.</p>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li class="event">
                        <div class="row ">
                            <a href="{{  url('article') }}">
                                <div class="col-md-12"><div class="title">Event Lorem Ipsum</div> <div class="date">November 24</div></div>
                                <div class="col-md-12 img">
                                    <p><b>Location:</b> Dubai (DIFC)</p>
                                    <p><b>Description:</b> We are seeking to recruit aself-motivated and driven Court Advocate to join our firm. UAE Nationals only.</p>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li class="event active">
                        <div class="row ">
                            <a href="{{  url('article') }}">
                                <div class="col-md-12"><div class="title">Event Lorem Ipsum</div> <div class="date">November 24</div></div>
                                <div class="col-md-12 img">
                                    <p><b>Location:</b> Dubai (DIFC)</p>
                                    <p><b>Description:</b> We are seeking to recruit aself-motivated and driven Court Advocate to join our firm. UAE Nationals only.</p>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li class="event">
                        <div class="row ">
                            <a href="{{  url('article') }}">
                                <div class="col-md-12"><div class="title">Event Lorem Ipsum</div> <div class="date">November 24</div></div>
                                <div class="col-md-12 img">
                                    <p><b>Location:</b> Dubai (DIFC)</p>
                                    <p><b>Description:</b> We are seeking to recruit aself-motivated and driven Court Advocate to join our firm. UAE Nationals only.</p>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li class="event">
                        <div class="row ">
                            <a href="{{  url('article') }}">
                                <div class="col-md-12"><div class="title">Event Lorem Ipsum</div> <div class="date">November 24</div></div>
                                <div class="col-md-12 img">
                                    <p><b>Location:</b> Dubai (DIFC)</p>
                                    <p><b>Description:</b> We are seeking to recruit aself-motivated and driven Court Advocate to join our firm. UAE Nationals only.</p>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li class="event">
                        <div class="row ">
                            <a href="{{  url('article') }}">
                                <div class="col-md-12"><div class="title">Event Lorem Ipsum</div> <div class="date">November 24</div></div>
                                <div class="col-md-12 img">
                                    <p><b>Location:</b> Dubai (DIFC)</p>
                                    <p><b>Description:</b> We are seeking to recruit aself-motivated and driven Court Advocate to join our firm. UAE Nationals only.</p>
                                </div>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</section>



<section id="where" class="pb-5 mt-5">
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-5">
                <div class="socials-carousel">
                    <div>
                        <span class="article-tag txt"><i class="fa fa-instagram"></i>  November 24</span><br/>
                        <img src="{{ asset('') }}/img/social1.png" width="100%">
                    </div>
                    <div class="linkedin">
                        <span class="article-tag txt"><i class="fa fa-linkedin"></i>  November 24</span><br/>
                        <img src="{{ asset('') }}/img/social2.png" width="100%">
                        <br/>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </div>
                    <div class="twitter">
                        <span class="article-tag txt"><i class="fa fa-twitter"></i>  November 24</span><br/>
                        “Lorem Ipsum sit Amet.
                            consectetur adipiscing elit.”
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <span class="mhuge">
                    BSA is social,<br/>follow us<br/>
                    <ul class="social">
                        <li><a href="#" class="fa fa-facebook" target="_blank"></a></li>
                        <li><a href="#" class="fa fa-twitter" target="_blank"></a></li>
                        <li><a href="#" class="fa fa-instagram" target="_blank"></a></li>
                    </ul>
                    <img src="{{ asset('') }}/img/ellipse-2.png" class="ellipse social">
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
            </div>
            <div class="col-md-7 pt-5 mb-5">
                <form id="subscribeForm" action="{{url('subscribe')}}">
                    @csrf
                    <div class="alert alert-success" id="subscribeSuccess"></div>
                    <div class="alert alert-warning" id="subscribeError"></div>

                    <input type="email" placeholder="Enter your e-mail" name="email" class="form-control"/>
                    <div class="row">
                        <div class="col-md-8 pt-3">
                            <h5 class="section-heading">Subscribe to our mailing list</h5>
                        </div>
                        <div class="col-md-4 pt-3">
                            <input type="submit" value="SUBSCRIBE" class="linebt">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection()

@section('js')
    <script src="{{ asset('') }}/js/news.js?v=<?php echo rand(1,99999); ?>"></script>
    <script>
        document.getElementById('banner').onmousemove = function(e) {
            var x = e.clientX - 2010;
            var y = e.clientY - 1700;
            document.getElementById('hole').style.left  = x+"px";
            document.getElementById('hole').style.top  = y+"px";
        }
    </script>
@endsection
