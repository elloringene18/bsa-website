@extends('partials.master')

@section('css')
    <link rel="stylesheet" href="css/article.css?v=<?php echo rand(1,99999); ?>">

    <style type="text/css">
        .content p:first-of-type {
            font-size: 20px;
        }

    </style>
@endsection

@section('content')

<section id="inner" class="mb-5">
    <div class="container">
        <p class="page-tag"><span class="tag-dot green"></span> Home / Blockchain / Tokenization of real estate assets</p>

        <h1 class="page-title">The Concept of Healthcare / Medical Trust</h1>

        <hr class="black"/>

        <div class="article-photo">
            <div class="lines"></div>
            <img src="img/article-sample.png" width="100%">
        </div>

        <div class="row mt-5">
            <div class="col-md-3">
                <img src="img/player-sample.png" class="authorThumb">
                <div class="authorCopy">
                    <span class="authorTitle">Lara Barbary</span>
                    <span class="authorDetails">CEO</span>
                    <span class="authorDetails">lara@barbary.com</span>
                    <span class="authorDetails">Sep 16, 2016 · 5 min read</span>
                </div>
            </div>
            <div class="col-md-9 content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                <blockquote>
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.”
                </blockquote>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="article-tag mt-3">
                    Health, UAE, Lara Barbary, Insurance.
                </div>
            </div>
        </div>
    </div>
</section>
<section id="highlights" class="mb-5">
    <div class="container">
        <h5 class="section-heading">
            Similar Articles
            <a href="#" class="highlights-next carousel-arrows next"></a>
            <a href="#" class="highlights-prev carousel-arrows prev"></a>
        </h5>

        <div class="highlights-carousel">
            <div>
                <div class="bordertop article">
                    <span class="article-tag">bankruptcy, news, uae.</span>
                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>
                    <p class="article-excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    <a href="article.php" class="readmore"></a>
                </div>
            </div>
            <div>
                <div class="bordertop article">
                    <span class="article-tag">bankruptcy, news, uae.</span>
                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>
                    <p class="article-excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    <a href="article.php" class="readmore"></a>
                </div>
            </div>
            <div>
                <div class="bordertop article">
                    <span class="article-tag">bankruptcy, news, uae.</span>
                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>
                    <p class="article-excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    <a href="article.php" class="readmore"></a>
                </div>
            </div>
            <div>
                <div class="bordertop article">
                    <span class="article-tag">bankruptcy, news, uae.</span>
                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>
                    <p class="article-excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <a href="article.php" class="readmore"></a>
                </div>
            </div>
            <div>
                <div class="bordertop article">
                    <span class="article-tag">bankruptcy, news, uae.</span>
                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>
                    <p class="article-excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <a href="article.php" class="readmore"></a>
                </div>
            </div>
            <div>
                <div class="bordertop article">
                    <span class="article-tag">bankruptcy, news, uae.</span>
                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>
                    <p class="article-excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <a href="article.php" class="readmore"></a>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection()

@section('js')
    <script src="{{ asset('') }}/js/home.js?v=<?php echo rand(1,99999); ?>"></script>
@endsection