@extends('partials.master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $pageContent = $contentService->getPageSections('careers'); ?>

@if($pageContent['meta-title'])
    @section('page-title'){{ $pageContent['meta-title'] }} | @endsection
@endif

@if($pageContent['meta-description'])
    @section('page-description'){{ $pageContent['meta-description'] }}@endsection
@endif

@section('css')
    <link rel="stylesheet" href="css/article.css?v=<?php echo rand(1,99999); ?>">
    <style>
        .job-vacancy h5, .job-vacancy label {
            color: red !important;
        }

        .conts h1, h2, h3, h4, h5, h6 {
            margin-bottom: 20px;
        }

        #where .container a {
            font-weight: bold;
        }
    </style>
@endsection

@section('content')


<section id="inner" class="mb-5">
    <div class="container">

        <h1 class="page-title">Careers</h1>

        <hr class="green"/>

        <div class="row">
            <div class="col-md-12 headline">
                {!! $pageContent['intro'] !!}
{{--                <p><b>At BSA, we think diversity is a strength.</b> We celebrate differences and welcome applications from people of all ethnic and racial backgrounds. It doesn’t matter where you’re from; if you can think the BSA way and can help our business to continue to evolve and grow, we’d love to hear from you.</p>--}}

                <img src="{{  asset($pageContent['banner-image']) }}" width="100%" class="mt-3 mb-5">
            </div>
        </div>

        <div class="row conts">
            <div class="col-md-6">
                {!! $pageContent['left-column-content'] !!}
            </div>
            <div class="col-md-6">
                {!! $pageContent['right-column-content'] !!}
            </div>
        </div>
    </div>
</section>

<section id="where" class="pb-5 mt-5 where">
    <div class="container">
        <div class="row">
            <div class="col-md-4 relative">
                <span class="medium">
                    Our core values
                    <img src="{{ asset('') }}/img/ellipse-1.png" class="ellipse where">
                </span>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-7">
                <ul class="timezone small clearfix">

                    <?php $values = $contentService->getValues(); $count=1;?>

                    @foreach($values as $item)
                        <li class="{{ $count==1 ? 'active' : '' }}">
                            <div class="row ">
                                <div class="col-md-12">{{ $item->content }}</div>
                                <div class="col-md-12 img">
                                    <img src="{{  asset($item->image) }}" width="100%">
                                </div>
                            </div>
                        </li>
                        <?php $count++; ?>
                    @endforeach
{{--                        --}}
{{--                    <li>--}}
{{--                        <div class="row ">--}}
{{--                            <div class="col-md-12">Local knowledge and experience</div>--}}
{{--                            <div class="col-md-12 img">--}}
{{--                                <img src="{{  asset('img/careers/knowledge.jpg') }}" width="100%">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </li>--}}
{{--                    <li class="active">--}}
{{--                        <div class="row ">--}}
{{--                            <div class="col-md-12">Ethics and approachability</div>--}}
{{--                            <div class="col-md-12 img">--}}
{{--                                <img src="{{  asset('img/careers/approachability.jpg') }}" width="100%">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <div class="row ">--}}
{{--                            <div class="col-md-12">Integrity and commitment</div>--}}
{{--                            <div class="col-md-12 img">--}}
{{--                                <img src="{{  asset('img/careers/integrity.jpg') }}" width="100%">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </li>--}}
                </ul>
            </div>
        </div>
        <div class="row">
            <h5 class="mt-5">{!! $pageContent['career-listing-heading'] !!}</h5>
            <hr class="black"/>
            <div class="col-md-12">
                <iframe id="MyFrame" style="border: 0; height: 500px; background: none" src="https://hrms.blueberry.software/appraisal_Module/JobVacancy.aspx?Client=1005" width="100%"></iframe>
            </div>
        </div>
        <div class="row">
            <h5 class="mt-5">{!! $pageContent['bottom-content-heading'] !!}</h5>
            <hr class="black"/>
            {!! $pageContent['bottom-content-body'] !!}
        </div>
    </div>
</section>

@endsection()

@section('js')
@endsection
