@extends('partials.master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $pageContent = $contentService->getPageSections('about'); ?>

@if($pageContent['meta-title'])
    @section('page-title'){{ $pageContent['meta-title'] }} | @endsection
@endif

@if($pageContent['meta-description'])
    @section('page-description'){{ $pageContent['meta-description'] }}@endsection
@endif

@section('css')
    <link rel="stylesheet" href="css/article.css?v=<?php echo rand(1,99999); ?>">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/ycodetech/horizontal-timeline-2.0@2/css/horizontal_timeline.2.0.min.css">

    <style>
        .horizontal-timeline {
            padding-top: 320px;
        }
        .events-content img {
            position: absolute;
            top: -400px;
            left: 50%;
            margin-left: -150px;
        }

        .scroll-right, .scroll-left {
        }
        .horizontal-timeline .events a::after {
            bottom: -9px;
            height: 20px;
            width: 3px;
            border-radius: 0;
        }

        .horizontal-timeline .events a.selected::after {
            bottom: -8px;
            height: 22px;
            width: 22px;
            border-radius: 50%;
            background-color: #006341;
            left: 38px;
        }

        .horizontal-timeline .events a::after {
            left: 50px;
        }

        .horizontal-timeline .events a.older-event::after,.horizontal-timeline .events a::after, .horizontal-timeline .filling-line {
            background-color: #006341;
        }

        .horizontal-timeline .events a {
            min-width: 100px;
            transition: width 300ms, border-radius 300ms;
        }

        .horizontal-timeline .events a.selected {
            color: #006341;
            font-weight: bold;
        }

        .horizontal-timeline .events {
            height: 1px;
            background: #b9b9b9;
        }

        .horizontal-timeline .events-content li::marker {
            content: '';
        }

        .timeline-navigation {
            width: 3%;
            top: 16px;
        }

        .timeline-navigation#leftNav, .timeline-navigation#rightNav {
            display: block !important;
            top: 15px!important;
        }

        .horizontal-timeline .events-wrapper {
            width: 90%;
        }
        .timeline-navigation a {
            font-size: 25px;
        }

        .timeline-navigation a.next {
            float: right;
        }

        .timeline-navigation a:hover {
            color: #006341;
        }

        .horizontal-timeline .events-content li {
            background-color: transparent;
            color: #006341;
            line-height: 26px;
            font-weight: bold;
            margin-bottom: 20px;
        }

        .fa-chevron-right:before {
             content: "\f054";
             float: right;
         }

        .timeline-carousel {
            text-align: center;
            padding: 0 50px;
            margin-bottom: 30px;
            position: relative;
        }

        .timeline-carousel-details {
            text-align: center;
            background-color: transparent;
            color: #006341;
            line-height: 26px;
            font-weight: bold;
            margin-bottom: 20px;
            font-size: 24px;
            padding-top: 20px;
        }

        .timeline-carousel .prev {
            position: absolute;
            left: 0;
            top: 20px;
            background-image: url({{ asset('img/arrow_left-active.png') }});
            background-repeat: no-repeat;
            background-position: center;
            height: 30px;
            width: 30px;
            background-color: #fff;
            background-size: 15px;
        }

        .timeline-carousel .prev.slick-disabled {
            opacity: .3;
        }

        .timeline-carousel .next {
            position: absolute;
            right: 0;
            top: 20px;
            background-image: url({{ asset('img/arrow_right-active.png') }});
            background-repeat: no-repeat;
            background-position: center;
            height: 30px;
            width: 30px;
            background-color: #fff;
            background-size: 15px;
        }

        .timeline-carousel-details .next.slick-disabled {
            opacity: .3;
        }

        .timeline-carousel-details img {
            display: inline-block;
            margin-bottom: 40px;
            opacity: 0;
            transition: opacity 300ms;
        }

        .timeline-carousel-details .slick-current img {
            opacity: 1;
        }

        .timeline-carousel .date {
            position: relative;
            padding-bottom: 10px;
            margin-bottom: 10px;
            cursor: pointer;
        }

        .timeline-carousel .date:before {
            content: '';
            background-color: #006341;
            display: inline-block;
            position: absolute;
            height: 18px;
            width: 3px;
            left: 50%;
            bottom: -10px;
            z-index: 2;
        }

        .timeline-carousel .date:after {
            content: '';
            border-bottom: 1px solid #000;
            display: inline-block;
            position: absolute;
            height: 1px;
            width: 100%;
            right: 0;
            bottom: -1px;
            z-index: 1;
        }

        .timeline-carousel .timeline:first-of-type .date:after {
            width: 50%;
        }

        .timeline-carousel .timeline:last-of-type .date:after {
            width: 50%;
            right: auto;
            left: 0;
        }

        .ellipse.where {
            left: -20px;
            top: -90px;
        }

        .timeline-carousel .slick-current .date:before {
            height: 18px;
            width: 18px;
            border-radius: 50%;
            margin-left: -9px;

        }

        .timezone.small {
            min-height: 350px;
        }

        @media only screen and (max-width: 1200px) {
            .timezone.small {
                min-height: 400px;
            }
        }

        @media only screen and (max-width: 991px) {
            .timezone.small {
                min-height: 500px;
            }
        }

        @media only screen and (max-width: 767px) {
            .timezone.small {
                min-height: 300px;
            }
        }

        @media only screen and (max-width: 1100px) {
            .events-content img {
                top: -440px;
            }
        }

        @media only screen and (max-width: 420px) {
            .events-content img {
                top: -490px;
                width: 100%;
                left: 0;
            }

            .horizontal-timeline {
                padding-top: 380px;
            }
        }
    </style>
@endsection

@section('content')

<section id="inner" class="mb-5">
    <div class="container">

        <h1 class="page-title">About Us</h1>

        <hr class="green"/>

        <div class="row">
            <div class="col-md-12">
                <div  class="headline">
                    {!! $pageContent['intro'] !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
{{--                <div class="horizontal-timeline" id="example">--}}
{{--                    <div class="events-content">--}}
{{--                        <ol>--}}
{{--                            <?php $timelines = $contentService->getTimeline(); ?>--}}

{{--                            @foreach($timelines as $item)--}}
{{--                                <li data-horizontal-timeline='{"date": "{{ \Carbon\Carbon::parse($item->date)->format('d/m/Y') }}"}'>--}}
{{--                                    <img src="{{ asset('') }}{{ $item->image }}" width="300">--}}
{{--                                    {{ $item->content }}--}}
{{--                                </li>--}}
{{--                            @endforeach--}}
{{--                        </ol>--}}
{{--                    </div>--}}
{{--                </div>--}}

                <?php $timelines = $contentService->getTimeline(); ?>

                <div class="timeline-carousel-details">
                    @foreach($timelines as $item)
                        <div>
                            <img src="{{ asset('') }}{{ $item->image }}" height="300">
                            <div class="title">{{ $item->content }}</div>
                        </div>
                    @endforeach
                </div>
                <div class="timeline-carousel">
                    @foreach($timelines as $item)
                        <div class="timeline">
                            <div class="date"> {{ \Carbon\Carbon::parse($item->date)->format('Y') }}</div>
                        </div>
                    @endforeach
                </div>


            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <br/>
                <h1 class="page-title">{!! $pageContent['inner-content-heading'] !!}</h1>
                <br/>
                {!! $pageContent['inner-content-body'] !!}
            </div>
        </div>
    </div>
</section>

<section id="where" class="pb-5 mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-6 relative">
                <span class="medium">
                    Our other <br/>
                    CSR activities<br/>
                    include:<br/>
                    <img src="{{ asset('') }}/img/ellipse-1.png" class="ellipse where">
                </span>
            </div>
            <div class="col-md-6">
                <ul class="timezone small clearfix">
                    <?php $activities = $contentService->getActivities(); ?>
                    @foreach($activities as $item)
                        <li><a href="{{ url('about-us/csr-activities/'.$item->slug) }}">{{ $item->title }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-5">
                <div class="socials-carousel" id="socials">

                    <?php $awards = $contentService->getAwards(); ?>
                    @foreach($awards as $item)
                        <div>
                            <img src="{{ asset($item->image) }}" width="100%" class="mb-2">
                            <p>{{ $item->content }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-7">
                <span class="mhuge">
                    Awards<br/>we’ve won<br/>
                    <img src="{{ asset('') }}/img/ellipse-2.png" class="ellipse social">
                </span>
            </div>
        </div>
    </div>
</section>

<section class="pb-5 mt-5">
    <div class="container">
        <div class="row">
            <?php $promobox = $contentService->getPromoboxById($pageContent['promobox']); ?>
            @include('partials.long-promoboxes')
        </div>
    </div>
</section>

@endsection()

@section('js')
    <script>

        $('.timeline-carousel').slick({
            centerMode: true,
            infinite: false,
            centerPadding: '0',
            slidesToShow: 5,
            asNavFor: '.timeline-carousel-details',
            prevArrow: '<a href="" class="prev"></a>',
            nextArrow: '<a href="" class="next"></a>',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        slidesToShow: 1
                    }
                }
            ]
        });

        $('.timeline-carousel-details').slick({
            infinite: false,
            centerMode: true,
            centerPadding: '0',
            slidesToShow: 1,
            asNavFor: '.timeline-carousel',
            focusOnSelect: true,
            arrows: false,
        });

        $('.timeline.slick-slide').on('click',function () {
            $('.timeline-carousel-details').slick('slickGoTo',$('.timeline.slick-slide').index($(this)));
        });


        $('#socials').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            draggable: true,
            arrows: false,
            adaptiveHeight: true,
            infinite: false,
            autoplay: true,
        });
    </script>
@endsection
