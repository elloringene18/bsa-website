@extends('admin.partials.master')

@section('css')
    <style>

        .greenbox {
            background-image: url({{ asset('img/greenbox.png') }});
            background-size: auto 100%;
            background-repeat: no-repeat;
            padding: 40px 20px;
            background-color: #A69FAC;
            height: 170px;
        }

        .greenbox .sub, .greenbox .head {
            display: inline-block;
            width: 100%;
            float: left;
        }

        .greenbox .sub {
            color: #0D2336;
        }

        .greenbox .head {
            font-size: 50px;
            line-height: 80px;
            font-weight: bold;
            text-transform: uppercase;
            color: #fff;
        }

        .greenbox .bt {
            position: absolute;
            bottom: 30px;
            right: 30px;
            background-color: #006341;
            color: #fff;
            border: 0;
            padding: 10px 30px;
            text-transform: uppercase;
            font-size: 12px;
        }

        .greenbox input[type="email"] {
            width: 80% !important;
            border: 0;
            border-bottom: 1px solid #000;
            background-color: transparent;
            border-radius: 0;
        }

        .greenbox .social {
            margin-bottom: 0;
            position: absolute;
            right: 40px;
            bottom: 30px;
        }

        .greenbox .social a {
            font-size: 40px;
            margin: 0 6px;
            color: #0D2336;
        }

        .promoboxsmall {
            background-image: url({{ asset('img/greenbox.png') }});
            background-size: auto 100%;
            background-repeat: no-repeat;
            padding: 40px 30px;
            background-color: #0D2336;
            color: #fff;
            border: 0;
            text-transform: uppercase;
            font-size: 12px;
            margin-bottom: 20px;
            width: 300px;
        }

        .promoboxsmall img {
            float: left;
            width: 100% !important;
            height: auto !important;
        }

        .promoboxsmall h3 {
            font-weight: bold;
            float: left;
            width: 100%;
        }

        .promoboxsmall h3, .promoboxsmall p {
            float: left;
        }

        .promoboxsmall .bt {
            background-color: #829EAA;
            color: #fff;
            border: 0;
            padding: 10px 30px;
            text-transform: uppercase;
            font-size: 12px;
            float: right;
            margin: 20px 0 0;
        }
    </style>
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>Promo Boxes</h3>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href="{{ url('admin/promos/create') }}">+ Add a Promo Box</a>
                            </div>
                            <div class="col-md-12">
                                &nbsp;
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @include('admin.partials.alert-box')

                            <div class="table-responsive">
                                <table class="table" id="dataTable">
                                    <tr>
                                        <th onclick="sortTable(0)">Title</th>
                                        <th>Action</th>
                                    </tr>
                                    @foreach($data as $item)
                                        @if($item->type=='newsletter'||$item->type=='social')
                                        @else
                                            <tr>
                                                <td>
                                                    <a href="{{ URL('admin/promos/edit/'.$item->id) }}">{{ $item->title }}</a>
                                                    <br/>
                                                    <br/>
                                                    @if($item->type=='long')
                                                        <div class="col-md-12 relative greenbox" style="background-color: {{ $item->color }}">
                                                            <span class="sub">{{ $item->subheading }}</span>
                                                            <span class="head">{{ $item->heading }}</span>
                                                            <a href="#"><button class="bt">{{ $item->link_text }}</button></a>
                                                        </div>
                                                    @elseif($item->type=='short')
                                                        <div class="promoboxsmall clearfix" style="background-color: {{ $item->color }}">
                                                            <img src="{{ asset($item->image) }}" width="100%" class="mb-2">
                                                            <h3>{{$item->heading}}</h3>
                                                            <a href="{{$item->link}}" class="bt">{{$item->link_text}}</a>
                                                        </div>
                                                    @endif
                                                </td>
                                                <td><a href="{{ URL('admin/promos/edit/'.$item->id) }}">Edit</a>
                                                    /
                                                    <a href="{{ URL('admin/promos/delete'.$item->id) }}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a></td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
