@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Edit Promo Box</h3>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/promos/update') }}" method="post" enctype="multipart/form-data">

                <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                <input type="hidden" value="{{$data->id}}" name="id">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @include('admin.partials.alert-box')

                                <div class="form-group">
                                    <label for="exampleInputNamea1">Title</label>
                                    <input required type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="title" value="{{ $data->title }}">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputNamea1">Heading</label>
                                    <input required type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="heading" value="{{ $data->heading }}">
                                </div>

                                <div class="form-group" id="subheading">
                                    <label for="exampleInputNamea1">Sub Heading</label>
                                    <input type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="subheading" value="{{ $data->subheading }}">
                                </div>

                                <div class="form-group" style="" id="content">
                                    <div class="form-group">
                                        <label>Content:</label>
                                        <textarea class="form-control" name="content" rows="20">{{ $data->content }}</textarea>
                                    </div>
                                </div>

                                <div class="form-group" id="image">
                                    <label for="exampleInputEmail3">Image (300x300)</label><br/>

                                    @if($data->image)
                                        <br/>
                                        <img src="{{ asset($data->image) }}" width="300">
                                        <br/>
                                        <br/>
                                    @endif

                                    <input type="file" accept="image/x-png,image/gif,image/jpeg" name="image"  value="" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputNamea1">Button Text</label>
                                    <input required type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="link_text" value="{{ $data->link_text }}">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputNamea1">Button Link</label>
                                    <input required type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="link" value="{{ $data->link }}">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputNamea1">Color</label>
                                    <select name="color" class="form-control">
                                        <option {{ $data->color == "#829EAA" ? 'selected' : '' }} value="#829EAA">Light Blue</option>
                                        <option {{ $data->color == "#0D2336" ? 'selected' : '' }} value="#0D2336">Dark Blue</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputNamea1">Type</label>
                                    <select name="type" class="form-control" id="type">
                                        <option {{ $data->type=='long' ? 'selected' : '' }} value="long">Long</option>
                                        <option {{ $data->type=='short' ? 'selected' : '' }} value="short">Short</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

@endsection

@section('js')
    <script>
        function checkType(){
            type = $('#type').val();

            if(type=='long'){
                $('#content').val('').hide();
                $('#image').val('').hide();
                $('#subheading').show();
            }

            else if(type=='short'){
                $('#content').val('').hide();
                $('#subheading').val('').hide();
                $('#image').show();
            }
        }

        checkType();

        $('#type').on('change',function () {
            checkType();
        })
    </script>
@endsection
