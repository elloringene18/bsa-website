@extends('admin.partials.master')

@section('content')
    <!-- partial -->

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">

                @if(Session::has('success'))
                    <div class="col-md-12">
                        <div class="alert-success alert">{{ Session::get('success') }}</div>
                    </div>
                @endif

                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <h3>Titles</h3>
                                </div>
                                <div class="col-md-6 text-right">
                                    <a href="{{ url('admin/lawyers/titles/create') }}">+ Add Title</a>
                                </div>
                                <div class="col-md-12">
                                    &nbsp;
                                </div>
                            </div>

                            <table class="table table-striped" width="100%">
                                    <tr>
                                        <td>Content</td>
                                        <td>Actions</td>
                                    </tr>
                                    @foreach($data as $item)
                                        <tr>
                                            <td>
                                                <a href="{{ url('admin/lawyers/titles/edit/'.$item->id) }}">{{ $item->name }}</a>
                                            </td>
                                            <td>
                                                <a href="{{ URL('admin/lawyers/titles/edit/'.$item->id) }}">Edit</a> |
                                                <a href="{{ URL('admin/lawyers/titles/delete/'.$item->id) }}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
