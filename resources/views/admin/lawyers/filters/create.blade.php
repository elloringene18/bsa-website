@extends('admin.partials.master')

@section('css')
    <link href="https://unpkg.com/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/css/dragSort.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .tagify__tag {
            width: 25%;
            height: 80px;
            text-align: center;
        }

        .tagify__tag>div {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            @if(Session::has('success'))
                <div class="alert alert-success">{{ Session::get('success') }}</div>
            @elseif(Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Create Lawyer Filter</h3>
                            <br/>
                            <br/>
                            <form action="{{ url('admin/lawyers/filters/store') }}" method="post" enctype="multipart/form-data">
                                @csrf

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Title</label>
                                            <select name="title_id" class="form-control">
                                                <option value="">None</option>
                                                @foreach($titles as $item)
                                                    <option value="{{$item->t_id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Service</label>
                                            <select name="service_id" class="form-control">
                                                <option value="">None</option>
                                                @foreach($types as $type=>$services)
                                                    <optgroup label="{{$type}}">
                                                        @foreach($services as $item)
                                                            <option value="{{$item->id}}">{{$item->title}}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Location</label>
                                            <select name="location_id" class="form-control">
                                                <option value="">None</option>
                                                @foreach($locations as $item)
                                                    <option value="{{$item->l_id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Lawyers</label>
                                            <input id="contacts" type="text" class="form-control" name="contacts" value=""/>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="form-group">
                                    <input type="submit" class="form-control btn-success" value="Save">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://unpkg.com/@yaireo/tagify"></script>
    <script src="https://unpkg.com/@yaireo/tagify/dist/tagify.polyfills.min.js"></script>
    <script src="{{ asset('admin/js/dragSort.js') }}"></script>
    <script type="text/javascript">
        var inputElm = document.getElementById('contacts'),
            tagify = new Tagify (inputElm,
                {
                    whitelist:
                        [
                            @foreach($lawyers as $item)
                            { id:'{{ $item->id }}', value:"{!! $item->name.' ('.$item->primaryTitle->name.')' !!}" },
                            @endforeach
                        ],
                    enforceWhitelist: true
                }
            );
        // bind "DragSort" to Tagify's main element and tell
        // it that all the items with the below "selector" are "draggable"
        var dragsort = new DragSort(tagify.DOM.scope, {
            selector: '.'+tagify.settings.classNames.tag,
            callbacks: {
                dragEnd: onDragEnd
            }
        })

        // must update Tagify's value according to the re-ordered nodes in the DOM
        function onDragEnd(elm){
            tagify.updateValueByDOMTags();
        }

        inputElm.addEventListener('change', onChange);

        function onChange(e){
            // outputs a String
            console.log(e.target.value)
        }
    </script>
@endsection
