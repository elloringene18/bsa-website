@extends('admin.partials.master')

@section('css')
    <link href="https://unpkg.com/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/css/dragSort.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .tagify__tag {
            width: 24%;
            height: 80px;
            text-align: center;
        }

        .tagify__tag>div {
            max-width: 80%;
            margin-left: 10%;
        }
    </style>
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            @if(Session::has('success'))
                <div class="alert alert-success">{{ Session::get('success') }}</div>
            @elseif(Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Edit Lawyer Filter</h3>
                            <br/>
                            @if(isset($currentFilter))
                                There is an existing filter for this combination. Click <a href="{{ url('admin/lawyers/filters/delete/'.$currentFilter->id) }}">here</a> to reset this filter.
                                <br/>
                            @endif
                            <br/>
                            <form id="filters" action="{{ url('admin/lawyers/filters') }}" method="get" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Title</label>
                                            <select name="title" class="form-control">
                                                <option value="">None</option>
                                                @foreach($titles as $item)
                                                    <option {{ isset($_GET['title'])?  $_GET['title'] == $item->slug ? 'selected' : '' : '' }} value="{{$item->slug}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Service</label>
                                            <select name="service" class="form-control">
                                                <option value="">None</option>
                                                @foreach($types as $type=>$services)
                                                    <optgroup label="{{$type}}">
                                                        @foreach($services as $item)
                                                            <option {{ isset($_GET['service'])?  $_GET['service'] == $item->slug ? 'selected' : '' : '' }} value="{{$item->slug}}">{{$item->title}}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Location</label>
                                            <select name="location" class="form-control">
                                                <option value="">None</option>
                                                @foreach($locations as $item)
                                                    <option {{ isset($_GET['location'])?  $_GET['location'] == $item->slug ? 'selected' : '' : '' }} value="{{$item->slug}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <form action="{{ url('admin/lawyers/filters/update') }}" method="post" enctype="multipart/form-data">
                                @csrf

                                @if($currentFilter)
                                    <input type="hidden" name="id" value="{{$currentFilter->id}}">
                                @endif

                                @if(isset($_GET['title']))
                                    <input type="hidden" name="title" value="{{$_GET['title']}}">
                                @endif

                                @if(isset($_GET['service']))
                                    <input type="hidden" name="service" value="{{$_GET['service']}}">
                                @endif

                                @if(isset($_GET['location']))
                                    <input type="hidden" name="location" value="{{$_GET['location']}}">
                                @endif

                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Lawyers</label>
                                            <input id="contacts" type="text" class="form-control" name="contacts" value=""/>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="form-group">
                                    <input type="submit" class="form-control btn-success" value="Save">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://unpkg.com/@yaireo/tagify"></script>
    <script src="https://unpkg.com/@yaireo/tagify/dist/tagify.polyfills.min.js"></script>
    <script src="{{ asset('admin/js/dragSort.js') }}"></script>
    <script type="text/javascript">
        var inputElm = document.getElementById('contacts'),
            tagify = new Tagify (inputElm,
                {
                    whitelist:
                        [
                            @foreach($lawyers as $item)
                            { id:'{{ $item->id }}', value:'{!! $item->name.' ('.$item->primaryTitle->name.')' !!} ' },
                            @endforeach
                        ],
                    enforceWhitelist: true
                }
            );

        @if($currentFilter)
            @if($currentFilter->items)
                @if(count($currentFilter->items))
                tagify.addTags(
                    [
                        @foreach($currentFilter->items as $item)
                        { id:'{{ $item->lawyer->id }}', value:'{{ $item->lawyer->name.' ('.$item->lawyer->primaryTitle->name.')' }}' },
                        @endforeach
                    ]
                );
                @endif
            @endif
        @else
            tagify.addTags(
                [
                    @foreach($data as $row)
                        @foreach($row as $item)
                            { id:'{{ $item->id }}', value:'{{ $item->name.' ('.$item->primaryTitle->name.')' }}' },
                        @endforeach
                    @endforeach
                ]
            );
        @endif

        // bind "DragSort" to Tagify's main element and tell
        // it that all the items with the below "selector" are "draggable"
        var dragsort = new DragSort(tagify.DOM.scope, {
            selector: '.'+tagify.settings.classNames.tag,
            callbacks: {
                dragEnd: onDragEnd
            }
        })

        // must update Tagify's value according to the re-ordered nodes in the DOM
        function onDragEnd(elm){
            tagify.updateValueByDOMTags();
        }

        inputElm.addEventListener('change', onChange);

        function onChange(e){
            // outputs a String
            console.log(e.target.value)
        }

        $('#filters select').on('change',function(){
            $('#filters').submit();
        });
    </script>
@endsection
