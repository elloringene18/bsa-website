@extends('admin.partials.master')

@section('css')
    <link href="https://unpkg.com/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
    <style>
        #experiences div, #educations div {
            position: relative;
        }
        #experiences button, #educations button {
            position: absolute;
            right: 0;
            top: 0;
        }
    </style>
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Add Lawyer</h3>
                            <br/>
                            @include('admin.partials.alert-box')
                            <br/>
                            <form action="{{ url('admin/lawyers/store') }}" method="post" enctype="multipart/form-data">
                                @csrf

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputName1">First Name</label>
                                            <input type="text" name="first_name" class="form-control" id="exampleInputName1" placeholder="" value="" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Last Name</label>
                                            <input type="text" name="last_name" class="form-control" id="exampleInputName1" placeholder="" value="" required>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Introduction (First paragraph)</label>
                                            <textarea name="excerpt" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Bio</label>
                                            <input type="hidden" name="content" value="">
                                            <div class="summernote">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr/>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Contact Number</label>
                                            <input type="text" name="telephone" class="form-control" id="exampleInputName1" placeholder="" value="" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Email</label>
                                            <input type="text" name="email" class="form-control" id="exampleInputName1" placeholder="" value="" required>
                                        </div>
                                    </div>
                                </div>
                                <hr/>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail3">Image (600x600)</label><br/>
                                            <input type="file" accept="image/x-png,image/gif,image/jpeg" name="photo"  value="" class="form-control" required>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="row">
                                    @for($x=0;$x<3;$x++)
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="exampleInputName1">Quotes #{{$x+1}}</label>
                                                <textarea class="form-control" name="quotes[{{$x}}][content]"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputName1">Quote #{{$x+1}} author (optional)</label>
                                                <input type="text" class="form-control" name="quotes[{{$x}}][author]">
                                            </div>
                                        </div>
                                    @endfor
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Primary Title</label>
                                            <select name="title-main" class="form-control">
                                                @foreach($titles as $item)
                                                    <option value="{{ $item->t_id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Secondary Title (optional)</label>
                                            <select name="title-sub" class="form-control">
                                                <option value="" readonly="">None</option>
                                                @foreach($titles as $item)
                                                    <option value="{{ $item->t_id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Artilce Reels</label>
                                            <input id="servicesIn2" type="text" class="form-control" name="reels" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Services</label>
                                            <input id="servicesIn" type="text" class="form-control" name="services" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Location</label>
                                            <select name="location" class="form-control">
                                                @foreach($locations as $item)
                                                    <option value="{{ $item->l_id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <hr/>

                                <h5>Experiences</h5>

                                <div id="experiences">
                                    <div><input type="text" name="experiences[0]" class="form-control" id="exampleInputName1" placeholder="" value=""></div>
                                </div>
                                <button id="addExperience">Add more</button>

                                <hr/>
                                <h5>Education</h5>

                                <div id="educations">
                                    <div><input type="text" name="educations[0]" class="form-control" id="exampleInputName1" placeholder="" value=""></div>
                                </div>
                                <button id="addEducations">Add more</button>

                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Tags (Auto fetch articles by tag)</label>
                                            <input id="tagsin" type="text" class="form-control" name="tags" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Related Articles (Manual article selection)</label>
                                            <input id="articlesin" type="text" class="form-control" name="articles" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Status</label>
                                            <select name="hidden" class="form-control">
                                                <option value="0">Published</option>
                                                <option value="1">Draft</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <h3>SEO Meta Tags</h3>
                                            <label for="exampleInputName1">Meta Title</label>
                                            <input type="text" name="meta_title" class="form-control mb-3">
                                            <label for="exampleInputName1">Meta Description</label>
                                            <input type="text" name="meta_description" class="form-control mb-3">
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <input type="submit" class="form-control btn-success" value="Save">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://unpkg.com/@yaireo/tagify"></script>
    <script src="https://unpkg.com/@yaireo/tagify/dist/tagify.polyfills.min.js"></script>
    <script type="text/javascript">

        $('#addExperience').on('click',function(e){
            e.preventDefault();
            $('#experiences').append('<div><input type="text" name="experiences['+Math.floor(Math.random()*10000)+']" class="form-control" id="exampleInputName1" placeholder="" value=""><button>Delete</button></div>')
        });

        $('#addEducations').on('click',function(e){
            e.preventDefault();
            $('#educations').append('<div><input type="text" name="educations['+Math.floor(Math.random()*10000)+']" class="form-control" id="exampleInputName1" placeholder="" value=""><button>Delete</button></div>')
        });

        var inputElm = document.getElementById('servicesIn'),
            tagify = new Tagify (inputElm,
                {
                    whitelist:
                        [
                            @foreach($services as $item)
                                { id:'{{ $item->id }}', value:"{!!  str_replace("'", '',str_replace('"', '', $item->title));  !!}" },
                            @endforeach
                        ],
                    enforceWhitelist: true
                }
            );

        inputElm.addEventListener('change', onChange);

        var inputElm = document.getElementById('servicesIn2'),
            tagify = new Tagify (inputElm,
                {
                    whitelist:
                        [
                            @foreach($services as $item)
                                { id:'{{ $item->id }}', value:"{!!  str_replace("'", '',str_replace('"', '', $item->title));  !!}" },
                            @endforeach
                        ],
                    enforceWhitelist: true
                }
            );

        inputElm.addEventListener('change', onChange);

        {{--var inputElm = document.getElementById('team'),--}}
        {{--tagify = new Tagify (inputElm,--}}
        {{--    {--}}
        {{--        whitelist:--}}
        {{--        [--}}
        {{--            @foreach($lawyers as $item)--}}
        {{--                { id:'{{ $item->id }}', value:'{{ $item->name }}' },--}}
        {{--            @endforeach--}}
        {{--        ]--}}
        {{--    }--}}
        {{--);--}}

        {{--inputElm.addEventListener('change', onChange);--}}

        var tags = [
                @foreach($tags as $item)
            { id:'{{ $item->t_id }}', value:"{!! str_replace('"', '', $item->name);  !!}" },
            @endforeach
        ];

        var inputElm = document.getElementById('tagsin'),
            tagify = new Tagify (inputElm,
                {
                    whitelist: tags,
                    enforceWhitelist: true
                }
            );

        inputElm.addEventListener('change', onChange);

        var inputElm = document.getElementById('articlesin'),
            tagify = new Tagify (inputElm,
                {
                    whitelist:
                        [
                                @foreach($articles as $item)
                            { id:'{{ $item->id }}', value:"{!! str_replace('"', '', $item->title);  !!}" },
                            @endforeach
                        ],
                    enforceWhitelist: true
                }
            );

        inputElm.addEventListener('change', onChange);

        function onChange(e){
            // outputs a String
            console.log(e.target.value)
        }

    </script>
@endsection
