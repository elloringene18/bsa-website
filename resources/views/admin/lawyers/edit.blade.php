@extends('admin.partials.master')

@section('css')
    <link href="https://unpkg.com/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
    <style>
        #experiences div, #educations div {
            position: relative;
        }
        #experiences button, #educations button {
            position: absolute;
            right: 0;
            top: 0;
        }
    </style>
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Edit Lawyer</h3>
                            <br/>
                            @include('admin.partials.alert-box')
                            <br/>

                            @if($data->hidden)
                                <div class="alert-warning">
                                    This content is currently set as "draft".  Click <a target="_blank" href="{{ url('lawyer/'.$data->slug.'/preview') }}">here</a> to preview.
                                </div>
                                <br/>
                            @endif

                            <form action="{{ url('admin/lawyers/update') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$data->id}}">

                                <hr/>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">URL: (https://bsabh.com/lawyer/<b>{{$data->slug}}</b>)</label>
                                            <input type="text" name="slug" class="form-control" id="exampleInputName1" placeholder="" value="{{$data->slug}}" required>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputName1">First Name</label>
                                            <input type="text" name="first_name" class="form-control" id="exampleInputName1" placeholder="" value="{{$data->first_name}}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Last Name</label>
                                            <input type="text" name="last_name" class="form-control" id="exampleInputName1" placeholder="" value="{{$data->last_name}}" required>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Introduction (First paragraph)</label>
                                            <textarea name="excerpt" class="form-control">{{$data->excerpt}}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Bio</label>
                                            <input type="hidden" name="content" value="{{$data->content}}">
                                            <div class="summernote">
                                                {!! $data->content !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr/>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Contact Number</label>
                                            <input type="text" name="telephone" class="form-control" id="exampleInputName1" placeholder="" value="{{$data->telephone}}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Email</label>
                                            <input type="text" name="email" class="form-control" id="exampleInputName1" placeholder="" value="{{$data->email}}" required>
                                        </div>
                                    </div>
                                </div>
                                <hr/>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail3">Image (600x600)</label><br/>
                                            <img src="{{ asset('/'.$data->photo) }}" width="300">
                                            <input type="file" accept="image/x-png,image/gif,image/jpeg" name="photo"  value="" class="form-control" >
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="row">
                                    @if($data->quotes)
                                        <?php $number = 0; ?>
                                        @foreach($data->quotes as $quote)
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label for="exampleInputName1">Quotes #{{$number+1}}</label>
                                                    <textarea class="form-control" name="quotes[{{$quote->id}}][content]">{{$quote->content}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="exampleInputName1">Quote #{{$number+1}} author (optional)</label>
                                                    <input type="text" class="form-control" name="quotes[{{$quote->id}}][author]" value="{{$quote->author}}">
                                                </div>
                                            </div>
                                            <?php $number++; ?>
                                            @endforeach
                                    @endif

                                    <?php $number++; ?>
                                    @for($x=0;$x<3;$x++)
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="exampleInputName1">Quotes #{{$x+$number}}</label>
                                                <textarea class="form-control" name="quotes[{{rand(1000,10000000)}}][content]"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputName1">Quote #{{$x+$number}} author (optional)</label>
                                                <input type="text" class="form-control" name="quotes[{{rand(1000,10000000)}}][author]">
                                            </div>
                                        </div>
                                    @endfor
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Primary Title</label>
                                            <select name="title-main" class="form-control">
                                                @foreach($titles as $item)
                                                    <option {{ $data->primaryTitle->t_id == $item->t_id ? 'selected' : '' }} value="{{ $item->t_id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Secondary Title (optional)</label>
                                            <select name="title-sub" class="form-control">
                                                <option value="" readonly="">None</option>
                                                @foreach($titles as $item)
                                                    <option {{ isset($data->subTitle) ? ( $data->subTitle->t_id == $item->t_id ? 'selected' : '' ) : '' }} value="{{ $item->t_id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Article Reels</label>
                                            <input id="servicesIn2" type="text" class="form-control" name="reels" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Services</label>
                                            <input id="servicesIn" type="text" class="form-control" name="services" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Location</label>
                                            <select name="location" class="form-control">
                                                @foreach($locations as $item)
                                                    <option {{$data->location->l_id == $item->l_id ? 'selected' : '' }} value="{{ $item->l_id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <hr/>

                                <h5>Experiences</h5>

                                <div id="experiences">
                                    @if($data->experience)
                                        @foreach($data->experience as $experience)
                                            <div><input type="text" name="experiences[{{$experience->id}}]" class="form-control" id="exampleInputName1" placeholder="" value="{{$experience->content}}"><button>Delete</button></div>
                                        @endforeach
                                    @endif
                                    <div><input type="text" name="experiences[0]" class="form-control" id="exampleInputName1" placeholder="" value=""></div>
                                </div>
                                <button id="addExperience">Add more</button>

                                <hr/>
                                <h5>Education</h5>

                                <div id="educations">
                                    @if($data->education)
                                        @foreach($data->education as $education)
                                            <div><input type="text" name="educations[{{$education->id}}]" class="form-control" id="exampleInputName1" placeholder="" value="{{$education->content}}"><button>Delete</button></div>
                                        @endforeach
                                    @endif
                                    <div><input type="text" name="educations[0]" class="form-control" id="exampleInputName1" placeholder="" value=""></div>
                                </div>
                                <button id="addEducations">Add more</button>

                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Tags (Auto fetch articles by tag)</label>
                                            <input id="tagsin" type="text" class="form-control" name="tags" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Related Articles (Manual article selection)</label>
                                            <input id="articlesin" type="text" class="form-control" name="articles" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Status</label>
                                            <select name="hidden" class="form-control">
                                                <option {{ $data->hidden == 0 ? 'selected' : '' }} value="0">Published</option>
                                                <option {{ $data->hidden == 1 ? 'selected' : '' }} value="1">Draft</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <h3>SEO Meta Tags</h3>
                                            <label for="exampleInputName1">Meta Title</label>
                                            <input type="text" name="meta_title" class="form-control mb-3" value="{{ $data->meta_title }}">
                                            <label for="exampleInputName1">Meta Description</label>
                                            <input type="text" name="meta_description" class="form-control mb-3" value="{{ $data->meta_description }}">
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <input type="submit" class="form-control btn-success" value="Save">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://unpkg.com/@yaireo/tagify"></script>
    <script src="https://unpkg.com/@yaireo/tagify/dist/tagify.polyfills.min.js"></script>
    <script type="text/javascript">

        $('#addExperience').on('click',function(e){
            e.preventDefault();
            $('#experiences').append('<div><input type="text" name="experiences['+Math.floor(Math.random()*10000)+']" class="form-control" id="exampleInputName1" placeholder="" value=""><button>Delete</button></div>')
        });

        $('#addEducations').on('click',function(e){
            e.preventDefault();
            $('#educations').append('<div><input type="text" name="educations['+Math.floor(Math.random()*10000)+']" class="form-control" id="exampleInputName1" placeholder="" value=""><button>Delete</button></div>')
        });

        var inputElm = document.getElementById('servicesIn'),
            tagify = new Tagify (inputElm,
                {
                    whitelist:
                        [
                                @foreach($services as $item)
                            { id:'{{ $item->id }}', value:"{!!  $item->title  !!}" },
                            @endforeach
                        ],
                    enforceWhitelist: true
                }
            );

        @if($data->services)
        @if(count($data->services))
        tagify.addTags(
            [
                    @foreach($data->services as $item)
                { id:'{{ $item->id }}', value:"{!! $item->title !!}" },
                @endforeach
            ]
        );
        @endif
        @endif

        var inputElm = document.getElementById('servicesIn2'),
            tagify = new Tagify (inputElm,
                {
                    whitelist:
                        [
                            @foreach($services as $item)
                            { id:'{{ $item->id }}', value:"{!!  $item->title  !!}" },
                            @endforeach
                        ],
                    enforceWhitelist: true
                }
            );

        @if($data->reels)
        @if(count($data->reels))
        tagify.addTags(
            [
                    @foreach($data->reels as $item)
                { id:'{{ $item->id }}', value:"{!! $item->title !!}" },
                @endforeach
            ]
        );
        @endif
        @endif

        inputElm.addEventListener('change', onChange);

        {{--var inputElm = document.getElementById('team'),--}}
        {{--tagify = new Tagify (inputElm,--}}
        {{--    {--}}
        {{--        whitelist:--}}
        {{--        [--}}
        {{--            @foreach($lawyers as $item)--}}
        {{--                { id:'{{ $item->id }}', value:'{{ $item->name }}' },--}}
        {{--            @endforeach--}}
        {{--        ]--}}
        {{--    }--}}
        {{--);--}}

        {{--inputElm.addEventListener('change', onChange);--}}

        var tags = [
                @foreach($tags as $item)
            { id:'{{ $item->t_id }}', value:"{!! str_replace('"', '', $item->name);  !!}" },
            @endforeach
        ];

        var inputElm = document.getElementById('tagsin'),
            tagify = new Tagify (inputElm,
                {
                    whitelist: tags,
                    enforceWhitelist: true
                }
            );

        @if($data->tags)
        @if(count($data->tags))
        tagify.addTags(
            [
                @foreach($data->tags as $item)
                { id:'{{ $item->id }}', value:'{!! str_replace('"', '', $item->name); !!}' },
                @endforeach
            ]
        );
        @endif
        @endif

        inputElm.addEventListener('change', onChange);

        var inputElm = document.getElementById('articlesin'),
            tagify = new Tagify (inputElm,
                {
                    whitelist:
                        [
                            @foreach($articles as $item)
                            { id:'{{ $item->id }}', value:"{!! str_replace("'", '',str_replace('"', '', $item->title));  !!}" },
                            @endforeach
                        ],
                    enforceWhitelist: true
                }
            );

        @if($data->articles)
            @if(count($data->articles))
            tagify.addTags(
                [
                    @foreach($data->articles as $item)
                        { id:'{{ $item->id }}', value:"{!!  str_replace("'", '',str_replace('"', '', $item->title));  !!}" },
                    @endforeach
                ]
            );
            @endif
        @endif

        inputElm.addEventListener('change', onChange);

        function onChange(e){
            // outputs a String
            console.log(e.target.value)
        }

    </script>
@endsection
