
<!-- partial:partials/_footer.html -->
<footer class="footer">
    <div class="container-fluid clearfix">
		<span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
			<a href="http://www.thisishatch.com/" target="_blank">Hatch</a>. All rights reserved.
	    </span>
    </div>
</footer>
<!-- partial -->
</div>
<!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- plugins:js -->
<script src="{{ asset('public/admin') }}/vendors/js/vendor.bundle.base.js"></script>
<script src="{{ asset('public/admin') }}/vendors/js/vendor.bundle.addons.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{ asset('public/admin') }}/js/off-canvas.js"></script>
<script src="{{ asset('public/admin') }}/js/misc.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{ asset('public/admin') }}/js/dashboard.js"></script>
<!-- End custom js for this page-->
<!-- include summernote css/js -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- include summernote-ko-KR -->
{{--<script src="{{ asset('public/admin') }}/js/summernote-image-attributes.js"></script>--}}
<script src="{{ asset('public/admin') }}/lang/summernote-ar-AR.js"></script>

<!-- MDB core JavaScript -->

</body>
<script>
    $('.summernote.short').summernote({
        placeholder: 'Add content here...',
        tabsize: 2,
        height: 70,
        toolbar: [
            ['style', ['blockquote']],//add this
        ],
        // codeCleaner: {
        //     stripTags: ['style', 'script', 'applet', 'embed', 'param', 'noframes', 'noscript', 'font', 'span', 'iframe', 'form', 'input', 'button', 'select', 'option', 'colgroup', 'col', 'std'],
        //     stripAttributes: ['rel', 'class', 'style'],
        //     wildCardAttributesPrefix: ['data-'],
        //     msgStyle: 'position:absolute;top:0;left:0;right:0',
        //     msgShow: true, //true of false
        //     msgTxt: "Your pasted text has been cleaned.",
        // }
//your other options here...
        onPaste: function(e) {
            console.log('clean');
            var thisNote = $(this);
            var updatePastedText = function(someNote){
                var original = someNote.code();
                var cleaned = CleanPastedHTML(original); //this is where to call whatever clean function you want. I have mine in a different file, called CleanPastedHTML.
                someNote.code('').html(cleaned); //this sets the displayed content editor to the cleaned pasted code.
            };
            setTimeout(function () {
                //this kinda sucks, but if you don't do a setTimeout,
                //the function is called before the text is really pasted.
                updatePastedText(thisNote);
            }, 10);
        }
    });

    $('.summernote').summernote({
        placeholder: 'Add content here...',
        tabsize: 1,
        height: 300,
        toolbar: [
            ['font', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph','blockquote']],
            ['height', ['height']],
            ['insert', ['picture', 'hr','video']],
            ['table', ['table']],
            ['view', ['codeview']],
            ['link', ['link']],
            ['misc', ['undo']],
            ['style', ['blockquote']],//add this
        ],
        popover: {
            image: [
                ['custom', ['imageAttributes']],
                ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['remove', ['removeMedia']]
            ],
        },
        imageAttributes:{
            icon:'<i class="note-icon-pencil"/>',
            removeEmpty:false, // true = remove attributes | false = leave empty if present
            disableUpload: false // true = don't display Upload Options | Display Upload Options
        },
        // codeCleaner: {
        //     stripTags: ['style', 'script', 'applet', 'embed', 'param', 'noframes', 'noscript', 'font', 'span', 'iframe', 'form', 'input', 'button', 'select', 'option', 'colgroup', 'col', 'std'],
        //     stripAttributes: ['rel', 'class', 'style'],
        //     wildCardAttributesPrefix: ['data-'],
        //     msgStyle: 'position:absolute;top:0;left:0;right:0',
        //     msgShow: true, //true of false
        //     msgTxt: "Your pasted text has been cleaned.",
        // }
//your other options here...
        callbacks: {
            // callback for pasting text only (no formatting)
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                bufferText = bufferText.replace(/\r?\n/g, '<br>');
                document.execCommand('insertHtml', false, bufferText);
                console.log('cleansss');
                //
                // var thisNote = $(this);
                // var updatePastedText = function(someNote){
                //     var original = someNote.code();
                //     var cleaned = CleanPastedHTML(original); //this is where to call whatever clean function you want. I have mine in a different file, called CleanPastedHTML.
                //     someNote.code('').html(cleaned); //this sets the displayed content editor to the cleaned pasted code.
                // };
                // setTimeout(function () {
                //     //this kinda sucks, but if you don't do a setTimeout,
                //     //the function is called before the text is really pasted.
                //     updatePastedText(thisNote);
                // }, 10);
            }
        },
    });

    function CleanPastedHTML(input) {
        // 1. remove line breaks / Mso classes
        var stringStripper = /(\n|\r| class=(")?Mso[a-zA-Z]+(")?)/g;
        var output = input.replace(stringStripper, ' ');
        // 2. strip Word generated HTML comments
        var commentSripper = new RegExp('<!--(.*?)-->','g');
        var output = output.replace(commentSripper, '');
        var tagStripper = new RegExp('<(/)*(meta|link|span|\\?xml:|st1:|o:|font)(.*?)>','gi');
        // 3. remove tags leave content if any
        output = output.replace(tagStripper, '');
        // 4. Remove everything in between and including tags '<style(.)style(.)>'
        var badTags = ['style', 'script','applet','embed','noframes','noscript'];

        for (var i=0; i< badTags.length; i++) {
            tagStripper = new RegExp('<'+badTags[i]+'.*?'+badTags[i]+'(.*?)>', 'gi');
            output = output.replace(tagStripper, '');
        }
        // 5. remove attributes ' style="..."'
        var badAttributes = ['style', 'start'];
        for (var i=0; i< badAttributes.length; i++) {
            var attributeStripper = new RegExp(' ' + badAttributes[i] + '="(.*?)"','gi');
            output = output.replace(attributeStripper, '');
        }
        return output;
    }

    $('.summernote').on('summernote.change', function(we, contents, $editable) {
        console.log($(this).closest('.form-group'));
        $(this).closest('.form-group').find('input').val(contents);
        console.log('summernote\'s content is changed.');
    });

    $(".summernote").on("summernote.enter", function(we, e) {
        $(this).summernote("pasteHTML", "");
        e.preventDefault();
    });

    $('.external-switch').change(function() {
        if(this.checked) {
            $(this).closest('.card-body').find('.content').hide();
            $(this).closest('.card-body').find('.link').show();
        }
        else {
            $(this).closest('.card-body').find('.content').show();
            $(this).closest('.card-body').find('.link').hide();
        }
    });

    $(document).ready(function(){

        $('.type_select').on('change',function(){
            lang = $(this).attr('data-lang');
            $(this).closest('.card-body').find('.type-box').hide();
            $(this).closest('.card-body').find('.type-box input').val('');

            if($(this).val()!='blank')
                $(this).closest('.card-body').find('.type-box.'+$(this).val()).show();
            else
                $(this).closest('.card-body').find('.type-box.url input').val('#');

            if($(this).val()!="page")
                $(this).closest('.card-body').find('.content').hide();
        });

        $('#type_select').trigger('change');

        $('.datetimepicker').datepicker({
            dateFormat: 'm/d/y'
        });
    });
</script>

<script>
    function sortTable(n) {
        var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
        table = document.getElementById("dataTable");
        switching = true;
        // Set the sorting direction to ascending:
        dir = "asc";
        /* Make a loop that will continue until
        no switching has been done: */
        while (switching) {
            // Start by saying: no switching is done:
            switching = false;
            rows = table.rows;
            /* Loop through all table rows (except the
            first, which contains table headers): */
            for (i = 1; i < (rows.length - 1); i++) {
                // Start by saying there should be no switching:
                shouldSwitch = false;
                /* Get the two elements you want to compare,
                one from current row and one from the next: */
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                /* Check if the two rows should switch place,
                based on the direction, asc or desc: */
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                /* If a switch has been marked, make the switch
                and mark that a switch has been done: */
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                // Each time a switch is done, increase this count by 1:
                switchcount ++;
            } else {
                /* If no switching has been done AND the direction is "asc",
                set the direction to "desc" and run the while loop again. */
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }
    }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tagify/4.17.7/tagify.min.js"></script>

@yield('js')
</html>
