
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link {{ isset($pageSlug) ? ($pageSlug=='home' ? 'collapsed' : '') : '' }}" data-toggle="collapse" href="#home" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Home</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <div class="collapse {{ isset($pageSlug) ? ($pageSlug=='home' ? 'show' : false) : 'in'}}" id="home">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/sections/home') }}">Page Contents</a>
                </li>
            </ul>
        </div>
        <li class="nav-item">
            <a class="nav-link {{ isset($pageSlug) ? ($pageSlug=='about' ? 'collapsed' : '') : '' }}" data-toggle="collapse" href="#about" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">About</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <div class="collapse {{ isset($pageSlug) ? ($pageSlug=='about' ? 'show' : false) : 'in'}}" id="about">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/sections/about') }}">Page Contents</a>
                    <a class="nav-link" href="{{ URL('admin/about/timelines') }}">View Timeline</a>
                    <a class="nav-link" href="{{ URL('admin/about/timelines/create') }}">Add Timeline</a>
                    <a class="nav-link" href="{{ URL('admin/about/activities') }}">View CSR Activities</a>
                    <a class="nav-link" href="{{ URL('admin/about/activities/create') }}">Add CSR Activity</a>
                    <a class="nav-link" href="{{ URL('admin/about/awards') }}">View Awards</a>
                    <a class="nav-link" href="{{ URL('admin/about/awards/create') }}">Add Award</a>
                </li>
            </ul>
        </div>
        <li class="nav-item">
            <a class="nav-link {{ isset($pageSlug) ? ($pageSlug=='services' ? 'collapsed' : '') : '' }}" data-toggle="collapse" href="#services" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Services</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <div class="collapse {{ isset($pageSlug) ? ($pageSlug=='services' ? 'show' : false) : 'in'}}" id="services">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/sections/services') }}">Page content</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/services/') }}">View Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/services/create') }}">Add Service</a>
                </li>
            </ul>
        </div>
        <li class="nav-item">
            <a class="nav-link {{ isset($pageSlug) ? ($pageSlug=='lawyers' ? 'collapsed' : '') : '' }}" data-toggle="collapse" href="#lawyers" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Our People</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <div class="collapse {{ isset($pageSlug) ? ($pageSlug=='lawyers' ? 'show' : false) : 'in'}}" id="lawyers">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/sections/our-people') }}">Page content</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/lawyers/') }}">View Lawyers</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/lawyers/create') }}">Add Lawyer</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/lawyers/titles/') }}">View Titles</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/lawyers/titles/create') }}">Add Title</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/lawyers/filters/') }}">Edit Filters</a>
                </li>
            </ul>
        </div>
        <li class="nav-item">
            <a class="nav-link {{ isset($pageSlug) ? ($pageSlug=='articles' ? 'collapsed' : '') : '' }}" data-toggle="collapse" href="#articles" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Knowledge</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <div class="collapse {{ isset($pageSlug) ? ($pageSlug=='articles' ? 'show' : false) : 'in'}}" id="articles">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/sections/knowledge') }}">Page content</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/articles/') }}">View Articles</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/articles/create') }}">Add Article</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/events/') }}">View Events</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/events/create') }}">Add Event</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/categories/') }}">View Categories</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/categories/create') }}">Add Category</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/tags/') }}">View Tags</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/tags/create') }}">Add Tag</a>
                </li>
            </ul>
        </div>
        <div class="collapse {{ isset($pageSlug) ? ($pageSlug=='articles' ? 'show' : false) : 'in'}}" id="articles">
            <ul class="nav flex-column sub-menu">
            </ul>
        </div>
        <li class="nav-item">
            <a class="nav-link {{ isset($pageSlug) ? ($pageSlug=='files' ? 'collapsed' : '') : '' }}" data-toggle="collapse" href="#files" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Files</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <div class="collapse {{ isset($pageSlug) ? ($pageSlug=='files' ? 'show' : false) : 'in'}}" id="files">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/files/') }}">View Files</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/files/create') }}">Add File</a>
                </li>
            </ul>
        </div>
        <li class="nav-item">
            <a class="nav-link {{ isset($pageSlug) ? ($pageSlug=='locations' ? 'collapsed' : '') : '' }}" data-toggle="collapse" href="#locations" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Locations</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <div class="collapse  {{ isset($pageSlug) ? ($pageSlug=='locations' ? 'show' : false) : 'in'}}" id="locations">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/sections/locations') }}">Page content</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/locations/') }}">View Locations</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/locations/create') }}">Add Location</a>
                </li>
            </ul>
        </div>
        <li class="nav-item">
            <a class="nav-link {{ isset($pageSlug) ? ($pageSlug=='careers' ? 'collapsed' : '') : '' }}" data-toggle="collapse" href="#careers" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Careers</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <div class="collapse  {{ isset($pageSlug) ? ($pageSlug=='careers' ? 'show' : false) : 'in'}}" id="careers">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/sections/careers') }}">Page content</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/careers/values/') }}">View Core Values</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/careers/values/create') }}">Add Core Values</a>
                </li>
            </ul>
        </div>
        <li class="nav-item">
            <a class="nav-link {{ isset($pageSlug) ? ($pageSlug=='media' ? 'collapsed' : '') : '' }}" data-toggle="collapse" href="#media" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Media</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <div class="collapse  {{ isset($pageSlug) ? ($pageSlug=='media' ? 'show' : false) : 'in'}}" id="media">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/podcasts/') }}">View Podcasts</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/podcasts/create') }}">Add Podcast</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/videos/') }}">View Videos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/videos/create') }}">Add Video</a>
                </li>
            </ul>
        </div>
        <li class="nav-item">
            <a class="nav-link {{ isset($pageSlug) ? ($pageSlug=='promos' ? 'collapsed' : '') : '' }}" data-toggle="collapse" href="#promos" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Promo Boxes</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <div class="collapse  {{ isset($pageSlug) ? ($pageSlug=='promos' ? 'show' : false) : 'in'}}" id="promos">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/promos/') }}">View Promo Boxes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/promos/create') }}">Add Promo Box</a>
                </li>
            </ul>
        </div>
        <li class="nav-item">
            <a class="nav-link {{ isset($pageSlug) ? ($pageSlug=='pages' ? 'collapsed' : '') : '' }}" data-toggle="collapse" href="#pages" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Custom Pages</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <div class="collapse {{ isset($pageSlug) ? ($pageSlug=='pages' ? 'show' : false) : 'in'}}" id="pages">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/pages/') }}">View Custom Pages</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/pages/create') }}">Add Custom Page</a>
                </li>
            </ul>
        </div>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('admin/bottomlinks') }}">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Footer Links</span>
                <i class="menu-arrow"></i>
            </a>
        </li>

        {{--<li class="nav-item">--}}
            {{--<a class="nav-link " data-toggle="collapse" href="{{ URL('admin/contacts') }}">--}}
                {{--<i class="menu-icon mdi mdi-content-copy"></i>--}}
                {{--<span class="menu-title">Inquiries</span>--}}
            {{--</a>--}}
        {{--</li>--}}
    </ul>
</nav>
