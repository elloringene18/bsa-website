@if(Session::has('success'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert-success alert">{{ Session::get('success') }}</div>
        </div>
    </div>
@endif

@if(Session::has('error'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert-warning alert">{{ Session::get('error') }}</div>
        </div>
    </div>
@endif
