@extends('admin.partials.master')

@section('css')
    <link href="https://unpkg.com/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/css/dragSort.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .tagify__tag {
            width: 100%;
            text-align: center;
        }

        .tagify__tag>div {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            @if(Session::has('success'))
                <div class="alert alert-success">{{ Session::get('success') }}</div>
            @elseif(Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Edit Tag</h3>
                            <br/>
                            @include('admin.partials.alert-box')
                            <br/>
                            <form action="{{ url('admin/tags/update') }}" method="post" enctype="multipart/form-data">
                                @csrf

                                <input type="hidden" name="id" value="{{$data->id}}">


                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Name</label>
                                            <input type="text" class="form-control" name="name" value="{{ $data->name }}"/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Articles</label>
                                            <input id="articlesIn" type="text" class="form-control" name="articles" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Services</label>
                                            <input id="servicesIn" type="text" class="form-control" name="services" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Lawyers</label>
                                            <input id="lawyersIn" type="text" class="form-control" name="lawyers" value=""/>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="form-group">
                                    <input type="submit" class="form-control btn-success" value="Save">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://unpkg.com/@yaireo/tagify"></script>
    <script src="https://unpkg.com/@yaireo/tagify/dist/tagify.polyfills.min.js"></script>
    <script src="{{ asset('admin/js/dragSort.js') }}"></script>
    <script type="text/javascript">
        var inputElm = document.getElementById('articlesIn'),
            tagify = new Tagify (inputElm,
                {
                    whitelist:
                        [
                            @foreach($articles as $item)
                            { id:'{{ $item->id }}', value:"{!! str_replace('"', '', $item->title) !!}" },
                            @endforeach
                        ],
                    enforceWhitelist: true
                }
            );

        @if($data->articles)
            @if(count($data->articles))
            tagify.addTags(
                [
                    @foreach($data->articles as $item)
                        { id:'{{ $item->id }}', value:"{!! str_replace('"', '', $item->title) !!}" },
                    @endforeach
                ]
            );
            @endif
        @endif


        var inputElm = document.getElementById('servicesIn'),
        tagify = new Tagify (inputElm,
            {
                whitelist:
                    [
                        @foreach($services as $item)
                        { id:'{{ $item->id }}', value:"{!! str_replace('"', '', $item->title) !!}" },
                        @endforeach
                    ],
                enforceWhitelist: true
            }
        );

        @if($data->services)
            @if(count($data->services))
            tagify.addTags(
                [
                    @foreach($data->services as $item)
                        { id:'{{ $item->id }}', value:"{!! str_replace('"', '', $item->title) !!}" },
                    @endforeach
                ]
            );
            @endif
        @endif

        var inputElm = document.getElementById('lawyersIn'),
                tagify = new Tagify (inputElm,
                    {
                        whitelist:
                            [
                                @foreach($lawyers as $item)
                                { id:'{{ $item->id }}', value:"{!! str_replace('"', '', $item->name) !!}" },
                                @endforeach
                            ],
                        enforceWhitelist: true
                    }
                );

        @if($data->lawyers)
            @if(count($data->lawyers))
            tagify.addTags(
                [
                    @foreach($data->lawyers as $item)
                        { id:'{{ $item->id }}', value:"{!! str_replace('"', '', $item->name) !!}" },
                    @endforeach
                ]
            );
            @endif
        @endif


        function onChange(e){
            // outputs a String
            console.log(e.target.value)
        }
    </script>
@endsection
