@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Upload a File</h3>
                            <br/>
                            <br/>
                            <form action="{{ url('admin/files/store') }}" method="post" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group">
                                    <label for="exampleInputName1">File:</label>
                                    <input type="file" name="file" class="form-control" id="exampleInputName1" placeholder="" value="">
                                </div>

                                <div class="form-group">
                                    <br/>
                                    <input type="submit" class="form-control btn-success" value="Save">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
