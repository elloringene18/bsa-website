@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Edit Podcast</h3>
                            @include('admin.partials.alert-box')
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/podcasts/update') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                <input type="hidden" value="{{$data->id}}" name="id">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputNamea1">Link</label><br/>
                                    <a href="{{ asset('/'.$data->audio_file) }}">{{ asset('/'.$data->audio_file) }}</a>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputNamea1">Title</label>
                                    <input required type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="title" value="{{ isset($data) ? $data->title : '' }}">
                                </div>
                                {{--                                <div class="form-group">--}}
                                {{--                                    <label>Speaker</label>--}}
                                {{--                                    <input type="text" class="form-control" placeholder="Speaker" name="speaker" value="{{ isset($page) ? $page->speaker : '' }}">--}}
                                {{--                                </div>--}}

                                <div class="form-group" style="">
                                    <div class="form-group">
                                        <label>Excerpt:</label>
                                        <input type="text" class="form-control" placeholder="Excerpt (Max 10 words)" name="excerpt" value="{{ isset($data) ? $data->excerpt : '' }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail3">Image (300x300)</label>
                                    <br/>
                                    @if($data->image)
                                    <br/>
                                    <img src="{{ asset($data->image) }}" width="100">
                                    <br/>
                                    <br/>
                                    @endif
                                    <input type="file" accept="image/x-png,image/gif,image/jpeg" name="image"  value="" class="form-control" >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Audio File (MP3) (Max size 20MB):</label>
                                    <br/>
                                    <audio controls>
                                        <source src="{{ asset('/'.$data->audio_file) }}" type="audio/mp3">
                                        Your browser does not support the audio element.
                                    </audio>
                                    <br/>
                                    <input type="file" class="form-control" placeholder="" name="audio_file" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Publish date</label>
                                    <?php
                                    $date = \Carbon\Carbon::now()->format('m/d/y');

                                    if(isset($page))
                                        if($page->publish_date)
                                            $date = $page->publish_date->format('m/d/y');
                                    ?>
                                    <input type="text" class="form-control datetimepicker" readonly placeholder="" name="date" value="{{ $date }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

@endsection

@section('js')
@endsection
