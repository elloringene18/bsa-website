@extends('admin.partials.master')

@section('content')
    <!-- partial -->

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">

                @if(Session::has('success'))
                    <div class="col-md-12">
                        <div class="alert-success alert">{{ Session::get('success') }}</div>
                    </div>
                @endif

                @if(Session::has('error'))
                    <div class="col-md-12">
                        <div class="alert-warning alert">{{ Session::get('error') }}</div>
                    </div>
                @endif

                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <h3>Services</h3>
                                </div>
                                <div class="col-md-6 text-right">
                                    <a href="{{ url('admin/services/create') }}">+ Add Service</a>
                                </div>
                                <div class="col-md-12">
                                    &nbsp;
                                </div>
                            </div>

                            <table class="table table-striped" width="100%">
                                    <tr>
                                        <td>Name</td>
                                        <td>Actions</td>
                                    </tr>
                                @foreach($data as $type)
                                    <tr><td><b>{{$type->name}}</b></td></tr>
                                    @foreach($type->services as $post)
                                        <tr>
                                            <td>
                                                <span style="width: 50px;display: inline-block"></span>
                                                <a href="{{ url('admin/services/edit/'.$post->id) }}">{{ $post->title }}</a>
                                            </td>
                                            <td>
                                                <a href="{{ URL('admin/services/edit/'.$post->id) }}">Edit</a> |
                                                <a href="{{ URL('admin/services/delete/'.$post->id) }}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
