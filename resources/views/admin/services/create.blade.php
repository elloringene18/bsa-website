@extends('admin.partials.master')

@section('css')
    <link href="https://unpkg.com/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Create Service</h3>
                            <br/>
                            <br/>
                            <form action="{{ url('admin/services/store') }}" method="post" enctype="multipart/form-data">
                                @csrf


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Name</label>
                                            <input type="text" name="name" class="form-control" id="exampleInputName1" placeholder="" value="" required>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Content</label>
                                            <input type="hidden" name="content" value="">
                                            <div class="summernote">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="row">
                                    @for($x=0;$x<3;$x++)
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="exampleInputName1">Quotes #{{$x+1}}</label>
                                                <textarea class="form-control" name="quotes[{{$x}}][content]"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputName1">Quote #{{$x+1}} author (optional)</label>
                                                <input type="text" class="form-control" name="quotes[{{$x}}][author]">
                                            </div>
                                        </div>
                                    @endfor
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Type</label>
                                            <select name="service_type_id" class="form-control">
                                                @foreach($types as $type)
                                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Key Contacts</label>
                                            <input id="contacts" type="text" class="form-control" name="contacts" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-12">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label for="exampleInputName1">Team</label>--}}
{{--                                            <input id="team" type="text" class="form-control" name="team" value=""/>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <hr/>--}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Tags (Auto fetch articles by tag)</label>
                                            <input id="tagsin" type="text" class="form-control" name="tags" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Related Articles (Manual article selection)</label>
                                            <input id="articlesin" type="text" class="form-control" name="articles" value=""/>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Promo Boxes</label>
                                            <input id="promosin" type="text" class="form-control" name="promos" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <h3>SEO</h3>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName1">Title</label>
                                            <input type="text" class="form-control" name="meta_title" value=""/>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName1">Description</label>
                                            <input type="text" class="form-control" name="meta_description" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Status</label>
                                            <select name="hidden" class="form-control">
                                                <option value="0">Published</option>
                                                <option value="1">Draft</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <input type="submit" class="form-control btn-success" value="Save">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://unpkg.com/@yaireo/tagify"></script>
    <script src="https://unpkg.com/@yaireo/tagify/dist/tagify.polyfills.min.js"></script>
    <script type="text/javascript">

        var inputElm = document.getElementById('contacts'),
        tagify = new Tagify (inputElm,
            {
                whitelist:
                [
                    @foreach($lawyers as $item)
                        { id:'{{ $item->id }}', value:'{{ $item->name }}' },
                    @endforeach
                ],
                enforceWhitelist: true
            }
        );

        inputElm.addEventListener('change', onChange)

        var inputElm = document.getElementById('promosin'),
            tagify = new Tagify (inputElm,
                {
                    whitelist:
                        [
                                @foreach($promos as $item){ id:'{{ $item->id }}', value:'{!! $item->title !!}' },@endforeach
                        ],
                    enforceWhitelist: true
                }
            );

        inputElm.addEventListener('change', onChange);

        {{--var inputElm = document.getElementById('team'),--}}
        {{--tagify = new Tagify (inputElm,--}}
        {{--    {--}}
        {{--        whitelist:--}}
        {{--        [--}}
        {{--            @foreach($lawyers as $item)--}}
        {{--                { id:'{{ $item->id }}', value:'{{ $item->name }}' },--}}
        {{--            @endforeach--}}
        {{--        ]--}}
        {{--    }--}}
        {{--);--}}

        {{--inputElm.addEventListener('change', onChange);--}}

        var tags = [
                @foreach($tags as $item)
            { id:'{{ $item->t_id }}', value:"{{ str_replace('"', '', $item->name) }}" },
            @endforeach
        ];

        var inputElm = document.getElementById('tagsin'),
        tagify = new Tagify (inputElm,
            {
                whitelist: tags,
                enforceWhitelist: true
            }
        );

        inputElm.addEventListener('change', onChange);

        var inputElm = document.getElementById('articlesin'),
        tagify = new Tagify (inputElm,
            {
                whitelist:
                    [
                        @foreach($articles as $item)
                        { id:'{{ $item->id }}', value:"{!!  str_replace("'", '',str_replace('"', '', $item->title));  !!}" },
                        @endforeach
                    ],
                enforceWhitelist: true
            }
        );

        inputElm.addEventListener('change', onChange);

        function onChange(e){
            // outputs a String
            console.log(e.target.value)
        }

    </script>
@endsection
