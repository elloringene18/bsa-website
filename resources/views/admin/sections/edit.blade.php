@extends('admin.partials.master')

@section('css')
    <link href="https://unpkg.com/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/css/dragSort.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    @inject('contentService', 'App\Services\ContentProvider')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>{{ $pageSlug }} Contents</h3>
                            @include('admin.partials.alert-box')
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/sections/update') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                <input type="hidden" value="{{ $pageSlug }}" name="page">
                <?php $tags = []; ?>

                @foreach($data as $item)

                    <div class="row">
                        <div class="col-md-12 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">

                    @if($item['type']=='text')
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputEmail3">{{ isset($item['title']) ? $item['title'] : $item['slug'] }} </label>
                                <input type="text" class="form-control" name="pageData[{{ $item['slug'] }}][content]"  value="{{ $item['content'] }}">
                            </div>
                        </div>
                    @elseif($item['type']=='image')
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputEmail3">{{ isset($item['name']) ? $item['name'] : $item['slug'] }}  (Max size: 2mb)</label><br/>
                                <img src="{{ asset('public/'.$item['content']) }}" width="200"><br/><br/>
                                <input type="file" accept="image/x-png,image/gif,image/jpeg" name="pageData[{{$item['slug']}}][content]"  value="" class="form-control">
                            </div>
                        </div>
                    @elseif($item['type']=='textarea')
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputEmail3">{{ isset($item['name']) ? $item['name'] : $item['slug'] }} </label><br/>
                                <textarea rows="10" name="pageData[{{$item['slug']}}][content]" class="form-control">{{ $item['content'] }}</textarea>
                            </div>
                        </div>
                    @elseif($item['type']=='video')
                        <div class="col-md-12">
                            <video width="320" height="240" controls>
                                <source src="{{ asset(''.$item['content']) }}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                            <br/>
                            <br/>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputEmail3">{{ isset($item['title']) ? $item['title'] : $item['slug'] }} (Max size: 20mb)</label><br/>
                                <input type="file" accept="video/mp4" name="pageData[{{$item['slug']}}][content]"  value="" class="form-control">
                            </div>
                        </div>
                    @elseif($item['type']=='external_video')
                        <div class="col-md-12">
                            <iframe height="480" width="500"
                                    src="{{ $item['content'] }}">
                            </iframe>
                            <br/>
                            <br/>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputEmail3">{{ isset($item['title']) ? $item['title'] : $item['slug'] }}</label><br/>
                                <input type="text" name="pageData[{{$item['slug']}}][content]"  value="{{$item['content']}}" class="form-control">
                            </div>
                        </div>
                    @elseif($item['type']=='tags')
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputEmail3">{{ isset($item['title']) ? $item['title'] : $item['slug'] }}</label>
                                <input id="articlesin" type="text" class="form-control" name="pageData[{{$item['slug']}}][content]" value="{{$item['content']}}"/>
                            </div>
                        </div>
                    @elseif($item['type']=='promobox')
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputEmail3">Promobox</label>
                                <?php $promoboxes = $contentService->getPromoboxes(); ?>
                                <select name="pageData[{{$item['slug']}}][content]" class="form-control">
                                    <option value="">None</option>
                                    @foreach($promoboxes as $promo)
                                        <option {{ $promo->id == $item['content'] ? 'selected' : '' }} value="{{ $promo->id }}">{{$promo->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @else
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputEmail3">{{ isset($item['title']) ? $item['title'] : $item['slug'] }}</label>
                                <input type="hidden" name="pageData[{{$item['slug']}}][content]"  value="{{$item['content']}}">
                                <div class="summernote">
                                    {!! $item['content'] !!}
                                </div>
                            </div>
                        </div>
                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    </form>
    </div>
    </div>

@endsection

@section('js')
    @if($pageSlug=='home')
        <script src="https://unpkg.com/@yaireo/tagify"></script>
        <script src="https://unpkg.com/@yaireo/tagify/dist/tagify.polyfills.min.js"></script>
        <script src="https://unpkg.com/@yaireo/tagify"></script>
        <script src="https://unpkg.com/@yaireo/tagify/dist/tagify.polyfills.min.js"></script>
        <script src="{{ asset('admin/js/dragSort.js') }}"></script>
        <script>
            var inputElm = document.getElementById('articlesin');

            <?php
                $articles = $contentService->getArticles();
            ?>

            tagify = new Tagify (inputElm,
                {
                    whitelist:
                        [
                            @foreach($articles as $item){ id:'{{ $item->id }}', value:"{!! str_replace('"', '', $item->title) !!}" },@endforeach
                        ]
                }
            );

            // bind "DragSort" to Tagify's main element and tell
            // it that all the items with the below "selector" are "draggable"
            var dragsort = new DragSort(tagify.DOM.scope, {
                selector: '.'+tagify.settings.classNames.tag,
                callbacks: {
                    dragEnd: onDragEnd
                }
            })

            // must update Tagify's value according to the re-ordered nodes in the DOM
            function onDragEnd(elm){
                tagify.updateValueByDOMTags();
            }

            inputElm.addEventListener('change', onChange);

            function onChange(e){
                // outputs a String
                console.log(e.target.value)
            }
        </script>
    @endif
@endsection
