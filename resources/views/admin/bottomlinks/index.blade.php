@extends('admin.partials.master')

@section('css')
    <link href="https://unpkg.com/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Edit Footer Links</h3>
                            <br/>
                            <br/>
                            <form action="{{ url('admin/bottomlinks/update') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Link 1 Title</label>
                                            <input type="text" class="form-control" placeholder="" name="link[0][title]" value="{{ isset($data[0]) ? $data[0]->title : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Link 1 URL</label>
                                            <input type="text" class="form-control" placeholder="" name="link[0][link]" value="{{ isset($data[0]) ? $data[0]->link : '' }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Link 2 Title</label>
                                            <input type="text" class="form-control" placeholder="" name="link[1][title]" value="{{ isset($data[1]) ? $data[1]->title : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Link 2 URL</label>
                                            <input type="text" class="form-control" placeholder="" name="link[1][link]" value="{{ isset($data[1]) ? $data[1]->link : '' }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Link 3 Title</label>
                                            <input type="text" class="form-control" placeholder="" name="link[2][title]" value="{{ isset($data[2]) ? $data[2]->title : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Link 3 URL</label>
                                            <input type="text" class="form-control" placeholder="" name="link[2][link]" value="{{ isset($data[2]) ? $data[2]->link : '' }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Link 4 Title</label>
                                            <input type="text" class="form-control" placeholder="" name="link[3][title]" value="{{ isset($data[3]) ? $data[3]->title : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Link 4 URL</label>
                                            <input type="text" class="form-control" placeholder="" name="link[3][link]" value="{{ isset($data[3]) ? $data[3]->link : '' }}">
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <hr/>
                                <div class="form-group">
                                    <input type="submit" class="form-control btn-success" value="Save">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
@endsection
