@extends('admin.partials.master')

@section('css')
    <link href="https://unpkg.com/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Edit Location</h3>
                            <br/>
                            @include('admin.partials.alert-box')
                            <br/>
                            <form action="{{ url('admin/locations/update') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$data->id}}">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Name</label>
                                            <input type="text" name="name" class="form-control" id="exampleInputName1" placeholder="" value="{{$data->name}}" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Content</label>
                                            <input type="hidden" name="content" value="{{$data->content}}"/>
                                            <div class="summernote">
                                                {!! $data->content !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Google Map Embed Link:</label>

                                            @if($data->map)
                                                <div class="mapouter">
                                                    <div class="gmap_canvas">
                                                        <iframe src="{{ $data->map }}" width="300" height="200" style="border:0;float:left;" allowfullscreen="" loading="lazy"></iframe>

                                                        <style>.mapouter{position:relative;text-align:right;height:200px;width:100%;}</style>
                                                        <style>.gmap_canvas {overflow:hidden;background:none!important;height:200px;width:100%;}</style>
                                                    </div>
                                                </div> <br>
                                            @endif

                                            <input type="text" name="map" class="form-control" id="exampleInputName1" placeholder="" value="{{$data->map}}" required>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="form-group">
                                    <input type="submit" class="form-control btn-success" value="Save">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
@endsection
