@extends('admin.partials.master')

@section('css')
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Edit Core Value</h3>
                            <br/>
                            @include('admin.partials.alert-box')
                            <br/>
                            <form action="{{ url('admin/careers/values/update') }}" method="post" enctype="multipart/form-data">
                                @csrf

                                <input type="hidden" name="id" value="{{ $data->id }}">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Description</label>
                                            <textarea name="content" class="form-control">{{ $data->content }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <hr/>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail3">Image (550x550)</label><br/>
                                            @if($data->image)
                                                <br/>
                                                <img src="{{ asset('/'.$data->image) }}" width="300">
                                                <br/>
                                                <br/>
                                            @endif
                                            <input type="file" accept="image/x-png,image/gif,image/jpeg" name="image"  value="" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="form-group">
                                    <input type="submit" class="form-control btn-success" value="Save">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
@endsection
