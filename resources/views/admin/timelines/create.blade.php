@extends('admin.partials.master')

@section('css')
    <link href="https://unpkg.com/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Create Timeline</h3>
                            <br/>
                            <br/>
                            <form action="{{ url('admin/about/timelines/store') }}" method="post" enctype="multipart/form-data">
                                @csrf


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Description</label>
                                            <textarea name="content" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <hr/>

                                <div class="form-group">
                                    <label>Date</label>
                                    <?php
                                    $date = \Carbon\Carbon::now()->format('m/d/y');

                                    //                                    if(isset($page))
                                    //                                        if($page->publish_date)
                                    //                                            $date = $page->publish_date->format('m/d/y');
                                    ?>
                                    <input type="text" class="form-control datetimepicker" readonly placeholder="" name="date" value="{{ $date }}">
                                </div>

                                <hr/>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail3">Image (550x450)</label><br/>
                                            <input type="file" accept="image/x-png,image/gif,image/jpeg" name="image"  value="" class="form-control" required>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="form-group">
                                    <input type="submit" class="form-control btn-success" value="Save">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
@endsection
