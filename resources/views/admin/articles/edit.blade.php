@extends('admin.partials.master')

@section('css')
    <link href="https://unpkg.com/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
    <style>
        #experiences div, #educations div {
            position: relative;
        }
        #experiences button, #educations button {
            position: absolute;
            right: 0;
            top: 0;
        }
    </style>
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Edit Article</h3>
                            <a href="{{ url('admin/articles/versions/'.$data->id) }}">See versions</a>
                            <br/>
                            @include('admin.partials.alert-box')
                            <br/>
                            @if($data->hidden)
                                <div class="alert-warning">
                                    This content is currently set as "draft". Click <a target="_blank" href="{{ url('knowledge-hub/'.$data->categories->slug.'/'.$data->slug.'/preview') }}">here</a> to preview.
                                </div>
                                <br/>
                            @endif
                            <form action="{{ url('admin/articles/update') }}" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="{{$data->id}}">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Title</label>
                                            <input type="text" name="title" class="form-control" id="exampleInputName1" placeholder="" value="{{$data->title}}" required>
                                        </div>
                                    </div>
                                </div>

                                <hr/>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">URL: (https://bsabh.com/knowledge-hub/{{$data->categories->slug}}/<b>{{$data->slug}}</b>)</label>
                                            <input type="text" name="slug" class="form-control" id="exampleInputName1" placeholder="" value="{{$data->slug}}" required>
                                        </div>
                                    </div>
                                </div>

                                <hr/>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Introduction (First paragraph)</label>
                                            <textarea name="excerpt" class="form-control">{{$data->excerpt}}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <hr/>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Content</label>
                                            <input type="hidden" name="content" value="{{$data->content}}">
                                            <div class="summernote">
                                                {!! $data->content !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Category</label>
                                            <select name="category" class="form-control">
                                                @foreach($categories as $item)
                                                    <option {{ $item->cat_id == $data->categories->cat_id ? 'selected' : '' }} value="{{ $item->cat_id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr/>

                                <div class="form-group">
                                    <label>Publish date</label>
                                    <?php
                                    $date = \Carbon\Carbon::now()->format('m/d/y');

                                    if($data->date)
                                        $date = \Carbon\Carbon::parse($data->date)->format('m/d/y');
                                    ?>
                                    <input type="text" class="form-control datetimepicker" readonly placeholder="" name="date" value="{{ $date }}">
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            @if($data->photo)
                                                <img src="{{ asset($data->photo) }}" width="600px">
                                                <br/>
                                                <br/>
                                            @endif
                                            <label for="exampleInputEmail3">Thumbnail (300x200)</label><br/>
                                            <input type="file" accept="image/x-png,image/gif,image/jpeg" name="photo"  value="" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <hr/>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            @if($data->photo_full)
                                                <img src="{{ asset($data->photo_full) }}" width="600px">
                                                <br/>
                                                <br/>
                                            @endif
                                            <label for="exampleInputEmail3">Banner Image (1300x350)</label><br/>
                                            <input type="file" accept="image/x-png,image/gif,image/jpeg" name="photo_full"  value="" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Services</label>
                                            <input id="servicesIn" type="text" class="form-control" name="services" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Contacts/Authors</label>
                                            <input id="contactsIn" type="text" class="form-control" name="contacts" value=""/>
                                        </div>
                                    </div>
                                </div>
{{--                                <hr/>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-12">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label for="exampleInputName1">Location</label>--}}
{{--                                            <select name="location" class="form-control">--}}
{{--                                                <option value="">None</option>--}}
{{--                                                @if($data->location)--}}
{{--                                                    @foreach($locations as $item)--}}
{{--                                                        <option {{ $item->l_id == $data->location->location_id ? 'selected' : ''}} value="{{ $item->l_id }}">{{ $item->name }}</option>--}}
{{--                                                    @endforeach--}}
{{--                                                @else--}}
{{--                                                    @foreach($locations as $item)--}}
{{--                                                        <option value="{{ $item->l_id }}">{{ $item->name }}</option>--}}
{{--                                                    @endforeach--}}
{{--                                                @endif--}}
{{--                                            </select>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Tags (Auto fetch articles by tag)</label>
                                            <input id="tagsin" type="text" class="form-control" name="tags" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Related Articles (Manual article selection)</label>
                                            <input id="articlesin" type="text" class="form-control" name="articles" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Promo Boxes</label>
                                            <input id="promosin" type="text" class="form-control" name="promos" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Status</label>
                                            <select name="hidden" class="form-control">
                                                <option {{ $data->hidden == 0 ? 'selected' : '' }} value="0">Published</option>
                                                <option {{ $data->hidden == 1 ? 'selected' : '' }} value="1">Draft</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <h3>SEO Meta Tags</h3>
                                            <label for="exampleInputName1">Meta Title</label>
                                            <input type="text" name="meta_title" class="form-control mb-3" value="{{ $data->meta_title }}">
                                            <label for="exampleInputName1">Meta Description</label>
                                            <input type="text" name="meta_description" class="form-control mb-3" value="{{ $data->meta_description }}">
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <input type="submit" class="form-control btn-success" value="Save">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://unpkg.com/@yaireo/tagify"></script>
    <script src="https://unpkg.com/@yaireo/tagify/dist/tagify.polyfills.min.js"></script>
    <script src="{{ asset('admin/js/dragSort.js') }}"></script>
    <script type="text/javascript">

        var inputElm = document.getElementById('promosin'),
            tagify = new Tagify (inputElm,
                {
                    whitelist:
                        [
                                @foreach($promos as $item){ id:'{{ $item->id }}', value:'{!! $item->title !!}' },@endforeach
                        ],
                    enforceWhitelist: true
                }
            );

        inputElm.addEventListener('change', onChange);

        @if($data->promos)
        @if(count($data->promos))
        tagify.addTags(
            [
                @foreach($data->promos as $item)
                { id:'{{ $item->id }}', value:"{!! $item->title !!}" },
                @endforeach
            ]
        );
        @endif
        @endif

        var inputElm = document.getElementById('servicesIn'),
            tagify = new Tagify (inputElm,
                {
                    whitelist:
                        [
                                @foreach($services as $item)
                            { id:'{{ $item->id }}', value:"{!!  $item->title  !!}" },
                            @endforeach
                        ],
                    enforceWhitelist: true
                }
            );

        @if($data->services)
        @if(count($data->services))
        tagify.addTags(
            [
                    @foreach($data->services as $item)
                { id:'{{ $item->id }}', value:"{!! $item->title !!}" },
                @endforeach
            ]
        );
        @endif
        @endif

        inputElm.addEventListener('change', onChange);

        var inputElm = document.getElementById('contactsIn'),
        tagify1 = new Tagify (inputElm,
            {
                whitelist:
                [
                    @foreach($lawyers as $item)
                        { id:'{{ $item->id }}', value:"{{ $item->name }}" },
                    @endforeach
                ],
                enforceWhitelist: true
            }
        );

        // bind "DragSort" to Tagify's main element and tell
        // it that all the items with the below "selector" are "draggable"
        var dragsort1 = new DragSort(tagify1.DOM.scope, {
            selector: '.'+tagify1.settings.classNames.tag,
            callbacks: {
                dragEnd: onDragEnd1
            }
        })

        // must update Tagify's value according to the re-ordered nodes in the DOM
        function onDragEnd1(elm){
            tagify1.updateValueByDOMTags();
        }

        @if($data->lawyer)
            @if(count($data->lawyer))
            tagify1.addTags(
                [
                    @foreach($data->lawyer as $item)
                        { id:'{{ $item->id }}', value:"{!! $item->name !!}" },
                    @endforeach
                ]
            );
            @endif
        @endif

        inputElm.addEventListener('change', onChange);

        var tags = [
                @foreach($tags as $item)
            { id:'{{ $item->t_id }}', value:"{!! str_replace('"', '', $item->name);  !!}" },
            @endforeach
        ];

        var inputElm = document.getElementById('tagsin'),
            tagify = new Tagify (inputElm,
                {
                    whitelist: tags,
                    enforceWhitelist: true
                }
            );

        @if($data->tags)
            @if(count($data->tags))
            tagify.addTags(
                [
                    @foreach($data->tags as $item)
                        { id:'{{ $item->id }}', value:"{!! $item->name !!}" },
                    @endforeach
                ]
            );
            @endif
        @endif

        inputElm.addEventListener('change', onChange);

        var inputElm2 = document.getElementById('articlesin'),
            tagify2 = new Tagify (inputElm2,
                {
                    whitelist:
                        [
                            @foreach($articles as $item)
                            { id:'{{ $item->id }}', value:"{!!  str_replace("'", '',str_replace('"', '', $item->title));  !!}" },
                            @endforeach
                        ],
                    enforceWhitelist: true
                }
            );

        @if($data->articles)
        @if(count($data->articles))
        tagify2.addTags(
            [
                @foreach($data->articles as $item)
                { id:'{{ $item->id }}', value:"{!!  str_replace("'", '',str_replace('"', '', $item->title));  !!}" },
                @endforeach
            ]
        );
        @endif
        @endif

        // bind "DragSort" to Tagify's main element and tell
        // it that all the items with the below "selector" are "draggable"
        var dragsort2 = new DragSort(tagify2.DOM.scope, {
            selector: '.'+tagify2.settings.classNames.tag,
            callbacks: {
                dragEnd: onDragEnd2
            }
        })

        // must update Tagify's value according to the re-ordered nodes in the DOM
        function onDragEnd2(elm){
            tagify2.updateValueByDOMTags();
        }
        inputElm2.addEventListener('change', onChange);

        function onChange(e){
            // outputs a String
            console.log(e.target.value)
        }


    </script>
@endsection
