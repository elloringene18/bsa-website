@extends('admin.partials.master')

@section('css')
    <link href="https://unpkg.com/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
    <style>
        #experiences div, #educations div {
            position: relative;
        }
        #experiences button, #educations button {
            position: absolute;
            right: 0;
            top: 0;
        }
    </style>
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Add Article</h3>
                            <br/>
                            <br/>
                            <form action="{{ url('admin/articles/store') }}" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Title</label>
                                            <input type="text" name="title" class="form-control" id="exampleInputName1" placeholder="" value="" required>
                                        </div>
                                    </div>
                                </div>

                                <hr/>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Introduction (First paragraph)</label>
                                            <textarea name="excerpt" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <hr/>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Content</label>
                                            <input type="hidden" name="content" value="">
                                            <div class="summernote">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Category</label>
                                            <select name="category" class="form-control">
                                                @foreach($categories as $item)
                                                    <option value="{{ $item->cat_id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr/>

                                <div class="form-group">
                                    <label>Publish date</label>
                                    <?php
                                    $date = \Carbon\Carbon::now()->format('m/d/y');

                                    if(isset($page))
                                        if($page->publish_date)
                                            $date = $page->publish_date->format('m/d/y');
                                    ?>
                                    <input type="text" class="form-control datetimepicker" readonly placeholder="" name="date" value="{{ $date }}">
                                </div>

                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail3">Thumbail (300x200)</label><br/>
                                            <input type="file" accept="image/x-png,image/gif,image/jpeg" name="photo"  value="" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <hr/>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail3">Banner Image (1300x350)</label><br/>
                                            <input type="file" accept="image/x-png,image/gif,image/jpeg" name="photo_full"  value="" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Services</label>
                                            <input id="servicesIn" type="text" class="form-control" name="services" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Contacts/Authors</label>
                                            <input id="contactsIn" type="text" class="form-control" name="contacts" value=""/>
                                        </div>
                                    </div>
                                </div>
{{--                                <hr/>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-12">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label for="exampleInputName1">Location</label>--}}
{{--                                            <select name="location" class="form-control">--}}
{{--                                                @foreach($locations as $item)--}}
{{--                                                    <option value="{{ $item->l_id }}">{{ $item->name }}</option>--}}
{{--                                                @endforeach--}}
{{--                                            </select>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Tags (Auto fetch articles by tag)</label>
                                            <input id="tagsin" type="text" class="form-control" name="tags" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Related Articles (Manual article selection)</label>
                                            <input id="articlesin" type="text" class="form-control" name="articles" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Promo Boxes</label>
                                            <input id="promosin" type="text" class="form-control" name="promos" value=""/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName1">Status</label>
                                            <select name="hidden" class="form-control">
                                                <option value="0">Published</option>
                                                <option value="1">Draft</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <h3>SEO Meta Tags</h3>
                                            <label for="exampleInputName1">Meta Title</label>
                                            <input type="text" name="meta_title" class="form-control mb-3">
                                            <label for="exampleInputName1">Meta Description</label>
                                            <input type="text" name="meta_description" class="form-control mb-3">
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <input type="submit" class="form-control btn-success" value="Save">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://unpkg.com/@yaireo/tagify"></script>
    <script src="https://unpkg.com/@yaireo/tagify/dist/tagify.polyfills.min.js"></script>
    <script type="text/javascript">

        var inputElm = document.getElementById('contactsIn'),
            tagify = new Tagify (inputElm,
                {
                    whitelist:
                        [
                            @foreach($lawyers as $item){ id:'{{ $item->id }}', value:'{!! $item->name !!}' },@endforeach
                        ],
                    enforceWhitelist: true
                }
            );

        inputElm.addEventListener('change', onChange);

        var inputElm = document.getElementById('promosin'),
            tagify = new Tagify (inputElm,
                {
                    whitelist:
                        [
                            @foreach($promos as $item){ id:'{{ $item->id }}', value:'{!! $item->title !!}' },@endforeach
                        ],
                    enforceWhitelist: true
                }
            );

        inputElm.addEventListener('change', onChange);

        var inputElm = document.getElementById('servicesIn'),
            tagify = new Tagify (inputElm,
                {
                    whitelist:
                        [
                                @foreach($services as $item)
                            { id:'{{ $item->id }}', value:'{!!  $item->title  !!}' },
                            @endforeach
                        ],
                    enforceWhitelist: true
                }
            );

        inputElm.addEventListener('change', onChange);

        var tags = [
                @foreach($tags as $item)
            { id:'{{ $item->t_id }}', value:"{!! str_replace('"', '', $item->name);  !!}" },
            @endforeach
        ];

        var inputElm = document.getElementById('tagsin'),
            tagify = new Tagify (inputElm,
                {
                    whitelist: tags,
                    enforceWhitelist: true
                }
            );

        inputElm.addEventListener('change', onChange);

        var inputElm = document.getElementById('articlesin'),
            tagify = new Tagify (inputElm,
                {
                    whitelist:
                        [
                            @foreach($articles as $item)
                                { id:'{{ $item->id }}', value:"{!!  str_replace("'", '',str_replace('"', '', $item->title));  !!}" },
                            @endforeach
                        ],
                    enforceWhitelist: true
                }
            );

        inputElm.addEventListener('change', onChange);

        function onChange(e){
            // outputs a String
            console.log(e.target.value)
        }

    </script>
@endsection
