
@extends('admin.partials.master')

@section('css')
    <link href="https://unpkg.com/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
    <style>
        #experiences div, #educations div {
            position: relative;
        }
        #experiences button, #educations button {
            position: absolute;
            right: 0;
            top: 0;
        }
        label {
            font-weight: bold;
        }
        p {
            margin-bottom: 5px;
        }
        button.form-control {
            background-color: green;
            color: #fff;
            font-size: 16px;
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Version {{$current->version}} (Current)</h3>
                            <br/>
                            <br/>
                            <label for="exampleInputName1" class="{{ $previous->title != $current->title ? 'red' : '' }}">Title</label><br/>
                            <p>{{ $current->title }}</p>
                            <br/><br/>
                            <label for="exampleInputName1" class="{{ $previous->slug != $current->slug ? 'red' : '' }}">URL/SLUG</label><br/>
                            <p>https://bsabh.com/knowledge-hub/news/<b>{{$current->slug}}</b></p>
                            <br/><br/>
                            <label for="exampleInputName1" class="{{ $previous->excerpt != $current->excerpt ? 'red' : '' }}">Introduction (First paragraph)</label><br/>
                            <p>{{ $current->excerpt }}</p>
                            <br/><br/>
                            <label for="exampleInputName1" class="{{ $previous->content != $current->content ? 'red' : '' }}">Content</label><br/>
                            {!! $current->content !!}
                            <br/><br/>
                            <label for="exampleInputName1" class="{{ $previous->categories != $current->categories ? 'red' : '' }}">Category</label><br/>
                            <?php
                            if($current->categories){
                                $items = json_decode($current->categories);
                                foreach ($items as $item){
                                    $found = \App\Models\Category::where('cat_id',$item)->first();
                                    if($found)
                                        echo '<p>- '.$found->name.'</p>';
                                }
                            }
                            ?>
                            <br/><br/>

                            <label>Publish date</label><br/>
                            <?php
                            $date = \Carbon\Carbon::now()->format('m/d/y');

                            if(isset($page))
                                if($current->publish_date)
                                    $date = $current->publish_date->format('m/d/y');
                            ?>
                            <p>{{ $date }}</p>
                            <br/><br/>

                            <label for="exampleInputEmail3" class="{{ $previous->photo != $current->photo ? 'red' : '' }}">Image (1300x350)</label><br/>
                            @if($previous->photo)
                                <img src="{{ asset($current->photo) }}" width="600px">
                                <img src="{{ asset($current->photo_full) }}" width="600px">
                                <br/>
                            @endif
                            <p>{{ asset('public/'.$current->photo) }}</p>
                            <br/><br/>

                            <label for="exampleInputName1" class="{{ $previous->services != $current->services ? 'red' : '' }}">Services</label><br/>
                            <?php
                            if($current->services){
                                $items = json_decode($current->services);
                                foreach ($items as $item){
                                    $found = \App\Models\Service::find($item);
                                    if($found)
                                        echo '<p>- '.$found->title.'</p>';
                                }
                            }
                            ?>
                            <br/><br/>

                            <label for="exampleInputName1" class="{{ $previous->lawyers != $current->lawyers ? 'red' : '' }}">Contacts/Authors</label><br/>
                            <?php
                            if($current->lawyers){
                                $items = json_decode($current->lawyers);
                                foreach ($items as $item){
                                    $found = \App\Models\Lawyer::find($item);
                                    if($found)
                                        echo '<p>- '.$found->name.'</p>';
                                }
                            }
                            ?>
                            <br/><br/>

                            <label for="exampleInputName1" class="{{ $previous->tags != $current->tags ? 'red' : '' }}">Tags (Auto fetch articles by tag)</label><br/>
                            <?php
                                if($current->tags){
                                    $items = json_decode($current->tags);
                                    foreach ($items as $item){
                                        $found = \App\Models\Tag::where('t_id',$item)->first();
                                        if($found)
                                            echo '<p>- '.$found->name.'</p>';
                                    }
                                }
                            ?>
                            <br/><br/>

                            <label for="exampleInputName1" class="{{ $previous->related != $current->related ? 'red' : '' }}">Related Articles (Manual article selection)</label><br/>
                            <?php
                            if($current->related){
                                $items = json_decode($current->related);
                                foreach ($items as $item){
                                    $found = \App\Models\Article::find($item);
                                    if($found)
                                        echo '<p>- '.$found->title.'</p>';
                                }
                            }
                            ?>
                            <br/><br/>

                            <label for="exampleInputName1" class="{{ $previous->promos != $current->promos ? 'red' : '' }}">Promo Boxes</label><br/>
                            <?php
                            if($current->promos){
                                $items = json_decode($current->promos);
                                foreach ($items as $item){
                                    $found = \App\Models\Promo::find($item);
                                    if($found)
                                        echo '<p>- '.$found->title.'</p>';
                                }
                            }
                            ?>
                            <br/><br/>

                            <label for="exampleInputName1" class="{{ $previous->hidden != $current->hidden ? 'red' : '' }}">Status</label><br/>
                            <p>{{ $current->hidden == 1 ? 'hidden' : 'active' }}</p>
                            <br/><br/>


                            <label for="exampleInputName1" class="{{ $previous->meta_title != $current->meta_title ? 'red' : '' }}">Meta Title</label><br/>
                            <p>{{ $current->meta_title }}</p>
                            <br/><br/>

                            <label for="exampleInputName1" class="{{ $previous->meta_description != $current->meta_description ? 'red' : '' }}">Meta Description</label><br/>
                            <p>{{ $current->meta_description }}</p>
                            <br/><br/>

                            <label for="exampleInputName1" class="{{ $previous->meta_keywords != $current->meta_keywords ? 'red' : '' }}">Meta keywords</label><br/>
                            <p>{{ $current->meta_keywords }}</p>
                            <br/><br/>

                        </div>
                    </div>
                </div>

                <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Version {{$previous->version}}</h3>
                            <br/>
                            <br/>
                            <label for="exampleInputName1" class="{{ $previous->title != $current->title ? 'red' : '' }}">Title</label><br/>
                            <p>{{ $previous->title }}</p>
                            <br/><br/>
                            <label for="exampleInputName1" class="{{ $previous->slug != $current->slug ? 'red' : '' }}">URL/SLUG</label><br/>
                            <p>https://bsabh.com/knowledge-hub/news/<b>{{$previous->slug}}</b></p>
                            <br/><br/>
                            <label for="exampleInputName1" class="{{ $previous->excerpt != $current->excerpt ? 'red' : '' }}">Introduction (First paragraph)</label><br/>
                            <p>{{ $previous->excerpt }}</p>
                            <br/><br/>
                            <label for="exampleInputName1" class="{{ $previous->content != $current->content ? 'red' : '' }}">Content</label><br/>
                            {!! $previous->content !!}
                            <br/><br/>
                            <label for="exampleInputName1" class="{{ $previous->categories != $current->categories ? 'red' : '' }}">Category</label><br/>
                            <?php
                            if($previous->categories){
                                $items = json_decode($previous->categories);
                                foreach ($items as $item){
                                    $found = \App\Models\Category::where('cat_id',$item)->first();
                                    if($found)
                                        echo '<p>- '.$found->name.'</p>';
                                }
                            }
                            ?>
                            <br/><br/>

                            <label  class="{{ $previous->publish_date != $current->publish_date ? 'red' : '' }}">Publish date</label><br/>
                            <?php
                            $date = \Carbon\Carbon::now()->format('m/d/y');

                            if(isset($previous))
                                if($previous->publish_date)
                                    $date = $previous->publish_date->format('m/d/y');
                            ?>
                            <p>{{ $date }}</p>
                            <br/><br/>

                            <label for="exampleInputEmail3" class="{{ $previous->photo != $current->photo ? 'red' : '' }}">Image (1300x350)</label><br/>
                            @if($previous->photo)
                                <img src="{{ asset($previous->photo) }}" width="600px">
                                <img src="{{ asset($previous->photo_full) }}" width="600px">
                                <br/>
                            @endif
                            <p>{{ asset('public/'.$previous->photo) }}</p>
                            <br/><br/>

                            <label for="exampleInputName1" class="{{ $previous->services != $current->services ? 'red' : '' }}">Services</label><br/>
                            <?php
                            if($previous->services){
                                $items = json_decode($previous->services);
                                foreach ($items as $item){
                                    $found = \App\Models\Service::find($item);
                                    if($found)
                                        echo '<p>- '.$found->title.'</p>';
                                }
                            }
                            ?>
                            <br/><br/>

                            <label for="exampleInputName1" class="{{ $previous->lawyers != $current->lawyers ? 'red' : '' }}">Contacts/Authors</label><br/>
                            <?php
                            if($previous->lawyers){
                                $items = json_decode($previous->lawyers);
                                foreach ($items as $item){
                                    $found = \App\Models\Lawyer::find($item);
                                    if($found)
                                        echo '<p>- '.$found->name.'</p>';
                                }
                            }
                            ?>
                            <br/><br/>

                            <label for="exampleInputName1" class="{{ $previous->tags != $current->tags ? 'red' : '' }}">Tags (Auto fetch articles by tag)</label><br/>
                            <?php
                            if($previous->tags){
                                $items = json_decode($previous->tags);
                                foreach ($items as $item){
                                    $found = \App\Models\Tag::where('t_id',$item)->first();
                                    if($found)
                                        echo '<p>- '.$found->name.'</p>';
                                }
                            }
                            ?>
                            <br/><br/>

                            <label for="exampleInputName1" class="{{ $previous->related != $current->related ? 'red' : '' }}">Related Articles (Manual article selection)</label><br/>
                            <?php
                            if($previous->related){
                                $items = json_decode($previous->related);
                                foreach ($items as $item){
                                    $found = \App\Models\Article::find($item);
                                    if($found)
                                        echo '<p>- '.$found->title.'</p>';
                                }
                            }
                            ?>
                            <br/><br/>

                            <label for="exampleInputName1" class="{{ $previous->promos != $current->promos ? 'red' : '' }}">Promo Boxes</label><br/>
                            <?php
                            if($previous->promos){
                                $items = json_decode($previous->promos);
                                foreach ($items as $item){
                                    $found = \App\Models\Promo::find($item);
                                    if($found)
                                        echo '<p>- '.$found->title.'</p>';
                                }
                            }
                            ?>
                            <br/><br/>

                            <label for="exampleInputName1" class="{{ $previous->hidden != $current->hidden ? 'red' : '' }}">Status</label><br/>
                            <p>{{ $previous->hidden == 1 ? 'hidden' : 'active' }}</p>
                            <br/><br/>

                            <label for="exampleInputName1" class="{{ $previous->meta_title != $current->meta_title ? 'red' : '' }}">Meta Title</label><br/>
                            <p>{{ $previous->meta_title }}</p>
                            <br/><br/>

                            <label for="exampleInputName1" class="{{ $previous->meta_description != $current->meta_description ? 'red' : '' }}">Meta Description</label><br/>
                            <p>{{ $previous->meta_description }}</p>
                            <br/><br/>

                            <label for="exampleInputName1" class="{{ $previous->meta_keywords != $current->meta_keywords ? 'red' : '' }}">Meta keywords</label><br/>
                            <p>{{ $previous->meta_keywords }}</p>
                            <br/><br/>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            Note: All contents deleted from the website like tags, categories, lawyers, services, etc., will not be restored.
                            <br/>
                            <br/>
                            <a href="{{ url('admin/articles/versions/'.$previous->id.'/restore') }}"><button class="form-control">Restore this version</button> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
@endsection
