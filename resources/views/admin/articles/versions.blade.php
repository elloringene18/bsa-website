@extends('admin.partials.master')

@section('content')
    <!-- partial -->

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <br/>
                @include('admin.partials.alert-box')
                <br/>
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <h3>{{ $post->title }}</h3>
                                </div>
                                <div class="col-md-12">
                                    &nbsp;
                                </div>
                            </div>

                            <table class="table table-striped" width="100%">
                                <tr>
                                    <td>Title</td>
                                    <td>Actions</td>
                                    <td>Date Created</td>
                                </tr>
                                @foreach($versions as $version)
                                    <tr>
                                        <td>Version {{$version->version}}</td>
                                        <td>
                                            @if($version->is_current)
                                                <b>(CURRENT)</b>
                                            @else
                                                <a href="{{ URL('admin/articles/versions/'.$post->id.'/compare/'.$version->id) }}">Compare</a>
                                            @endif
                                        </td>
                                        <td>{{ $version->created_at }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
