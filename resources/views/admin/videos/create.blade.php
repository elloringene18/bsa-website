@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Add a Video</h3>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/videos/store') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputNamea1">Title</label>
                                    <input required type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="title" value="{{ isset($page) ? $page->title : '' }}">
                                </div>
{{--                                <div class="form-group">--}}
{{--                                    <label>Speaker</label>--}}
{{--                                    <input type="text" class="form-control" placeholder="Speaker" name="speaker" value="{{ isset($page) ? $page->speaker : '' }}">--}}
{{--                                </div>--}}

                                <div class="form-group" style="">
                                    <div class="form-group">
                                        <label>Excerpt:</label>
                                        <input type="text" class="form-control" placeholder="Excerpt (Max 10 words)" name="excerpt" value="{{ isset($page) ? $page->excerpt : '' }}">
                                    </div>
                                </div>

{{--                                <div class="form-group">--}}
{{--                                    <label for="exampleInputEmail3">Image (300x300)</label><br/>--}}
{{--                                    <input type="file" accept="image/x-png,image/gif,image/jpeg" name="image"  value="" class="form-control" required>--}}
{{--                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Youtue Video URL (Example:https://www.youtube.com/watch?v=KLT4dLJMYBc&t=130s):</label>
                                    <input type="text" class="form-control" name="video_url" value="https://www.youtube.com/watch?v=KLT4dLJMYBc&t=130s" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Publish date</label>
                                    <?php
                                    $date = \Carbon\Carbon::now()->format('m/d/y');

                                    if(isset($page))
                                        if($page->publish_date)
                                            $date = $page->publish_date->format('m/d/y');
                                    ?>
                                    <input type="text" class="form-control datetimepicker" readonly placeholder="" name="date" value="{{ $date }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $('#add-file').on('click',function(){
            index = $('#file-uploads .file-upload').length + 1;

            el = '\n' +
                '            <div class="file-upload card-box">\n' +
                '                <div class="row">\n' +
                '                <button class="remove-box" type="button">X</button>\n' +
                '                    <div class="col-md-12">\n' +
                '                        <div class="form-group">\n' +
                '                            <label>Additional Image (1000x1000):</label>\n' +
                '                            <input type="file" class="form-control" name="images['+index+'][square]" placeholder="Upload Image">\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '                <div class="row">\n' +
                '                    <div class="col-md-6">\n' +
                '                        <div class="form-group">\n' +
                '                            <label>Image Caption EN:</label>\n' +
                '                            <input type="text" class="form-control" placeholder="Image Caption EN" name="captions['+index+'][EN]">\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    <div class="col-md-6">\n' +
                '                        <div class="form-group">\n' +
                '                            <label>Image Caption AR:</label>\n' +
                '                            <input type="text" class="form-control" placeholder="Image Caption AR" name="captions['+index+'][AR]">\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>';

            $('#file-uploads').append(el);


            removeBoxBtEvent();
        });

        function removeBoxBtEvent(){
            $('.remove-box').on('click',function(){
                $(this).closest('.file-upload').remove();
            });
        }

        $(document).ready(function(){
            removeBoxBtEvent();
        });

    </script>
@endsection
