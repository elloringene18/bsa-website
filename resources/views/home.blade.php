@extends('partials.master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $pageContent = $contentService->getPageSections('home'); ?>

@if($pageContent['meta-title'])
    @section('page-title'){{ $pageContent['meta-title'] }} | @endsection
@endif

@if($pageContent['meta-description'])
    @section('page-description'){{ $pageContent['meta-description'] }}@endsection
@endif

@section('css')
<link rel="stylesheet" href="{{ asset('') }}/css/home.css?v=<?php echo rand(1,99999); ?>">
<link rel="stylesheet" href="{{ asset('') }}/css/podcasts.css?v=<?php echo rand(1,99999); ?>">
<script type="text/javascript" src="https://521dimensions.com/img/open-source/amplitudejs/visualizations/michaelbromley.js"></script>
<link rel="stylesheet" href="{{ asset('public/bigpicture/css/main.css') }}" />

<style>

    html, body {
        scroll-behavior: smooth;
    }

    .cropphoto {
        height: 120px;
        overflow: hidden;
        display: block;
    }

    .swing-in-top-fwd{-webkit-animation:swing-in-top-fwd .5s cubic-bezier(.175,.885,.32,1.275) both;animation:swing-in-top-fwd .5s cubic-bezier(.175,.885,.32,1.275) both}
    @-webkit-keyframes swing-in-top-fwd{0%{-webkit-transform:rotateX(-100deg);transform:rotateX(-100deg);-webkit-transform-origin:top;transform-origin:top;opacity:0}100%{-webkit-transform:rotateX(0deg);transform:rotateX(0deg);-webkit-transform-origin:top;transform-origin:top;opacity:1}}@keyframes swing-in-top-fwd{0%{-webkit-transform:rotateX(-100deg);transform:rotateX(-100deg);-webkit-transform-origin:top;transform-origin:top;opacity:0}100%{-webkit-transform:rotateX(0deg);transform:rotateX(0deg);-webkit-transform-origin:top;transform-origin:top;opacity:1}}

    .tracking-in-expand{-webkit-animation:tracking-in-expand .7s cubic-bezier(.215,.61,.355,1.000) both;animation:tracking-in-expand .7s cubic-bezier(.215,.61,.355,1.000) both}
    @-webkit-keyframes tracking-in-expand{0%{letter-spacing:-.5em;opacity:0}40%{opacity:.6}100%{opacity:1}}@keyframes tracking-in-expand{0%{letter-spacing:-.5em;opacity:0}40%{opacity:.6}100%{opacity:1}}

    .fade-in{-webkit-animation:fade-in 1.2s cubic-bezier(.39,.575,.565,1.000) both;animation:fade-in 1.2s cubic-bezier(.39,.575,.565,1.000) both}
    @-webkit-keyframes fade-in{0%{opacity:0}100%{opacity:1}}@keyframes fade-in{0%{opacity:0}100%{opacity:1}}

    #subscribeForm input[type=email] {
        border-bottom: 1px solid #006341;
        color: #000;
    }

    .scale-in-center{-webkit-animation:scale-in-center .5s cubic-bezier(.25,.46,.45,.94) both;animation:scale-in-center .5s cubic-bezier(.25,.46,.45,.94) both}
    @-webkit-keyframes scale-in-center{0%{-webkit-transform:scale(0);transform:scale(0);opacity:1}100%{-webkit-transform:scale(1);transform:scale(1);opacity:1}}@keyframes scale-in-center{0%{-webkit-transform:scale(0);transform:scale(0);opacity:1}100%{-webkit-transform:scale(1);transform:scale(1);opacity:1}}

    .article-thumb {
        margin-bottom: 20px;
    }
</style>
@endsection

@section('content')


<section id="homeBanner">
    <div id="ladderLine"></div>
    <span id="horLine"></span>
    <span id="verLine"></span>
    <div id="bannerLines" class="slide-in-bck-center"></div>
    <div class="container clearfix">
        <div class="copy tracking-in-contract">
            {!!  $pageContent['intro-heading']  !!}
        </div>
    </div>
    <video width="100%" autoplay muted loop id="homevid">
        <source src="{{ asset($pageContent['video']) }}" type="video/mp4">
        Your browser does not support the video tag.
    </video>

</section>

<section id="hoverTexts">
    <div class="container">

        <div class="copy mb-5 mt-5">
            <div class="pops">
                <div class="pop">
                    <img src="{{ asset('img/bubble-arrow.png') }}" class="scale-in-center">
                    <a href="{{ url('locations') }}" class="fade-in">
                        {{-- <span class="title">We are where you are</span><span class="details">a Middle East law firm with international reach</span>--}}
                        {!!  $pageContent['hover-message-1']  !!}
                    </a>
                </div>
                <div class="pop">
                    <img src="{{ asset('img/bubble-arrow.png') }}" class="scale-in-center">
                    <a href="{{ url('services') }}" class="fade-in">
{{--                        <span class="title">We understand your business</span><span class="details">a holistic legal approach to meet your commercial objectives</span>--}}
                        {!!  $pageContent['hover-message-2']  !!}
                    </a>
                </div>
            </div>

            <div class="hovs">
                {!!  $pageContent['intro-content']  !!}
            </div>
            <br/>
{{--            Our curated team of <span class="hovertext hv1">talented advocates and 15 partners </span> not only helps our clients succeed but provides an innate cultural understanding, local insight, and an unmatched contact network.--}}
{{--            <span class="hovertext hv1">BSA </span> is driven, modern, and truly in tune with the market and the world--}}
{{--            from the boardroom to the courtroom.--}}
            <div class="pops">
                <div class="pop">
                    <img src="{{ asset('img/bubble-arrow.png') }}" class="scale-in-center">
                    <a href="{{ url('our-people') }}" class="fade-in">
{{--                        <span class="title">A diverse & inclusive team</span><span class="details">50/50 gender split covering 35 different nationalities who speak 22 languages</span>--}}
                        {!!  $pageContent['hover-message-3']  !!}
                    </a>
                </div>
                <div class="pop">
                    <img src="{{ asset('img/bubble-arrow.png') }}" class="scale-in-center">
                    <a href="{{ url('about-us') }}" class="fade-in">
{{--                        <span class="title">Dedicated to Better</span><span class="details">A unique client service with a commitment to community</span>--}}
                        {!!  $pageContent['hover-message-4']  !!}
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="lastestarticles" class="mb-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <h5 class="section-heading">Highlights
{{--                    <a href="{{ url('knowledge-hub') }}" class="viewAll">SEE ALL</a> --}}
                </h5>
                <div class="bordertop">
                    <div class="row">
                        <div class="col-md-12">

                            <?php
                            // $highlights = $contentService->getHighlightArticles();
                                $highlights = $pageContent['article-highlights'];
                                $highlights = $contentService->getArticlesFromJSON($highlights);
                            ?>

                            @foreach($highlights as $item)
                                <div class="row article">
                                    <div class="col-lg-3 col-md-6">
                                        <a href="article.php" class=""><img src="{{ $item->photo }}" width="100%" class="article-thumb"></a>
                                    </div>
                                    <div class="col-lg-9 col-md-6">
                                        <a href="{{ url('knowledge-hub/news/'.$item->slug) }}"><h3 class="article-title">{{$item->title}}</h3></a>
                                        <p class="article-excerpt">
                                            @if($item->excerpt)
                                                {{ \Illuminate\Support\Str::words(strip_tags($item->excerpt), 20) }}
                                            @else
                                                {{ \Illuminate\Support\Str::words(strip_tags($item->content), 20) }}
                                            @endif
                                        </p>
                                        <a href="{{ url('knowledge-hub/news/'.$item->slug) }}" class="readmore"></a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="highlights" class="mb-5">
    <div class="container">
        <h5 class="section-heading">
            Latest Articles
            <a href="#" class="highlights-next carousel-arrows next"></a>
            <a href="#" class="highlights-prev carousel-arrows prev"></a>
        </h5>

        <div class="highlights-carousel">
            @foreach($news as $item)
                <div>
                    <div class="bordertop article">
                        <span class="article-tag">
                        </span>
                        <a href="{{ url('knowledge-hub/news/'.$item->slug) }}"><h3 class="article-title">{{$item->title}}</h3></a>
                        <p class="article-excerpt">
                            @if($item->excerpt)
                                {{ \Illuminate\Support\Str::words(strip_tags($item->excerpt), 20) }}
                            @else
                                {{ \Illuminate\Support\Str::words(strip_tags($item->content), 20) }}
                            @endif
                        </p>
                        <a href="{{ url('knowledge-hub/news/'.$item->slug) }}" class="readmore"></a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>


    <?php
        $podcasts = $contentService->getPodcasts();
        $videos = $contentService->getVideos();
    ?>

    @if(count($podcasts) > 0 || count($videos) > 0)
        <div class="container mt-3 mb-3">
            <div class="row">

                @if(count($videos) > 0)
                    <div class="col-md-12 mb-5">
                        <h5 class="section-heading">VIDEOS</h5>
                        <div class="bordertop pl-0">
                            @foreach($videos as $video)
                                <div class="podcast clearfix">
                                    <button vidSrc="{{ $video->video_file }}" class="vid htmlvid  control"></button>
                                    <h3 class="podcast-title">{{$video->title}}</h3>
                                    <span class="podcast-time"></span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif

                @if(count($podcasts) > 0)
                    <div class="col-md-12">
                        <h5 class="section-heading">PODCASTS</h5>
                        <div class="bordertop pl-0">
                            @foreach($podcasts as $podcast)
                                <div class="podcast clearfix">
                                    <button data-image="{{$podcast->image}}" data-title="{{$podcast->title}}" data-excerpt="{{$podcast->excerpt}}" data-audio="{{$podcast->audio_file}}" class="podcast control"></button>
                                    <h3 class="podcast-title">{{$podcast->title}}</h3>
                                    <span class="podcast-time"></span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-md-12">
                        <hr class="black"/>
                        <a href="{{ url('knowledge-hub') }}" class="line-bt">GO TO KNOWLEDGE HUB</a>
                    </div>
                </div>
            </div>
        </div>
    @endif
</section>

<section id="where" class="pb-5 mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <span class="huge">
                    We are <br/> where <br/>you are
                    <img src="{{ asset('') }}/img/ellipse-1.png" class="ellipse where">
                </span>
            </div>
            <div class="col-md-6">

                <?php $date = new DateTime(); ?>

                <ul class="timezone clearfix">
                    <li class="active"><div class="place">Dubai</div><div class="time uae"></div> </li>
                    <li><div class="place">Abu Dhabi</div><div class="time uae"></div> </li>
                    <li><div class="place">Ras Al Khaimah</div><div class="time uae"></div> </li>
                    <li><div class="place">Sharjah</div><div class="time uae"></div> </li>
                    <li><div class="place">Erbil</div><div class="time baghdad"></div> </li>
                    <li><div class="place">Beirut</div><div class="time beirut"></div> </li>
                    <li><div class="place">Muscat</div><div class="time oman"></div> </li>
                    <li><div class="place">Riyadh</div><div class="time riyadh"></div> </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row">
{{--            <div class="col-md-5">--}}
{{--                <div class="socials-carousel" id="socials">--}}
{{--                </div>--}}
{{--            </div>--}}
            <div class="col-md-12">
                <span class="mhuge">
                    BSA is social,<br/>follow us<br/>
                    @include('partials.socials')
                    <img src="{{ asset('') }}/img/ellipse-2.png" class="ellipse social">
                </span>
            </div>
        </div>
        <div class="row">
{{--            <div class="col-md-5">--}}
{{--            </div>--}}
            <div class="col-md-12 pt-5 mb-5">
                <form id="subscribeForm" action="#">
                    @csrf
                    <div class="alert alert-success" id="subscribeSuccess"></div>
                    <div class="alert alert-warning" id="subscribeError"></div>

{{--                    <input type="email" placeholder="Enter your e-mail" name="email" class="form-control"/>--}}
                    <div class="row">
                        <div class="col-md-8 pt-3">
                            <h5 class="section-heading">Subscribe to our mailing list</h5>
                        </div>
                        <div class="col-md-4 pt-3">
                            <a href="https://bsa.concep.com/preferences/bsa/signup" target="_blank"> <input type="button" value="SUBSCRIBE"/></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<section id="podcastPlayer">

    <div id="single-song-player">
        <img data-amplitude-song-info="cover_art_url"/>
        <div class="bottom-container">

            <div class="time-container" style="display: none">
                  <span class="current-time">
                    <span class="amplitude-current-minutes"></span>:<span class="amplitude-current-seconds"></span>
                  </span>
                <span class="duration">
                    <span class="amplitude-duration-minutes"></span>:<span class="amplitude-duration-seconds"></span>
                  </span>
            </div>

            <div class="control-container" style="display: none">
                <div class="amplitude-play-pause" id="play-pause"></div>
                <div class="meta-container">
                    <span data-amplitude-song-info="name" class="song-name"></span>
                    <span data-amplitude-song-info="artist"></span>
                </div>
            </div>

            <progress class="amplitude-song-played-progress" id="song-played-progress"></progress>
        </div>
    </div>

{{--    <div class="scrubber">--}}
{{--        <input type="range" min="1" max="100" value="50" class="podcastslider" id="podcastSlider">--}}
{{--    </div>--}}

    <div class="container">
        <img src="{{ asset('') }}/img/player-sample.png" id="podcastThumb">
        <div id="podcastCopy">
            <span id="podcastTitle"></span>
            <span id="podcastDetails"></span>
        </div>
        <a href="#" id="closePlayer"></a>
    </div>
</section>

@endsection()

@section('js')
    <script src="{{ asset('') }}/js/home.js?v=<?php echo rand(1,99999); ?>"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/amplitudejs@v5.0.3/dist/amplitude.js"></script>
    <script src="{{ asset('bigpicture/js/BigPicture.js') }}"></script>
    <script>



        $('.htmlvid').on('click',function(e){
            BigPicture({
                el: e.target,
                ytSrc: e.target.getAttribute('vidSrc')
            });
        });

        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];

        function getTweets(){
            $.ajax({
                type: "GET",
                url: '{{url('/')}}/userTimeline/',
                success: function(response){
                    tData = JSON.parse(response);
                    console.log(tData.includes.media);
                    media = [];
                    $('#socials').empty();

                    Object.keys(tData.includes.media).map(function(objectKey, index) {
                        var value = tData.includes.media[objectKey];

                        if(value.type=='photo')
                            media[value.media_key] = value.url;
                    });

                    Object.keys(tData.data).map(function(objectKey, index) {
                        var value = tData.data[objectKey];

                        dt = new Date(value.created_at);

                        date = (monthNames[dt.getMonth()+1]) + ' ' + dt.getDate()  + ", " + dt.getFullYear();


                        if( typeof media[value.attachments.media_keys[0]] !== 'undefined'){
                            item = '<div>\n' +
                                '    <a target="_blank" href="'+value.entities.urls[0].url+'"><span class="article-tag txt"><i class="fa fa-twitter"></i> '+date+'</span><br/>\n' +
                                '    <img src="'+media[value.attachments.media_keys[0]]+'" width="100%">' +
                                '    <p class="mt-3">'+value.text+'</p></a>\n' +
                                '</div>';
                        } else {
                            item = '<div>\n' +
                                '    <span class="article-tag txt"><i class="fa fa-twitter"></i> '+date+'</span><br/>\n' +
                                '    <a target="_blank" href="'+value.entities.urls[0].url+'"><p class="mt-3">'+value.text+'</p></a>\n' +
                                '</div>';
                        }

                        $('#socials').append(item);
                    });


                    setTimeout(function () {
                        $('#socials').slick({
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            dots: true,
                            draggable: true,
                            arrows: false,
                            adaptiveHeight: true,
                            infinite: false,
                            autoplay: true,
                        });
                    },1000);
                }
            }).done(function(){
            });
        }
        getTweets();

        function getTimes(){
            $.ajax({
                url: "{{url('/get-times')}}",
                context: document.body
            }).done(function(data) {
                $('.time.uae').text(data.uae);
                $('.time.baghdad').text(data.baghdad);
                $('.time.beirut').text(data.beirut);
                $('.time.oman').text(data.oman);
                $('.time.riyadh').text(data.riyadh);
            });
        }

        setInterval(function (){
            getTimes();
        },60000);

        getTimes();

        currentPodcast = "";

        Amplitude.init({
            "bindings": {
                37: 'prev',
                39: 'next',
                32: 'play_pause'
            },
            "songs": [
                @foreach($podcasts as $podcast)
                { "name": "{{$podcast->title}}", "artist": "{{$podcast->excerpt}}", "url": "{{$podcast->audio_file}}", },
                @endforeach
            ]
        });

        /*
          Handles a click on the song played progress bar.
        */
        document.getElementById('song-played-progress').addEventListener('click', function( e ){
            var offset = this.getBoundingClientRect();
            var x = e.pageX - offset.left;

            Amplitude.setSongPlayedPercentage( ( parseFloat( x ) / parseFloat( this.offsetWidth) ) * 100 );
        });

        function playPodcast(title,excerpt,image,songIndex){
            $('#podcastTitle').html(title);
            $('#podcastDetails').html(excerpt);
            $('#podcastThumb').attr('src',image);
            console.log(Amplitude.playSongAtIndex(songIndex));
        }

        $('.podcast .control').on('click', function (){
            bt = $(this);

            if($(bt).hasClass('vid')){

            }
            else {
                if($(bt).hasClass('active')){
                    $('.podcast .control').removeClass('active');
                    $('#podcastPlayer').removeClass('active');
                    Amplitude.pause();
                }
                else {
                    $('.podcast .control').removeClass('active');
                    $(bt).addClass('active');
                    $('#podcastPlayer').addClass('active');

                    playPodcast($(bt).attr('data-title'),$(bt).attr('data-excerpt'),$(bt).attr('data-image'),$('.podcast.control').index(bt));
                }
            }

        });

        $('#closePlayer').on('click', function (e){
            e.preventDefault();
            $('.podcast .control').removeClass('active');
            $('#podcastPlayer').removeClass('active');
            Amplitude.pause();
        });

        var wheelDistance = function(evt) {

            // wheelDelta inidicates how
            // Far the wheel is turned
            var w = evt.wheelDelta;

            // Returning normalized value
            return w / 220;
        }

        // Adding event listener for some element
        window.addEventListener("mousewheel", wheelDistance);

    </script>
@endsection
