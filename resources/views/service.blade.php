@extends('partials.master')

@section('css')
    <link rel="stylesheet" href="css/article.css?v=<?php echo rand(1,99999); ?>">
    <style type="text/css">
        .partners-carousel .article-tag{
            float: left;
        }
        .partners-carousel .article{
            padding-right: 10px;
        }
    </style>
@endsection

@section('content')

<section id="inner" class="mb-5">
    <div class="container">

        <h1 class="page-title">Real Estate Law</h1>

        <hr class="green"/>

        <div class="row">
            <div class="col-md-12">
                <p class="headline"><b>Middle East real estate is one of the fastest growing sectors in the world.</b> Overseas developers have invested billions of dollars into Oman, Qatar, Dubai and Saudi Arabia, which has pushed the growth of the market in areas such as retail, residential and hospitality.</p>

                <img src="{{  asset('img/article.png') }}" width="100%" class="mt-3 mb-3">
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 offset-2 mt-5">
                <p>Middle East real estate is one of the fastest growing sectors in the world. Overseas developers have invested billions of dollars into Oman, Qatar, Dubai and Saudi Arabia, which has pushed the growth of the market in areas such as retail, residential and hospitality.Middle East real estate is one of the fastest growing sectors in the world. Overseas developers have invested billions of dollars into Oman, Qatar, Dubai and Saudi Arabia, which has pushed the growth of the market in areas such as retail, residential and hospitality.Middle East real estate is one of the fastest growing sectors in the world. </p>
                <p>Overseas developers have invested billions of dollars into Oman, Qatar, Dubai and Saudi Arabia, which has pushed the growth of the market in areas such as retail, residential and hospitality.Middle East real estate is one of the fastest growing sectors in the world. Overseas developers have invested billions of dollars into Oman, Qatar, Dubai and Saudi Arabia, which has pushed the growth of the market in areas such as retail, residential and hospitality.Middle East real estate is one of the fastest growing sectors in the world. </p>
                <p>Overseas developers have invested billions of dollars into Oman, Qatar, Dubai and Saudi Arabia, which has pushed the growth of the market in areas such as retail, residential and hospitality.Middle East real estate is one of the fastest growing sectors in the world. Overseas developers have invested billions of dollars into Oman, Qatar, Dubai and Saudi Arabia, which has pushed the growth of the market in areas such as retail, residential and hospitality.Middle East real estate is one of the fastest growing sectors in the world. Overseas developers have invested billions of dollars into Oman, Qatar, Dubai and Saudi Arabia, which has pushed the growth of the market in areas such as retail, residential and hospitality.</p>
            </div>
        </div>
</section>

<section id="highlights" class="mb-5">
    <div class="container">
        <h5 class="section-heading">
            Partners
            <a href="#" class="highlights-next carousel-arrows next"></a>
            <a href="#" class="highlights-prev carousel-arrows prev"></a>
        </h5>

        <div class="partners-carousel">
            <div>
                <div class="article">
                    <span class="border-top"></span>
                    <a href="{{ url('people') }}"><img src="img/profile.png" width="100%" class="article-thumb mb-3"></a>
                    <h3 class="article-title">Dr. Ahmad Bin Hezeem</h3>
                    <span class="article-tag">Senior Partner</span>
                </div>
            </div>
            <div>
                <div class="article">
                    <span class="border-top"></span>
                    <a href="{{ url('people') }}"><img src="img/profile.png" width="100%" class="article-thumb mb-3"></a>
                    <h3 class="article-title">Dr. Ahmad Bin Hezeem</h3>
                    <span class="article-tag">Senior Partner</span>
                </div>
            </div>
            <div>
                <div class="article">
                    <span class="border-top"></span>
                    <a href="{{ url('people') }}"><img src="img/profile.png" width="100%" class="article-thumb mb-3"></a>
                    <h3 class="article-title">Dr. Ahmad Bin Hezeem</h3>
                    <span class="article-tag">Senior Partner</span>
                </div>
            </div>
            <div>
                <div class="article">
                    <span class="border-top"></span>
                    <a href="{{ url('people') }}"><img src="img/profile.png" width="100%" class="article-thumb mb-3"></a>
                    <h3 class="article-title">Dr. Ahmad Bin Hezeem</h3>
                    <span class="article-tag">Senior Partner</span>
                </div>
            </div>
            <div>
                <div class="article">
                    <span class="border-top"></span>
                    <a href="{{ url('people') }}"><img src="img/profile.png" width="100%" class="article-thumb mb-3"></a>
                    <h3 class="article-title">Dr. Ahmad Bin Hezeem</h3>
                    <span class="article-tag">Senior Partner</span>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="highlights" class="mb-5">
    <div class="container">
        <h5 class="section-heading">
            Similar Articles
            <a href="#" class="highlights-next carousel-arrows next"></a>
            <a href="#" class="highlights-prev carousel-arrows prev"></a>
        </h5>

        <div class="highlights-carousel">
            <div>
                <div class="bordertop article">
                    <span class="article-tag">bankruptcy, news, uae.</span>
                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>
                    <p class="article-excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    <a href="article.php" class="readmore"></a>
                </div>
            </div>
            <div>
                <div class="bordertop article">
                    <span class="article-tag">bankruptcy, news, uae.</span>
                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>
                    <p class="article-excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    <a href="article.php" class="readmore"></a>
                </div>
            </div>
            <div>
                <div class="bordertop article">
                    <span class="article-tag">bankruptcy, news, uae.</span>
                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>
                    <p class="article-excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    <a href="article.php" class="readmore"></a>
                </div>
            </div>
            <div>
                <div class="bordertop article">
                    <span class="article-tag">bankruptcy, news, uae.</span>
                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>
                    <p class="article-excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <a href="article.php" class="readmore"></a>
                </div>
            </div>
            <div>
                <div class="bordertop article">
                    <span class="article-tag">bankruptcy, news, uae.</span>
                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>
                    <p class="article-excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <a href="article.php" class="readmore"></a>
                </div>
            </div>
            <div>
                <div class="bordertop article">
                    <span class="article-tag">bankruptcy, news, uae.</span>
                    <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>
                    <p class="article-excerpt">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <a href="article.php" class="readmore"></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="pb-5 mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 relative greenbox">
                <span class="sub">Stop and meet the the team</span>
                <span class="head">Visit us</span>
                <button class="bt">Go to page</button>
            </div>
        </div>
    </div>
</section>  

@endsection()

@section('js')
<script type="text/javascript">
    
$('.partners-carousel').slick({
    infinite: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
            }
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});
</script>
@endsection