@if($promobox->type=='social')
    <div class="col-md-12 relative greenbox" style="background-color: {{$promobox->color}}">
        <span class="sub">Stop and meet the the team</span>
        <span class="head">VISIT US</span>
        @include('partials.socials')
    </div>
@elseif($promobox->type=='newsletter')
    <div class="col-md-12 relative greenbox" style="background-color: {{$promobox->color}}">
        <div class="row">
            <div class="col-md-6">
                <span class="head">NEWSLETTER</span>
            </div>
            <div class="col-md-6">
                <a href="https://bsa.concep.com/preferences/bsa/signup" target="_blank"> <input type="button" class="bt subscribe" value="Subscribe" style=""></a>
            </div>
        </div>
{{--        <form id="subscribeForm" action="">--}}
{{--            @csrf--}}
{{--            <div class="alert alert-success" id="subscribeSuccess"></div>--}}
{{--            <div class="alert alert-warning" id="subscribeError"></div>--}}

{{--            <input type="email" placeholder="Enter your email" class="form-control" name="email">--}}
{{--        </form>--}}
    </div>
@else
<div class="col-md-12 relative greenbox" style="background-color: {{$promobox->color}}">
    <span class="sub">{{ $promobox->subheading }}</span>
    <span class="head">{{ $promobox->heading }}</span>
    <a href="{{ url($promobox->link) }}"><button class="bt">{{ $promobox->link_text }}</button></a>
</div>
@endif
