@foreach($data->promos as $promo)
    <div class="promobox clearfix" style="background-color: {{$promo->color}}">
        <img src="{{ asset($promo->image) }}" width="100%" class="mb-2">
        <h3>{{$promo->heading}}</h3>
        <a href="{{$promo->link}}" class="bt">{{$promo->link_text}}</a>
    </div>
@endforeach
