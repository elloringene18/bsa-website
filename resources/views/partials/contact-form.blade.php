<form class="form" action="{{url('contact')}}" method="post" autocomplete="off">
    <input type="hidden" name="source" value="contact">
    @csrf
    @include('partials.form-errors')
    <div class="row">
        <div class="col-md-12">
            <input type="text" class="form-control" placeholder="Name" name="name" required>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <input type="text" class="form-control" placeholder="Phone" name="phone" required>
        </div>
        <div class="col-md-6">
            <input type="text" class="form-control" placeholder="Email" name="email" required>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <textarea class="form-control" placeholder="Message" name="message" rows="10" required></textarea>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group mt-4 mb-4">
                <div class="captcha">
                    <span>{!! captcha_img() !!}</span>
                    <button type="button" class="btn btn-danger" class="reload" id="reload">
                        &#x21bb;
                    </button>
                </div>
            </div>
            <div class="form-group mb-4">
                <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha" required>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-right">
            <input type="submit" class="line-bt" value="SEND MESSAGE" />
        </div>
    </div>
</form>
