<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12"><a href="#"><img src="{{ asset('') }}/img/footer-logo-full.png" width="150"></a> </div>
            <div class="col-md-9 col-sm-12">
                @include('partials.socials')

                @inject('contentService', 'App\Services\ContentProvider')
                <?php $bottomLinks = $contentService->getBottomLinks(); ?>

                @foreach($bottomLinks as $link)
                    <a href="{{ $link->link }}" class="link">{{$link->title}}</a>
                @endforeach

                <span class="link hover">
                    Employee resources
                    <div class="foothoverlinks">
                        <a href="https://outlook.office365.com" target="_blank">Employee Webmail</a>
                        <a href="https://office.com" target="_blank">Employee Workspace   </a>
                        <a href="https://hrms.blueberry.software" target="_blank">HRMS   </a>
                        <a href="https://bsa.whiztle.com" target="_blank">Intranet</a>
                    </div>
                </span>

            </div>
        </div>
    </div>
</footer>
</div>
<script src="{{ asset('') }}/js/vendor/jquery-1.11.2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/simple-parallax-js@5.5.1/dist/simpleParallax.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="{{ asset('') }}/js/simpleParallax.js"></script>
<script src="{{ asset('') }}/js/vendor/bootstrap.min.js"></script>

<script src="{{ asset('') }}/js/main.js?v=<?php echo rand(1,99999); ?>"></script>

@yield('js')
<!-- Google Analytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-91990023-1', 'auto');
    ga('send', 'pageview');
</script>
<!-- End Google Analytics -->
</body>
</html>
