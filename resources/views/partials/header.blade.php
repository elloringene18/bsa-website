<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>@yield('page-title') BSA Middle East Law Firm</title>

    @hasSection('page-description')
        <meta name="description" content="@yield('page-description')">
        <meta property="og:description" content="@yield('page-description')">
    @else
        <meta name="description" content="BSA is one of the most highly respected commercial finance practices in the Middle East. Learn more about our banking and financial service attorneys!">
        <meta property="og:description" content="BSA is one of the most highly respected commercial finance practices in the Middle East. Learn more about our banking and financial service attorneys!">
    @endif

    @hasSection('page-keywords')
        <meta name="keywords" content="@yield('page-keywords')">
    @endif

    <!--  Essential META Tags -->
    <meta property="og:title" content="@yield('page-title') BSA Middle East Law Firm">
    <meta property="og:type" content="article" />
    <meta property="og:image" content="https://bsabh.com/img/main-logo-full.png">
    <meta property="og:url" content="{{ url()->current() }}">
    <meta name="twitter:card" content="https://bsabh.com/img/main-logo-full.png">

    <!--  Non-Essential, But Recommended -->
    <meta property="og:site_name" content="BSA Middle East Law Firm">
    <meta name="twitter:image:alt" content="Alt text for image">

    <!--  Non-Essential, But Required for Analytics -->
    <meta name="twitter:site" content="@bsa_bh">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('') }}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('') }}/css/fontawesome.css">
    <link rel="icon" type="image/png" href="{{ asset('') }}/favicon.png">
    <style>
        body {
            /*background-color: #000;*/
        }
    </style>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css">
    <link rel="stylesheet" href="{{ asset('') }}/css/main.css?v=<?php echo rand(1,99999); ?>">

    @yield('css')

    <script src="{{ asset('') }}/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <meta name="google-site-verification" content="UstvTUdIK07it7Wpbkl-xWahQCTR7jfJvtE14ptoiyw" />
</head>
<body>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '973322696893012',
            xfbml      : true,
            version    : 'v13.0'
        });
        FB.AppEvents.logPageView();
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<div id="wrapper">
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <img src="{{ asset('') }}/img/green-dot.png" class="step-left" id="bigCircle">
    <img src="{{ asset('') }}/img/green-dot.png" class="step-right" id="bigCircle2">

    @inject('contentService', 'App\Services\ContentProvider')
    <header id="header">
        <div class="wrap">
            <a href="{{ url('/') }}"><img src="{{ asset('') }}/img/main-logo-full.png" id="mainLogo"></a>
            <ul id="mainNav">
                <li><a href="{{ url('/about-us') }}">About Us</a></li>
                <li class="knowledge-link">
                    <a href="#">Services</a>
                    <div class="float-menu">
                        <div class="rw">
                            <div class="colm">
                                <h5 class="section-heading">Groups</h5>
                                <ul>
                                    <?php $mlinks = $contentService->getServicesByType('groups'); $count = 1; ?>
                                    @foreach($mlinks as $mlink)
                                        <li><a href="{{  url('legal-practice-areas/'.$mlink->slug) }}"><span>{{ $count < 10 ? '0'.$count : $count }}</span>{{ $mlink->title }}</a></li>
                                        <?php $count++; ?>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="colm">
                                <h5 class="section-heading">Industries</h5>
                                <ul>
                                    <?php $mlinks = $contentService->getServicesByType('industry'); $count = 1; ?>
                                    @foreach($mlinks as $mlink)
                                        <li><a href="{{  url('legal-practice-areas/'.$mlink->slug) }}"><span>{{ $count < 10 ? '0'.$count : $count }}</span>{{ $mlink->title }}</a></li>
                                        <?php $count++; ?>
                                    @endforeach
{{--                                    <li><a href="{{  url('legal-practice-areas/construction-law') }}"><span>01</span>Construction and Engineering </a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/education') }}"><span>02</span>Education  </a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/energy-law') }}"><span>03</span>Energy</a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/regulatory-and-compliance-1') }}"><span>04</span>Financial Institutions </a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/insurance-reinsurance-law') }}"><span>05</span>Insurance</a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/international-trade-transport-and-maritime') }}"><span>06</span>International Trade, Transport & Maritime </a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/leisure-and-hospitality') }}"><span>07</span>Leisure & Hospitality </a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/life-sciences-and-healthcare') }}"><span>08</span>Life Sciences & Healthcare</a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/real-estate-law') }}"><span>09</span>Real Estate </a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/retail-and-consumer-goods') }}"><span>10</span>Retail & Consumer Goods</a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/technology-media-and-telecommunications') }}"><span>11</span>Technology, Media & Telecommunications (TMT) </a></li>--}}
                                </ul>
                            </div>
                            <div class="colm">
                                <h5 class="section-heading">Services</h5>
                                <ul>
                                    <?php $mlinks = $contentService->getServicesByType('services'); $count = 1; ?>
                                    @foreach($mlinks as $mlink)
                                        <li><a href="{{  url('legal-practice-areas/'.$mlink->slug) }}"><span>{{ $count < 10 ? '0'.$count : $count }}</span>{{ $mlink->title }}</a></li>
                                        <?php $count++; ?>
                                    @endforeach
{{--                                    <li><a href="{{  url('legal-practice-areas/arbitration-dispute-resolution') }}"><span>01</span>Arbitration</a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/banking-finance-law') }}"><span>02</span>Banking & Finance</a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/commercial-law') }}"><span>03</span>Commercial </a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/corporate-law-and-ma') }}"><span>04</span>Corporate & M&A</a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/cybersecurity') }}"><span>05</span>Cyber Security & Data Protection</a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/environment-social-and-governance') }}"><span>06</span>Environment, Social & Governance</a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/employment-law-firm') }}"><span>07</span>Employment </a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/intellectual-property-law') }}"><span>08</span>Intellectual Property</a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/litigation-law-firm') }}"><span>08</span>Litigation</a></li>--}}
{{--                                    <li><a href="{{  url('legal-practice-areas/regulatory-and-compliance') }}"><span>10</span>Regulatory & Compliance</a></li>--}}
                                </ul>
                            </div>

                        </div>
                        <div class="rw">
                            @include('partials.socials')
                        </div>
                    </div>
                </li>
                <li><a href="{{ url('/locations') }}">Locations</a></li>
                <li><a href="{{ url('/our-people') }}">Our People</a></li>
                <li class="knowledge-link">
                    <a href="{{ url('/knowledge-hub') }}">Knowledge</a>
                    <div class="float-menu half">
                        <div class="rw">
                            <div class="colm">
                                <h5 class="section-heading">Knowledge Hub</h5>
                                <ul>
                                    <li><a href="{{ url('knowledge-hub/news') }}"><span>01</span>News</a></li>
                                    <li><a href="{{ url('knowledge-hub/regulatory-and-legal-updates') }}"><span>02</span>Regulatory & Legal Updates</a></li>
                                    <li><a href="{{ url('knowledge-hub/reports') }}"><span>03</span>Reports</a></li>
                                    <li><a href="{{ url('knowledge-hub/events') }}"><span>04</span>Events</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="rw">
                            <ul class="social">
                                <li><a href="#" class="fa fa-facebook" target="_blank"></a></li>
                                <li><a href="#" class="fa fa-twitter" target="_blank"></a></li>
                                <li><a href="#" class="fa fa-linkedin" target="_blank"></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li><a href="{{ url('/careers') }}">Careers</a></li>
                <li><a href="{{ url('/contact-us') }}">Contact Us</a></li>
                <li><a href="#" class="search">&nbsp;</a></li>
            </ul>

            <div id="mainSearchbox">
                <a href="#" id="mainCloseSearch"></a>
                <form action="{{url('search-full')}}" method="get">
                    <input type="text" id="mainSearchfield" name="keyword">
                </form>
            </div>

            <div class="menucontainer" id="menuBt" onclick="toggleMenu(this)">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>
        </div>
</header>
