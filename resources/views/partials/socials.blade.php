<ul class="social">
    <li><a href="https://www.linkedin.com/company/bsalaw/" class="fa fa-linkedin" target="_blank"></a></li>
    <li><a href="https://twitter.com/bsa_bh" class="fa fa-twitter" target="_blank"></a></li>
    <li><a href="https://www.instagram.com/bsa_law/" class="fa fa-instagram" target="_blank"></a></li>
    <li><a href="https://www.youtube.com/channel/UCR2DctgrnR6VG0yGKzrHRKQ" class="fa fa-youtube" target="_blank"></a></li>
</ul>
