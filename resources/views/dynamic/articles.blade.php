@extends('partials.master')
@section('page-title'){{$category->name}} | @endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('') }}/css/inner.css?v=<?php echo rand(1,99999); ?>">
    <link rel="stylesheet" href="{{ asset('') }}/css/news.css?v=<?php echo rand(1,99999); ?>">
    <style>
        .cropphoto {
            max-height: 200px;
            overflow: hidden;
            display: block;
        }


        @media only screen and (max-width: 991px){
            .article-thumb {
                margin-bottom: 0;
            }
        }

        @media only screen and (max-width: 767px){
            .cropphoto {
                display: block;
                max-height: 600px;
            }
            .article-thumb {
                height: auto;
                width: 100%;
            }
        }

        .posts {
            min-height: 480px;
        }

        #form select {
            margin: 0 5px;
            float: right;
            width: auto;
        }

    </style>
@endsection


@section('content')
    <section id="inner" class="mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="position-relative">
                        <h5 class="section-heading">{{ $category ? $category->name : 'Insights' }}
                            <a href="#" id="searchBt"></a></h5>
                        <hr class="black mt-3"/>
                        <div id="searchbox">
                            <a href="#" id="closeSearch"></a>
                            <form action="{{ url('search') }}" method="get">
                                <input type="text" id="searchfield" name="keyword">
                                <input type="submit" style="display: none">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{ url('knowledge-hub/news') }}" method="get" id="form">
                        <select name="year" id="datefilter" class="form-control">
                            <option value="">Year</option>
                            <option value="">All</option>
                            <?php for($x=2022;$x>=2013;$x--){ ?>
                            <option {{ isset($_GET['year']) ? $_GET['year']==$x ? 'selected' : false : '' }} value="<?php echo $x; ?>"><?php echo $x; ?></option>
                            <?php } ?>
                        </select>
                        <select name="service" class="form-control">
                            <option value="">All Services</option>
                            @foreach($types as $type=>$services)
                                <optgroup label="{{$type}}">
                                    @foreach($services as $service)
                                        <option {{ isset($_GET['service']) ? ($_GET['service'] == $service->slug ? 'selected' : false) : '' }} value="{{$service->slug}}">{{$service->title}}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </form>
{{--                    <div class="custom-select" id="datefilter">--}}
{{--                        <select>--}}
{{--                            <option value="latest">Sector</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-12">--}}
{{--                    <div id="news-carousel">--}}
{{--                        <?php--}}
{{--                            $months = ['January','February','March','April','May','June','July','August','September','October','November','December'];--}}
{{--                        ?>--}}
{{--                        <?php for($x=0;$x<count($months);$x++){ ?>--}}
{{--                        <div>--}}
{{--                            <span class="date"><?php echo $months[$x]; ?></span>--}}

{{--                            @foreach($dotPosts[$x] as $post)--}}
{{--                                    <a href="{{ url('/knowledge-hub/'.$category->slug.'/'.$post->slug) }}"><div class="dot"><div class="rel"><span class="title tracking-in-contract">{{$post->title}}</span></div></div></a>--}}
{{--                            @endforeach--}}

{{--                        </div>--}}
{{--                        <?php } ?>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="container posts">
            <div class="row mt-5">
                <div class="col-md-12">
                    <div class="row">
                        @if(count($data))
                            @foreach($data as $item)
                                <div class="col-md-3 article">
                                    <span class="border-top"></span>
                                    <a href="{{ url('/knowledge-hub/'.$category->slug.'/'.$item->slug) }}" class="cropphoto"><img src="{{ asset($item->photo) }}" width="100%" class="article-thumb"></a>
{{--                                    <span class="article-tag">--}}
{{--                                        @foreach($item->tags as $tag)--}}
{{--                                            {{ $tag->name }},--}}
{{--                                        @endforeach--}}
{{--                                    </span>--}}
                                    <a href="{{ url('/knowledge-hub/'.$category->slug.'/'.$item->slug) }}"><h3 class="article-title">{{$item->title}}</h3></a>
                                </div>
                            @endforeach
                        @else
                            No posts in this category. Please come back later.
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="pagination mt-5">
                            <?php
                                $pages = ceil($data->total()/12);

                                for($x=1;$x<=$pages;$x++){
                            ?>
                                <li><a {{ isset($_GET['page']) ? $x == $_GET['page'] ? 'class=active' : false : '' }} href="{{ url('/knowledge-hub/'.$category->slug.'?page='.$x) }}">{{$x}}</a></li>
                            <?php
                                }
                            ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection()

@section('js')
    <script src="{{ asset('') }}/js/news.js?v=<?php echo rand(1,99999); ?>"></script>
    <script>
        $('#form select').on('change',function(){
            $('#form').submit();
        });
    </script>
@endsection
