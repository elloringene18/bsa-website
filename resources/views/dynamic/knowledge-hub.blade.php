@extends('partials.master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $pageContent = $contentService->getPageSections('knowledge'); ?>

@if($pageContent['meta-title'])
    @section('page-title'){{ $pageContent['meta-title'] }} | @endsection
@endif

@if($pageContent['meta-description'])
    @section('page-description'){{ $pageContent['meta-description'] }}@endsection
@endif

@section('css')
    <link rel="stylesheet" href="{{ asset('') }}/css/inner.css?v=<?php echo rand(1,99999); ?>">
    <link rel="stylesheet" href="{{ asset('') }}/css/news.css?v=<?php echo rand(1,99999); ?>">

    <style>
        #banner {
            height: 700px;
            width: 100%;
            position: relative;
            overflow: hidden;
        }

        #banner .lines {
            position: absolute;
            width: 100%;
            height: 100%;
            background-image: url({{ asset('img/knowledge-lines.png') }});
            background-repeat: no-repeat;
            background-size: auto 100%;
            background-repeat: no-repeat;
            top: 0;
        }

        #banner .bg {
            position: absolute;
            width: 4020px;
            height: 3200px;
            background-image: url({{ asset('img/knowledge-bg.png') }});
            background-repeat: no-repeat;
            background-size: 4020px auto;
            background-position: center;
            top: -300px;
        }

        #banner h1 {
            font-size: 120px;
            margin-top: 300px;
            font-weight: bold;
        }

        #inner {
            background-position: top center;
            background-size: 100% auto;
            background-repeat: repeat-x;
            margin-top: 0;
            padding-top: 60px;
        }

        #subscribeForm input[type=submit] {
            border: 1px solid #54AB3B;
            width: 170px;
            text-align: center;
            background-color: transparent;
            height: 36px;
            line-height: 36px;
            float: right;
            color: inherit;
            transition: color 300ms, background-color 300ms;
        }

        #subscribeForm input[type=email] {
            background-color: transparent;
            border: 0;
            border-bottom: 1px solid #000;
            border-radius: 0;
            padding-left: 0;
            padding-bottom: 10px;
            font-size: 20px;
        }

        .service .title {
            font-size: 70px;
            line-height: 70px;
            font-weight: bold;
            text-transform: uppercase;
            background-image: url({{ asset('img/service-arrow.png') }});
            background-position: top left;
            background-repeat: no-repeat;
            padding-left: 100px;
            background-size: 80px auto;
            float: left;
            cursor: pointer;
            transition: padding-right 300ms,padding-left 300ms;
        }

        .service .title.right {
            background-image: url({{ asset('img/service-arrow-right.png') }});
            background-position: top right;
            padding-left: 0;
            padding-right: 100px;
            text-align: right;
            float: right;
        }

        .service .title.right:hover {
            background-image: url({{ asset('img/service-arrow-right-open.png') }});
            padding-right: 120px;
        }

        .service:hover .title {
            background-size: 60px auto !important;
        }

        .service  {
            padding-bottom: 20px;
            padding-top: 20px;
            float: left;
            width: 100%;
        }

        .service:hover .title {
            background-image: url({{ asset('img/service-arrow-open.png') }});
            padding-left: 120px;
        }

        .service h5 {
            display: inline-block;
            margin-left: 10px;
            margin-bottom: 0;
            font-size: 14px;
        }

        .service.even .title, .service.even ul li {
            float: right;
        }

        #where .medium {
            position: relative;
            width: auto;
            float: left;
        }

        .timezone.small {
            margin-top: 0;
            padding-left: 0;
        }

        .ellipse.where {
            right: -20px;
            top: -190px;
        }

        .event .title {
            float: left;
            width: 70%;
            margin-top: 10px;
        }

        .event .date {
            float: right;
            width: 30%;
            text-align: right;
            background-image: url({{ asset('img/event-arrow.png') }});
            background-position: right center;
            background-repeat: no-repeat;
            padding-right: 30px;
            background-size: 20px auto;
        }

        .timezone.small li {
            padding: 5px 0;
            margin: 0;
        }

        .timezone.small .img {
            display: none;
            margin-top: 20px;
            padding-left: 10%;
            font-weight: normal;
            color: #000;
        }

        .timezone.small .img p {
            line-height: 26px;
        }

        .timezone .event.active .date {
            font-size: 16px;
        }

        .timezone.small li.active .img {
            float: left;
        }

        .event .title {
            width: 100%;
        }

        .cropphoto {
            height: 200px;
            overflow: hidden;
            display: block;
        }

        .timezone.small {
            min-height: 120px;
        }

        .timezone.small li {
            padding: 10px 0;
            margin: 0;
        }

        .event .title {
            width: 70%;
            margin-top: 0;
        }

        @media only screen and (max-width: 1200px){
            .service .title {
                background-size: 50px auto !important;
                font-size: 40px;
                line-height: 50px;
                min-height: 80px;
            }

            .timezone.small {
                min-height: 560px;
            }
        }

        @media only screen and (max-width: 991px){
            .service .title {
                padding-left: 90px;
                background-size: 70px auto !important;
                float: left;
            }
            .service:hover .title {
                background-size: 50px auto !important;
            }
        }

        @media only screen and (max-width: 767px){

            .service .title {
                font-size: 60px;
                padding-left: 90px;
                background-size: 70px auto !important;
                float: left !important;
            }

            .service.even .title {
                float: left;
            }

            .service:hover .title {
                background-size: 50px auto !important;
            }

            .service .title.right {
                background-image: url({{ asset('img/service-arrow.png') }});
                background-position: top left;
                background-repeat: no-repeat;
                padding-left: 60px;
                background-size: 80px auto;
                float: left;
                padding-right: 0;
                text-align: left;
            }

            .service {
                padding-top: 0px;
                padding-bottom: 0;
            }
        }

        @media only screen and (max-width: 767px){

            .service .title {
                font-size: 40px;
                padding-left: 60px;
                background-size: 40px auto !important;
                float: left !important;
            }

            .service:hover .title {
                background-size: 30px auto !important;
            }

            .service.even .title {
                float: left;
            }

            .service {
                padding-top: 20px;
            }

            .article-thumb {
                height: auto;
                width: 100%;
            }
        }

        @media only screen and (max-width: 520px){

            .service .title {
                font-size: 30px;
                padding-left: 40px;
                background-size: 30px auto !important;
                min-height: 29px;
                background-position: 0 10px;
            }

            .service .title.right {
                padding-left: 40px;
                background-position: 0 10px;
            }
            .ellipse.where {
                top: -50px;
            }
        }
    </style>
@endsection


@section('content')

{{--    <section id="banner">--}}
{{--        <div class="container">--}}
{{--            <h1>KNOWLEDGE<br/>HUB</h1>--}}
{{--        </div>--}}
{{--        <div class="bg" id="hole"></div>--}}
{{--        <div class="lines"></div>--}}
{{--    </section>--}}


    <section id="inner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="position-relative">
                        <h1 class="page-title">Knowledge Hub
                            <a href="#" id="searchBt"></a></h1>
                        <hr class="black mt-3"/>
                        <div id="searchbox">
                            <a href="#" id="closeSearch"></a>
                            <form action="{{ url('search') }}" method="get">
                                <input type="text" id="searchfield" name="keyword">
                                <input type="submit" style="display: none">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row mt-md-5 mt-sm-0">
                <div class="col-md-6">
                    <div class="services">
                        <div class="service">
                            <a href="{{ url('knowledge-hub/news') }}"><div class="title">News</div></a>
                        </div>
                        <div class="service">
                            <a href="{{ url('knowledge-hub/reports') }}"><div class="title">Reports</div></a>
                        </div>
                        <div class="service">
                            <a href="{{ url('knowledge-hub/events') }}"><div class="title">Events</div></a>
                        </div>
{{--                        <div class="service">--}}
{{--                            <a href="{{ url('knowledge-hub/events') }}"><div class="title">Events</div></a>--}}
{{--                        </div>--}}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="services">
{{--                        <div class="service">--}}
{{--                            <a href="{{ url('knowledge-hub/covid-19') }}"><div class="title  right">Coronavirus Insight Centre</div></a>--}}
{{--                        </div>--}}
                        <div class="service">
                            <a href="{{ url('knowledge-hub/regulatory-and-legal-updates') }}"><div class="title  right">regulatory & Legal updates</div></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


{{--        <section id="highlights" class="mb-5 mt-5">--}}
{{--            <div class="container">--}}
{{--                <h5 class="section-heading">--}}
{{--                    Highlights--}}
{{--                </h5>--}}

{{--                <div class="highlights-carousel">--}}
{{--                    <div>--}}
{{--                        <div class="bordertop article">--}}
{{--                            <span class="article-tag">bankruptcy, news, uae.</span>--}}
{{--                            <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>--}}
{{--                            <p class="article-excerpt">--}}
{{--                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>--}}
{{--                            <a href="article.php" class="readmore"></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div>--}}
{{--                        <div class="bordertop article">--}}
{{--                            <span class="article-tag">bankruptcy, news, uae.</span>--}}
{{--                            <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>--}}
{{--                            <p class="article-excerpt">--}}
{{--                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>--}}
{{--                            <a href="article.php" class="readmore"></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div>--}}
{{--                        <div class="bordertop article">--}}
{{--                            <span class="article-tag">bankruptcy, news, uae.</span>--}}
{{--                            <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>--}}
{{--                            <p class="article-excerpt">--}}
{{--                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>--}}
{{--                            <a href="article.php" class="readmore"></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div>--}}
{{--                        <div class="bordertop article">--}}
{{--                            <span class="article-tag">bankruptcy, news, uae.</span>--}}
{{--                            <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>--}}
{{--                            <p class="article-excerpt">--}}
{{--                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}
{{--                            <a href="article.php" class="readmore"></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div>--}}
{{--                        <div class="bordertop article">--}}
{{--                            <span class="article-tag">bankruptcy, news, uae.</span>--}}
{{--                            <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>--}}
{{--                            <p class="article-excerpt">--}}
{{--                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}
{{--                            <a href="article.php" class="readmore"></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div>--}}
{{--                        <div class="bordertop article">--}}
{{--                            <span class="article-tag">bankruptcy, news, uae.</span>--}}
{{--                            <a href="article.php"><h3 class="article-title">Lorem Ipsum sit Amet.</h3></a>--}}
{{--                            <p class="article-excerpt">--}}
{{--                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}
{{--                            <a href="article.php" class="readmore"></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}

        <section id="lastestarticles" class="mb-5 mt-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h5 class="section-heading">News</h5>
                        <div class="bordertop">
                            <div class="row">
                                @foreach($news as $item)
                                    <div class="col-md-3 article">
                                        <a href="{{ url('knowledge-hub/news/'.$item->slug) }}" class=""><img src="{{ $item->photo }}" width="100%" class="article-thumb"></a>
{{--                                        <span class="article-tag">--}}
{{--                                            <?php $titleLoop = 1; ?>--}}
{{--                                            @foreach($item->tags as $title)--}}
{{--                                                {{ $title->name }}{{ count($item->tags) > $titleLoop ? ', ' : ''}}--}}
{{--                                                <?php $titleLoop++; ?>--}}
{{--                                            @endforeach--}}
{{--                                        </span>--}}
                                        <h3 class="article-title">{{ $item->title }}</h3>
                                    </div>
                                @endforeach
                            </div>

                            <div class="row">
                                <div class="col-md-12 mt-3">
                                    <a href="{{ url('knowledge-hub/news/') }}" class="line-bt">VIEW ALL</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 mt-5">
                        <h5 class="section-heading">Regulatory & legal updates</h5>
                        <div class="bordertop">
                            <div class="row">
                                @foreach($updates as $item)
                                    <div class="col-md-3 article">
                                        <a href="{{ url('knowledge-hub/regulatory-and-legal-updates/'.$item->slug) }}" class=""><img src="{{ $item->photo }}" width="100%" class="article-thumb"></a>
{{--                                        <span class="article-tag">--}}
{{--                                            <?php $titleLoop = 1; ?>--}}
{{--                                            @foreach($item->tags as $title)--}}
{{--                                                {{ $title->name }}{{ count($item->tags) > $titleLoop ? ', ' : ''}}--}}
{{--                                                <?php $titleLoop++; ?>--}}
{{--                                            @endforeach--}}
{{--                                        </span>--}}
                                        <h3 class="article-title">{{ $item->title }}</h3>
                                    </div>
                                @endforeach
                            </div>
                            <div class="row">
                                <div class="col-md-12 mt-3">
                                    <a href="{{ url('knowledge-hub/regulatory-and-legal-updates/') }}" class="line-bt">VIEW ALL</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 mt-5">
                        <h5 class="section-heading">Reports</h5>
                        <div class="bordertop">
                            <div class="row">
                                @foreach($reports as $item)
                                    <div class="col-md-3 article">
                                        <a href="{{ url('knowledge-hub/reports/'.$item->slug) }}" class="">
                                            <img src="{{ $item->photo }}" width="100%" class="article-thumb"></a>
                                        {{--                                        <span class="article-tag">--}}
                                        {{--                                            <?php $titleLoop = 1; ?>--}}
                                        {{--                                            @foreach($item->tags as $title)--}}
                                        {{--                                                {{ $title->name }}{{ count($item->tags) > $titleLoop ? ', ' : ''}}--}}
                                        {{--                                                <?php $titleLoop++; ?>--}}
                                        {{--                                            @endforeach--}}
                                        {{--                                        </span>--}}
                                        <h3 class="article-title">{{ $item->title }}</h3>
                                    </div>
                                @endforeach
                            </div>
                            <div class="row">
                                <div class="col-md-12 mt-3">
                                    <a href="{{ url('knowledge-hub/reports/') }}" class="line-bt">VIEW ALL</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
{{--                <div class="row">--}}
{{--                    <div class="col-lg-12 mt-5">--}}
{{--                        <h5 class="section-heading">Latest Publications</h5>--}}
{{--                        <div class="bordertop">--}}
{{--                            <div class="row">--}}
{{--                                @foreach($publications as $item)--}}
{{--                                    <div class="col-md-3 article">--}}
{{--                                        <a href="{{ url('knowledge-hub/publications-2/'.$item->slug) }}" class="cropphoto"><img src="{{ $item->photo }}" width="100%" class="article-thumb"></a>--}}
{{--                                        <span class="article-tag">--}}
{{--                                            <?php $titleLoop = 1; ?>--}}
{{--                                            @foreach($item->tags as $title)--}}
{{--                                                {{ $title->name }}{{ count($item->tags) > $titleLoop ? ', ' : ''}}--}}
{{--                                                <?php $titleLoop++; ?>--}}
{{--                                            @endforeach--}}
{{--                                        </span>--}}
{{--                                        <h3 class="article-title">{{ $item->title }}</h3>--}}
{{--                                    </div>--}}
{{--                                @endforeach--}}
{{--                            </div>--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-12 mt-3">--}}
{{--                                    <a href="{{ url('knowledge-hub/publications-2/') }}" class="line-bt">VIEW ALL</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="row">--}}
{{--                    <div class="col-lg-12 mt-5 mb-5">--}}
{{--                        <h5 class="section-heading">Coronavirus Insights</h5>--}}
{{--                        <div class="bordertop">--}}
{{--                            <div class="row">--}}
{{--                                @foreach($covid as $item)--}}
{{--                                    <div class="col-md-3 article">--}}
{{--                                        <a href="{{ url('knowledge-hub/covid-19/'.$item->slug) }}" class="cropphoto"><img src="{{ $item->photo }}" width="100%" class="article-thumb"></a>--}}
{{--                                        <span class="article-tag">--}}
{{--                                            <?php $titleLoop = 1; ?>--}}
{{--                                            @foreach($item->tags as $title)--}}
{{--                                                {{ $title->name }}{{ count($item->tags) > $titleLoop ? ', ' : ''}}--}}
{{--                                                <?php $titleLoop++; ?>--}}
{{--                                            @endforeach--}}
{{--                                        </span>--}}
{{--                                        <h3 class="article-title">{{ $item->title }}</h3>--}}
{{--                                    </div>--}}
{{--                                @endforeach--}}
{{--                            </div>--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-12 mt-3">--}}
{{--                                    <a href="{{ url('knowledge-hub/covid-19/') }}" class="line-bt">VIEW ALL</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </section>

        <section id="where" class="pb-5 mt-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                <span class="medium">
                    Save the date, <br/>
                    See you there
                    <img src="{{ asset('') }}/img/ellipse-1.png" class="ellipse where">
                </span>
                    </div>
                    <div class="col-md-8 offset-md-4 pt-5">
                        <h3 class="section-heading">
                            UPCOMING EVENTS
                            @if(count($upcomingevents))
                                <a href="{{ url('knowledge-hub/events/') }}" class="viewAll">SEE ALL</a>
                            @else
                            @endif
                        </h3>
                        <ul class="timezone small clearfix bordertop">

                            @if(count($upcomingevents))
                            @foreach($upcomingevents as $item)
                                <li class="event {{ $loop->index==0 ? 'active' : '' }}">
                                    <div class="row ">
                                        <a href="{{ url('knowledge-hub/events/'.$item->slug) }}">
                                            <div class="col-md-12"><div class="title">{{ $item->title }}</div>
                                                <div class="date">{{ \Carbon\Carbon::parse($item->date)->format('M, d Y') }}</div>
                                            </div>
                                            <div class="col-md-12 img swing-in-top-fwd">
                                                <p>{!! \Illuminate\Support\Str::words(strip_tags($item->content),30) !!}</p>
                                            </div>
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                            @else
                                <li class="event">
                                    <div class="row ">
                                        <div class="title">No upcoming events.</div>
                                    </div>
                                </li>
                            @endif
                        </ul>
                    </div>
                    <div class="col-md-8 offset-md-4 pt-5">
                        <h3 class="section-heading">
                            PAST EVENTS <a href="{{ url('knowledge-hub/events/') }}" class="viewAll">SEE ALL</a>
                        </h3>
                        <ul class="timezone small clearfix bordertop">

                            @foreach($pastevents as $item)
                                <li class="event {{ $loop->index==0 ? 'active' : '' }}">
                                    <div class="row ">
                                        <a href="{{ url('knowledge-hub/events/'.$item->slug) }}">
                                            <div class="col-md-12"><div class="title">{{ $item->title }}</div>
                                                <div class="date">{{ \Carbon\Carbon::parse($item->date)->format('M, d Y') }}</div>
                                            </div>
                                            <div class="col-md-12 img swing-in-top-fwd">
                                                <p>
                                                    @if($item->excerpt)
                                                        {{ \Illuminate\Support\Str::words(strip_tags($item->excerpt), 20) }}
                                                    @else
                                                        {{ \Illuminate\Support\Str::words(strip_tags($item->content), 20) }}
                                                    @endif
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </li>
                            @endforeach

{{--                            <li class="event">--}}
{{--                                <div class="row ">--}}
{{--                                    <a href="{{  url('article') }}">--}}
{{--                                        <div class="col-md-12"><div class="title">Event Lorem Ipsum</div> <div class="date">November 24</div></div>--}}
{{--                                        <div class="col-md-12 img">--}}
{{--                                            <p><b>Location:</b> Dubai (DIFC)</p>--}}
{{--                                            <p><b>Description:</b> We are seeking to recruit aself-motivated and driven Court Advocate to join our firm. UAE Nationals only.</p>--}}
{{--                                        </div>--}}
{{--                                    </a>--}}
{{--                                </div>--}}
{{--                            </li>--}}
                        </ul>
                    </div>
                </div>

            </div>
        </section>

{{--        <section class="pb-5 mt-5">--}}
{{--            <div class="container">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-12 relative">--}}
{{--                        <div class="col-md-12 pt-5">--}}
{{--                            <h5 class="section-heading">--}}
{{--                                Read our previous newsletters--}}
{{--                                @if(count($upcomingevents))--}}
{{--                                    <a href="{{ url('knowledge-hub/events/') }}" class="viewAll">SEE ALL</a>--}}
{{--                                @else--}}
{{--                                @endif--}}
{{--                            </h5>--}}
{{--                        </div>--}}

{{--                        <div class="col-md-12 bordertop">--}}
{{--                            None--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}
        <section class="pb-5 mt-5">
            <div class="container">
                <div class="row">
                    @inject('contentService', 'App\Services\ContentProvider')
                    <?php $pageContent = $contentService->getPageSections('knowledge'); ?>
                    <?php $promobox = $contentService->getPromoboxById($pageContent['promobox']); ?>
                    @include('partials.long-promoboxes')
                </div>
            </div>
        </section>

        @endsection()

        @section('js')
            <script src="{{ asset('') }}/js/news.js?v=<?php echo rand(1,99999); ?>"></script>
{{--            <script>--}}
{{--                document.getElementById('banner').onmousemove = function(e) {--}}
{{--                    var x = e.clientX - 2010;--}}
{{--                    var y = e.clientY - 1700;--}}
{{--                    document.getElementById('hole').style.left  = x+"px";--}}
{{--                    document.getElementById('hole').style.top  = y+"px";--}}
{{--                }--}}
{{--            </script>--}}
@endsection
