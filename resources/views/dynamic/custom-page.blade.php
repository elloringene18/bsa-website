@extends('partials.master')

@section('page-title'){{ $data->meta_title ? $data->meta_title : $data->title }} | @endsection

@section('page-description'){{ $data->meta_description ? $data->meta_description : \Illuminate\Support\Str::limit(strip_tags($data->content),160) }}@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('') }}/css/inner.css?v=<?php echo rand(1,99999); ?>">
    <link rel="stylesheet" href="{{ asset('') }}/css/news.css?v=<?php echo rand(1,99999); ?>">
    <style>
        .cropphoto {
            max-height: 200px;
            overflow: hidden;
            display: block;
        }


        @media only screen and (max-width: 991px){
            .article-thumb {
                margin-bottom: 0;
            }
        }

        @media only screen and (max-width: 767px){
            .cropphoto {
                display: block;
                max-height: 600px;
            }
        }

        #inner p {
            padding-left: 13px;
        }
    </style>
@endsection


@section('content')
    <section id="inner" class="mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="position-relative">
                        <h1 class="page-title">{{ $data->title }}</h1>
                        <hr class="black mt-3"/>
                        {!!  $data->content  !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection()

@section('js')
@endsection
