@extends('partials.master')
@section('page-title'){{$data->meta_title ? $data->meta_title : $data->title }} | @endsection

@section('page-description'){!! $data->meta_description ? $data->meta_description : \Illuminate\Support\Str::words(strip_tags($data->content),30) !!}@endsection

@if($data->meta_keywords)
    @section('page-keywords'){{ $data->meta_keywords }}@endsection
@endif

@section('css')
    <link rel="stylesheet" href="{{ asset('') }}/css/article.css?v=<?php echo rand(1,99999); ?>">

    <style type="text/css">
        .content p:first-of-type {
            font-size: 20px;
        }
        .content h1,
        .content h2,
        .content h3,
        .content h4,
        .content h5,
        .content h6,
        .content table
        {
            margin-top: 20px;
        }

        .content table
        {
            margin-bottom: 20px;
        }

        .content table td
        {
            padding-right: 20px;
        }

        .cropphoto {
            max-height: 300px;
            overflow: hidden;
            display: block;
        }

        .cropphoto img {
            margin-top: -300px;
            transition: filter 300ms;
            /*filter: grayscale(1);*/
        }

        .content ul {
            margin-top: 20px;
        }

        .content, .content p {
            line-height: 30px;
        }

        .righauth {
            display: none;
        }

        .authorThumb {
            transition: filter 300ms;
            filter: grayscale(1);
        }

        .authorThumb:hover {
            transition: filter 300ms;
            filter: grayscale(0);
        }



        @media only screen and (max-width: 1200px){
            .leftauth {
                display: none !important;
            }

            .righauth {
                 display: block;
            }
        }

        @media only screen and (max-width: 991px){
            .article-thumb {
                margin-bottom: 0;
            }
            .promobox h3 {
                float: left;
                font-size: 14px;
            }
            .promobox p {
                float: left;
                font-size: 10px;
            }
            .promobox {
                padding: 20px 20px;
            }
        }

        @media only screen and (max-width: 767px){
            .cropphoto {
                display: block;
                max-height: 1500px;
            }
            .promobox h3 {
                float: left;
                width: 100%;
            }
            .promobox p {
                width: 100%;
            }
            .promobox {
                margin-bottom: 30px;
            }
        }

        .content p, .content p span, .content p b span {
            font-family: 'Montserrat', sans-serif !important;
            font-size: 16px !important;
            line-height: 30px !important;
        }

        .content .line-bt {
            float: left;
            margin-top: 10px;
        }
    </style>
@endsection

@section('content')

    <section id="inner" class="mb-5">
        <div class="container">
            <p class="page-tag"><span class="tag-dot green"></span> <a href="{{url('/')}}">Home</a> / <a href="{{url('/knowledge-hub')}}">Knowledge Hub</a> <?php if(isset($category)){ ?>/ <a href="{{url('/knowledge-hub/'.$category->slug)}}">{{ $category->name }}</a> <?php } ?></p>
            <h1 class="page-title">{{ $data->title }}</h1>
            <hr class="black"/>

            <div class="article-photo">
                <div class="lines"></div>
                <img src="{{ $data->photo_full ? asset($data->photo_full) : asset($data->photo) }}" width="100%">
            </div>

            <div class="row mt-5">
                <div class="col-md-3">
                    @if($data->event_time)
                        <b>Event Date:</b><br/>
                        <p>{{ $data->event_time }}</p>
                    @endif
                    @if($data->event_location)
                        <b>Location:</b><br/>
                        <p>{{ $data->event_location }}</p>
                    @endif
                    @if($data->event_location || $data->event_time)
                        <br/>
                    @endif

                    @if(count($data->lawyer))
                        @foreach($data->lawyer as $lawyr)
                        <div class="d-inline-block leftauth mb-2">
                            <a href="{{ url('lawyer/'.$lawyr->slug) }}"><img src="{{$lawyr->photoUrl}}" class="authorThumb"></a>
                            <div class="authorCopy mb-5">
                                <a href="{{ url('lawyer/'.$lawyr->slug) }}"><span class="authorTitle">{{$lawyr->name}}</span></a>
                                <span class="authorDetails">
                                    <?php $titleLoop = 1; ?>
                                    @foreach($lawyr->titles()->get() as $title)
                                        {{ $title->name }}{{ $lawyr->titles()->count() > $titleLoop ? ', ' : ''}}
                                        <?php $titleLoop++; ?>
                                    @endforeach
                                </span>
                                <a href="mailto:{{ $lawyr->email }}"><span class="authorDetails" title="{{ $lawyr->email }}">{{ Str::limit($lawyr->email,20,'...')}}</span></a>
                            </div>
                        </div>
                        @endforeach
                    @endif

                    @if(count($data->promos))
                        @include('partials.short-promoboxes')
                    @endif
                </div>
                <div class="col-md-9 content">

                    @if(count($data->lawyer))
                        @foreach($data->lawyer as $lawyr)
                        <div class="righauth  mb-2">
                            <div class="d-inline-block">
                                <a href="{{ url('lawyer/'.$lawyr->slug) }}"><img src="{{$lawyr->photoUrl}}" class="authorThumb"></a>
                                <div class="authorCopy mb-5">
                                    <a href="{{ url('lawyer/'.$lawyr->slug) }}"><span class="authorTitle">{{$lawyr->name}}</span></a>
                                    <span class="authorDetails">
                                        <?php $titleLoop = 1; ?>
                                        @foreach($lawyr->titles()->get() as $title)
                                            {{ $title->name }}{{ $lawyr->titles()->count() > $titleLoop ? ', ' : ''}}
                                            <?php $titleLoop++; ?>
                                        @endforeach
                                    </span>
                                    <a href="mailto:{{ $lawyr->email }}"><span class="authorDetails" title="{{ $lawyr->email }}">{{ $lawyr->email }}</span></a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @endif

                    @if($data->excerpt)
                        <p>{!! $data->excerpt !!}</p>
                    @endif

                    {!! $data->content !!}


                    @if($data->rsvp_text && $data->rsvp_link)
                        <p>
                            <a href="{{ $data->rsvp_link }}" class="line-bt">{{$data->rsvp_text}}</a>
                        </p>
                    @endif
{{--                    <div class="row">--}}
{{--                        <div class="article-tag mt-3">--}}
{{--                            <?php $titleLoop = 1; ?>--}}
{{--                            @foreach($data->tags as $title)--}}
{{--                                {{ $title->name }}{{ count($data->tags) > $titleLoop ? ', ' : ''}}--}}
{{--                                <?php $titleLoop++; ?>--}}
{{--                            @endforeach--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </section>

    @if(isset($category))
        <section id="highlights" class="mb-5">
            <div class="container">
                <h5 class="section-heading">
                    Related Insights
                    <a href="#" class="highlights-next carousel-arrows next"></a>
                    <a href="#" class="highlights-prev carousel-arrows prev"></a>
                </h5>

                <div class="highlights-carousel">

                    @foreach($data->articles as $d)
                        <div>
                            <div class="bordertop article">
                                <a href="{{ url('/knowledge-hub/events/'.$d->slug) }}"><h3 class="article-title">{{ $d->title }}</h3></a>
                                <p class="article-excerpt">
                                    {{ \Illuminate\Support\Str::words(strip_tags($d->content),10) }}
                                </p>
                                <a href="{{ url('/knowledge-hub/events/'.$d->slug) }}" class="readmore"></a>
                            </div>
                        </div>
                    @endforeach

                    @foreach($similar as $d)
                        <div>
                            <div class="bordertop article">
                                <a href="{{ url('/knowledge-hub/events/'.$d->slug) }}"><h3 class="article-title">{{ $d->title }}</h3></a>
                                <p class="article-excerpt">
                                    {{ \Illuminate\Support\Str::words(strip_tags($d->content),10) }}
                                </p>
                                <a href="{{ url('/knowledge-hub/events/'.$d->slug) }}" class="readmore"></a>
                            </div>
                        </div>
                    @endforeach

                    @foreach($data->tagArticles as $item)
                        <div>
                            <div class="bordertop article">
                            <span class="article-tag">
                            </span>
                                <a href="{{ url('/knowledge-hub/news/'.$item->slug) }}"><h3 class="article-title">{{ $item->title }}</h3></a>
                                <p class="article-excerpt">
                                    @if($item->excerpt)
                                        {{ \Illuminate\Support\Str::words(strip_tags($item->excerpt), 20) }}
                                    @else
                                        {{ \Illuminate\Support\Str::words(strip_tags($item->content), 20) }}
                                    @endif
                                </p>
                                <a href="{{ url('/knowledge-hub/news/'.$item->slug) }}" class="readmore"></a>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </section>
    @endif

    <section class="pb-5 mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 relative greenbox">
                    <span class="sub">Got a question or enquiry?</span>
                    <span class="head">Contact us</span>
                    <a href="{{ url('contact') }}"><button class="bt">Go to page</button></a>
                </div>
            </div>
        </div>
    </section>


@endsection()

@section('js')
    <script src="{{ asset('') }}/js/home.js?v=<?php echo rand(1,99999); ?>"></script>
@endsection
