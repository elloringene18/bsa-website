@extends('partials.master')

@section('page-title'){{$data->meta_title ? $data->meta_title : $data->title}} | @endsection
@section('keywords'){{$data->keywords ? $data->keywords : $data->title}}@endsection
@section('meta_description'){{$data->meta_description}}@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('') }}/css/article.css?v=<?php echo rand(1,99999); ?>">
    <style type="text/css">
        .partners-carousel .article-tag{
            float: left;
        }
        .partners-carousel .article, .team-carousel .article {
            padding-right: 10px;
        }

        .content h1,
        .content h2,
        .content h3,
        .content h4,
        .content h5,
        .content h6,
        .content table
        {
            margin-top: 20px;
            margin-bottom: 20px;
        }

        .content ul {
            margin-top: 20px;
        }

        .cropphoto {
            overflow: hidden;
            display: block;
            height: 250px;
            margin-bottom: 10px;
        }

        .content, .content p {
            line-height: 30px;
        }
        .content p:first-of-type {
            font-size: 20px;
        }

        .article-tag {
            float: left;
        }

        .article:hover .article-thumb {
            /*transition: filter 300ms;*/
            /*filter: grayscale(1);*/
        }
    </style>
@endsection

@section('content')

    @inject('contentService', 'App\Services\ContentProvider')
    <?php $pageContent = $contentService->getPageSections('services'); ?>

    <section id="inner" class="mb-5">
        <div class="container">

            <h1 class="page-title">{{ $data->title }}</h1>

            <hr class="green"/>

            <div class="row">
                <div class="col-md-12">
{{--                    <p class="headline"><b>Middle East real estate is one of the fastest growing sectors in the world.</b> Overseas developers have invested billions of dollars into Oman, Qatar, Dubai and Saudi Arabia, which has pushed the growth of the market in areas such as retail, residential and hospitality.</p>--}}

{{--                    <img src="{{  asset('img/article.png') }}" width="100%" class="mt-3 mb-3">--}}
                </div>
            </div>

            <div class="row">
                @if(count($data->promos))
                    <div class="col-md-3 mt-5 content">
                        @include('partials.short-promoboxes')
                    </div>
                    <div class="col-md-9 mt-5 content">
                @else
                    <div class="col-md-8 offset-md-2 mt-5 content">
                @endif
                    {!! $data->content !!}

                    @foreach($data->quotes as $quote)
                        <blockquote>{{$quote->content}}</blockquote>
                        <p>{{$quote->author}}</p>
                    @endforeach
                </div>
            </div>
    </section>

    @if(count($data->contacts))
        <section id="highlights" class="mb-5 contacts">
            <div class="container">
                <h5 class="section-heading">
                    Key Contacts
                    @if(count($data->contacts)>4)
                        <a href="#" class="partners-next carousel-arrows next"></a>
                        <a href="#" class="partners-prev carousel-arrows prev"></a>
                    @endif
                </h5>

                @if(count($data->contacts)>4)
                    <div class="partners-carousel">
                        @foreach($data->contacts as $item)
                            <div>
                                <div class="article">
                                    <span class="border-top"></span>
                                    <a href="{{ url('lawyer/'.$item->slug) }}" class=""><img src="{{ $item->photoUrl }}" width="100%" class="article-thumb mb-3"></a>
                                    <h3 class="article-title">{{ $item->name }}</h3>
                                    <span class="article-tag">
                                        <?php $titleLoop = 1; ?>
                                        @foreach($item->titles()->get() as $title)
                                            {{ $title->name }}{{ $item->titles()->count() > $titleLoop ? ', ' : ''}}
                                            <?php $titleLoop++; ?>
                                        @endforeach
                                    </span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    @if(count($data->contacts))
                        <div class="row">
                        @foreach($data->contacts as $item)
                            <div class="col-md-3">
                                <div class="article">
                                    <span class="border-top"></span>
                                    <a href="{{ url('lawyer/'.$item->slug) }}" class=""><img src="{{ $item->photoUrl }}" width="100%" class="article-thumb mb-3"></a>
                                    <h3 class="article-title">{{ $item->name }}</h3>
                                    <span class="article-tag">
                                            <?php $titleLoop = 1; ?>
                                        @foreach($item->titles()->get() as $title)
                                            {{ $title->name }}{{ $item->titles()->count() > $titleLoop ? ', ' : ''}}
                                            <?php $titleLoop++; ?>
                                        @endforeach
                                        </span>
                                </div>
                            </div>
                        @endforeach
                        </div>
                    @endif
                @endif
            </div>
        </section>
    @endif

    <section id="highlights" class="mb-5 team">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ url('/our-people?title=&industry=&location=&service='.$data->slug) }}" class="line-bt" style="float: right">{{ $pageContent['team-button-text'] }}</a>
                </div>
            </div>
        </div>
    </section>

    @if(count($data->articles) || count($data->tagArticles) )
        <section id="highlights" class="mb-5">
            <div class="container">
                <h5 class="section-heading">
                    Latest insights
                    <a href="#" class="highlights-next carousel-arrows next"></a>
                    <a href="#" class="highlights-prev carousel-arrows prev"></a>
                </h5>

                <div class="highlights-carousel">
                    @foreach($data->articles as $item)
                        <div>
                            <div class="bordertop article">
                            <span class="article-tag">
                            </span>
                                <a href="{{ url('/knowledge-hub/news/'.$item->slug) }}"><h3 class="article-title">{{ $item->title }}</h3></a>
                                <p class="article-excerpt">{!!  \Illuminate\Support\Str::words(strip_tags($item->content),30)  !!}</p>
                                <a href="{{ url('/knowledge-hub/news/'.$item->slug) }}" class="readmore"></a>
                            </div>
                        </div>
                    @endforeach
                    @foreach($data->tagArticles as $item)
                    <div>
                        <div class="bordertop article">
                            <span class="article-tag">
                            </span>
                            <a href="{{ url('/knowledge-hub/news/'.$item->slug) }}"><h3 class="article-title">{{ $item->title }}</h3></a>
                            <p class="article-excerpt">{!!  \Illuminate\Support\Str::words(strip_tags($item->content),30)  !!}</p>
                            <a href="{{ url('/knowledge-hub/news/'.$item->slug) }}" class="readmore"></a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    <section class="pb-5 mt-5">
        <div class="container">
            <div class="row">
                <?php $promobox = $contentService->getPromoboxById($pageContent['promobox']); ?>
                @include('partials.long-promoboxes')
            </div>
        </div>
    </section>

@endsection()

@section('js')
    <script type="text/javascript">

        $('.partners-carousel').slick({
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: false,
            autoplay: true,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4,
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

        $('.partners-next').on('click', function (e) {
            e.preventDefault();
            $('.partners-carousel').slick('slickNext');
        });

        $('.partners-prev').on('click', function (e) {
            e.preventDefault();
            $('.partners-carousel').slick('slickPrev');
        });

        $('.team-carousel').slick({
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: false,
            autoplay: true,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4,
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

        $('.team .highlights-next').on('click', function (e) {
            e.preventDefault();
            $('.team-carousel').slick('slickNext');
        });

        $('.team .highlights-prev').on('click', function (e) {
            e.preventDefault();
            $('.team-carousel').slick('slickPrev');
        });
    </script>
@endsection
