@extends('partials.master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $pageContent = $contentService->getPageSections('our-people'); ?>

@if($pageContent['meta-title'])
    @section('page-title'){{ $pageContent['meta-title'] }} | @endsection
@endif

@if($pageContent['meta-description'])
    @section('page-description'){{ $pageContent['meta-description'] }}@endsection
@endif

@section('css')
    <link rel="stylesheet" href="{{ asset('') }}/css/news.css?v=<?php echo rand(1,99999); ?>">
    <link rel="stylesheet" href="{{ asset('') }}/css/inner.css?v=<?php echo rand(1,99999); ?>">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@tarekraafat/autocomplete.js@10.2.6/dist/css/autoComplete.min.css">

    <style>

        .article-title {
            margin-top: 15px;
        }

        .photo {
            overflow: hidden;
            display: block;
        }

        .article:hover .article-thumb {
            /*transition: filter 300ms;*/
            /*filter: grayscale(1);*/
        }

        .article-thumb {
            /*filter: grayscale(0) !important;*/
        }


        #searchBt {
            float: right;
            width: 30px;
            height: 30px;
            background-image: url({{ asset('/img/news/search.png') }});
            background-position: center;
            background-size: 30px auto;
            background-repeat: no-repeat;
            display: inline-block;
        }

        #searchbox {
            position: absolute;
            top: 0;
            height: 50px;
            width: 100%;
            display: none;
        }

        #searchfield {
            width: 100%;
            border: 0;
            border-bottom: 1px solid #54AB3B;
            height: 50px;
            text-align: right;
            font-size: 40px;
            font-weight: 300;
            font-style: italic;
            outline-color: transparent;
            padding-left: 60px;
        }

        #where {
            margin-bottom: 60px;
        }

        #closeSearch {
            height: 40px;
            width: 40px;
            background-image: url({{ asset('/img/news/close.png') }});
            background-position: center;
            background-repeat: no-repeat;
            background-size: 40px auto;
            position: absolute;
        }


        .timezone.small {
            min-height: 250px;
        }

        @media only screen and (max-width: 1200px){
            .article-thumb {
                margin-left: -40px;
            }
            .timezone.small {
                min-height: 300px;
            }
        }

        @media only screen and (max-width: 991px){
            .photo {
                max-height: 220px;
            }

            .article-thumb {
                height: 220px;
            }
            .timezone.small {
                min-height: 300px;
            }
        }

        @media only screen and (max-width: 767px){

            .timezone.small {
                min-height: 250px;
            }
            .article-thumb {
                margin-left: 0;
            }

            .photo {
                display: block;
                max-height: 600px;
            }

            .article-thumb {
                margin-bottom: 0;
            }

            .article-thumb {
                height: auto;
                width: 100%;
            }
        }

        @media only screen and (max-width: 520px){
        }

        .article-tag {
            margin: 0 0 3px 0;
            width: 100%;
            vertical-align: top;
        }

        .autoComplete_wrapper>input, .autoComplete_wrapper {
            width: 100%;
        }

        .autoComplete_wrapper>input {
            border: 0;
            border-radius: 0;
            color: #006341;
            border: #006341;
            background-image: url({{ asset('img/search.png') }});
        }

        .autoComplete_wrapper>input::placeholder {
            color: #006341 !important;
        }

        .autoComplete_wrapper>input:focus {
            color: #006341;
            border: #006341;
        }

        .autoComplete_wrapper>ul>li mark,.autoComplete_wrapper>input:hover {
            color: #006341;
        }

        .autoComplete_wrapper>ul>li:hover {

        }

    </style>
@endsection

@section('content')


    <section id="inner" class="mb-5">
        <div class="container">
            <div class="position-relative">
                <h1 class="page-title">Looking for someone in particular?
                    <a href="#" id="searchBt"></a></h1>
                <div id="searchbox">
                    <a href="#" id="closeSearch"></a>
                    <form action="{{ url('search') }}" method="get">
                        <input type="text" id="autoComplete" name="keyword" dir="ltr" spellcheck=false autocorrect="off" autocomplete="off" autocapitalize="off">
                        <input type="submit" style="display: none">
                    </form>
                </div>
            </div>
            <hr class="green"/>

            <form action="{{ url('our-people') }}" method="get" id="form">
                <div class="row">
                    <div class="col-md-4">
                        <select name="title" class="form-control">
                            <option readonly="" value="">Position</option>
                            <option value="managing-partner" {{ isset($_GET['title']) ? $_GET['title']=='managing-partner' ? 'selected' : false : '' }} >Managing Partner</option>
                            <option value="senior-partner" {{ isset($_GET['title']) ? $_GET['title']=='senior-partner' ? 'selected' : false : '' }} >Senior Partner</option>
                            <option value="partner" {{ isset($_GET['title']) ? $_GET['title']=='partner' ? 'selected' : false : '' }} >Partner </option>
                            <option value="of-counsel" {{ isset($_GET['title']) ? $_GET['title']=='of-counsel' ? 'selected' : false : '' }} >Of Counsel</option>
                            <option value="senior-associate" {{ isset($_GET['title']) ? $_GET['title']=='senior-associate' ? 'selected' : false : '' }} >Senior Associate</option>
                            <option value="associate" {{ isset($_GET['title']) ? $_GET['title']=='associate' ? 'selected' : false : '' }} >Associate</option>
                            <option value="paralegal" {{ isset($_GET['title']) ? $_GET['title']=='paralegal' ? 'selected' : false : '' }} >Paralegal</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <select name="service" id="serviceSelect" class="form-control">
                            <option readonly="" value="">Service</option>
                            @foreach($types as $type=>$services)
                                <optgroup label="{{$type}}">
                                    @foreach($services as $service)
                                        <option {{ isset($_GET['service']) ? ($_GET['service'] == $service->slug ? 'selected' : false) : '' }} value="{{$service->slug}}">{{$service->title}}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <select name="location" class="form-control">
                            <option readonly="" value="">Location</option>
                            <option value="">All</option>
                            @foreach($locations as $item)
                                <option value="{{ $item->slug }}" {{ isset($_GET['location']) ? $_GET['location']==$item->slug ? 'selected' : false : '' }}>{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </form>

            <div class="row">
                <div class="col-md-12 mt-5 mb-5 headline">
                    {!! $pageContent['intro'] !!}
                </div>
            </div>

            <div class="row">
                @if(count($filtered))
                    @foreach($filtered as $item)
                        <div class="col-md-3 article">
                            <span class="border-top"></span>

{{--                            @if($_SERVER['QUERY_STRING'])--}}
{{--                                <a href="{{ url('lawyer/'.$item->slug.'?'.$_SERVER['QUERY_STRING']) }}" class="photo">--}}
{{--                            @else--}}
                                <a href="{{ url('lawyer/'.$item->slug) }}" class="photo">
{{--                            @endif--}}
                                <img src="{{ $item->photoUrl }}" width="100%" class="article-thumb">
                            </a>

                            <h3 class="article-title">{{ $item->name }}</h3>
                            <span class="article-tag">
                                    <?php $titleLoop = 1; ?>
                                @foreach($item->titles()->get() as $title)
                                    {{ $title->name }}{{ $item->titles()->count() > $titleLoop ? ', ' : ''}}
                                    <?php $titleLoop++; ?>
                                @endforeach
                                </span>
                        </div>
                    @endforeach
                @elseif(count($data))
                    @foreach($data as $row)
                        @foreach($row as $item)
                            <div class="col-md-3 article">
                                <span class="border-top"></span>

{{--                                @if($_SERVER['QUERY_STRING'])--}}
{{--                                    <a href="{{ url('lawyer/'.$item->slug.'?'.$_SERVER['QUERY_STRING']) }}" class="photo">--}}
{{--                                @else--}}
                                    <a href="{{ url('lawyer/'.$item->slug) }}" class="photo">
{{--                                @endif--}}
                                    <img src="{{ $item->photoUrl }}" width="100%" class="article-thumb">
                                </a>

                                <h3 class="article-title">{{ $item->name }}</h3>
                                <span class="article-tag">
                                    <?php $titleLoop = 1; ?>
                                    @foreach($item->titles()->get() as $title)
                                        {{ $title->name }}{{ $item->titles()->count() > $titleLoop ? ', ' : ''}}
                                        <?php $titleLoop++; ?>
                                    @endforeach
                                </span>
                            </div>
                        @endforeach
                    @endforeach
                @else
                    <p>No Results</p>
                @endif
            </div>

        </div>
    </section>


    <section id="where" class="pb-5 mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6 relative">
                <span class="medium">
                    BSA is<br/>
                    different<br/>
                    because<br/>
                     we are
                    <img src="{{ asset('') }}/img/ellipse-1.png" class="ellipse where">
                </span>
                </div>
                <div class="col-md-6">
                    <ul class="timezone small clearfix">
                        <li>Attentive to every detail</li>
                        <li>Modern, progressive and commercially focused</li>
                        <li>Focused on excellence</li>
                        <li class="active">Innovative in everything we do</li>
                        <li>Fluent in a range of 22 languages including Arabic, English and French</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="pb-5 mt-5">
        <div class="container">
            <div class="row">
                <?php $promobox = $contentService->getPromoboxById($pageContent['promobox']); ?>
                @include('partials.long-promoboxes')
            </div>
        </div>
    </section>
@endsection()

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/@tarekraafat/autocomplete.js@10.2.6/dist/autoComplete.min.js"></script>

    <script src="{{ asset('') }}/js/news.js?v=<?php echo rand(1,99999); ?>"></script>
    <script>

//        $('.select-items div').on('click',function(){
//            $('#form').submit();
//        });

        $('#form select').on('change',function(){
            $('#form').submit();
        });

        const autoCompleteJS = new autoComplete({
            placeHolder: "Search for lawyer name...",
            data: {
                src: [
                    @foreach($lawyers as $item){ "lawyer": "{{$item->name}}","slug": "{{$item->slug}}" },@endforeach
                ],
                keys: ["lawyer"],
                cache: false,
            },
            resultItem: {
                highlight: true
            },
            events: {
                input: {
                    selection: (event) => {
                        const selection = event.detail.selection.value.lawyer;
                        autoCompleteJS.input.value = selection;
                        window.location.replace("{{url('/lawyer')}}/"+event.detail.selection.value.slug);
                    }
                }
            }
        });
    </script>
@endsection
