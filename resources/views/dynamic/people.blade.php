@extends('partials.master')
@section('page-title'){{$data->name}} | @endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('') }}/css/article.css?v=<?php echo rand(1,99999); ?>">
    <style>
        .contact-head {
            color: #006341;
            text-transform: uppercase;
            font-weight: bold;
            padding-bottom: 15px;
            margin-bottom: 15px;
            border-bottom: 1px solid #000;
        }

        .contact p {
            font-size: 14px;
            margin-bottom: 3px;
        }

        .content p:first-of-type {
            font-weight: bold;
        }

        .page-tag {
            font-size: 14px;
            color: #626262;
            margin-bottom: 3px;
        }

        .content ul {
            margin-top: 20px;
        }

        .content, .content p {
            line-height: 26px;
        }

        .content p:first-of-type {
            font-size: 20px;
            line-height: 26px;
            text-shadow: 0px 0px 0px rgb(0 0 0 / 50%);
            font-weight: normal;
        }

        .practice {
            color: #006341;
        }

        .dropbox .title {
            font-weight: bold;
            position: relative;
            display: inline-block;
            width: 100%;
            border-bottom: 1px solid #006341;
            padding-bottom: 5px;
            margin-top: 20px;
        }

        .dropbox .dropcontent {
            display: none;
        }

        .dropbox .title {
            cursor: pointer;
        }

        .dropbox .title:hover {
            color: #ACD1A7;
        }

        .dropbox .title .arrow {
            background-image: url('{{asset('img/arrow-right-white.png')}}');
            background-position: center;
            width: 24px;
            height: 24px;
            background-size: 14px;
            border-radius: 50%;
            background-color: #152726;
            float: right;
            background-repeat: no-repeat;
            transition: background-color 300ms;
            position: absolute;
            right: 0;
            background-size: 15px auto;
        }

        .dropbox.active .arrow {
            background-image: url('{{asset('img/arrow-down-white.png')}}');
        }

        .dropbox.active .dropcontent {
            display: block;
        }

        .profilephoto {
            transition: filter 300ms;
            /*filter: grayscale(1);*/
        }

        .dropcontent li {
            margin-bottom: 5px;
        }

        .dropcontent h5  {
            text-transform: uppercase;
            font-size: 14px;
            margin-top: 15px;
            margin-bottom: 0;
            font-weight: bold;
        }
    </style>
@endsection

@section('content')

    <section id="inner" class="mb-5">
        <div class="container">
            <div class="row mt-5">
                <div class="col-lg-1 col-md-1"></div>
                <div class="col-lg-3 col-md-3">
                    <img src="{{ $data->photoUrl }}" width="100%" class="mb-3 profilephoto">
                    <br/>
                </div>
                <div class="col-lg-7 col-md-7">


                    <h1 class="page-title">{{ $data->name }}</h1>

                    <p class="page-tag"><span class="tag-dot green"></span>
                        <?php $titleLoop = 1; ?>
                        @foreach($data->titles()->get() as $title)
                            {{ $title->name }}{{ $data->titles()->count() > $titleLoop ? ', ' : ', '}}
                            <?php $titleLoop++; ?>
                        @endforeach
                        <br/>
                        @foreach($data->locations()->get() as $location)
                            {{ $location->name }}
                        @endforeach
                    </p>

                    <p>
                        <b>T.</b> {{ $data->telephone }}<br/>
                        <b>E.</b> {{ $data->email }}<br/>
                    </p>

                    <hr class="green"/>
                    <div class=" content">
                        @if($data->excerpt)
                            <p>{{ $data->excerpt }}</p>
                        @endif

                        {!! $data->content !!}

                        @foreach($data->quotes as $quote)
                            <blockquote>{{$quote->content}}</blockquote>
                            @if($quote->author)
                                <p>{{$quote->author}}</p>
                            @endif
                        @endforeach
{{--                        <blockquote>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</blockquote>--}}
                        <br/>

{{--                        @if(count($data->services))--}}
{{--                            <div class="dropbox">--}}
{{--                                <div class="title">Services <span class="arrow"></span></div>--}}
{{--                                <div class="dropcontent">--}}
{{--                                    <ul>--}}
{{--                                        @foreach($data->services as $item)--}}
{{--                                            <li>{{ $item->title }}</li>--}}
{{--                                        @endforeach--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        @endif--}}

{{--                        @if(count($data->industries))--}}
{{--                            <div class="dropbox">--}}
{{--                                <div class="title">Industries <span class="arrow"></span></div>--}}
{{--                                <div class="dropcontent">--}}
{{--                                    <ul>--}}
{{--                                        @foreach($data->industries as $item)--}}
{{--                                            <li>{{ $item->title }}</li>--}}
{{--                                        @endforeach--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        @endif--}}

                            <div class="dropbox">
                                <div class="title">Areas of expertise <span class="arrow"></span></div>
                                <div class="dropcontent">

                                    @if($data->servicesByType['total'])
                                            @foreach($data->servicesByType['data'] as $key=>$type)
                                                @if(count($type))
                                                    <h5>{{$key}}</h5>
                                                    <ul>
                                                        @foreach($type as $item)
                                                            <li>{{ $item->title }}</li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            @endforeach
                                    @endif
                                </div>
                            </div>

                        @if(count($data->experience))
                            <div class="dropbox">
                                <div class="title">Highlights of experience <span class="arrow"></span></div>
                                <div class="dropcontent">
                                    <ul>
                                        @foreach($data->experience as $item)
                                            <li>{{ $item->content }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif

                        @if(count($data->education))
                            <div class="dropbox">
                                <div class="title">Education & qualifications <span class="arrow"></span></div>
                                <div class="dropcontent">
                                    <ul>
                                        @foreach($data->education as $item)
                                            <li>{{ $item->content }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="highlights" class="mb-5">
        <div class="container">
            <h5 class="section-heading">
                Latest Insights
                <a href="#" class="highlights-next carousel-arrows next"></a>
                <a href="#" class="highlights-prev carousel-arrows prev"></a>
            </h5>

            <div class="highlights-carousel">

                @foreach($data->articles as $item)
                    <div>
                        <div class="bordertop article">
                            <span class="article-tag">
                            </span>
                            <a href="{{ url('/knowledge-hub/news/'.$item->slug) }}"><h3 class="article-title">{{ $item->title }}</h3></a>
                            <p class="article-excerpt">{!!  \Illuminate\Support\Str::words(strip_tags($item->content),30)  !!}</p>
                            <a href="{{ url('/knowledge-hub/news/'.$item->slug) }}" class="readmore"></a>
                        </div>
                    </div>
                @endforeach

{{--                @if(count($data->tagArticles)+count($data->articles)==0)--}}
                @foreach($data->insights as $item)
                    <div>
                        <div class="bordertop article">
                            <a href="{{ url('/knowledge-hub/news/'.$item->slug) }}"><h3 class="article-title">{{ $item->title }}</h3></a>
                            <p class="article-excerpt">
                                {{ \Illuminate\Support\Str::words(strip_tags($item->content),10) }}
                            </p>
                            <a href="{{ url('/knowledge-hub/news/'.$item->slug) }}" class="readmore"></a>
                        </div>
                    </div>
                @endforeach
{{--                @endif--}}

                @foreach($data->tagArticles as $item)
                    <div>
                        <div class="bordertop article">
                        <span class="article-tag">
                        </span>
                            <a href="{{ url('/knowledge-hub/news/'.$item->slug) }}"><h3 class="article-title">{{ $item->title }}</h3></a>
                            <p class="article-excerpt">{!!  \Illuminate\Support\Str::words(strip_tags($item->content),30)  !!}</p>
                            <a href="{{ url('/knowledge-hub/news/'.$item->slug) }}" class="readmore"></a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

@endsection()

@section('js')
    <script src="{{ asset('') }}/js/home.js?v=<?php echo rand(1,99999); ?>"></script>
    <script>
        $('.dropbox .title').on('click', function (){
            $(this).closest('.dropbox').toggleClass('active');
        });
    </script>
@endsection
