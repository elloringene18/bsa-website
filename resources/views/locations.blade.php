@extends('partials.master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $pageContent = $contentService->getPageSections('locations'); ?>

@if($pageContent['meta-title'])
    @section('page-title'){{ $pageContent['meta-title'] }} | @endsection
@endif

@if($pageContent['meta-description'])
    @section('page-description'){{ $pageContent['meta-description'] }}@endsection
@endif

@section('css')
    <link rel="stylesheet" href="css/article.css?v=<?php echo rand(1,99999); ?>">
    <style>
        .timezone.map {
            min-height: 700px;
        }

        @media only screen and (max-width: 1200px) {
            .timezone.small {
                min-height: 400px;
            }
        }

        @media only screen and (max-width: 991px) {
            .timezone.small {
                min-height: 500px;
            }
        }

        @media only screen and (max-width: 767px) {
            .timezone.small {
                min-height: 300px;
            }
        }
    </style>
@endsection

@section('content')


<section id="inner" class="mb-5">
    <div class="container">

        <h1 class="page-title">Locations</h1>

        <hr class="green"/>

        <div class="row">
            <div class="col-md-8 offset-md-2 headline">
                {!! $pageContent['intro'] !!}
            </div>
        </div>
    </div>
</section>

<section id="where" class="pb-5 mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-4 relative">
                <span class="medium">
                    We are<br/> where <br/>you are
                    <img src="{{ asset('') }}/img/ellipse-1.png" class="ellipse where">
                </span>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-7">
                <ul class="timezone map small clearfix">

                    @inject('contentService', 'App\Services\ContentProvider')
                    <?php $locations = $contentService->getLocations(); ?>

                    @foreach($locations as $item)
                    <li class="{{ $item->slug=='dubai-uae' ? 'active' : '' }}">
                        <div class="row">
                            <div class="col-lg-3">{{ $item->name }}</div>
                            <div class="col-lg-9 address">
                                <div class="mapouter">
                                    <div class="gmap_canvas">
                                        <iframe src="{{ $item->map }}" width="100%" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                                        <br>
                                        <style>.mapouter{position:relative;text-align:right;height:200px;width:100%;}</style>
                                        <style>.gmap_canvas {overflow:hidden;background:none!important;height:200px;width:100%;}</style>
                                    </div>
                                </div>
                                <br/>
                                {!! $item->content !!}
                            </div>
                        </div>
                    </li>
                    @endforeach
{{--                    <li>--}}
{{--                        <div class="row ">--}}
{{--                            <div class="col-md-3">Abu Dhabi</div>--}}
{{--                            <div class="col-md-9 address">--}}
{{--                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3631.3920784224715!2d54.3720301508728!3d24.471868384161322!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5e6795a58f6167%3A0x5ed7237523283b74!2sAl%20Wahda%20Commercial%20Tower!5e0!3m2!1sen!2sae!4v1639641918354!5m2!1sen!2sae" width="100%" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>--}}
{{--                                <br/>--}}
{{--                                <p><b>Location:</b></p>--}}
{{--                                <p>Office No. 2204, Floor 22nd, Al Wahda Commercial Tower </p>--}}
{{--                                <p>PO Box 36678, Abu Dhabi, United Arab Emirates</p>--}}
{{--                                <p><b>Phone:</b></p><p> +971 2 644 4474</p>--}}
{{--                                <p><b>Email:</b></p><p> info@bsabh.com</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </li>--}}
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="pb-5 mt-5">
    <div class="container">
        <div class="row">
            <?php $promobox = $contentService->getPromoboxById($pageContent['promobox']); ?>
            @include('partials.long-promoboxes')
        </div>
    </div>
</section>

@endsection()

@section('js')
@endsection
