@extends('partials.master')

@section('css')
    <link rel="stylesheet" href="{{ asset('') }}/css/inner.css?v=<?php echo rand(1,99999); ?>">
    <link rel="stylesheet" href="{{ asset('') }}/css/news.css?v=<?php echo rand(1,99999); ?>">
    <style>
        .cropphoto {
            max-height: 200px;
            overflow: hidden;
            display: block;
        }


        @media only screen and (max-width: 991px){
            .article-thumb {
                margin-bottom: 0;
            }
        }

        @media only screen and (max-width: 767px){
            .cropphoto {
                display: block;
                max-height: 600px;
            }
        }
    </style>
@endsection


@section('content')
    <section id="inner" class="mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="position-relative">
                        <h5 class="section-heading">Search
                            <a href="#" id="searchBt"></a></h5>
                        <hr class="black mt-3"/>
                        <div id="searchbox">
                            <a href="#" id="closeSearch"></a>
                            <form>
                                <input type="text" id="searchfield">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if(count($data['articles']))
            <div class="container">
                <div class="row mt-5">
                    <div class="col-md-12">
                        <div class="row">
                            @foreach($data['articles'] as $item)
                                <div class="col-md-3 article">
                                    <span class="border-top"></span>
                                    <a href="{{ url('/knowledge-hub/news/'.$item->slug) }}" class="cropphoto"><img src="{{ $item->photo }}" width="100%" class="article-thumb"></a>
                                    <span class="article-tag">
                                        @foreach($item->tags as $tag)
                                            {{ $tag->name }},
                                        @endforeach
                                    </span>
                                    <a href="{{ url('/knowledge-hub/news/'.$item->slug) }}"><h3 class="article-title">{{$item->title}}</h3></a>
                                </div>
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="pagination mt-5">
                                    <?php
                                    $pages = floor($data['articles']->total()/8);

                                    for($x=1;$x<=$pages;$x++){
                                    ?>
                                    <li><a {{ isset($_GET['page']) ? $x == $_GET['page'] ? 'class=active' : false : '' }} href="{{ url('/knowledge-hub/search/?page='.$x) }}">{{$x}}</a></li>
                                    <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

{{--        @if(count($data['lawyers']))--}}
{{--            <div class="container">--}}
{{--                <div class="row mt-5">--}}
{{--                    <div class="col-md-12">--}}
{{--                        <div class="row">--}}
{{--                            @foreach($data['lawyers'] as $item)--}}
{{--                                <div class="col-md-3 article">--}}
{{--                                    <span class="border-top"></span>--}}
{{--                                    <a href="{{ url('/knowledge-hub/'.$category->slug.'/'.$item->slug) }}" class="cropphoto"><img src="{{ $item->photo }}" width="100%" class="article-thumb"></a>--}}
{{--                                    <span class="article-tag">--}}
{{--                                        @foreach($item->tags as $tag)--}}
{{--                                            {{ $tag->name }},--}}
{{--                                        @endforeach--}}
{{--                                    </span>--}}
{{--                                    <a href="{{ url('/knowledge-hub/'.$category->slug.'/'.$item->slug) }}"><h3 class="article-title">{{$item->title}}</h3></a>--}}
{{--                                </div>--}}
{{--                            @endforeach--}}
{{--                        </div>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-md-12">--}}
{{--                                <ul class="pagination mt-5">--}}
{{--                                    <?php--}}
{{--                                    $pages = floor($data->total()/8);--}}

{{--                                    for($x=1;$x<=$pages;$x++){--}}
{{--                                    ?>--}}
{{--                                    <li><a {{ isset($_GET['page']) ? $x == $_GET['page'] ? 'class=active' : false : '' }} href="{{ url('/knowledge-hub/'.$category->slug.'?page='.$x) }}">{{$x}}</a></li>--}}
{{--                                    <?php--}}
{{--                                    }--}}
{{--                                    ?>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        @endif--}}

    </section>

@endsection()

@section('js')
    <script src="{{ asset('') }}/js/news.js?v=<?php echo rand(1,99999); ?>"></script>
@endsection
