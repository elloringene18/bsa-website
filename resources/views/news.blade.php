@extends('partials.master')

@section('css')
    <link rel="stylesheet" href="css/inner.css?v=<?php echo rand(1,99999); ?>">
    <link rel="stylesheet" href="css/news.css?v=<?php echo rand(1,99999); ?>">
@endsection


@section('content')
    <section id="inner" class="mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="position-relative">
                        <h5 class="section-heading">Insights
                            <a href="#" id="searchBt"></a></h5>
                        <hr class="black mt-3"/>
                        <div id="searchbox">
                            <a href="#" id="closeSearch"></a>
                            <form>
                                <input type="text" id="searchfield">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="custom-select" id="datefilter">
                        <select>
                            <?php for($x=2021;$x>=2018;$x--){ ?>
                                <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="custom-select" id="datefilter">
                        <select>
                            <option value="News">Service</option>
                        </select>
                    </div>
                    <div class="custom-select" id="datefilter">
                        <select>
                            <option value="latest">Sector</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="news-carousel">
                        <?php
                            $months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
                            $words = ['Lorem ','ipsum ','dolor ','sit ','amet','consectetur ','adipiscing ','elit','Nunc','quis ','nibh ','maximus'];
                        ?>
                        <?php for($x=0;$x<count($months);$x++){ ?>
                            <div>
                                <span class="date"><?php echo$months[$x]; ?></span>

                                <?php $dots = rand(5,20); ?>

                                <?php for($y=0;$y<$dots;$y++){ ?>

                                    <?php $wordCount = rand(3,6); $title = ''; ?>

                                    <?php for($c=0;$c<$wordCount;$c++) { ?>
                                        <?php $title .= $words[rand(1,count($words)-1)]; ?>
                                    <?php } ?>

                                    <a href="{{ url('/article') }}"><div class="dot"><div class="rel"><span class="title tracking-in-contract"><?php echo $title; ?></span></div></div></a>
                                <?php } ?>

                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row mt-5">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3 article">
                            <span class="border-top"></span>
                            <a href="{{ url('/article') }}"><img src="img/article-thumb.png" width="100%" class="article-thumb"></a>
                            <span class="article-tag">bankruptcy, news, uae.</span>
                            <h3 class="article-title">Lorem Ipsum sit Amet.</h3>
                        </div>
                        <div class="col-md-3 article">
                            <span class="border-top"></span>
                            <a href="{{ url('/article') }}"><img src="img/article-thumb-2.png" width="100%" class="article-thumb"></a>
                            <span class="article-tag">bankruptcy, news, uae.</span>
                            <h3 class="article-title">Lorem Ipsum sit Amet.</h3>
                        </div>
                        <div class="col-md-3 article">
                            <span class="border-top"></span>
                            <a href="{{ url('/article') }}"><img src="img/article-thumb-3.png" width="100%" class="article-thumb"></a>
                            <span class="article-tag">bankruptcy, news, uae.</span>
                            <h3 class="article-title">Lorem Ipsum sit Amet.</h3>
                        </div>
                        <div class="col-md-3 article">
                            <span class="border-top"></span>
                            <a href="{{ url('/article') }}"><img src="img/article-thumb.png" width="100%" class="article-thumb"></a>
                            <span class="article-tag">bankruptcy, news, uae.</span>
                            <h3 class="article-title">Lorem Ipsum sit Amet.</h3>
                        </div>
                        <div class="col-md-3 article">
                            <span class="border-top"></span>
                            <a href="{{ url('/article') }}"><img src="img/article-thumb-2.png" width="100%" class="article-thumb"></a>
                            <span class="article-tag">bankruptcy, news, uae.</span>
                            <h3 class="article-title">Lorem Ipsum sit Amet.</h3>
                        </div>
                        <div class="col-md-3 article">
                            <span class="border-top"></span>
                            <a href="{{ url('/article') }}"><img src="img/article-thumb-3.png" width="100%" class="article-thumb"></a>
                            <span class="article-tag">bankruptcy, news, uae.</span>
                            <h3 class="article-title">Lorem Ipsum sit Amet.</h3>
                        </div>
                        <div class="col-md-3 article">
                            <span class="border-top"></span>
                            <a href="{{ url('/article') }}"><img src="img/article-thumb.png" width="100%" class="article-thumb"></a>
                            <span class="article-tag">bankruptcy, news, uae.</span>
                            <h3 class="article-title">Lorem Ipsum sit Amet.</h3>
                        </div>
                        <div class="col-md-3 article">
                            <span class="border-top"></span>
                            <a href="{{ url('/article') }}"><img src="img/article-thumb-2.png" width="100%" class="article-thumb"></a>
                            <span class="article-tag">bankruptcy, news, uae.</span>
                            <h3 class="article-title">Lorem Ipsum sit Amet.</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="pagination">
                                <li><a href="#" class="active">01</a></li>
                                <li><a href="#">02</a></li>
                                <li><a href="#">03</a></li>
                                <li><a href="#">04</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection()

@section('js')
    <script src="{{ asset('') }}/js/news.js?v=<?php echo rand(1,99999); ?>"></script>
@endsection
